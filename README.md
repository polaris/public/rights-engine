# POLARIS - Rights Engine / Consent Engine
This is the repository contains the source code of the rights engine of the Polaris project.
The Consent Engine is a software solution designed to provide a secure and efficient way for obtaining and managing user consent. The project is divided into two main components: frontend and backend. The frontend component is responsible for providing a user-friendly interface for obtaining consent, while the backend component is responsible for storing and managing user consent data.


## Bug reporting
If you encounter a bug or have a feature request, kindly open an issue within the project repository.

## Credits
The "Provider Oriented Open Learning Analytics (POLARIS)" software was developed as part of the "KI:edu.nrw - Didactics, Ethics and Technology of Learning Analytics and AI in Higher Education" project funded by the Ministry of Culture and Science of the State of North Rhine-Westphalia. The starting point for the development was the "EXCALIBUR LA" software developed by Sven Judel and Ulrik Schroeder at RWTH University Aachen. POLARIS was programmed by Digital Learning GmbH, Duderstadt (Managing Director: Benjamin Ledel). The following people were involved in the software development:

RWTH University Aachen: Wolfram Barodte, Martin Breuer, Kevin Esser, Marcus Gerards, Sven Judel, Sebastian Schilling, Malte Persike


Ruhr University Bochum: Christopher Lentzsch, Jonas Leschke, Christian Metzger, Peter Salden, Tabea Schwarz, Christos Simis 
