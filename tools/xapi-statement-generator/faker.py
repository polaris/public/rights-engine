import json
import random
from datetime import datetime


class XApiFaker:

    def read_provider_config_file(self):
        try:
            with open("provider_config.json","r") as f:
                data = json.load(f)
                return data
        except IOError:
            print("Couldn't read provider_config.json.")

    def fake_statement(self, tan = ""):
        providers =  self.read_provider_config_file()
        provider_config = random.choice(providers)

        verb_config = random.choice(provider_config["verbs"])
        xapi_verb = {
            "id": verb_config["id"],
        }
        xapi_object = random.choice(verb_config["objects"])

        now = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        actor_obj = {}
        if tan == "":
            actor = random.choice(provider_config["actors"])
            actor_obj = {"mbox": f"mailto:{actor}"}
        else:
            actor_obj = {"tan": f'{tan}', "provider_id": 1}
        return {
            "token": provider_config["token"],
            "provider_name": provider_config["name"],
            "statement": {
                "actor": actor_obj,
                "verb": xapi_verb,
                "object": xapi_object,
                "timestamp": now,
            },
        }
