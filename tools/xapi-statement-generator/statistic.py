class Statistic:

    providers = {}

    def add(self, provider_name, accepted):
        existing_provider = self.providers.get(
            provider_name, {"accepted": 0, "rejected": 0}
        )
        provider = {
            provider_name: {
                "accepted": existing_provider["accepted"] + (1 if accepted else 0),
                "rejected": existing_provider["rejected"] + (0 if accepted else 1),
            }
        }
        self.providers.update(provider)

    def __str__(self):
        accepted_statements = sum(
            [provider["accepted"] for provider in self.providers.values()]
        )
        rejected_statements = sum(
            [provider["rejected"] for provider in self.providers.values()]
        )

        success_rate = int(
            (accepted_statements / (accepted_statements + rejected_statements)) * 100
        )

        output = f"\nImporting {accepted_statements + rejected_statements} statements completed"
        output += f"\nLRS accepted {accepted_statements} statements ({success_rate}%) and rejected {rejected_statements} statements ({100 - success_rate}%)\n"

        for (provider_name, data) in self.providers.items():
            provider_success_rate = int(
                (data["accepted"] / (data["accepted"] + data["rejected"]) * 100)
            )
            output += f"\nProvider: {provider_name}"
            output += f"\n\tAccepted statements: {data['accepted']} ({provider_success_rate}%)"
            output += f"\n\tRejected statements: {data['rejected']} ({100 - provider_success_rate}%)"

        return output
