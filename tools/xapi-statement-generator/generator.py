import argparse
import json
import logging
import sys
import time
from datetime import datetime

import requests

from faker import XApiFaker
from statistic import Statistic

logging.basicConfig(
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler(f"lrs_import_{time.strftime('%Y_%m_%d_%H_%M_%S')}.log"),
        logging.StreamHandler(sys.stdout),
    ],
)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def read_json_dump(file_name):
    f = open(file_name)
    x_api_statements = json.load(f)
    f.close()
    return x_api_statements


def transform_statement(statement, overwrite_ts):
    actor = json.loads(statement.get("actor"))
    object = statement.get("object")
    if object:
        object = json.loads(object)
        email = actor.get("email")
        verb_str = statement.get("verb")
        created_at = statement.get("created_at")
        date = datetime.strptime(created_at, "%Y-%m-%d %H:%M:%S").strftime(
            "%Y-%m-%dT%H:%M:%SZ"
        )
        now = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        timestamp = now if overwrite_ts else date
        if not email.__contains__("@"):
            email = f"{email}@test.de"
        return {
            "actor": {"mbox": f"mailto:{email}"},
            "verb": json.loads(verb_str),
            "object": {
                "id": f"http://educa-portal.de/expapi/activity/{object.get('id')}",
                "objectType": "Activity",
            },
            "timestamp": timestamp,
        }
    return None


def send_x_api_statement(token, statistic, url, statement, provider_name):
    headers = {"Authorization": f"Basic {token}"}
    request = requests.post(url, json=statement, headers=headers)
    try:
        response = request.json()
        if request.ok:
            logging.info(
                f"Accepted: {statement.get('actor').get('mbox')} {statement.get('verb').get('id')} - {response.get('provider')}"
            )
            statistic.add(provider_name, True)
        else:
            logging.warning(
                f"Rejected: {statement.get('actor').get('mbox')} {statement.get('verb').get('id')} - {response.get('provider')} - {response.get('message')}"
            )
            statistic.add(provider_name, False)
        return [request.status_code, response.get("message")]
    except json.JSONDecodeError:
        logging.error(
            f"Failed: {statement.get('actor').get('mbox')} {statement.get('verb').get('id')}"
        )


def import_json(args):
    token = args.token[0]
    file_name = args.input[0]
    x_api_statements = read_json_dump(file_name)
    overwrite_ts = args.overwrite_ts
    print(f"overwrite timestamp {overwrite_ts}")

    url = args.target[0]

    statistic = Statistic()

    print("Running...")
    for statement in x_api_statements:

        valid_statement = transform_statement(statement, overwrite_ts)
        if valid_statement:
            send_x_api_statement(token, statistic, url, valid_statement, "My provider")
        else:
            logging.error("Skipped xAPI statement: %s", statement.get("verb"))

    print(statistic)


def start_random_generation(args):
    url = args.target[0]
    tan = args.use_tan if args.use_tan is not None else ""
    print("Running...")
    statistic = Statistic()
    faker = XApiFaker()
    while True:
        try:
            data = faker.fake_statement(tan)
            send_x_api_statement(data["token"], statistic, url, data["statement"], data["provider_name"])
            time.sleep(0.2)
        except KeyboardInterrupt:
            print(statistic)
            raise SystemExit


def main():
    parser = argparse.ArgumentParser(description="Simple xAPI Statement Generator.")

    parser.add_argument(
        "-a",
        "--token",
        type=str,
        nargs=1,
        metavar="token",
        default=None,
        help="Application Token",
    )

    parser.add_argument(
        "-t",
        "--target",
        type=str,
        nargs=1,
        metavar="target_url",
        default=None,
        help="Address to which xAPI statements are sent.",
        required=True,
    )

    parser.add_argument(
        "-i",
        "--input",
        type=str,
        nargs=1,
        metavar="file_name",
        default=None,
        help="Reads the specified JSON file as indiviual xAPI statements.",
    )

    parser.add_argument(
        "-o",
        "--overwrite_ts",
        default=False,
        action=argparse.BooleanOptionalAction,
        help="Overwrite timestamp with current date, which results in new LRS entries in case the xAPI statements are already stored in the LRS.",
    )

    parser.add_argument(
        "-r",
        "--random",
        default=False,
        action=argparse.BooleanOptionalAction,
        help="Creates and sends a random xAPI statement to an LRS in an interval of 1/s.",
    )

    parser.add_argument(
        "-u",
        "--use_tan",
        nargs='?',
        help="Only in combination with -r: instead of a user (email), store a TAN and provider ID 1.",
    )

    args = parser.parse_args()

    if (
        args.token is not None
        and args.target is not None
        and args.input is not None
        and not args.random
    ):
        import_json(args)

    if args.random and args.target is not None:
        start_random_generation(args)

    parser.print_help()


if __name__ == "__main__":
    main()
