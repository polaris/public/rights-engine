from django.db import models

class PolarisSetting(models.Model):
    key = models.CharField(max_length=255)
    value = models.JSONField()
