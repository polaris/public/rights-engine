from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.views import APIView

from .models import PolarisSetting


class PrivacyPolicyView(APIView):
    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.request.method == 'GET':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAuthenticated, IsAdminUser]
        return [permission() for permission in permission_classes]

    def get(self, request):
        try:
            privacy_policy = PolarisSetting.objects.get(key="privacy_policy")
            return JsonResponse(privacy_policy.value)
        except ObjectDoesNotExist:
            return JsonResponse({"content": ""})

    def post(self, request):
        try:
            privacy_policy = PolarisSetting.objects.get(key="privacy_policy")
            privacy_policy.value = {"content": request.data["content"]}
            privacy_policy.save()

        except ObjectDoesNotExist:
            privacy_policy = PolarisSetting.objects.create(
                key = "privacy_policy",
                value = {"content": request.data["content"]}
            )
        return JsonResponse(privacy_policy.value)