export const environment = {
  production: true,
  apiUrl: '',
  pageVisibility: {
    analyses: false,
    consent_history: true,
    consent_management: false,
    merge_data: false,
    data_disclosure: true,
  }
};
