import { Component, OnInit, Input } from '@angular/core'
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms'
import * as moment from 'moment'
import { ApiService } from 'src/app/services/api.service'
import { AnalyticsTokenNameValidator } from 'src/app/validators/analytics-token-name.validator'
import { NzModalRef } from 'ng-zorro-antd/modal'
import { faTimesCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons'

const MINIMUM_TOKEN_EXPIRATION_DAYS = 5

@Component({
  selector: 'app-create-token-dialog',
  templateUrl: './create-token-dialog.html',
  styleUrls: ['./create-token-dialog.scss']
})
export class CreateTokenDialog implements OnInit {
  @Input() headerText: string = ''

  form: FormGroup

  constructor(
    public _dialogRef: NzModalRef<CreateTokenDialog>,
    private _formBuilder: FormBuilder,
    private _apiService: ApiService
  ) {
    this.form = this._formBuilder.group({
      expires: [null, validateDate()],
      description: ['', Validators.required],
      name: [
        '',
        Validators.required,
        AnalyticsTokenNameValidator.createValidator(this._apiService)
      ]
    })
  }

  get dateController(): FormControl {
    return this.form.get('expires') as FormControl
  }

  get nameController(): FormControl {
    return this.form.get('name') as FormControl
  }

  get descriptionController(): FormControl {
    return this.form.get('description') as FormControl
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    if (this.form.valid)
      this._dialogRef.close({
        confirmed: true,
        expires: this.form.value.expires
          ? this.form.value.expires.format('MM/DD/YYYY')
          : null,
        name: this.form.value.name,
        description: this.form.value.description
      })
  }

  handleClose(): void {
    this._dialogRef.destroy()
  }

  protected readonly faCheckCircle = faCheckCircle
  protected readonly faTimesCircle = faTimesCircle
}

function validateDate(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value
    if (!value) return null
    const now = moment()

    const isValid =
      value.isAfter(now) && value.diff(now, 'days') > MINIMUM_TOKEN_EXPIRATION_DAYS
    return isValid ? null : { validDate: false }
  }
}
