import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core'
import {
  AbstractControl, FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms'
import * as moment from 'moment'
import { ApiService } from 'src/app/services/api.service'
import { NzModalRef } from 'ng-zorro-antd/modal'
import { faTimesCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { XApiVerbSchema } from '../../consent-management/consentDeclaration'
import { AuthService } from '../../services/auth.service'

interface SelectedVerb {
  label: string
  value: string
  checked: boolean
}

@Component({
  selector: 'app-privacy-policy-dialog',
  templateUrl: './privacy-policy-dialog.html',
  styleUrls: ['./privacy-policy-dialog.scss']
})
export class PrivacyPolicyDialog {
  @Input() headerText: string = '';
  @Input() text: string = "";

  constructor(
    public _dialogRef: NzModalRef<PrivacyPolicyDialog>,
    private _authService: AuthService,
    private _apiService: ApiService
  ) {
  }

  logout(): void {
    this._authService.logout()
    this._dialogRef.close({accepted: false})
  }

  onSubmit(): void {
    this._dialogRef.close({
      accepted: true
    })
  }

}
