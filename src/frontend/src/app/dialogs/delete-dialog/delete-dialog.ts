import { Component, Input } from '@angular/core'
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms'
import { NzModalRef } from 'ng-zorro-antd/modal'


@Component({
  selector: 'delete-dialog',
  templateUrl: 'delete-dialog.html',
  styleUrls: ['./delete-dialog.scss']
})
export class DeleteDialog {
  @Input() headerText: string = ''

  confirmation = ''
  confirmationText = $localize`:@@deleteConfirmationText:delete`
  form: FormGroup
  deleteConfirmed = false

  constructor(
    public _dialogRef: NzModalRef<DeleteDialog>,
    private _formBuilder: FormBuilder
  ) {
    this.form = this._formBuilder.group({
      confirmation: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.form.get('confirmation')?.valueChanges.subscribe((val) => {
      this.confirmation = val
      this.deleteConfirmed = val.localeCompare(this.confirmationText) === 0
    })
  }

  get confirmationControl(): FormControl {
    return this.form.get('confirmation') as FormControl
  }

  onSubmit(): void {
    if (this.deleteConfirmed) this._dialogRef.close(this.deleteConfirmed)
  }

  handleClose(): void {
    this._dialogRef.destroy();
  }
}
