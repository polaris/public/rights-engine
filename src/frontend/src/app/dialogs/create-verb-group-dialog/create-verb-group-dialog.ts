import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core'
import {
  AbstractControl, FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms'
import * as moment from 'moment'
import { ApiService } from 'src/app/services/api.service'
import { NzModalRef } from 'ng-zorro-antd/modal'
import { faTimesCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { XApiVerbSchema } from '../../consent-management/consentDeclaration'

interface SelectedVerb {
  label: string
  value: string
  checked: boolean
}

@Component({
  selector: 'app-create-verb-group-dialog',
  templateUrl: './create-verb-group-dialog.html',
  styleUrls: ['./create-verb-group-dialog.scss']
})
export class CreateVerbGroupDialog implements OnChanges, OnInit {
  @Input() headerText: string = '';
  @Input() verbs: XApiVerbSchema[] = [];

  form: FormGroup

  protected selectedVerbs: SelectedVerb[] = [];

  constructor(
    public _dialogRef: NzModalRef<CreateVerbGroupDialog>,
    private _formBuilder: FormBuilder,
    private _apiService: ApiService
  ) {
    this.form = this._formBuilder.group({
      label: [
        '',
        Validators.required
      ],
      description: ['', Validators.required],
      purposeOfCollection: ['', Validators.required],
      requiresConsent: ["1", Validators.required],
    })
  }

  get labelController(): FormControl {
    return this.form.get('label') as FormControl
  }

  get descriptionController(): FormControl {
    return this.form.get('description') as FormControl
  }

  get purposeController(): FormControl {
    return this.form.get('purposeOfCollection') as FormControl
  }

  get requiresConsentController(): FormControl {
    return this.form.get('requiresConsent') as FormControl
  }

  updateSelectedVerbs(): void {
    this.selectedVerbs = this.verbs.map(verb => {
      return {
        label: verb.label,
        value: verb.id,
        checked: false
      }
    })
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['verbs'] && this.verbs) {
      this.updateSelectedVerbs();
    }
  }

  ngOnInit(): void {
    this.updateSelectedVerbs();
  }

  onSubmit(): void {
    if (this.form.valid)
      this._dialogRef.close({
        label: this.form.value.label,
        description: this.form.value.description,
        purposeOfCollection: this.form.value.purposeOfCollection,
        requiresConsent: this.form.value.requiresConsent == "1",
        selectedVerbs: this.selectedVerbs.filter(verb => verb.checked).map(verb => {return {id: verb.value}}),
      })
  }

  handleClose(): void {
    this._dialogRef.destroy()
  }

}
