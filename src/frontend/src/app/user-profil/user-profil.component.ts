import { Component, OnDestroy, OnInit } from '@angular/core'
import { AuthService, Userinfo } from '../services/auth.service'
import { DeleteDialog } from '../dialogs/delete-dialog/delete-dialog'
import { Subscription } from 'rxjs'
import { NzMessageService } from 'ng-zorro-antd/message'
import { NzModalService } from 'ng-zorro-antd/modal'

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.component.html',
  styleUrls: ['./user-profil.component.scss']
})
export class UserProfilComponent implements OnInit, OnDestroy {
  loggedIn: Userinfo | null = null
  dialogSub?: Subscription

  constructor(
    private _authService: AuthService,
    private _dialog: NzModalService,
    private _messageService: NzMessageService
  ) {
  }

  ngOnInit(): void {
    this._authService.loggedIn.subscribe((x) => (this.loggedIn = x))
  }

  ngOnDestroy(): void {
    this.dialogSub?.unsubscribe()
  }

  deleteAccount(): void {
    const dialogRef = this._dialog.create({
      nzContent: DeleteDialog,
      nzComponentParams: {
        headerText: $localize`:@@deleteAccountText:Delete Account`
      }
    })

    this.dialogSub = dialogRef.afterClose.subscribe((confirmed) => {
      if (confirmed)
        this._authService.deleteUser().subscribe((response) => {
          this._messageService.success($localize`:@@toastMessageUserDeleted:User Deleted`)
          this._authService.logout()
        })
    })
  }
}
