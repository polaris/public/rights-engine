import { TestBed } from '@angular/core/testing';

import { DataRemovalService } from './data-removal.service';

describe('DataRemovalService', () => {
  let service: DataRemovalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataRemovalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
