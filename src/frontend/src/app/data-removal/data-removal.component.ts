import { Component, OnDestroy, OnInit } from '@angular/core'
import { DataRemovalJob, DataRemovalService } from '../data-removal.service'
import { DeleteDialog } from '../dialogs/delete-dialog/delete-dialog'
import { Subscription } from 'rxjs'
import { NzModalService } from 'ng-zorro-antd/modal'

@Component({
  selector: 'app-data-removal',
  templateUrl: './data-removal.component.html',
  styleUrls: ['./data-removal.component.scss']
})
export class DataRemovalComponent implements OnInit, OnDestroy {
  jobs: DataRemovalJob[] = []
  dialogSub?: Subscription

  constructor(private _removalService: DataRemovalService, private _dialog: NzModalService) {
  }

  displayedColumns: string[] = [
    'id',
    'created_at',
    'execute_at',
    'description',
    'matched_statements',
    'finished'
  ]

  ngOnInit(): void {
    this.getJobList()
  }

  ngOnDestroy(): void {
    this.dialogSub?.unsubscribe()
  }

  requestDataRemoval(): void {
    const dialogRef = this._dialog.create({
      nzContent: DeleteDialog,
      nzComponentParams: {
        headerText: $localize`:@@deleteAllLrsStatements:Delete all saved data`
      }
    })

    this.dialogSub = dialogRef.afterClose.subscribe((confirmed) => {
      if (confirmed)
        this._removalService
          .create({ immediately: true, scope: {} })
          .subscribe((response) => {
            this.getJobList()
          })
    })
  }

  getJobList(): void {
    this._removalService.listJobs().subscribe((response) => {
      this.jobs = response
    })
  }
}
