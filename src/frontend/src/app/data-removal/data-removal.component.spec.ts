import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataRemovalComponent } from './data-removal.component';

describe('DataRemovalComponent', () => {
  let component: DataRemovalComponent;
  let fixture: ComponentFixture<DataRemovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataRemovalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataRemovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
