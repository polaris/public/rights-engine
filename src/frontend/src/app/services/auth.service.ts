import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from '../../environments/environment'
import { Observable, BehaviorSubject } from 'rxjs'
import { map } from 'rxjs/operators'
import jwt_decode from 'jwt-decode'
import { LocalStorageService } from './localstorage.service'
import { Router } from '@angular/router'

interface TokenResponse {
  access: string
  refresh: string
}

interface RefreshResponse {
  access: string
}

interface DeleteResponse {
  message: string
}

export interface MergeResponse {
  status: string
  message: string
}

interface Tokens {
  accessToken: any
  refreshToken: any
}

export interface Userinfo {
  email: string
  isProvider: boolean
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject: BehaviorSubject<Tokens | null>
  public loggedIn: BehaviorSubject<Userinfo | null> = new BehaviorSubject<Userinfo | null>(null)
  public readonly loggedIn$: Observable<Userinfo | null> = this.loggedIn.asObservable()
  private refreshTokenTimeout?: NodeJS.Timeout

  constructor(
    private _http: HttpClient,
    private _localStorage: LocalStorageService,
    private _router: Router
  ) {
    this.userSubject = new BehaviorSubject<Tokens | null>(null)
  }

  public get accessToken(): string | undefined {
    return this.userSubject.value?.accessToken
  }

  login(email: string, password: string): Observable<any> {
    return this._http
      .post<TokenResponse>(`${environment.apiUrl}/api/v1/auth/token`, { email, password })
      .pipe(
        map((tokens) => {
          const userTokens = {
            accessToken: tokens.access,
            refreshToken: tokens.refresh
          }
          this._localStorage.set('refreshToken', tokens.refresh)
          this.userSubject?.next(userTokens)
          const accessTokenInfo: any = jwt_decode(tokens.access)
          this.loggedIn.next({
            email: accessTokenInfo?.email ?? '',
            isProvider: accessTokenInfo?.is_provider ?? false,
          })

          this.startRefreshTokenTimer()
          return tokens
        })
      )
  }

  logout() {
    this.userSubject.next(null)
    this.loggedIn.next(null)
    this._localStorage.remove('refreshToken')
    clearTimeout(this.refreshTokenTimeout)
    //this._router.navigateByUrl('/home')

    window.location.href = `${environment.apiUrl}/logout`
  }

  refreshToken(): Observable<RefreshResponse> {
    const refreshToken = this._localStorage.get('refreshToken')
    return this._http
      .post<RefreshResponse>(`${environment.apiUrl}/api/v1/auth/token/refresh`, {
        refresh: refreshToken
      })
      .pipe(
        map((tokens) => {
          this.userSubject?.next({
            accessToken: tokens.access,
            refreshToken: this.userSubject.value?.refreshToken
          })
          const accessTokenInfo: any = jwt_decode(tokens.access)

          this.loggedIn.next({
            email: accessTokenInfo?.email ?? '',
            isProvider: accessTokenInfo?.is_provider ?? false,
          })
          this.startRefreshTokenTimer()
          return tokens
        })
      )
  }

  deleteUser(): Observable<DeleteResponse> {
    return this._http.delete<DeleteResponse>(`${environment.apiUrl}/api/v1/auth/deleteUser`, {})
  }

  mergeData(provider: number, tan: string): Observable<MergeResponse> {
    return this._http
      .post<MergeResponse>(`${environment.apiUrl}/api/v1/auth/mergeData`, { provider: provider, tan: tan })
  }

  private startRefreshTokenTimer() {
    const accessToken: any = jwt_decode(this.userSubject?.value?.accessToken)
    const expires = new Date(accessToken.exp * 1000)
    const timeout = expires.getTime() - Date.now() - 60 * 1000
    if (this.refreshTokenTimeout) clearTimeout(this.refreshTokenTimeout)
    this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), timeout)
  }
}
