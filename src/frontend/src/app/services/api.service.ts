import { Injectable } from '@angular/core'
import {
  ProviderId,
  ProviderSchema,
  UserConsent,
  UserConsentVerbs, XApiVerbGroupSchema, XApiVerbSchema
} from '../consent-management/consentDeclaration'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from '../../environments/environment'
import { GroupConsentStatus } from '../consent-management/wizard/wizard.component'

export interface UserConsentResponse {
  paused_data_recording: boolean
  consent: UserConsent | null
  provider_schema?: ProviderSchema
}

export interface ConsentHistoryConsent {
  id: number
  consented: boolean
  verb: XApiVerbSchema
  object: string
  active: boolean
}

export interface ConsentHistoryGroup {
  id: number
  active: boolean
  consented: boolean
  purposeOfCollection: string
  group_id: string
  label: string
  description: string
  requires_consent: boolean
  created: Date
  updated: Date
  providerName: string
  consents: ConsentHistoryConsent[]
}

export interface GroupedConsentHistory {
  created: Date
  updated: Date
  group: ConsentHistoryGroup
}

export interface UserConsentHistoryResponse {
  groups: GroupedConsentHistory[]
  message?: boolean
  no_consent_record?: boolean
}

export interface Provider {
  id: number
  name: string
  description: string
  createdAt: string
  versions: ProviderSchema[]
  groups?: XApiVerbGroupSchema[]
}

export interface ApplicationTokens {
  provider: Provider
  keys: string[]
}

export interface AvailableAnalyticTokensDef {
  providerId: number,
  label: string,
  analyticTokenVerbs: Array<AnalyticsTokenVerb>
}

export interface AnalyticsToken {
  id: number
  name: string
  description: string | null,
  image_path: string | null,
  key: string
  created: string
  expires: string
  can_access: AnalyticsToken[]
  analyticTokenVerbs: AnalyticsTokenVerb[]
  selected?: boolean
  isExpired?: boolean
}

export interface AnalyticsTokenVerb {
  id: string,
  label: string,
  selected: boolean,
  provider: number,
  verb: string,
  analytics_token: number
}

export interface ProviderAnalyticsToken {
  id: number
  name: string
  analytics_tokens: AnalyticsToken[]
}


interface VisualizationToken {
  id: number
  provider: number
  key: string
  created: string
  expires: string
}

export interface ProviderVisualizationTokens {
  id: number
  name: string
  visualization_tokens: VisualizationToken[]
}

export interface UserConsentStatus {
  result: Record<
    ProviderId,
    { status: 'latest' | 'outdated' | 'none'; userConsent?: UserConsent }
  >
}

export interface AuthPermission {
  id: number
  name: string
  codename: string
}
export interface AuthGroup {
  id: number
  name: string
  permissions: AuthPermission[]
}

export interface AuthUser {
  id: number
  last_login?: Date
  is_superuser: boolean
  first_name: string
  last_name: string
  is_staff: boolean
  is_active: boolean
  date_joined: Date
  email: string
  paused_data_recording: boolean
  general_privacy_policy: boolean
  groups: AuthGroup[]
  user_permissions: AuthPermission[]
}

export interface Statistics {
  message: string
  user_count: number,
  statement_count: number,
  result_count: number,
  statement_history: number[]
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) {
  }

  getProviders(): Observable<Provider[]> {
    return this.http.get<Provider[]>(`${environment.apiUrl}/api/v1/consents/provider`)
  }

  getUsers(): Observable<AuthUser[]> {
    return this.http.get<AuthUser[]>(`${environment.apiUrl}/api/v1/auth/users`)
  }

  getGroups(): Observable<AuthGroup[]> {
    return this.http.get<AuthGroup[]>(`${environment.apiUrl}/api/v1/auth/groups`)
  }

  getPermissions(): Observable<AuthPermission[]> {
    return this.http.get<AuthPermission[]>(`${environment.apiUrl}/api/v1/auth/permissions`)
  }

  saveUserPermissions(user_id: number, groups: number[], user_permissions: number[]): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/api/v1/auth/user/${user_id}/permissions`, {
      groups: groups,
      user_permissions: user_permissions,
    })
  }


  getPrivacyPolicy(): Observable<{ content: string }> {
    return this.http.get<{content: string}>(`${environment.apiUrl}/api/v1/settings/privacy-policy`)
  }
  setPrivacyPolicy(content: string): Observable<{ content: string }> {
    return this.http.post<{content: string}>(`${environment.apiUrl}/api/v1/settings/privacy-policy`, {
      content: content
    })
  }
  getPrivacyPolicyStatus(): Observable<{status: boolean}> {
    return this.http.get<{status: boolean}>(`${environment.apiUrl}/api/v1/auth/accept-privacy-policy`)
  }
  acceptPrivacyPolicy(): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/api/v1/auth/accept-privacy-policy`, {
    })
  }

  getStatistics(): Observable<Statistics> {
    return this.http.get<Statistics>(`${environment.apiUrl}/api/v1/xapi/statistics`)
  }

  sendStatement(statements: any, key: String): Observable<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Basic ${key}` // Replace with actual token retrieval method
    });

    return this.http.post<string>(`${environment.apiUrl}/xapi/statements`, [
      ...statements
    ],    { headers })
  }

  getStatement(filter: any, key: String, page_size: number): Observable<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Basic ${key}` // Replace with actual token retrieval method
    });

    return this.http.post<string>(`${environment.apiUrl}/api/v1/provider/data`, {
      page_size: page_size,
      ...filter,
  },    { headers })
  }

  storeResult(results: any, key: String): Observable<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Basic ${key}` // Replace with actual token retrieval method
    });

    return this.http.post<string>(`${environment.apiUrl}/api/v1/provider/store-result`, {
      ...results},    { headers })
  }

  getResult(filter: any, key: String): Observable<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Basic ${key}` // Replace with actual token retrieval method
    });

    return this.http.post<string>(`${environment.apiUrl}/api/v1/provider/results`, {...filter}, { headers })
  }

  getUserConsent(providerId: number): Observable<UserConsentResponse> {
    return this.http.get<UserConsentResponse>(
      `${environment.apiUrl}/api/v1/consents/user/${providerId}`
    )
  }

  getUserConsentHistory(): Observable<UserConsentHistoryResponse> {
    return this.http.get<UserConsentHistoryResponse>(
      `${environment.apiUrl}/api/v1/consents/user/history`
    )
  }

  updateUserConsentGroupActive(id: number, active: boolean): Observable<{message: string}> {
    return this.http.post<{message: string}>(
      `${environment.apiUrl}/api/v1/consents/user/update-consent-group-active`,
      {id: id, active: active},
    )
  }

  revokeUserConsentGroup(id: number): Observable<{message: string}> {
    return this.http.post<{message: string}>(
      `${environment.apiUrl}/api/v1/consents/user/revoke-consent-group`,
      {id: id},
    )
  }

  saveUserConsent(data: GroupConsentStatus): Observable<{message: string}> {
    return this.http.post<{message: string}>(
      `${environment.apiUrl}/api/v1/consents/user/save`,
      {groups: Object.keys(data).filter(key => data[parseInt(key)]).map(key => parseInt(key))} // submit as a list of group ids
    )
  }

  toggleDataRecording(): Observable<void> {
    return this.http.post<void>(`${environment.apiUrl}/api/v1/auth/toggle-data-recording`, {})
  }

  getApplicationTokens(): Observable<ApplicationTokens[]> {
    return this.http.get<ApplicationTokens[]>(`${environment.apiUrl}/api/v1/provider/keys`)
  }

  getUserConsentStatus(): Observable<UserConsentStatus> {
    return this.http.get<UserConsentStatus>(`${environment.apiUrl}/api/v1/consents/user/status`)
  }

  getAnalyticsTokens(): Observable<AnalyticsToken[]> {
    return this.http.get<AnalyticsToken[]>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens`
    )
  }

  getAnalyticsTokensUsers(): Observable<AnalyticsToken[]> {
    return this.http.get<AnalyticsToken[]>(
      `${environment.apiUrl}/api/v1/consents/user/analytics-tokens`
    )
  }

  consentVerbsForAnalyticToken(analyticTokenId: number): Observable<AnalyticsToken> {
    return this.http.post<AnalyticsToken>(
      `${environment.apiUrl}/api/v1/consents/user/analytics-tokens/consent`,
      { analyticTokenId }
    )
  }

  getProvidersUsers(): Observable<Provider[]> {
    return this.http.get<Provider[]>(
      `${environment.apiUrl}/api/v1/consents/user/providers`
    )
  }

  getAnalyticTokensAvailableVerbs(): Observable<AvailableAnalyticTokensDef[]> {
    return this.http.get<AvailableAnalyticTokensDef[]>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens/available-verbs`
    )
  }

  createAnalyticsToken(
    expires: string,
    name: string,
    description: string
  ): Observable<AnalyticsToken> {
    return this.http.post<AnalyticsToken>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens/create`,
      { expires, name, description }
    )
  }

  saveAnalyticTokenActiveVerbs(
    tokenId: number,
    activeVerbs: AnalyticsTokenVerb[]
  ): Observable<{ message: string }> {
    return this.http.post<{ message: string }>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens/${tokenId}/update-verbs`,
      { active_verbs: activeVerbs }
    )
  }

  deleteAnalyticsToken(tokenId: number): Observable<void> {
    return this.http.delete<void>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens/${tokenId}/delete`
    )
  }

  isAnalyticsTokenNameAvailable(name: string): Observable<{ is_available: boolean }> {
    return this.http.post<{ is_available: boolean }>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens/name-available`,
      { name }
    )
  }

  createOrUpdateVerbGroup(provider_id: number, id: string, label: string, description: string,
                  purposeOfCollection: string, requiresConsent: boolean,
                  selectedVerbs: string[]) {
    console.log(`${environment.apiUrl}/api/v1/consents/provider/${provider_id}/create-verb-group`)
    return this.http.post<{message: string, group?: XApiVerbGroupSchema}>(`${environment.apiUrl}/api/v1/consents/provider/${provider_id}/create-verb-group`, {
      id: id,
      label: label,
      description: description,
      purposeOfCollection: purposeOfCollection,
      requiresConsent: requiresConsent,
      verbs: selectedVerbs
    })
  }

  saveAccessRelation(target: number, ids: number[]): Observable<AnalyticsToken> {
    return this.http.post<AnalyticsToken>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens/sync-engine-access`,
      { target, ids }
    )
  }

  uploadImageForAnalyticToken(analyticTokenId: number, image: File) {
    const formData: FormData = new FormData()
    formData.append('image', image, image.name)
    formData.append('analyticTokenId', analyticTokenId + '')

    return this.http.post<void>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens/${analyticTokenId}/image`, formData)
  }

  deleteImageForAnalyticToken(analyticTokenId: number) {
    return this.http.delete<void>(
      `${environment.apiUrl}/api/v1/provider/analytics-tokens/${analyticTokenId}/image`
    )
  }
}
