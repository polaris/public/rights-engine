import { Injectable } from '@angular/core'
import { Observable, BehaviorSubject } from 'rxjs'
import { LocalStorageService } from './localstorage.service'

@Injectable({
    providedIn: 'root'
})
export class ThemeService {
    public darkModeSubject: BehaviorSubject<boolean>

    constructor(private _localStorage: LocalStorageService) {
        this.darkModeSubject = new BehaviorSubject<boolean>(false)
    }

    toggleTheme() {
        this.darkModeSubject.next(!this.darkModeSubject.value)
        this._localStorage.set('themePreference', this.darkModeSubject.value ? 'dark' : 'light')
    }

    activeDarkMode(): void {
        this.darkModeSubject.next(true)
    }

    activeLightMode(): void {
        this.darkModeSubject.next(false)
    }
}
