export interface XApiObjectSchema {
  definition: Object
  consented?: boolean
  defaultConsent: boolean
  objectType: string
  label: string
  id: string
}

export interface XApiObjectConsented {
  definition: Object
  consented: boolean
  defaultConsent: boolean
  objectType: string
  label: string
  id: string
}

export interface XApiVerbSchema {
  id: string
  label: string
  description: string
  defaultConsent: boolean
  essential: boolean
  objects: XApiObjectSchema[]
}

export interface XApiVerbConsented extends XApiVerbSchema {
  consented: boolean
  objects: XApiObjectConsented[]
}

export interface XApiVerbGroupSchema {
  id: number
  label: string
  description: string
  purposeOfCollection: string
  requires_consent: boolean
  provider_schema: number
  verbs: XApiVerbSchema[]
}

export interface XApiVerbGroupConsented extends XApiVerbGroupSchema {
  id: number
  label: string
  description: string
  purposeOfCollection: string
  showVerbDetails: boolean
  verbs: XApiVerbSchema[]
  isDefault: boolean
}

export interface ConsentGroupConsented {
  id: number
  label: string
  description: string
  purposeOfCollection: string
  showVerbDetails: boolean
  verbs: XApiVerbConsented[]
  isDefault: boolean
}

interface ConsentDeclaration {
  id: number
  description: string
}

export interface ProviderSchemaDefinition {
  id: string
  name: string
  description: string
  verbs: XApiVerbSchema[]
  essential_verbs: XApiVerbSchema[] // verbs which can be collected without the user consent
}

export interface ProviderSchema {
  id: number
  description: string
  superseded_by: number | null
  createdAt: string
  groups: XApiVerbGroupConsented[]
  essential_verbs: XApiVerbSchema[] // verbs which can be collected without the user consent
  definition: ProviderSchemaDefinition
}

export interface UserConsent extends ConsentDeclaration {
  id: number
  groups: ConsentGroupConsented[]
  essential_verbs: XApiVerbSchema[] // verbs which can be collected without the user consent
  created?: Date
}

export interface UserConsentVerbs {
  providerId: ProviderId
  providerSchemaId: ProviderSchemaId
  verbs: { id: string; consented?: boolean; objects: string }[]
}

export type ProviderId = number
export type ProviderSchemaId = number

/**
 * Iterates over each verb and object of a provider schema and sets the consented field to the default consent
 * @param providerSchema
 * @returns
 */
export const providerSchemaToUserConsent = (providerSchema: ProviderSchema): UserConsent => {
  return {
    id: providerSchema.id,
    description: providerSchema.description,
    groups:  providerSchema.groups? providerSchema.groups.map((group) => ({
      ...group,
      verbs: group.verbs.map((verb) => ({
        ...verb,
        consented: verb.defaultConsent,
        objects: verb.objects ? verb.objects.map((object) => ({
          ...object,
          consented: object.defaultConsent
        })) : []
      }))
    })) : [],
    essential_verbs: providerSchema.essential_verbs
  }
}
