import { HttpClient, HttpEventType } from '@angular/common/http'
import { Component, Input, OnInit } from '@angular/core'
import { Subscription } from 'rxjs'
import { environment } from '../../../environments/environment'
import { ApiService, Provider } from 'src/app/services/api.service'
import {
  ProviderSchema,
  providerSchemaToUserConsent,
  UserConsent, XApiVerbGroupSchema
} from '../consentDeclaration'
import { faPaperclip, faTrash } from '@fortawesome/free-solid-svg-icons'
import { NzMessageService } from 'ng-zorro-antd/message'

type ProviderId = string
type ProviderSchemaVersionId = string

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  @Input() showTitle: boolean = true;
  providers: Provider[] = []
  fileName = $localize`:@@selectProviderSchema:Please select a JSON file.`
  selectedFile?: File
  uploadProgress: number | null = null
  uploadSub: Subscription | null = null
  definitionVisible: Record<ProviderId, Record<ProviderSchemaVersionId, boolean>> = {}

  constructor(
    private _http: HttpClient,
    private _apiService: ApiService,
    private _messageService: NzMessageService
  ) {
  }

  ngOnInit(): void {
    this.getAllProviders()
  }

  getAllProviders() {
    this._apiService.getProviders().subscribe((providers) => {
      this.providers = providers

      providers.map((provider) => {
        const res = provider.versions.reduce((acc, e) => {
          {
            acc[e.id] = false
          }
          return acc
        }, {} as Record<ProviderSchemaVersionId, boolean>)
        this.definitionVisible[provider.id] = res
      })
    })
  }

  getGroupsForProviderSchema(provider_schema: ProviderSchema): XApiVerbGroupSchema[] {
    let groups: XApiVerbGroupSchema[] = [];
    this.providers.forEach((provider) => {
      provider.versions.forEach((version) => {
        if(version.id === provider_schema.id) {
          let temp_groups = provider.groups?.filter((group) => group.provider_schema == provider_schema.id);
          if(temp_groups !== undefined) {
            groups = temp_groups;
          }
        }
      })
    })
    return groups;
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0]

    if (file) {
      this.selectedFile = file
      this.fileName = file.name
    }
  }

  onSubmit() {
    if (this.selectedFile) {
      const formData = new FormData()
      formData.append('filename', this.fileName)
      formData.append('provider-schema', this.selectedFile)

      const upload$ = this._http.put(
        `${environment.apiUrl}/api/v1/consents/provider/create`,
        formData,
        {
          reportProgress: true,
          observe: 'events'
        }
      )

      this.uploadSub = upload$.subscribe({
        next: (event) => {
          if (event.type == HttpEventType.UploadProgress) {
            this.uploadProgress = Math.round(100 * (event.loaded / (event.total ?? 1)))
          }
        },
        complete: () => {
          this.reset()
          this.getAllProviders()
          this._messageService.success($localize`:@@toastMessageCreated:Created`)
        },
        error: () => {
          // Error toast is triggered via interceptor
          this.reset()
        }
      })
    }
  }

  cancelUpload() {
    this.uploadSub?.unsubscribe()
    this.reset()
  }

  reset() {
    this.fileName = $localize`:@@selectProviderSchema:Please select a JSON file.`
    this.uploadProgress = null
    this.uploadSub = null
    window.scroll(0, 0)
  }

  providerSchemaToUserConsent(providerSchema: ProviderSchema): UserConsent {
    return providerSchemaToUserConsent(providerSchema)
  }

  protected readonly faPaperclip = faPaperclip
  protected readonly faTrash = faTrash
}
