import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationTokensComponent } from './application-tokens.component';

describe('ApplicationTokensComponent', () => {
  let component: ApplicationTokensComponent;
  let fixture: ComponentFixture<ApplicationTokensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicationTokensComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ApplicationTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
