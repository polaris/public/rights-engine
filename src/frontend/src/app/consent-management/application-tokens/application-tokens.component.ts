import { Component, OnInit } from '@angular/core'
import {
  ApplicationTokens,
  ApiService
} from 'src/app/services/api.service'
import { Clipboard } from '@angular/cdk/clipboard'
import { faCopy, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import { NzMessageService } from 'ng-zorro-antd/message'

@Component({
  selector: 'app-application-tokens',
  templateUrl: './application-tokens.component.html',
  styleUrls: ['./application-tokens.component.scss'],
})
export class ApplicationTokensComponent implements OnInit {
  dataSource: ApplicationTokens[] = []
  columnsToDisplay = ['name']
  columnsToDisplayWithExpand = [...this.columnsToDisplay, 'expand']

  expandSet = new Set<number>()

  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id)
    } else {
      this.expandSet.delete(id)
    }
  }

  constructor(
    private _apiService: ApiService,
    private _clipboard: Clipboard,
    private _messageService: NzMessageService
  ) {
  }

  ngOnInit(): void {
    this.getApplicationTokens()
  }

  getApplicationTokens() {
    this._apiService.getApplicationTokens().subscribe((response) => {
      this.dataSource = response
    })
  }

  toggleExpand(id: number) {
    if(!this.expandSet.has(id)) {
      this.expandSet.add(id)
    } else {
      this.expandSet.delete(id)
    }
  }

  copyTokenToClipboard(token: string): void {
    this._clipboard.copy(token)
    this._messageService.info($localize`:@@copiedToClipboard: Copied to Clipboard`)
  }

  protected readonly faCopy = faCopy
  protected readonly faEye = faEye
  protected readonly faEyeSlash = faEyeSlash
}
