import { Component, Input } from '@angular/core'
import { XApiVerbGroupSchema, XApiVerbSchema } from '../consentDeclaration'
import { NzModalService } from 'ng-zorro-antd/modal'
import { CreateVerbGroupDialog } from '../../dialogs/create-verb-group-dialog/create-verb-group-dialog'
import { ApiService } from '../../services/api.service'
import { NzMessageService } from 'ng-zorro-antd/message'

@Component({
  selector: 'app-verb-groups',
  templateUrl: './verb-groups.component.html',
  styleUrls: ['./verb-groups.component.scss']
})
export class VerbGroupsComponent {
  @Input() provider_id: number = -1;
  @Input() allVerbs: XApiVerbSchema[] = [];
  @Input() verbGroups: XApiVerbGroupSchema[] = [];

  protected additionalVerbGroups: XApiVerbGroupSchema[] = [];

  constructor(private _apiService: ApiService, private _dialog: NzModalService, private _messageService: NzMessageService) {
  }

  createVerbGroup(): void {
    const dialogRef = this._dialog.create({
      nzContent: CreateVerbGroupDialog,
      nzComponentParams: {
        headerText: $localize`:@@createGroup:Create Data Packet`,
        verbs: this.allVerbs,
      }
    });
    dialogRef.afterClose.subscribe(result => {
      const {label, description, purposeOfCollection, requiresConsent, selectedVerbs} = result;
      this._apiService.createOrUpdateVerbGroup(this.provider_id, "", label, description, purposeOfCollection, requiresConsent, selectedVerbs)
        .subscribe(result => {
          if(result.group)
          {
            this.additionalVerbGroups = [...this.additionalVerbGroups, result.group];
            this._messageService.success("Das Datenbündel wurde erstellt.")
          }
        })
    });
  }
}
