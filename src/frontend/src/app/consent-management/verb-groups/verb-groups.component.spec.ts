import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerbGroupsComponent } from './verb-groups.component';

describe('VerbGroupsComponent', () => {
  let component: VerbGroupsComponent;
  let fixture: ComponentFixture<VerbGroupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerbGroupsComponent ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(VerbGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
