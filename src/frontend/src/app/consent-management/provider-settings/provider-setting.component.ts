import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core'
import {
  ConsentGroupConsented,
  UserConsent,
  XApiVerbConsented
} from '../consentDeclaration'

import { DeleteDialog } from 'src/app/dialogs/delete-dialog/delete-dialog'
import { DataRemovalService } from 'src/app/data-removal.service'

import { faArrowRight, faInfo, faTrash } from '@fortawesome/free-solid-svg-icons'
import { NzMessageService } from 'ng-zorro-antd/message'
import { NzModalService } from 'ng-zorro-antd/modal'

interface LookUp {
  [key: string]: boolean
}

@Component({
  selector: 'app-provider-setting[consentDeclaration]',
  templateUrl: './provider-setting.component.html',
  styleUrls: ['./provider-setting.component.scss']
})
export class PrivacySettingComponent implements OnInit {
  faArrowRight = faArrowRight
  faInfo = faInfo
  faTrash = faTrash

  userConsentVerbsLookUp: LookUp = {}
  userConsentObjectLookUp: LookUp = {}
  @Input() consentDeclaration!: UserConsent
  @Input() previousUserConsent: UserConsent | null = null
  @Input() deletable? = false
  @Input() preview: boolean = false
  @Output()
  change: EventEmitter<UserConsent> = new EventEmitter<UserConsent>()

  constructor(
    private _dialog: NzModalService,
    private _removalService: DataRemovalService,
    private _messageService: NzMessageService
  ) {
  }

  ngOnInit(): void {
    if (this.previousUserConsent) this.buildUserConsentVerbsLookUps()
  }

  buildUserConsentVerbsLookUps(): void {
    const verbLookUp: LookUp = {}
    const objectLookUp: LookUp = {}

    if (this.previousUserConsent) {
      for (const group of this.previousUserConsent.groups) {
        for (const verb of group.verbs) {
          verbLookUp[verb.id] = verb.consented ?? verb.defaultConsent
          for (const object of verb.objects)
            objectLookUp[object.id] = object.consented ?? object.defaultConsent
        }
      }
    }
    this.userConsentVerbsLookUp = verbLookUp
    this.userConsentObjectLookUp = objectLookUp
  }

  toggleGroup(group: ConsentGroupConsented, checked: boolean): void {
    group.verbs.forEach((verb) => {
      verb.consented = checked
      verb.objects.forEach((object) => (object.consented = checked))
      this.toggleVerbWithoutDialog(verb.id, checked)
    })
    this.change.emit(this.consentDeclaration)
  }

  toggleVerbWithoutDialog(verbId: string, checked: boolean): void {
    this.consentDeclaration.groups.forEach((group) => {
      if (group.verbs.find(({ id }) => id == verbId))
        group.verbs.forEach((verb) => {
          if (verb.id === verbId) {
            verb.consented = checked
            verb.objects.forEach((object) => {
              object.consented = checked
            })
          }
        })
    })
  }

  isGroupActive(group: ConsentGroupConsented): boolean {
    return group.verbs.reduce((isActive, verb) => isActive && verb.consented, true)
  }

  /**
   * Toggle verb from active to inactive or vice versa.
   * In case a verb is active, a confirmation dialog pops up.
   * Toggling a verb influences all sibling objects.
   * @param verbId
   */
  toggleVerb(verbId: string): void {
    let isConfirmationRequired = false
    this.consentDeclaration.groups.forEach((group) => {
      if (group.verbs.find(({ id }) => id == verbId))
        group.verbs.forEach((verb) => {
          if (verb.id === verbId) {
            const newConsentedValue = !verb.consented
            isConfirmationRequired = !newConsentedValue
            // user clicked on active verb
            verb.consented = newConsentedValue
            // activate all objects
            verb.objects.forEach((object) => {
              object.consented = newConsentedValue
            })
          }
        })
    })
    if (isConfirmationRequired && this.previousUserConsent != null) {
      const onConfirm = () => this.change.emit(this.consentDeclaration)
      const onCancel = () => {
        // revert
        this.consentDeclaration.groups.forEach((group) => {
          if (group.verbs.find(({ id }) => id == verbId))
            group.verbs.forEach((verb) => {
              if (verb.id === verbId) {
                const newConsentedValue = !verb.consented
                isConfirmationRequired = !newConsentedValue
                verb.consented = newConsentedValue
                verb.objects.forEach((object) => {
                  object.consented = newConsentedValue
                })
              }
            })
        })
        this.change.emit(this.consentDeclaration)
      }
      this.openVerbWarningDialog(onConfirm, onCancel, verbId)
    } else this.change.emit(this.consentDeclaration)
  }

  toggleVerbBasedOnObjects(verb: XApiVerbConsented): void {
    const allObjectsActive = verb.objects.reduce(
      (allActive, object) => allActive && object.consented,
      true
    )
    const allObjectsInActive = verb.objects.reduce(
      (allActive, object) => allActive && !object.consented,
      true
    )

    if (allObjectsActive) verb.consented = true
    if (allObjectsInActive) verb.consented = false
  }

  /**
   * Toggle object from active to inactive or vice versa.
   * In case an object is active, a confirmation dialog pops up.
   * Toggeling a verb influences all sibling objects.
   * @param verbId
   * @param objectId
   */
  toggleObjectConsent(verbId: string, objectId: string): void {

    let isConfirmationRequired = false
    this.consentDeclaration.groups.forEach((group) => {
      if (group.verbs.find(({ id }) => id == verbId))
        group.verbs.forEach((verb) => {
          if (verb.id === verbId) {
            if (verb.objects.find(({ id }) => id === objectId)) {
              verb.objects.forEach((object) => {
                if (object.id === objectId) {
                  const newConsentedValue = !object.consented
                  if (!newConsentedValue) {
                    // user clicked on active object
                    isConfirmationRequired = true
                    object.consented = newConsentedValue
                    this.toggleVerbBasedOnObjects(verb)
                  } else {
                    // user clicked on inactive object
                    object.consented = newConsentedValue
                    this.toggleVerbBasedOnObjects(verb)
                  }
                }
              })
            }
          }
        })
    })
    if (isConfirmationRequired && this.previousUserConsent != null) {
      const onConfirm = () => this.change.emit(this.consentDeclaration)
      const onCancel = () => {
        // revert
        this.consentDeclaration.groups.forEach((group) => {
          if (group.verbs.find(({ id }) => id == verbId))
            group.verbs.forEach((verb) => {
              if (verb.id === verbId) {
                if (verb.objects.find(({ id }) => id === objectId)) {
                  verb.objects.forEach((object) => {
                    if (object.id === objectId) {
                      object.consented = !object.consented
                      this.toggleVerbBasedOnObjects(verb)
                    }
                  })
                }
              }
            })
        })
        this.change.emit(this.consentDeclaration)
      }
      this.openObjectWarningDialog(onConfirm, onCancel, verbId)
    } else this.change.emit(this.consentDeclaration)
  }

  openVerbWarningDialog(onConfirm: () => void, onCancel: () => void, verbId: string): void {
    if (this.previousUserConsent && !this.getVerbConsentFromUserConsent(verbId))
      return
    this._dialog.create({
      nzTitle: $localize`:@@pauseVerb:Pause Verb`,
      nzFooter: [
        {
          label: $localize`:@@cancel:Cancel`,
          onClick: onCancel
        },
        {
          label: $localize`:@@yes:Yes`,
          onClick: onConfirm
        }
      ]
    })
  }

  openObjectWarningDialog(onConfirm: () => void, onCancel: () => void, verbId: string): void {
    if (this.previousUserConsent && !this.getVerbConsentFromUserConsent(verbId))
      return
    this._dialog.create({
      nzTitle: $localize`:@@pauseObject:Pause Object`,
      nzFooter: [
        {
          label: $localize`:@@cancel:Cancel`,
          onClick: onCancel
        },
        {
          label: $localize`:@@yes:Yes`,
          onClick: onConfirm
        }
      ]
    })
  }

  /**
   * Looks in user consent for a matching verbId and returns value of consented field, if matching verbId exists.
   * @param verbId string
   * @returns boolean | null
   */
  getVerbConsentFromUserConsent(verbId: string): boolean | null {
    if (!this.previousUserConsent) return null

    for (const group of this.previousUserConsent.groups) {
      for (const verb of group.verbs) {
        if (verb.id === verbId) return verb.consented ?? verb.defaultConsent
      }
    }
    return null
  }

  openDeleteVerbDataDialog(event: any, verbId: string): void {
    event.stopPropagation()
    const dialogRef = this._dialog.create({
      nzContent: DeleteDialog,
      nzComponentParams: {
        headerText: $localize`:@@deleteStoredVerbData: Delete Stored Data for Verb`
      }
    })

    dialogRef.afterClose.subscribe((confirmed) => {
      if (confirmed) {
        this._removalService
          .create({ immediately: true, scope: { verbId } })
          .subscribe((response) => {
            this._messageService.success($localize`:@@toastMessageSubmitted:Submitted`)
          })
      }
    })
  }

  openDeleteObjectDataDialog(event: any, verbId: string, objectId: string): void {
    event.stopPropagation()
    const dialogRef = this._dialog.create({
      nzContent: DeleteDialog,
      nzComponentParams: {
        headerText: $localize`:@@deleteStoredObjectData: Delete Stored Data for Object`
      }
    })

    dialogRef.afterClose.subscribe((confirmed) => {
      if (confirmed) {
        this._removalService
          .create({ immediately: true, scope: { verbId, objectId } })
          .subscribe((response) => {
            this._messageService.success($localize`:@@toastMessageSubmitted:Submitted`)
          })
      }
    })
  }
}
