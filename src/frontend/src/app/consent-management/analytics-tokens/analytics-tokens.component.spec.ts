import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticsTokensComponent } from './analytics-tokens.component';

describe('AnalyticsTokensComponent', () => {
  let component: AnalyticsTokensComponent;
  let fixture: ComponentFixture<AnalyticsTokensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalyticsTokensComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AnalyticsTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
