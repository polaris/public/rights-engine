import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core'
import {
  ApiService,
  AnalyticsToken,
  AvailableAnalyticTokensDef,
  AnalyticsTokenVerb
} from '../../services/api.service'
import { Clipboard } from '@angular/cdk/clipboard'
import { CreateTokenDialog } from 'src/app/dialogs/create-token-dialog/create-token-dialog'
import * as moment from 'moment'
import { DeleteDialog } from 'src/app/dialogs/delete-dialog/delete-dialog'
import { Subscription } from 'rxjs'
import { environment } from 'src/environments/environment'

import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { NzMessageService } from 'ng-zorro-antd/message'
import { NzModalService } from 'ng-zorro-antd/modal'

type AnalyticsTokenId = number

@Component({
  selector: 'app-analytics-tokens',
  templateUrl: './analytics-tokens.component.html',
  styleUrls: ['./analytics-tokens.component.scss'],
})
export class AnalyticsTokensComponent implements OnInit, OnDestroy {

  analyticTokens: Array<AnalyticsToken> = []
  availableAnalyticTokenVerbs: Array<AvailableAnalyticTokensDef> = []
  displayedColumns: string[] = ['name', 'key', 'created', 'expires']
  columnsToDisplayWithExpand = [...this.displayedColumns, 'expand']
  createTokenSub?: Subscription
  deleteTokenSub?: Subscription
  Object = Object
  @ViewChild('fileInput') fileInputRef!: ElementRef
  selectedFile: File | null = null

  expandSet = new Set<number>();
  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.clear();
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }

  constructor(
    private _apiService: ApiService,
    private _clipboard: Clipboard,
    private _messageService: NzMessageService,
    private _dialog: NzModalService
  ) {
  }

  ngOnInit(): void {
    this.getAnalyticsTokens()
    this.getAvailableVerbs()
  }

  ngOnDestroy(): void {
    this.createTokenSub?.unsubscribe()
    this.deleteTokenSub?.unsubscribe()
  }

  getExpandedElement(): AnalyticsToken | undefined{
    let token_id: number = this.expandSet.values().next().value;
    return this.analyticTokens.find((tok) => tok.id === token_id);
  }

  getAnalyticsTokens(): void {
    this._apiService.getAnalyticsTokens().subscribe((analyticTokens) => {
      this.analyticTokens = analyticTokens.map((e) => {
        return this.fillCanAccessOptions(e, analyticTokens)
      })
    })
  }

  getAvailableVerbs(): void {
    this._apiService.getAnalyticTokensAvailableVerbs().subscribe((availableVerbDefinition) => {

      this.availableAnalyticTokenVerbs = availableVerbDefinition.map(def => {
        let seen: { [key: string]: boolean } = {}
        return ({
          ...def, analyticTokenVerbs: def.analyticTokenVerbs.filter((item) =>
            seen.hasOwnProperty(item.id) ? false : (seen[item.id] = true)
          )
        })
      })
    })
  }

  onCreateAnalyticsTokenClick(): void {
    const dialogRef = this._dialog.create({
      nzContent: CreateTokenDialog,
      nzComponentParams: {
        headerText: $localize`:@@createAnalyticsTokenDialogHeader:Create New Analytics Token`
      }
    })

    this.deleteTokenSub = dialogRef.afterClose.subscribe((data) => {
      if (data?.confirmed) {
        this.createToken(data.expires, data.name, data.description)
      }
    })
  }

  createToken(expires: string, name: string, description: string): void {
    this._apiService
      .createAnalyticsToken(expires, name, description)
      .subscribe((newToken) => {
        this.analyticTokens = [...this.analyticTokens, newToken]
        this.getAnalyticsTokens()
        this._messageService.success($localize`:@@tokenCreated: Token created`)
      })
  }

  copyTokenToClipboard(token: string): void {
    this._clipboard.copy(token)
    this._messageService.info($localize`:@@copiedToClipboard: Copied to Clipboard`)
  }

  onDeleteTokenClick(tokenId: number): void {
    const dialogRef = this._dialog.create({
      nzContent: DeleteDialog,
      nzComponentParams: {
        headerText: $localize`:@@deleteSelectedToken:Delete selected token`
      }
    });

    this.createTokenSub = dialogRef.afterClose.subscribe((confirmed) => {
      if (confirmed)
        this._apiService.deleteAnalyticsToken(tokenId).subscribe(() => {
          this.getAnalyticsTokens()
          this._messageService.success($localize`:@@tokenDeleted: Token deleted`)
        })
    })
  }

  isVerbSelected(verb: AnalyticsTokenVerb, providerId: number): boolean {
    return !!this.getExpandedElement()?.analyticTokenVerbs?.find(v => v.verb == verb.id && providerId == v.provider)
  }

  onSelectionChanged(verb: AnalyticsTokenVerb, providerId: number) {
    console.log(verb)
    let expandedElement : AnalyticsToken | undefined = this.getExpandedElement();
    if (!expandedElement)
      return

    if (this.isVerbSelected(verb, providerId))
      expandedElement.analyticTokenVerbs = expandedElement.analyticTokenVerbs.filter(v => v.verb !== verb.id)
    else
      expandedElement.analyticTokenVerbs = [...expandedElement.analyticTokenVerbs, {
        ...verb,
        verb: verb.id,
        id: '-1'
      }]
  }

  saveActiveVerbs(): void {
    let expandedElement : AnalyticsToken | undefined = this.getExpandedElement();
    if (!expandedElement)
      return
    this._apiService.saveAnalyticTokenActiveVerbs(expandedElement.id, expandedElement.analyticTokenVerbs).subscribe(() => {
      this._messageService.success($localize`:@@toastMessageSaved:Saved`)
      this.expandSet.clear()
    })
  }

  isTokenExpired(expires: string): boolean {
    if (!expires) return false
    if (moment(expires) <= moment()) return true
    return false
  }

  fillCanAccessOptions(token: AnalyticsToken, options: AnalyticsToken[]): AnalyticsToken {
    return {
      ...token,
      can_access: options
        .filter((c) => c.id !== token.id)
        .map((b) => ({
          ...b,
          selected: !!token.can_access.find((a) => a.id === b.id)
        }))
    }
  }

  saveEngineResultAccess(id: AnalyticsTokenId): void {
    let expandedElement : AnalyticsToken | undefined = this.getExpandedElement();
    if (!expandedElement)
      return
    this._apiService
      .saveAccessRelation(
        id,
        expandedElement?.can_access.filter((e) => !!e.selected).map((e) => e.id) ?? []
      )
      .subscribe((data: any) => {
        this._messageService.success($localize`:@@toastMessageSaved:Saved`)

        this.analyticTokens.map((e) => {
          if (e.id === id) return this.fillCanAccessOptions(data, this.analyticTokens)
          else return e
        })
      })

  }

  onUploadButtonClick() {
    if (!this.selectedFile)
      this.fileInputRef.nativeElement.click()
    else
      this.uploadSelectedFile()
  }

  uploadFileChanged(event: Event) {
    this.selectedFile = (event.target as HTMLInputElement).files![0]
  }

  uploadSelectedFile() {
    this._apiService.uploadImageForAnalyticToken(
      this.getExpandedElement()!.id,
      this.selectedFile!
    )
      .subscribe((data: any) => {
        this._messageService.success($localize`:@@toastMessageSaved:Saved`)
        this.getAnalyticsTokens()
      })
  }

  getImageSource() {
    return `${environment.apiUrl}` + '/api/v1/provider/analytics-tokens/' + this.getExpandedElement()?.id + '/image/' + this.getExpandedElement()?.image_path
  }

  protected readonly faTimes = faTimes
}
