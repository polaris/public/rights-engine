import { Component, Input, OnInit } from '@angular/core'
import {
  UserConsent,
  providerSchemaToUserConsent,
  ProviderSchema,
  ProviderId,
  UserConsentVerbs, XApiVerbGroupSchema
} from '../consentDeclaration'
import {
  ApiService,
  ConsentHistoryGroup,
  GroupedConsentHistory,
  Provider,
  UserConsentStatus
} from 'src/app/services/api.service'
import { AuthService } from 'src/app/services/auth.service'
import { zip } from 'rxjs'
import { NzMessageService } from 'ng-zorro-antd/message'
import { NzModalRef } from 'ng-zorro-antd/modal'
import { faArrowLeft, faArrowRight, faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'

export type GroupConsentStatus = Record<number, boolean>

export const extractVerbs = (providerSchema: UserConsent, providerId: ProviderId): any[] => {
  return providerSchema.groups.flatMap((group) => {
    return group.verbs.flatMap(({ id, consented, defaultConsent, objects }) => ({
      provider: providerId,
      id,
      consented: consented,
      objects: objects ? JSON.stringify(
        objects.map((object) => ({
          ...object,
          consented: object.consented
        }))
      ) : []
    }))
  })
}

@Component({
  selector: 'wizard-dialog',
  templateUrl: 'wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardDialog implements OnInit {
  @Input() consentStatus: GroupConsentStatus = {}

  constructor(
    public dialogRef: NzModalRef<WizardDialog>,
    private _apiService: ApiService,
    private _messageService: NzMessageService,
    private _authService: AuthService
  ) {
  }

  loading = false

  providers: Provider[] = []
  providerIds: number[] = []
  providerUserConsent: Record<
    ProviderId,
    {
      provider: Provider
      userConsent: UserConsent | null
      defaultConsent: UserConsent
      acceptedProviderSchema?: ProviderSchema
    }
  > = {}
  wizardUserConsents: Record<
    ProviderId,
    {
      provider: Provider
      userConsent: UserConsent | null
      defaultConsent: UserConsent
      acceptedProviderSchema?: ProviderSchema
    }
  > = {}
  userConsents: UserConsentVerbs[] = []
  selectedProviderId: number | null = null
  isSummary = false

  groupsToConsent: GroupConsentStatus = {}

  _initProviderIds(): void {
    this.providers.forEach(provider => {
      let containsGroupToConsent: boolean = false
      provider.groups?.forEach(group => {
        if(group.requires_consent)
        {
          if(!Object.keys(this.consentStatus).includes(String(group.id)) || (Object.keys(this.consentStatus).includes(String(group.id)) && !this.consentStatus[group.id]))
          {
            this.groupsToConsent[group.id] = false
            containsGroupToConsent = true;
          }
        }
      })
      if(containsGroupToConsent) {
        this.providerIds.push(provider.id);
        if(this.selectedProviderId === null) {
          this.selectedProviderId = provider.id;
        }
      }
    })
  }
  ngOnInit(): void {
    this.loading = true
    this._apiService.getProviders().subscribe((providers) => {
      this.providers = providers

      if (providers.length === 0) this.loading = false

      // maybe load consents
      if(Object.keys(this.consentStatus).length === 0)
      {
        this._apiService.getUserConsentHistory().subscribe(history => {
          history.groups.forEach((group: GroupedConsentHistory) => {
            this.consentStatus[group.group.id] = group.group.consented
          })
          this._initProviderIds()
          this.loading = false
        })
      } else {
        this._initProviderIds()
        this.loading = false
      }
    })
  }

  next(): void {
    if (this.selectedProviderId) {
      const idx = this.providerIds.indexOf(this.selectedProviderId)
      if (idx + 1 < this.providerIds.length)
        this.selectedProviderId = this.providerIds[idx + 1]
    }
  }

  prev(): void {
    if (this.isSummary) {
      this.isSummary = false
      this.selectedProviderId = this.providerIds[this.providerIds.length - 1]
    } else {
      if (this.selectedProviderId) {
        const idx = this.providerIds.indexOf(this.selectedProviderId)
        if (idx - 1 >= 0) this.selectedProviderId = this.providerIds[idx - 1]
      }
    }
  }

  getSelectedProvider(): Provider | null {
    let _selectedProvider : Provider | null = null
    this.providers.forEach(provider => {
      if(provider.id === this.selectedProviderId) {
        _selectedProvider = provider;
      }
    })
    return _selectedProvider;
  }

  consentChanged() : boolean {
    let changed = false;
    for (let group_id in this.groupsToConsent) {
      if(this.groupsToConsent[group_id]) {
        changed = true;
      }
    }
    return changed;
  }

  showSummary(): void {
    this.isSummary = true
  }


  submit(): void {
    this._apiService.saveUserConsent(this.groupsToConsent).subscribe((response) => {
      this._messageService.success($localize`:@@toastMessageSubmitted:Submitted`)
      this.dialogRef.close()
    })
  }


  protected readonly faArrowLeft = faArrowLeft
  protected readonly faArrowRight = faArrowRight
  protected readonly Object = Object
  protected readonly String = String
}
