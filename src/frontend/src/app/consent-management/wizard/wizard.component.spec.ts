import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardDialog } from './wizard.component';

describe('WizardDialog', () => {
  let component: WizardDialog;
  let fixture: ComponentFixture<WizardDialog>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WizardDialog ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WizardDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
