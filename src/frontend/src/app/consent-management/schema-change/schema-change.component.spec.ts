import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchemaChangeComponent } from './schema-change.component';

describe('SchemaChangeComponent', () => {
  let component: SchemaChangeComponent;
  let fixture: ComponentFixture<SchemaChangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchemaChangeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SchemaChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
