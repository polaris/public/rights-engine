import { Component, OnInit, Input } from '@angular/core'
import { ObjectChange } from '../schema-change.component'
import { faPlus, faTrash, faWrench } from '@fortawesome/free-solid-svg-icons'

@Component({
    selector: 'app-object-changes',
    templateUrl: './object-changes.component.html',
    styleUrls: ['./object-changes.component.scss']
})
export class ObjectChangesComponent implements OnInit {
    @Input() objectChanges: ObjectChange[] = []

    constructor() {}

    ngOnInit(): void {}

  protected readonly faPlus = faPlus
  protected readonly faWrench = faWrench
  protected readonly faTrash = faTrash
}
