import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectChangesComponent } from './object-changes.component';

describe('ObjectChangesComponent', () => {
  let component: ObjectChangesComponent;
  let fixture: ComponentFixture<ObjectChangesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectChangesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ObjectChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
