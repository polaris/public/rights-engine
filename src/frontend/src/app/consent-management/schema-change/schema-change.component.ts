import { Component, OnInit, Input } from '@angular/core'
import {
  XApiVerbGroupConsented,
  ProviderSchemaDefinition,
  XApiObjectSchema,
  XApiVerbSchema
} from '../consentDeclaration'
import { faPlus, faTrash, faWrench } from '@fortawesome/free-solid-svg-icons'

type VerbChangeType = 'new' | 'removed' | 'changed' | 'onlyObjectChanged' | 'moved'

type ObjectChangeType = 'new' | 'removed' | 'changed'

export interface ObjectChange {
  type: ObjectChangeType
  old?: XApiObjectSchema
  new?: XApiObjectSchema
  description: string
}

interface VerbChange {
  type: VerbChangeType
  old?: XApiVerbSchema
  new?: XApiVerbSchema
  description: string
  isEssentialVerb: boolean
  objectChanges: ObjectChange[]
}

interface SchemaChange {
  id: string
  name: string
  type: 'new' | 'removed' | 'changed' | 'moved'
  changedVerbs?: VerbChange[]
  newVerbs?: VerbChange[]
  removedVerbs?: VerbChange[]
  verbsWithChangedObjects?: VerbChange[]
}

@Component({
  selector: 'app-schema-change',
  templateUrl: './schema-change.component.html',
  styleUrls: ['./schema-change.component.scss']
})
export class SchemaChangeComponent implements OnInit {
  @Input() oldSchema?: ProviderSchemaDefinition
  @Input() newSchema?: ProviderSchemaDefinition

  schemaChanges: SchemaChange[] = []

  constructor() {
  }

  ngOnInit(): void {
    this.detectChanges()
  }

  detectObjectChanges(oldVerb: XApiVerbSchema, newVerb: XApiVerbSchema): ObjectChange[] {
    const objectChanges: ObjectChange[] = []

    for (const oldObject of oldVerb.objects) {
      const matchingNewObject = newVerb.objects.find((e) => e.id === oldObject.id)
      if (matchingNewObject) {
        if (oldObject.defaultConsent && !matchingNewObject.defaultConsent)
          objectChanges.push({
            description: `Object ${matchingNewObject.label} changed from Opt Out to Opt In.`,
            type: 'changed',
            old: oldObject,
            new: matchingNewObject
          })
        if (!oldObject.defaultConsent && matchingNewObject.defaultConsent)
          objectChanges.push({
            description: `Object ${matchingNewObject.label} changed from Opt In to Opt Out.`,
            type: 'changed',
            old: oldObject,
            new: matchingNewObject
          })
      } else {
        objectChanges.push({
          description: `Object ${oldObject.label} removed.`,
          type: 'removed',
          old: oldObject
        })
      }
    }
    for (const newObject of newVerb.objects) {
      const matchingOldObject = oldVerb.objects.find((e) => e.id === newObject.id)
      if (!matchingOldObject) {
        objectChanges.push({
          description: `Object ${newObject.label} added.`,
          type: 'new',
          new: newObject
        })
      }
    }
    return objectChanges
  }

  /**
   * Compares default consent between two xAPI Verbs.
   * @param oldVerb
   * @param newVerb
   * @returns
   */
  detectVerbChange(
    oldVerb: XApiVerbSchema,
    newVerb: XApiVerbSchema
  ): VerbChange | null {
    const isEssentialVerb = oldVerb.essential
    if (
      (oldVerb.defaultConsent && newVerb.defaultConsent) ||
      (!oldVerb.defaultConsent && !newVerb.defaultConsent)
    ) {
      const objectChanges = this.detectObjectChanges(oldVerb, newVerb)
      if (objectChanges.length > 0) {
        return {
          description: $localize`:@@schemaChangeVerbObjChanges:Verb ${newVerb.label} includes object changes`,
          type: 'onlyObjectChanged',
          isEssentialVerb: isEssentialVerb,
          objectChanges: objectChanges
        }
      }
      return null
    }
    if (!oldVerb.defaultConsent && newVerb.defaultConsent)
      return {
        description: $localize`:@@schemaChangeVerbOptInToOptOut:Verb ${newVerb.label} changed from Opt In to Opt Out`,
        old: oldVerb,
        new: newVerb,
        type: 'changed',
        isEssentialVerb: isEssentialVerb,
        objectChanges: this.detectObjectChanges(oldVerb, newVerb)
      }
    if (oldVerb.defaultConsent && !newVerb.defaultConsent)
      return {
        description: $localize`:@@schemaChangeVerbOptOutToOptIn:Verb ${newVerb.label} changed from Opt Out to Opt In`,
        old: oldVerb,
        new: newVerb,
        type: 'changed',
        isEssentialVerb: isEssentialVerb,
        objectChanges: this.detectObjectChanges(oldVerb, newVerb)
      }
    return null
  }

  detectChanges(): void {
    if (!this.newSchema || !this.oldSchema) return

    const matchingOldSchema = this.oldSchema.id === this.newSchema.id ? this.oldSchema : null
    if (matchingOldSchema) {
      const newSchemaVerbs = this.newSchema.verbs
      const oldSchemaVerbs = matchingOldSchema.verbs

      for (const newSchemaVerb of newSchemaVerbs) {
        const oldVerb = oldSchemaVerbs.find((oldVerb) => oldVerb.id === newSchemaVerb.id)
        if (oldVerb) {
          const verbChange = this.detectVerbChange(oldVerb, newSchemaVerb)
          if (verbChange) this.updateSchemaChanges(matchingOldSchema, verbChange)
        } else {
          const verbChange: VerbChange = {
            description: $localize`:@@schemaChangeVerbAdded:Verb ${newSchemaVerb.label} added`,
            type: 'new',
            new: newSchemaVerb,
            isEssentialVerb: false,
            objectChanges: []
          }
          this.updateSchemaChanges(matchingOldSchema, verbChange)
        }
      }

      for (const oldVerb of oldSchemaVerbs) {
        const matchingNewVerb = newSchemaVerbs.find((newVerb) => newVerb.id === oldVerb.id)
        if (!matchingNewVerb) {
          const verbChange: VerbChange = {
            description: $localize`:@@schemaChangeVerbRemoved:Verb ${oldVerb.label} removed`,
            old: oldVerb,
            type: 'removed',
            isEssentialVerb: false,
            objectChanges: []
          }
          this.updateSchemaChanges(matchingOldSchema, verbChange)
        }
      }

      for (const newEssentialVerb of this.newSchema.essential_verbs) {
        const matchingOldVerb = matchingOldSchema.essential_verbs.find(
          (e) => e.id === newEssentialVerb.id
        )

        if (matchingOldVerb) {
          const verbChange = this.detectVerbChange(
            matchingOldVerb,
            newEssentialVerb
          )
          if (verbChange) this.updateSchemaChanges(matchingOldSchema, verbChange)
        } else {
          const verbChange: VerbChange = {
            description: $localize`:@@schemaChangeEssentialVerbAdded:Essential Verb ${newEssentialVerb.label} added`,
            type: 'new',
            new: newEssentialVerb,
            isEssentialVerb: true,
            objectChanges: []
          }
          this.updateSchemaChanges(this.newSchema, verbChange)
        }
      }

      for (const oldEssentialVerb of matchingOldSchema.essential_verbs) {
        const matchingNewVerb = this.newSchema.essential_verbs.find(
          (e) => e.id === oldEssentialVerb.id
        )

        if (!matchingNewVerb) {
          const verbChange: VerbChange = {
            description: $localize`:@@schemaChangeEssentialVerbRemoved:Essential Verb ${oldEssentialVerb.label} removed`,
            old: oldEssentialVerb,
            type: 'removed',
            isEssentialVerb: true,
            objectChanges: []
          }
          this.updateSchemaChanges(matchingOldSchema, verbChange)
        }
      }
    } else {
      // New schema added
      this.schemaChanges.push({
        id: this.newSchema.id,
        name: this.newSchema.name,
        type: 'new'
      })
      const verbChanges: VerbChange[] = [
        ...this.newSchema.verbs.map((verb) =>
           {
            const verbChange: VerbChange = {
              description: $localize`:@@schemaChangeVerbAdded:Verb ${verb.label} added`,
              type: 'new',
              new: verb,
              isEssentialVerb: false,
              objectChanges: []
            }
            return verbChange
          }
        ),
        ...this.newSchema.essential_verbs.map((verb) => {
          const verbChange: VerbChange = {
            description: $localize`:@@schemaChangeVerbAdded:Verb ${verb.label} added`,
            type: 'new',
            new: verb,
            isEssentialVerb: false,
            objectChanges: []
          }
          return verbChange
        })
      ]
      for (const verbChange of verbChanges)
        this.updateSchemaChanges(this.newSchema, verbChange)
    }
    const matchingNewSchema = this.newSchema.id === this.oldSchema.id ? this.newSchema : null
    if (!matchingNewSchema) {
      // Schema was removed
      this.schemaChanges.push({
        id: this.oldSchema.id,
        name: this.oldSchema.name,
        type: 'removed'
      })
      const verbChanges: VerbChange[] = [
        ...this.oldSchema.verbs.map((verb) => {
            const verbChange: VerbChange = {
              description: $localize`:@@schemaChangeVerbRemoved:Verb ${verb.label} removed`,
              type: 'removed',
              new: verb,
              isEssentialVerb: false,
              objectChanges: []
            }
            return verbChange
          }
        ),
        ...this.oldSchema.essential_verbs.map((verb) => {
          const verbChange: VerbChange = {
            description: $localize`:@@schemaChangeVerbRemoved:Verb ${verb.label} removed`,
            type: 'removed',
            new: verb,
            isEssentialVerb: false,
            objectChanges: []
          }
          return verbChange
        })
      ]
      for (const verbChange of verbChanges)
        this.updateSchemaChanges(this.oldSchema, verbChange)
    }

  }


  updateSchemaChanges(schema: ProviderSchemaDefinition, verbChange: VerbChange): void {
    if (!this.schemaChanges.find((e) => e.id === schema.id)) {
      switch (verbChange.type) {
        case 'new':
          this.schemaChanges.push({
            id: schema.id,
            name: schema.name,
            type: 'changed',
            newVerbs: [verbChange]
          })
          return
        case 'changed':
          this.schemaChanges.push({
            id: schema.id,
            name: schema.name,
            type: 'changed',
            changedVerbs: [verbChange]
          })
          return
        case 'removed':
          this.schemaChanges.push({
            id: schema.id,
            name: schema.name,
            type: 'changed',
            removedVerbs: [verbChange]
          })
          return
        case 'onlyObjectChanged':
          this.schemaChanges.push({
            id: schema.id,
            name: schema.name,
            type: 'changed',
            verbsWithChangedObjects: [verbChange]
          })
          return
        case 'moved':
          this.schemaChanges.push({
            id: schema.id,
            name: schema.name,
            type: 'moved',
            changedVerbs: [verbChange]
          })
          return
        default:
          throw Error(`Verb change type '${verbChange.type}' didn't match!`)
      }
    }

    this.schemaChanges = this.schemaChanges.map((schemaChange) => {
      if (schemaChange.id === schema.id) {
        if (verbChange.type === 'new')
          return {
            ...schemaChange,
            newVerbs: [...(schemaChange.newVerbs ?? []), verbChange]
          }
        if (verbChange.type === 'changed')
          return {
            ...schemaChange,
            changedVerbs: [...(schemaChange.changedVerbs ?? []), verbChange]
          }
        if (verbChange.type === 'removed')
          return {
            ...schemaChange,
            removedVerbs: [...(schemaChange.removedVerbs ?? []), verbChange]
          }
        if (verbChange.type === 'onlyObjectChanged')
          return {
            ...schemaChange,
            verbsWithChangedObjects: [
              ...(schemaChange.verbsWithChangedObjects ?? []),
              verbChange
            ]
          }
        if (verbChange.type === 'moved')
          return {
            ...schemaChange,
            changedVerbs: [...(schemaChange.changedVerbs ?? []), verbChange]
          }
      }
      return schemaChange
    })
  }

  protected readonly faWrench = faWrench
  protected readonly faPlus = faPlus
  protected readonly faTrash = faTrash
}
