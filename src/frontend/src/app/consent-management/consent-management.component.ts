import { Component, OnInit, OnDestroy } from '@angular/core'
import { ProviderId, UserConsent, UserConsentVerbs } from './consentDeclaration'
import { ApiService, Provider } from '../services/api.service'
import { DataRemovalService } from '../data-removal.service'
import { Subscription, zip } from 'rxjs'
import { extractVerbs } from './wizard/wizard.component'

import { faPauseCircle } from '@fortawesome/free-regular-svg-icons'
import { NzMessageService } from 'ng-zorro-antd/message'
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal'
import { DeleteDialog } from '../dialogs/delete-dialog/delete-dialog'

@Component({
  selector: 'app-consent-management',
  templateUrl: './consent-management.component.html',
  styleUrls: ['./consent-management.component.scss'],
})
export class ConsentManagementComponent implements OnInit, OnDestroy {
  protected readonly faPauseCircle = faPauseCircle;

  providers: Provider[] = []
  providerUserConsent: Record<ProviderId, UserConsent | null> = {}
  userConsents: UserConsentVerbs[] = []
  pauseDialogSub?: Subscription
  pausedDataRecording: boolean = false
  constructor(
    private _apiService: ApiService,
    private _removalService: DataRemovalService,
    private _messageService: NzMessageService,
    private _dialog: NzModalService,
  ) {}

  ngOnInit(): void {
    this.getProviderUserConsents()
  }

  getProviderUserConsents() {
    this._apiService.getProviders().subscribe((providers) => {
      this.providers = providers

      const userConsents$ = zip(
        providers.map((provider) => this._apiService.getUserConsent(provider.id))
      )
      userConsents$.subscribe((userConsents) => {
        this.providerUserConsent = userConsents.reduce((acc, userConsent, idx) => {
          acc[providers[idx].id] = userConsent.consent
          return acc
        }, {} as Record<ProviderId, UserConsent | null>)

        this.userConsents = providers.map((provider) => {
          const userConsent = this.providerUserConsent[provider.id]!
          return {
            providerId: provider.id,
            providerSchemaId: provider.versions[0].id,
            verbs: extractVerbs(userConsent, provider.id)
          }
        })

        this.pausedDataRecording = !!userConsents.find((e) => !!e.paused_data_recording)
      })
    })
  }

  ngOnDestroy(): void {
    this.pauseDialogSub?.unsubscribe()
  }

  scroll(selector: string) {
    document.getElementById(selector)?.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest'
    })
  }

  changePrivacySetting(userConsent: UserConsent, provider: Provider, providerIdx: number) {
    //  this.userConsents[providerIdx].verbs = extractVerbs(userConsent, provider.id)
    let consent = this.userConsents?.find(ele => ele.providerId == provider.id)
    if(consent)
      consent.verbs = extractVerbs(userConsent, provider.id)
  }

  saveChanges(): void {
    /*this._apiService.saveUserConsent(this.userConsents).subscribe((response) => {
      this._messageService.success($localize`:@@toastMessageSaved:Saved`)
      this.getProviderUserConsents()
    })*/
  }

  showPauseDialog(): void {
    const modal: NzModalRef = this._dialog.create({
      nzTitle: this.pausedDataRecording ?
        $localize`:@@pauseDataRecordingHeader:Continue Data Recording`:
        $localize`:@@continueDataRecordingHeader:Pause Data Recording`,
      nzContent: this.pausedDataRecording ?
        $localize`:@@pausedDataRecordingDescription:Would you like to continue sharing selected data with Polaris?`:
        $localize`:@@continueDataRecordingDescription:Would you like to pause sharing selected data with Polaris?`+
          " <b>" + $localize`:@@continueDataRecordingDescriptionExtended:A delete job is set up for data deletion, which is only executed after a fixed time window.` + "</b>",
      nzFooter: [
        {
          label: $localize`:@@cancel:Cancel`,
          onClick: () => modal.destroy()
        },
        {
          label: this.pausedDataRecording ? $localize`:@@continueDataRecording:Continue Data Recording` : $localize`:@@pauseDataRecording:Pause Data Recording`,
          onClick: () => {
            this._apiService.toggleDataRecording().subscribe((response) => {
              this._messageService.success($localize`:@@toastMessageChanged:Changed`)
              this.getProviderUserConsents()
            })
          },
          type: "primary"
        },
        {
          label: $localize`:@@pauseAndDeleteDataButtonText:Pause & Delete Data`,
          disabled: this.pausedDataRecording,
          onClick: () => {
            const confirmDeleteModal = this._dialog.create({
              nzContent: DeleteDialog,
              nzComponentParams: {
                headerText: $localize`:@@pauseRecordingAndDeleteData:Pause and Delete Confirmation`
              }
            });
            confirmDeleteModal.afterClose.subscribe((result) => {
              if (result) {
                this._apiService.toggleDataRecording().subscribe((response) => {
                  this._removalService
                    .create({ immediately: false, scope: {} })
                    .subscribe((response) => {
                      this._messageService.success(
                        $localize`:@@toastMessageChanged:Changed`
                      )
                      this.getProviderUserConsents()
                    })
                })
              }
            })
          }
        }
      ]
    });

  }
}
