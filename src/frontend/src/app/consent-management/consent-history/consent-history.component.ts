import { Component } from '@angular/core'
import { ApiService, ConsentHistoryGroup, GroupedConsentHistory, UserConsentStatus } from '../../services/api.service'
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal'
import { NzMessageService } from 'ng-zorro-antd/message'
import { WizardDialog, GroupConsentStatus } from '../wizard/wizard.component'


type ActiveGroupsMap = Record<number, boolean>;

@Component({
  selector: 'consent-history',
  templateUrl: 'consent-history.component.html',
  styleUrls: ['./consent-history.component.scss']
})
export class ConsentHistoryComponent {
  // icons
  protected readonly faCheckCircle = faCheckCircle

  protected consentHistory: GroupedConsentHistory[] = [];
  protected activeGroups: ActiveGroupsMap = {};
  private _apiService: ApiService
  private _dialog: NzModalService
  private _messageService: NzMessageService
  constructor(_apiService: ApiService, _dialog: NzModalService, _messageService: NzMessageService,) {
    this._apiService = _apiService
    this._dialog = _dialog
    this._messageService = _messageService
    this.updateConsentHistory()
  }

  updateConsentHistory() {
    this._apiService.getUserConsentHistory().subscribe(history => {
      this.consentHistory = history.groups
      history.groups.forEach((group: GroupedConsentHistory) => {
        this.activeGroups[group.group.id] = group.group.active
      })
    })
  }

  toggleGroupActive(group: ConsentHistoryGroup) {
    let content : string = this.activeGroups[group.id] ?
      'Soll die Pausierung der Gruppe ' + group.label + ' wirklich aufgehoben werden?' :
      'Soll die Gruppe ' + group.label + ' wirklich pausiert werden?';
    this._dialog.confirm({
      nzTitle: this.activeGroups[group.id] ? "Pausierung aufheben?" : "Gruppe pausieren?",
      nzContent: content,
      nzOnOk: () => {

        this._apiService.updateUserConsentGroupActive(group.id, this.activeGroups[group.id])
          .subscribe((result) => {
            this.updateConsentHistory();
            this._messageService.success( this.activeGroups[group.id] ? "Die Pausierung wurde aufgehoben." : "Die gewählte Erfassung wurde pausiert.")
        })
      },
      nzOnCancel: () => {
        this.activeGroups[group.id] = !this.activeGroups[group.id]; // toggle back
      }
    })
  }

  revokeGroupConsent(group: ConsentHistoryGroup) {
    this._dialog.confirm({
      nzTitle: "Einwilligung widerrufen?",
      nzContent: `Möchtest du deine Einwilligung für die Erfassung im Bündel „${group.label}“ wirklich widerrufen?<br>Dies kann <b>nicht</b> rückgängig gemacht werden.`,
      nzOnOk: () => {
        this._apiService.revokeUserConsentGroup(group.id)
          .subscribe((result) => {
            this.updateConsentHistory();
            this._messageService.success("Die Einwilligung wurde widerrufen.")
          })
      }
    })
  }


  openWizard(): void {
    let groupConsentStatus: GroupConsentStatus = {}
    this.consentHistory.forEach((group: GroupedConsentHistory) => {
      groupConsentStatus[group.group.id] = group.group.consented;
    })
    let wizardDialogRef = this._dialog.create({
      nzContent: WizardDialog,
      nzMaskClosable: false,
      nzWidth: '90vw',
      nzComponentParams: {
        consentStatus: groupConsentStatus
      }
    })

    wizardDialogRef.afterClose.subscribe((result) => {
      this.updateConsentHistory();
    })
  }

  protected readonly faTimesCircle = faTimesCircle
  protected readonly open = open
}
