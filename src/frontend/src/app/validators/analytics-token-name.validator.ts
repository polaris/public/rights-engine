import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { ApiService } from '../services/api.service'

export class AnalyticsTokenNameValidator {
    static createValidator(service: ApiService): AsyncValidatorFn {
        return (control: AbstractControl): Observable<ValidationErrors | null> => {
            return service
                .isAnalyticsTokenNameAvailable(control.value)
                .pipe(map(({ is_available }) => (is_available ? null : { invalidAsync: true })))
        }
    }
}
