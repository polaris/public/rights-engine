import { Injectable } from '@angular/core'
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError, finalize, map } from 'rxjs/operators'
import { LoadingService } from '../services/loading.service'
import { AuthService } from '../services/auth.service'
import { NzMessageService } from 'ng-zorro-antd/message'

@Injectable({
    providedIn: 'root'
})
export class HttpRequestInterceptor implements HttpInterceptor {
    constructor(
        private _loading: LoadingService,
        private _messageService: NzMessageService,
        private _authService: AuthService
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const isAuthRefresh = request.url.includes('auth/token/refresh');
        if (!isAuthRefresh) {
            this._loading.setLoading(true, request.url);
        }

        return next.handle(request).pipe(
            map<HttpEvent<any>, any>((evt: HttpEvent<any>) => {
                if (evt instanceof HttpResponse) {
                    this._loading.setLoading(false, request.url);
                }
                return evt;
            }),
            catchError((err: HttpErrorResponse) => {
                console.error('HTTP Error:', err);

                if (err.url?.includes("xapi")) {
                    return throwError(() => err); // Re-throw the error without handling
                }

                if (err.status === 0) {
                    this._messageService.error(
                        $localize`:@@toastMessageCouldConnectBackend:Couldn't connect to backend`
                    );
                } else if (err.error?.code === 'user_not_found') {
                    this._messageService.error(
                        $localize`:@@toastMessageAccountNotFound: Account not found`
                    );
                    this._authService.logout();
                } else {
                    this._messageService.error(err.error?.message ?? err.statusText);
                }

                return throwError(() => new Error(`${err.status} ${err.message}`));
            }),
            finalize(() => {
                if (!isAuthRefresh) {
                    this._loading.setLoading(false, request.url);
                }
            })
        );
    }
}
