import { AuthService } from './services/auth.service'
import { LocalStorageService } from './services/localstorage.service'
import jwt_decode from 'jwt-decode'
import { ThemeService } from './services/theme.service'

export function appInitializer(
    _authService: AuthService,
    _localStorage: LocalStorageService,
    _theme: ThemeService
) {
    return () =>
        new Promise<void>((resolve) => {
            const prefersDarkScheme = window.matchMedia('(prefers-color-scheme: dark)')
            const themePreference = _localStorage.get('themePreference')
            if (themePreference) {
                themePreference === 'dark' ? _theme.activeDarkMode() : _theme.activeLightMode
            } else if (prefersDarkScheme) _theme.activeDarkMode()

            const refreshToken = _localStorage.get('refreshToken')
            if (refreshToken) {
                const refreshTokenInfo: any = jwt_decode(refreshToken)
                const expires = refreshTokenInfo.exp * 1000
                if (expires - new Date().getTime() > 0)
                    _authService.refreshToken().subscribe((e) => resolve())
                else {
                    console.log('your refresh token has aleady expires, you need to login again')
                    resolve()
                }
            } else resolve()
        })
}
