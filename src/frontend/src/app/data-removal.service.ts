import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from '../environments/environment'

export interface DataRemovalJob {
  user_id?: number
  finished: boolean
  job_id?: number
  execute_at: string
  created_at: string
  description: string
  matched_statements: number
}

interface DeleteAllUserStatements {}

interface DeleteVerbStatements {
    verbId: string
}

interface DeleteObjectStatements {
    verbId: string
    objectId: string
}

@Injectable({
    providedIn: 'root'
})
export class DataRemovalService {
    constructor(private http: HttpClient) {}

    create(data: {
        immediately: boolean
        scope: DeleteAllUserStatements | DeleteVerbStatements | DeleteObjectStatements
    }): Observable<void> {
        return this.http.post<void>(`${environment.apiUrl}/api/v1/data-removal/create`, {
            immedialty: data.immediately,
            scope: data.scope
        })
    }

    listJobs(): Observable<DataRemovalJob[]> {
        return this.http.get<DataRemovalJob[]>(`${environment.apiUrl}/api/v1/data-removal/list`)
    }
}
