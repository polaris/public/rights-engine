import { Component, OnInit } from '@angular/core';
import { ApiService, Provider } from '../services/api.service'
import { AuthService } from '../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message'

@Component({
  selector: 'app-merge-data',
  templateUrl: './merge-data.component.html',
  styleUrls: ['./merge-data.component.scss']
})
export class MergeDataComponent implements OnInit {

  constructor(
        private _apiService: ApiService,
        private _authService: AuthService,
        private _messageService: NzMessageService,
  ) {}

  providers: Provider[] = []

  mergeForm: FormGroup = new FormGroup({
    provider: new FormControl('', Validators.required),
    tan: new FormControl('', Validators.required),
  });


  submit() {
    if(this.mergeForm.valid)
    {
      this._authService.mergeData(this.mergeForm.get('provider')!.value, this.mergeForm.get('tan')!.value).subscribe((response) => {
        if(response.status == "success")
        {
          this._messageService.success($localize`:@@toastMessageMerged:Merged`)
        }
        else
        {
          this._messageService.error($localize`:@@toastMessageMergeError:Error`)
        }
      });
    }
  }


  getProviderList(): void {
    this._apiService.getProviders().subscribe((providers) => {
      this.providers = providers;
    });
  }

  ngOnInit(): void {
    this.getProviderList();
  }

}
