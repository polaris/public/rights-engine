import { Component, OnInit } from '@angular/core'
import { DataDisclosure, DataDisclosureService } from '../data-disclosure.service'
import { environment } from '../../environments/environment'

import { faDownload, faTrash } from '@fortawesome/free-solid-svg-icons'
import { NzMessageService } from 'ng-zorro-antd/message'
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal'

@Component({
  selector: 'app-data-disclosure',
  templateUrl: './data-disclosure.component.html',
  styleUrls: ['./data-disclosure.component.scss']
})
export class DataDisclosureComponent implements OnInit {
  faDownload = faDownload
  faTrash = faTrash

  exports: DataDisclosure[] = []
  isExportInProgress: boolean = true

  constructor(
    private _service: DataDisclosureService,
    private _messageService: NzMessageService,
    public dialog: NzModalService
  ) {
  }

  displayedColumns: string[] = ['id', 'status', 'created', 'expires', 'actions']

  ngOnInit(): void {
    this.getExports()
  }

  getExports() {
    this._service.list().subscribe((response) => {
      this.exports = response

      this.updateIsExportInProgress(response)
    })
  }

  updateIsExportInProgress(dataDisclosures: DataDisclosure[]) {
    for (const dataDisclosure of dataDisclosures) {
      if (!dataDisclosure.running && !dataDisclosure.status) {
        this.isExportInProgress = true
        return
      }
    }
    this.isExportInProgress = false
  }

  requestDataDisclosure(): void {
    this._service.create().subscribe((response) => {
      this._messageService.success($localize`:@@successToastMessage:Request sent`)
      this.getExports()
    })
  }

  downloadZip(fileId: string): void {
    this._service.getFileSecret(fileId).subscribe((response) => {
      const url = `${environment.apiUrl}/api/v1/data-disclosure/files/${fileId}/${response.secret}`
      const link = document.createElement('a')
      link.setAttribute('target', '_blank')
      link.setAttribute('href', url)
      link.setAttribute('download', `example_config_polaris.json`)
      document.body.appendChild(link)
      link.click()
      link.remove()
    })
  }

  onDeleteClick(id: number): void {
    const deleteDialog: NzModalRef = this.dialog.create({
      nzTitle: $localize`:@@deleteDataDisclosure:Delete data disclosure`,
      nzContent: $localize`:@@deleteDataDisclosure:Delete data disclosure`,
      nzFooter: [
        {
          label: 'Cancel',
          onClick: () => deleteDialog.destroy()
        },
        {
          label: 'Confirm',
          onClick: () => {
            this._service.delete(id).subscribe((response) => {
              this._messageService.success(
                $localize`:@@deleteDataDisclosureSucces:Data disclosure deleted`
              ),
                (this.exports = response)
              this.updateIsExportInProgress(response)
            })
          }
        }
      ]
    })
  }
}
