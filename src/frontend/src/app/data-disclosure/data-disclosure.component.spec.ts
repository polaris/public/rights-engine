import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDisclosureComponent } from './data-disclosure.component';

describe('DataDisclosureComponent', () => {
  let component: DataDisclosureComponent;
  let fixture: ComponentFixture<DataDisclosureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataDisclosureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataDisclosureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
