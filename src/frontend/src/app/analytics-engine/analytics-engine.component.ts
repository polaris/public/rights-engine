import { Component, OnInit } from '@angular/core';
import { AnalyticsToken, AnalyticsTokenVerb, ApiService, Provider, UserConsentResponse } from '../services/api.service';
import { ProviderSchema, UserConsent, XApiVerbConsented } from '../consent-management/consentDeclaration';


interface UserConsentProviderPair
{
  userConsentResponse : UserConsentResponse,
  provider : Provider
}

interface ProviderVerbConsentPair
{
  provider : Provider,
  providerSchema : ProviderSchema,
  allVerbs : Array<XApiVerbConsentedWithProvider>
}

export interface AnalyticsTokenConsent extends AnalyticsToken{
  consentedVerbs : Array<XApiVerbConsentedWithProvider>
  notConsentedVerbs : Array<XApiVerbConsentedWithProvider>,
  active : boolean
}

export interface XApiVerbConsentedWithProvider extends XApiVerbConsented{
  providerId : number
}

@Component({
  selector: 'app-analytics-engine',
  templateUrl: './analytics-engine.component.html',
  styleUrls: ['./analytics-engine.component.scss'],
})

export class AnalyticsEngineComponent implements OnInit {

  analysisTokens : AnalyticsToken[] = []
  activeAnalyses : AnalyticsTokenConsent[] = []
  inactiveAnalyses : AnalyticsTokenConsent[] = []


  providers : Provider[] = [];
  userConsentProviderPairs : UserConsentProviderPair[] = [];
  providerVerbConsentPairs : ProviderVerbConsentPair[] = []

  constructor(private _apiService: ApiService) {
      this._apiService = _apiService

      this.reloadList = this.reloadList.bind(this)
  }

  ngOnInit(): void {
    this.loadAnalyticsTokens()
  }

  reloadList()
  {
    this.analysisTokens = []
    this.activeAnalyses = []
    this.inactiveAnalyses = []
    this.providers = [];
    this.userConsentProviderPairs = [];
    this.providerVerbConsentPairs = []

    this.loadAnalyticsTokens()
  }

  loadAnalyticsTokens()
  {
    this._apiService.getAnalyticsTokensUsers().subscribe((analyticsTokens) => {
        this.analysisTokens = analyticsTokens
      })
      .add(() => this.loadProviders())
  }

  loadProviders()
  {
    this._apiService.getProvidersUsers().subscribe((providers) => {
      this.providers = providers
      for(let provider of providers)
        this.loadConsent(provider)
    })

  }

  loadConsent(provider : Provider)
  {
    let tmp : UserConsentProviderPair;
    this._apiService.getUserConsent(provider.id).subscribe((userConsent) => {
      tmp = {userConsentResponse : userConsent, provider : provider}
      this.userConsentProviderPairs.push(tmp)
    })
    .add(() => {
      this.calculateProviderVerbConsentPair(tmp)
      this.calculateActiveAalyses()
    });
  }

  /*
    Extracts a list of consented and not consented verbs per provider
  */

  calculateProviderVerbConsentPair(pair : UserConsentProviderPair)
  {
    let provider = pair.provider;
    let consentGroups = pair.userConsentResponse.consent?.groups??[];
    let providerSchema = pair.userConsentResponse.provider_schema;

    let allVerbs :XApiVerbConsentedWithProvider[] = []

    for(let group of consentGroups)
        for(let verb of group.verbs)
          allVerbs.push({...verb, providerId : provider.id});

    this.providerVerbConsentPairs.push({provider : provider, providerSchema : providerSchema!, allVerbs : allVerbs})

  }

  calculateActiveAalyses()
  {
    let active : AnalyticsTokenConsent[] = []
    let inactive : AnalyticsTokenConsent[] = []


    for(let analysis of this.analysisTokens){
      let consentedVerbs : XApiVerbConsentedWithProvider[] = []
      let notConsentedVerbs : XApiVerbConsentedWithProvider[]= []

      const verbs = analysis.analyticTokenVerbs
      for(let verb of verbs)
      {
        const pair = this.providerVerbConsentPairs.find( ele => ele.provider.id == verb.provider)
        let found = pair?.allVerbs?.find(v => v.id == verb.verb)
        if(found?.consented)
            consentedVerbs.push(found)
        else if( found?.id && !found.consented )
            notConsentedVerbs.push(found)
      }
        if(notConsentedVerbs.length == 0)
          active.push( {...analysis, notConsentedVerbs : notConsentedVerbs, consentedVerbs : consentedVerbs, active : true})
        else
          inactive.push( {...analysis, notConsentedVerbs : notConsentedVerbs, consentedVerbs : consentedVerbs,  active : false})
      }

      this.activeAnalyses = active
      this.inactiveAnalyses = inactive
  }

}
