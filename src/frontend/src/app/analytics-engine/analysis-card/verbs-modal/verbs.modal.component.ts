import { Component, Input, OnInit } from '@angular/core'
import {NgFor, NgIf} from '@angular/common';
import {FormsModule} from '@angular/forms';
import { AnalyticsTokenConsent, XApiVerbConsentedWithProvider } from '../../analytics-engine.component';
import { AnalyticsTokenVerb, ApiService, Provider } from 'src/app/services/api.service';
import { NzModalModule, NzModalRef } from 'ng-zorro-antd/modal'
import { NzListModule } from 'ng-zorro-antd/list'
import { NzButtonModule } from 'ng-zorro-antd/button'
import { NzToolTipModule } from 'ng-zorro-antd/tooltip'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { NzMessageService } from 'ng-zorro-antd/message'
@Component({
  selector: 'verbs-modal',
  templateUrl: 'verbs-modal.component.html',
  standalone: true,
  styleUrls: ['verbs-modal.component.scss'],
  imports: [
    FormsModule,
    NgIf,
    NgFor,
    NzModalModule,
    NzListModule,
    NzButtonModule,
    NzToolTipModule,
    FontAwesomeModule
  ]})


export class VerbsModal implements OnInit {
  // icons
  faCheck = faCheck;
  faTimes = faTimes;

  @Input() providers?: Provider[];
  @Input() analysis?: AnalyticsTokenConsent;

  providerMapping : {
    provider : Provider,
    notConsentedVerbs : XApiVerbConsentedWithProvider[],
    consentedVerbs : XApiVerbConsentedWithProvider[],
    analyticTokenVerbs : AnalyticsTokenVerb []}[] = [];

  constructor(
    public dialogRef: NzModalRef<VerbsModal>,
    private _apiService: ApiService,
    private _messageService: NzMessageService
  ) {
    this._apiService = _apiService
    this._messageService = _messageService
  }

  // allocate verbs to relevant providers
  calculateProviderMapping()
  {
    if(this.providers && this.analysis)
    {
      let mapping = [];
      for (let provider of this.providers)
      {
        let consentedVerbs = []
        let notConsentedVerbs = []
        let analyticTokenVerbs = []

        for(let verb of this.analysis.consentedVerbs)
          if(verb.providerId == provider.id)
            consentedVerbs.push(verb)


        for(let verb of this.analysis.notConsentedVerbs)
          if(verb.providerId == provider.id)
            notConsentedVerbs.push(verb)


        for(let verb of this.analysis.analyticTokenVerbs)
          if(verb.provider == provider.id)
            analyticTokenVerbs.push(verb)

        if(consentedVerbs.length || notConsentedVerbs.length || analyticTokenVerbs.length)
          mapping.push({provider, notConsentedVerbs, consentedVerbs, analyticTokenVerbs})
      }
      this.providerMapping = mapping
    }
  }

  handleClose(): void {
    this.dialogRef.destroy();
  }

  doAccept()
  {
    if(this.analysis)
      this._apiService.consentVerbsForAnalyticToken(this.analysis.id)
        .subscribe((analyticsTokens) =>
        {
          this._messageService.success($localize`:@@toastMessageConsentSaved:The consent has been saved.`)
          this.dialogRef.destroy()
        })
  }

  hasNotConsentedElements()
  {
    return !!this.providerMapping.reduce( (acc, providerMap) => !!providerMap.notConsentedVerbs.length || acc, false)
  }

  hasConsentedElements()
  {
    return !!this.providerMapping.reduce( (acc, providerMap) => !!providerMap.consentedVerbs.length || acc, false)
  }

  ngOnInit() {
    this.calculateProviderMapping()
  }
}
