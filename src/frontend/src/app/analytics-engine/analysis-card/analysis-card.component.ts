import { Component, Input, OnInit} from '@angular/core';
import { VerbsModal } from './verbs-modal/verbs.modal.component';
import { AnalyticsTokenConsent } from '../analytics-engine.component';
import { Provider } from 'src/app/services/api.service';
import { environment } from '../../../environments/environment'
import { NzModalService } from 'ng-zorro-antd/modal'

@Component({
  selector: 'app-analysis-card',
  templateUrl: './analysis-card.component.html',
  styleUrls: ['./analysis-card.component.scss']})

export class AnalysisCard implements OnInit {

  @Input() analysis! : AnalyticsTokenConsent;
  @Input() reloadList! : Function;
  @Input() providers! : Provider[];

  title : string = "";
  subtitle : string = "";
  src : string = "/assets/ng2-charts_12.png"
  alt : string= ""
  description : string | null = ""

  constructor(public dialog: NzModalService) {

  }

  ngOnInit(): void {
    this.title = this.analysis.name;
    this.description = this.analysis.description;
    this.src = this.analysis.image_path? `${environment.apiUrl}`+"/api/v1/provider/analytics-tokens/"+this.analysis.id+"/image/"+this.analysis.image_path : "/assets/ng2-charts_12.png"
  }

  openDialog(): void {
    console.log("external:", this.analysis, this.providers);
    const dialogRef = this.dialog.create({
      nzContent: VerbsModal,
      nzComponentParams: {
        analysis: this.analysis,
        providers: this.providers
      }
    });

    dialogRef.afterClose.subscribe(result => {
      if(result)
        this.reloadList()
    });
  }



}
