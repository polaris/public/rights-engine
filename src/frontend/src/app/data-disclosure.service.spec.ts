import { TestBed } from '@angular/core/testing';

import { DataDisclosureService } from './data-disclosure.service';

describe('DataDisclosureService', () => {
  let service: DataDisclosureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataDisclosureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
