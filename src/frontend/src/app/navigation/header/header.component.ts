import { OverlayContainer } from '@angular/cdk/overlay'
import { Component, OnInit } from '@angular/core'
import { AuthService, Userinfo } from 'src/app/services/auth.service'
import { ThemeService } from 'src/app/services/theme.service'
import { environment } from 'src/environments/environment'

import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faDownload } from '@fortawesome/free-solid-svg-icons'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  faUser = faUser;
  faDownload = faDownload;
  faTrash = faTrash;
  faSignOutAlt = faSignOutAlt;

  loggedIn: Userinfo | null = null
  isDarkModeOn: boolean = false

  constructor(
    private _authService: AuthService,
    private _theme: ThemeService,
    private _overlay: OverlayContainer
  ) {
    this._authService.loggedIn.subscribe((x) => (this.loggedIn = x))
    this._theme.darkModeSubject.subscribe((x) => (this.isDarkModeOn = x))
  }

  ngOnInit(): void {
    if (this.isDarkModeOn) this._overlay.getContainerElement().classList.add('dark-mode')
  }

  logout(): void {
    this._authService.logout()
  }

  toggleTheme(): void {
    this._theme.toggleTheme()
    if (this.isDarkModeOn) this._overlay.getContainerElement().classList.add('dark-mode')
    else this._overlay.getContainerElement().classList.remove('dark-mode')
  }

  /**
   * Redirect user to SSO page.
   */
  handleSSOClick(): void {
    window.location.href = `${environment.apiUrl}/login`
  }

    protected readonly environment = environment
}
