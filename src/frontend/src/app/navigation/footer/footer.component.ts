import { Component, OnInit } from '@angular/core';
import { AuthService, Userinfo } from '../../services/auth.service'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  loggedIn: Userinfo | null = null
  currentYear = new Date().getFullYear();
  constructor(private _authService: AuthService) {
    this._authService.loggedIn.subscribe((x) => (this.loggedIn = x))
  }

  ngOnInit(): void {
  }

}
