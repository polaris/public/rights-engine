import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ApiService, AuthGroup, AuthPermission, AuthUser, Statistics, ApplicationTokens, AnalyticsToken } from '../services/api.service';
import { Chart, registerables } from 'chart.js';
import { catchError } from 'rxjs/operators';
import { of, forkJoin } from 'rxjs';

Chart.register(...registerables);

interface AuthGroupCheckbox {
  label: string;
  value: string;
  checked?: boolean;
}
type UserGroupCheckboxMap = Record<number, AuthGroupCheckbox[]>; // id to checkboxes
type UserPermissionChangeMap = Record<number, boolean>;

@Component({
  selector: 'app-control-center',
  templateUrl: './control-center.component.html',
  styleUrls: ['./control-center.component.scss']
})
export class ControlCenterComponent implements OnInit {
  protected users: AuthUser[] = [];
  protected auth_permissions: AuthPermission[] = [];
  protected auth_groups: AuthGroup[] = [];
  protected privacy_policy: string = "";
  protected statistics?: Statistics;
  protected selectedProvider?: ApplicationTokens;
  protected providers: ApplicationTokens[] = [];
  protected xapiStatement: string = '';

  protected responseMessage: string | null = null;
  protected responseType: 'success' | 'error' = 'success';

  protected selectedAnalysis?: AnalyticsToken;
  protected analyticsToken: AnalyticsToken[] = [];
  protected responseStatementMessage: string | null = null;
  protected responseStatementType: 'success' | 'error' = 'success';

  protected filterObject: string = '';

  expandSet = new Set<number>();
  protected user_group_checkboxes: UserGroupCheckboxMap = {};
  protected user_permission_checkboxes: UserGroupCheckboxMap = {};
  protected user_permissions_changed: UserPermissionChangeMap = {};

  constructor(
    private _messageService: NzMessageService,
    private _apiService: ApiService,
    public dialog: NzModalService
  ) {}

  ngOnInit(): void {
    // Load all data concurrently using forkJoin.
    forkJoin({
      groups: this._apiService.getGroups(),
      permissions: this._apiService.getPermissions(),
      users: this._apiService.getUsers(),
      privacy: this._apiService.getPrivacyPolicy(),
      statistics: this._apiService.getStatistics(),
      providers: this._apiService.getApplicationTokens(),
      analyticsTokens: this._apiService.getAnalyticsTokens()
    })
    .subscribe(({ groups, permissions, users, privacy, statistics, providers, analyticsTokens }) => {
      this.auth_groups = groups;
      this.auth_permissions = permissions;
      this.users = users;
      this.privacy_policy = privacy?.content || "";
      this.statistics = statistics;
      this.providers = providers;
      this.analyticsToken = analyticsTokens;
      this.renderChart();

      // Process user checkboxes
      const newGroupCheckboxes: UserGroupCheckboxMap = {};
      const newPermissionCheckboxes: UserGroupCheckboxMap = {};
      const newPermissionsChanged: UserPermissionChangeMap = {};

      users.forEach((user) => {
        newGroupCheckboxes[user.id] = this.auth_groups.map((group) => ({
          label: group.name,
          value: group.id.toString(),
          checked: user.groups.map(gr => gr.id).includes(group.id)
        }));
        newPermissionCheckboxes[user.id] = this.auth_permissions.map((permission) => ({
          label: permission.name,
          value: permission.id.toString(),
          checked: user.user_permissions.map(pr => pr.id).includes(permission.id)
        }));
        newPermissionsChanged[user.id] = false;
      });
      this.user_group_checkboxes = newGroupCheckboxes;
      this.user_permission_checkboxes = newPermissionCheckboxes;
      this.user_permissions_changed = newPermissionsChanged;
    });
  }

  renderChart() {
    // Ensure a canvas is available before rendering the chart.
    const canvas = document.querySelector('canvas');
    if (!canvas) return;
    const ctx = canvas.getContext('2d');
    if (!ctx) return;

    new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.statistics?.statement_history.map((_, i) => `Day ${i + 1}`),
        datasets: [{
          label: 'Statements Over Time',
          data: this.statistics?.statement_history,
          borderColor: 'blue',
          backgroundColor: 'rgba(0, 0, 255, 0.1)',
          fill: true,
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }

  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.clear();
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }

  saveUserPermissions(user_id: number): void {
    this._apiService.saveUserPermissions(
      user_id,
      this.user_group_checkboxes[user_id].filter(data => data.checked).map(data => parseInt(data.value)),
      this.user_permission_checkboxes[user_id].filter(data => data.checked).map(data => parseInt(data.value)),
    ).subscribe(() => {
      this._messageService.success("Benutzerrechte und Gruppen gespeichert.");
      this.user_permissions_changed[user_id] = false;
    });
  }

  updatePrivacyPolicy(content: string): void {
    this.privacy_policy = content;
  }

  savePrivacyPolicy(): void {
    this._apiService.setPrivacyPolicy(this.privacy_policy).subscribe(result => {
      this.privacy_policy = result?.content;
      this._messageService.success("Datenschutzerklärung gespeichert.");
    });
  }

  sendStatement() {
    console.log('Sending xAPI Statement:', this.xapiStatement, 'to', this.selectedProvider);
    this.responseMessage = `Error:`;
    this._apiService.sendStatement(JSON.parse(this.xapiStatement), this.selectedProvider?.keys[0] ?? '')
      .pipe(
        catchError(err => {
          this.responseMessage = `Error: ${JSON.stringify(err.error, null, 2)}`;
          this.responseType = 'error';
          return of(null);
        })
      )
      .subscribe(result => {
        if (result) {
          this.responseMessage = JSON.stringify(result, null, 2);
          this.responseType = 'success';
        }
      });
  }

  generateStatement() {
    this.xapiStatement = JSON.stringify([
      {
        actor: { name: 'User', mbox: 'mailto:user1@polaris.com' },
        verb: { id: 'https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/accepted', display: { en: 'completed' } },
        object: {
          id: 'https://example.adlnet.gov/AUidentifier',
          objectType: 'Activity',
          definition: { name: { "en-US": 'Example Course' } }
        }
      },
      {
        actor: { name: 'User', mbox: 'mailto:user1@polaris.com' },
        verb: { id: 'https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/accepted', display: { en: 'completed' } },
        object: {
          id: 'https://example.adlnet.gov/AUidentifier',
          objectType: 'Activity',
          definition: { name: { "en-US": 'Advanced Course' } }
        }
      }
    ], null, 2);
  }

  sendFilter() {
    console.log('Sending xAPI Filter:', this.filterObject, 'to', this.selectedAnalysis);

    let parsedFilterObject = {};

    if (this.filterObject && this.filterObject.trim()) {
      try {
        parsedFilterObject = JSON.parse(this.filterObject);
      } catch (error) {
        this.responseStatementMessage = 'Error: Invalid JSON format for filter';
        this.responseStatementType = 'error';
        return;
      }
    }

    this._apiService.getStatement(parsedFilterObject, this.selectedAnalysis?.key ?? '', 10)
      .pipe(
        catchError(err => {
          this.responseStatementMessage = `Error: ${JSON.stringify(err.error, null, 2)}`;
          this.responseStatementType = 'error';
          return of(null);
        })
      )
      .subscribe(result => {
        if (result) {
          this.responseStatementMessage = JSON.stringify(result, null, 2);
          this.responseStatementType = 'success';
        }
      });
  }
}
