import { Component } from '@angular/core'
import { NzMessageService } from 'ng-zorro-antd/message'
import { NzModalService } from 'ng-zorro-antd/modal'
import { ApiService, AuthGroup, AuthPermission, AuthUser, Statistics, ApplicationTokens, AnalyticsToken } from '../../services/api.service'
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-result-tools',
  templateUrl: './result-tools.component.html',
  styleUrls: ['./result-tools.component.scss']
})
export class ResultToolsComponent {

    protected selectedAnalysis?: AnalyticsToken;
    protected analyticsToken: AnalyticsToken[] = [];
    protected responseMessage: string | null = null;
    protected responseType: 'success' | 'error' = 'success';
  
    protected resultObject: string = '';

    protected filterResultObject : string = '';
    protected responseResultMessage: string | null = null;
    protected responseResultType: 'success' | 'error' = 'success';
  


      constructor(
        private _messageService: NzMessageService,
        private _apiService: ApiService,
        public dialog: NzModalService
      ) {

        this._apiService.getAnalyticsTokens().subscribe(provider => {
            this.analyticsToken = provider;
          })
      }

      importResults() {
        console.log('Sending xAPI Statement:', this.resultObject, 'to', this.selectedAnalysis);
      
        let parsedResultObject;
        
        try {
          parsedResultObject = typeof this.resultObject === 'string' 
            ? JSON.parse(this.resultObject) 
            : this.resultObject;
        } catch (error) {
          this.responseMessage = 'Error: Invalid JSON format in resultObject';
          this.responseType = 'error';
          return;
        }
      
        if (!parsedResultObject) {
          this.responseMessage = 'Error: resultObject is null or undefined';
          this.responseType = 'error';
          return;
        }
      
        this._apiService.storeResult(parsedResultObject, this.selectedAnalysis?.key ?? '')
          .pipe(
            catchError(err => {
              this.responseMessage = `Error: ${err.error}`;
              this.responseType = 'error';
              return of(null);
            })
          )
          .subscribe(result => {
            if (result) {
              this.responseMessage = JSON.stringify(result, null, 2);
              this.responseType = 'success';
            }
          });
      }
      

      generateExample() {
        this.resultObject = JSON.stringify({
          result: {
            score: Math.floor(Math.random() * 100),
            completion: Math.random() > 0.5,
            success: Math.random() > 0.5,
            duration: `${Math.floor(Math.random() * 3600)}s`,
            response: "Example response text",
          },
          context_id: `context-${Math.random().toString(36).substr(2, 9)}`
        }, null,2);
      }

      getResults() {
        let parsedFilterObject = {};
        
        if (this.filterResultObject && this.filterResultObject.trim()) {
          try {
            parsedFilterObject = JSON.parse(this.filterResultObject);
          } catch (error) {
            this.responseResultMessage = 'Error: Invalid JSON format in filterResultObject';
            this.responseResultType = 'error';
            return;
          }
        }
      
        this._apiService.getResult(parsedFilterObject, this.selectedAnalysis?.key ?? '')
          .pipe(
            catchError(err => {
              this.responseResultMessage = `Error: ${err}`;
              this.responseResultType = 'error';
              return of(null);
            })
          )
          .subscribe(result => {
            if (result) {
              this.responseResultMessage = JSON.stringify(result, null, 2);
              this.responseResultType = 'success';
            }
          });
      }

      generateExampleFilter() {
        const filters = {
            page: Math.floor(Math.random() * 5) + 1, // Random page between 1 and 5
            page_size: 10, // Fixed page size
            context_id: `context-${Math.random().toString(36).substr(2, 9)}`,
            created_at: new Date().toISOString().split('T')[0], // Today's date in YYYY-MM-DD format
            "result.score": Math.floor(Math.random() * 100),
          };
          
          this.filterResultObject = JSON.stringify(filters, null, 2);
      }
}