import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultToolsComponent } from './result-tools.component';

describe('ResultToolsComponent', () => {
  let component: ResultToolsComponent;
  let fixture: ComponentFixture<ResultToolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultToolsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
