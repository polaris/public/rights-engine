import { Injectable } from '@angular/core'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { map, Observable } from 'rxjs'
import { AuthService } from '../services/auth.service'
import { NzMessageService } from 'ng-zorro-antd/message'

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private _router: Router,
        private _authService: AuthService,
        private _messageService: NzMessageService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this._authService.loggedIn$.pipe(
            map((loggedIn) => {
                const isLoggedIn = !!loggedIn
                if (!isLoggedIn) {
                    this._messageService.warning($localize`:@@toastMessageUnauthorized:Unauthorized`)
                    this._router.navigateByUrl('/login')
                }
                return isLoggedIn
            })
        )
    }
}
