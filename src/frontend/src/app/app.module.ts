import { NgModule, APP_INITIALIZER } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { LayoutModule } from '@angular/cdk/layout'
import { HeaderComponent } from './navigation/header/header.component'
import {
  ConsentManagementComponent
} from './consent-management/consent-management.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { LoginPageComponent } from './login-page/login-page.component'
import { WizardDialog } from './consent-management/wizard/wizard.component'
import { FooterComponent } from './navigation/footer/footer.component'
import { LegalNoticeComponent } from './legal-notice/legal-notice.component'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import {
  PrivacySettingComponent
} from './consent-management/provider-settings/provider-setting.component'
import { HttpRequestInterceptor } from './interceptors/http-request-interceptor'
import { JwtInterceptor } from './interceptors/jwt.interceptor'
import { HomeComponent } from './home/home.component'
import { appInitializer } from './app.initializer'
import { AuthService } from './services/auth.service'
import { LocalStorageService } from './services/localstorage.service'
import { ThemeService } from './services/theme.service'
import { ProviderComponent } from './consent-management/provider/provider.component'
import { UserProfilComponent } from './user-profil/user-profil.component'
import { DataDisclosureComponent } from './data-disclosure/data-disclosure.component'
import { DataRemovalComponent } from './data-removal/data-removal.component'
import { DeleteDialog } from './dialogs/delete-dialog/delete-dialog'
import { ApplicationTokensComponent } from './consent-management/application-tokens/application-tokens.component'
import { AnalyticsTokensComponent } from './analytics-tokens/analytics-tokens.component'
import { SchemaChangeComponent } from './consent-management/schema-change/schema-change.component'
import { ObjectChangesComponent } from './consent-management/schema-change/object-changes/object-changes.component'
import { CreateTokenDialog } from './dialogs/create-token-dialog/create-token-dialog'
import { OrderByPipe } from './order-by.pipe'
import { MergeDataComponent } from './merge-data/merge-data.component'
import { AnalyticsEngineComponent } from './analytics-engine/analytics-engine.component'
import { AnalysisCard } from './analytics-engine/analysis-card/analysis-card.component'
import { NZ_I18N } from 'ng-zorro-antd/i18n'
import { de_DE } from 'ng-zorro-antd/i18n'
import { NgOptimizedImage, registerLocaleData } from '@angular/common'
import de from '@angular/common/locales/de'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NzTabsModule } from 'ng-zorro-antd/tabs'
import { NzGridModule } from 'ng-zorro-antd/grid'
import { NzCardModule } from 'ng-zorro-antd/card'
import { NzButtonModule } from 'ng-zorro-antd/button'
import { NzModalModule, NzModalService } from 'ng-zorro-antd/modal'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { NzLayoutModule } from 'ng-zorro-antd/layout'
import { NzMenuModule } from 'ng-zorro-antd/menu'
import { NzAffixModule } from 'ng-zorro-antd/affix'
import { NzSwitchModule } from 'ng-zorro-antd/switch'
import { NzDropDownModule } from 'ng-zorro-antd/dropdown'
import { NzCollapseModule } from 'ng-zorro-antd/collapse'
import { NzListModule } from 'ng-zorro-antd/list'
import { NzToolTipModule } from 'ng-zorro-antd/tooltip'
import { NzSelectModule } from 'ng-zorro-antd/select'
import { NzInputModule } from 'ng-zorro-antd/input'
import { NzSpaceModule } from 'ng-zorro-antd/space'
import { NzFormModule } from 'ng-zorro-antd/form'
import { NzBadgeModule } from 'ng-zorro-antd/badge'
import { NzTagModule } from 'ng-zorro-antd/tag'
import { NzTableModule } from 'ng-zorro-antd/table'
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox'
import { NzProgressModule } from 'ng-zorro-antd/progress'
import { NzAlertModule } from 'ng-zorro-antd/alert'
import { NzMessageService } from 'ng-zorro-antd/message'
import { NzSpinModule } from 'ng-zorro-antd/spin'
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker'
import { ConsentHistoryComponent } from './consent-management/consent-history/consent-history.component'
import { ControlCenterComponent } from './control-center/control-center.component'
import { VerbGroupsComponent } from './consent-management/verb-groups/verb-groups.component'
import { MarkdownModule, MarkdownService } from 'ngx-markdown'
import { CreateVerbGroupDialog } from './dialogs/create-verb-group-dialog/create-verb-group-dialog'
import { NzRadioModule } from 'ng-zorro-antd/radio'
import { PrivacyPolicyDialog } from './dialogs/privacy-policy-dialog/privacy-policy-dialog'
import { MarkdownEditorComponent } from './helper-components/markdown-editor/markdown-editor.component'
import { ResultToolsComponent } from './control-center/result-tools/result-tools.component'

registerLocaleData(de)

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ConsentManagementComponent,
    ConsentHistoryComponent,
    ControlCenterComponent,
    PageNotFoundComponent,
    LoginPageComponent,
    WizardDialog,
    FooterComponent,
    LegalNoticeComponent,
    PrivacySettingComponent,
    HomeComponent,
    ProviderComponent,
    UserProfilComponent,
    DataDisclosureComponent,
    DataRemovalComponent,
    DeleteDialog,
    ApplicationTokensComponent,
    AnalyticsTokensComponent,
    SchemaChangeComponent,
    ObjectChangesComponent,
    CreateTokenDialog,
    CreateVerbGroupDialog,
    OrderByPipe,
    MergeDataComponent,
    AnalysisCard,
    AnalyticsEngineComponent,
    VerbGroupsComponent,
    PrivacyPolicyDialog,
    MarkdownEditorComponent,
    ResultToolsComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        NzTabsModule,
        NzGridModule,
        NzCardModule,
        NzButtonModule,
        FontAwesomeModule,
        NzLayoutModule,
        NgOptimizedImage,
        NzMenuModule,
        NzAffixModule,
        NzSwitchModule,
        NzDropDownModule,
        NzCollapseModule,
        NzListModule,
        NzToolTipModule,
        NzSelectModule,
        NzInputModule,
        NzSpaceModule,
        NzFormModule,
        NzBadgeModule,
        NzTagModule,
        NzTableModule,
        NzCheckboxModule,
        NzProgressModule,
        NzAlertModule,
        NzModalModule,
        NzSpinModule,
        NzDatePickerModule,
        MarkdownModule.forRoot(),
        NzRadioModule,
        NzStatisticModule,
        NzIconModule
    ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [AuthService, LocalStorageService, ThemeService]
    },
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: NZ_I18N, useValue: de_DE },
    NzModalService,
    NzMessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
