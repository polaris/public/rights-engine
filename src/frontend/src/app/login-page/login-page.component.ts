import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms'
import { environment } from '../../environments/environment'
import { AuthService } from '../services/auth.service'
import { Router } from '@angular/router'
import { faEye, faEyeSlash, faUser } from '@fortawesome/free-solid-svg-icons'
import { NzMessageService } from 'ng-zorro-antd/message'

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
    hide = true
    email = ''
    password = ''
    form: FormGroup

    constructor(
        private _formBuilder: FormBuilder,
        private _authService: AuthService,
        private _messageService: NzMessageService,
        private _router: Router
    ) {
        this.form = this._formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        })
    }

    get emailControl(): FormControl {
        return this.form.get('email') as FormControl
    }

    get passwordControl(): FormControl {
        return this.form.get('password') as FormControl
    }

    ngOnInit(): void {
        this.form.get('email')?.valueChanges.subscribe((val) => {
            this.email = val
        })

        this.form.get('password')?.valueChanges.subscribe((val) => {
            this.password = val
        })
    }

    onSubmit(): void {
        this._authService.login(this.email, this.password).subscribe((response) => {
            this._messageService.success($localize`:@@toastMessageSuccessfulLogin:Successful Login`)
            this._router.navigateByUrl('/')
        })
    }

    /**
     * Redirect user to SSO page.
     */
    handleSSOClick(): void {
        window.location.href = `${environment.apiUrl}/login`
    }

  protected readonly faUser = faUser
  protected readonly faEye = faEye
  protected readonly faEyeSlash = faEyeSlash
}
