import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from '../environments/environment'

export interface DataDisclosure {
    id: number
    running: boolean
    status: 'success' | 'failed'
    filename?: string
    user_id: number
    expires: string
    created: string
}

@Injectable({
    providedIn: 'root'
})
export class DataDisclosureService {
    constructor(private http: HttpClient) {}

    create(): Observable<DataDisclosure> {
        return this.http.post<DataDisclosure>(
            `${environment.apiUrl}/api/v1/data-disclosure/create`,
            {}
        )
    }

    list(): Observable<DataDisclosure[]> {
        return this.http.get<DataDisclosure[]>(`${environment.apiUrl}/api/v1/data-disclosure/list`)
    }

    delete(id: number): Observable<DataDisclosure[]> {
        return this.http.delete<DataDisclosure[]>(
            `${environment.apiUrl}/api/v1/data-disclosure/${id}/delete`
        )
    }

    getFileSecret(fileId: string): Observable<{ secret: string }> {
        return this.http.get<{ secret: string }>(
            `${environment.apiUrl}/api/v1/data-disclosure/file_secret/${fileId}`
        )
    }
}
