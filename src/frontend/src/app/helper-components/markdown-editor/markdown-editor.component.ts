import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core'
import { faBold, faItalic, faListOl, faListUl, faStrikethrough, faTable } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-markdown-editor',
  templateUrl: './markdown-editor.component.html',
  styleUrls: ['./markdown-editor.component.scss']
})
export class MarkdownEditorComponent {
  @Input() markdownContent: string = '';
  @Output() markdownChange: EventEmitter<string> = new EventEmitter();

  @ViewChild('markdownTextarea', { static: false }) markdownTextarea!: ElementRef<HTMLTextAreaElement>;

  /**
   * Inserts the given strings around the current selection.
   * @param before - Text to insert before the selection.
   * @param after - Text to insert after the selection.
   * @param placeholder - Default text if nothing is selected.
   */
  private insertAtCursor(before: string, after: string = '', placeholder: string = ''): void {
    const textarea = this.markdownTextarea.nativeElement;
    const start = textarea.selectionStart;
    const end = textarea.selectionEnd;
    const selectedText = textarea.value.substring(start, end) || placeholder;
    const newText = before + selectedText + after;
    this.markdownContent =
      textarea.value.substring(0, start) + newText + textarea.value.substring(end);

    setTimeout(() => {
      textarea.focus();
      textarea.setSelectionRange(start + newText.length, start + newText.length);
    }, 0);
  }

  onBold(): void {
    this.insertAtCursor('**', '**', 'fettgedruckt');
  }

  onItalic(): void {
    this.insertAtCursor('*', '*', 'schräg gedruckt');
  }

  onStrikethrough(): void {
    this.insertAtCursor('~~', '~~', 'durchgestrichen');
  }

  onHeader(level: number): void {
    const headerPrefix = '#'.repeat(level) + ' ';
    const textarea = this.markdownTextarea.nativeElement;
    const start = textarea.selectionStart;
    // Find beginning of the current line
    const valueBeforeCursor = textarea.value.substring(0, start);
    const lineStart = valueBeforeCursor.lastIndexOf('\n') + 1;
    this.markdownContent =
      textarea.value.substring(0, lineStart) + headerPrefix + textarea.value.substring(lineStart);
    setTimeout(() => {
      textarea.focus();
      textarea.setSelectionRange(start + headerPrefix.length, start + headerPrefix.length);
    }, 0);
  }

  onOrderedList(): void {
    const textarea = this.markdownTextarea.nativeElement;
    const start = textarea.selectionStart;
    const valueBeforeCursor = textarea.value.substring(0, start);
    const lineStart = valueBeforeCursor.lastIndexOf('\n') + 1;
    this.markdownContent =
      textarea.value.substring(0, lineStart) + '1. ' + textarea.value.substring(lineStart);
    setTimeout(() => {
      textarea.focus();
      textarea.setSelectionRange(start + 3, start + 3);
    }, 0);
  }

  onUnorderedList(): void {
    const textarea = this.markdownTextarea.nativeElement;
    const start = textarea.selectionStart;
    const valueBeforeCursor = textarea.value.substring(0, start);
    const lineStart = valueBeforeCursor.lastIndexOf('\n') + 1;
    this.markdownContent =
      textarea.value.substring(0, lineStart) + '- ' + textarea.value.substring(lineStart);
    setTimeout(() => {
      textarea.focus();
      textarea.setSelectionRange(start + 2, start + 2);
    }, 0);
  }

  onInsertTable(): void {
    const tableSnippet = `
| Überschrift 1 | Überschrift 2 |
| ------------- | ------------- |
| Inhalt 1 | Inhalt 2 |
`;
    const textarea = this.markdownTextarea.nativeElement;
    const start = textarea.selectionStart;
    this.markdownContent =
      textarea.value.substring(0, start) +
      tableSnippet +
      textarea.value.substring(textarea.selectionEnd);
    setTimeout(() => {
      textarea.focus();
      const newPos = start + tableSnippet.length;
      textarea.setSelectionRange(newPos, newPos);
    }, 0);
  }

  protected readonly faBold = faBold
  protected readonly faItalic = faItalic
  protected readonly faStrikethrough = faStrikethrough
  protected readonly faListOl = faListOl
  protected readonly faListUl = faListUl
  protected readonly faTable = faTable
}
