import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core'
import { LoadingService } from './services/loading.service'
import { delay, Subscription } from 'rxjs'
import { ThemeService } from './services/theme.service'
import { AuthService, Userinfo } from './services/auth.service'
import { WizardDialog } from './consent-management/wizard/wizard.component'
import { ApiService, UserConsentStatus } from './services/api.service'
import { Router } from '@angular/router'
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal'
import { PrivacyPolicyDialog } from './dialogs/privacy-policy-dialog/privacy-policy-dialog'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  @HostBinding('class') className = ''
  title = 'frontend'
  loading = false
  isDarkModeOn: boolean = false
  loggedIn: Userinfo | null = null
  privacyPolicyDialogCloseSubscription?: Subscription
  privacyPolicyDialogRef: NzModalRef<PrivacyPolicyDialog> | null = null

  constructor(
    private _loading: LoadingService,
    private _theme: ThemeService,
    private _authService: AuthService,
    private _dialog: NzModalService,
    private _apiService: ApiService,
    private _router: Router
  ) {
    this._theme.darkModeSubject.subscribe((x) => (this.isDarkModeOn = x))
  }

  ngOnInit() {
    this.listenToLoading()
    this._authService.loggedIn.subscribe((userInfo) => {
      if (userInfo && !userInfo.isProvider) {
        this._apiService.getPrivacyPolicyStatus().subscribe(result => {
            if(!result?.status)
            {
              this._apiService.getPrivacyPolicy().subscribe(result => {
                this.openPrivacyPolicy(result?.content);
              })
            }
        })
      }

      this.loggedIn = userInfo
    })

  }

  ngOnDestroy(): void {
    this.privacyPolicyDialogCloseSubscription?.unsubscribe()
  }

  listenToLoading(): void {
    this._loading.loadingSub
      .pipe(delay(0)) // This prevents a ExpressionChangedAfterItHasBeenCheckedError for subsequent requests
      .subscribe((loading) => {
        this.loading = loading
      })
  }

  openPrivacyPolicy(content: string): void {
    if (!this.privacyPolicyDialogRef && this.loggedIn != null) {
      this.privacyPolicyDialogRef = this._dialog.create({
        nzContent: PrivacyPolicyDialog,
        nzMaskClosable: false,
        nzClosable: false,
        nzWidth: '90vw',
        nzComponentParams: {
          headerText: "Datenschutzerklärung",
          text: content
        }
      })
      this.privacyPolicyDialogCloseSubscription = this.privacyPolicyDialogRef.afterClose.subscribe((result) => {
        if(result?.accepted)
        {
          console.log(this.loggedIn)
          this._apiService.acceptPrivacyPolicy().subscribe(() => {
            this._router
              .navigateByUrl('/', { skipLocationChange: true })
              .then(() => this._router.navigate(['/consent-history']))
          })
        }
      })
    }
  }
}
