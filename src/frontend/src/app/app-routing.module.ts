import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ConsentManagementComponent } from './consent-management/consent-management.component'
import { MergeDataComponent } from './merge-data/merge-data.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { LoginPageComponent } from './login-page/login-page.component'
import { LegalNoticeComponent } from './legal-notice/legal-notice.component'
import { AuthGuard } from './guards/auth.guard'
import { HomeComponent } from './home/home.component'
import { ProviderComponent } from './consent-management/provider/provider.component'
import { UserProfilComponent } from './user-profil/user-profil.component'
import { DataDisclosureComponent } from './data-disclosure/data-disclosure.component'
import { DataRemovalComponent } from './data-removal/data-removal.component'
import { ApplicationTokensComponent } from './consent-management/application-tokens/application-tokens.component'
import { AnalyticsTokensComponent } from './analytics-tokens/analytics-tokens.component'
import { AnalyticsEngineComponent } from './analytics-engine/analytics-engine.component'
import { ConsentHistoryComponent } from './consent-management/consent-history/consent-history.component'
import { ControlCenterComponent } from './control-center/control-center.component'

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'consent-management', component: ConsentManagementComponent, canActivate: [AuthGuard] },
  { path: 'consent-history', component: ConsentHistoryComponent, canActivate: [AuthGuard] },
  { path: 'control-center', component: ControlCenterComponent, canActivate: [AuthGuard]},
  { path: 'merge-actors', component: MergeDataComponent, canActivate: [AuthGuard] },
  { path: 'analytics-engines', component: AnalyticsEngineComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginPageComponent },
  { path: 'provider', component: ProviderComponent, canActivate: [AuthGuard] },
  { path: 'application-tokens', component: ApplicationTokensComponent, canActivate: [AuthGuard] },
  { path: 'analytics-tokens', component: AnalyticsTokensComponent, canActivate: [AuthGuard] },
  { path: 'profil', component: UserProfilComponent, canActivate: [AuthGuard] },
  { path: 'data-disclosure', component: DataDisclosureComponent, canActivate: [AuthGuard] },
  { path: 'data-removal', component: DataRemovalComponent, canActivate: [AuthGuard] },
  { path: 'legal-notice', component: LegalNoticeComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
