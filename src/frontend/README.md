# Polaris Frontend

## Getting started
If angular cli is not already installed, you can install it with the following command

```console
npm install -g @angular/cli@latest
```

Then, please install all package dependencies 
```console
$ npm install
```

## Development server
To start the development server 
```console
$ ng serve
```

```console
$ ng serve --port 8002 --host 0.0.0.0 --disable-host-check 
```

### Enviroment settings

Environment settings are managed in `/src/environments`

For development, the variable `apiUrl` in `/src/environments/enviroment.ts` should be updated.

## i18n

```console
$ ng extract-i18n --output-path src/locale 
$ cp src/locale/messages.xlf src/locale/messages.de.xlf
```

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.
