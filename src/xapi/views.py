import hashlib
import json
import os
from venv import logger
import datetime
from dateutil import parser
from datetime import timedelta

import requests
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import JsonResponse
from django.shortcuts import render
from jsonschema import ValidationError, validate
from jsonschema.validators import validator_for
from rest_framework import status
from rest_framework.views import APIView
from prometheus_client import Counter

from backend.utils import lrs_db
from consents.models import UserConsents
from providers.models import Provider, ProviderAuthorization, ProviderSchema, Verb, VerbObject, ProviderVerbGroup
from users.models import CustomUser
import redis
import zeep
from pathlib import Path

from .tasks import retry_forward_statements

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))

# Prometheus Counters
STATEMENTS_PROCESSED = Counter(
    "xapi_statements_processed_total", "Total number of xAPI statements processed"
)
STATEMENTS_ACCEPTED = Counter(
    "xapi_statements_accepted_total", "Total number of xAPI statements accepted"
)
STATEMENTS_REJECTED = Counter(
    "xapi_statements_rejected_total", "Total number of xAPI statements rejected"
)

with open(os.path.join(PROJECT_PATH, "static/xapi_statement.schema.json")) as f:
    schema = json.loads(f.read())
    cls = validator_for(schema)
    cls.check_schema(schema)
    instance = cls(schema)


def store_in_db(x_api_statement):
    collection = lrs_db["statements"]
    try:
        x_api_statement["stored"] = datetime.datetime.now().isoformat()  # append "stored"-field - see https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Data.md#248-stored
        result = collection.insert_one(x_api_statement)
        return str(result.inserted_id)
    except Exception as e:
        print("Failed to store xAPI statement in DB. ", e)
        return None


def anonymize_statement(x_api_statement):
    mbox = dict(
        enumerate(x_api_statement.get("actor", {}).get("mbox", "").split("mailto:"))
    ).get(1)
    account_email = (
        x_api_statement.get("actor", {}).get("account", {}).get("name", None)
    )
    email = mbox if mbox else account_email

    hashed_actor = hashlib.sha3_512(email.encode('utf-8')).hexdigest()

    x_api_statement.set("actor", {"name": "anonymous", "mbox":  settings.ANON_HASH_PREFIX + ": " + hashed_actor})


def shib_connector_resolver_to_pairwaise_id(email, provider):
    if settings.SHIB_ID_CONNECTOR_ENABLED:
        r = None
        if settings.SHIB_ID_CONNECTOR_CACHE:
            r = redis.from_url(settings.CELERY_BROKER_URL)
        user_id = ""
        lrs_type = ""

        if settings.SHIB_ID_CONNECTOR_USE_FILE_MAPPING:
            BASE_DIR = Path(__file__).resolve().parent.parent
            mapping = json.load(open(os.path.join(BASE_DIR,'static/shibboleth_mapping.json')))
            if provider.name in mapping:
                lrs_type = mapping[provider.name]
                user_id = email
        else:
            user_id, lrs_type = email.split("@")

        if settings.SHIB_ID_CONNECTOR_CACHE and r.exists(email):
            shib_id = str(r.get(email), encoding='utf-8')
        else:
            client = zeep.Client(wsdl=settings.SHIB_ID_CONNECTOR_URL)
            app_secret = settings.SHIB_ID_CONNECTOR_APP_SECRET
            processId = settings.SHIB_ID_CONNECTOR_PROCESS_ID
            linkType = settings.SHIB_ID_CONNECTOR_LINK_TYPE
            if settings.DEBUG:
                print("Try to connect with app_secret={0}, processId={1}, linkType={2}, givenType={3}, user_id={4}".format(app_secret,processId, linkType, lrs_type, user_id))
            additionalData = None
            shib_id = client.service.GetOrGenerateIdAndConnect(app_secret, user_id, lrs_type, linkType, processId, additionalData)
            if len(shib_id) != 1:
                print("Multiple pairwise ids found, only use the first one!")
            shib_id = shib_id[0]
            if settings.DEBUG:
                print("Result shib_id: {0}".format(shib_id))
            if settings.SHIB_ID_CONNECTOR_CACHE:
                r.set(email, shib_id)
                r.expire(email, settings.SHIB_ID_CONNECTOR_CACHE_MAX_AGE*60)
        if shib_id is not None:
            return shib_id
    return ""

def shib_connector_resolver(email, provider):
    if settings.SHIB_ID_CONNECTOR_ENABLED:
        shib_id = shib_connector_resolver_to_pairwaise_id(email=email, provider=provider)
        # Shib-ID is the pairwise id, so we need to resolve to the user email
        try:
            user = CustomUser.objects.get(shibboleth_connector_identifier=shib_id)
        except ObjectDoesNotExist:
            return "not_found"
        email = user.email
        print("resolved to email: {0}".format(email))
    
    return email

def process_statement(x_api_statement, provider, latest_schema):
    """
    Process xAPI statement by checking for validation errors and user consent settings.
    """
    try:
        instance.validate(x_api_statement)
    except ValidationError as e:
        return {"valid": False, "accepted": False, "reason": e.message}

    if settings.DISABLE_XAPI_CONSENT:
        return {"valid": True, "accepted": True}

    # system statements are directly stored without checking for consent
    if x_api_statement.get("actor", {}).get("mbox", "").startswith("system:"):
        if int((x_api_statement.get("actor", {}).get("mbox", "").split("system:"))[1]) == provider.id:
            return {"valid": True, "accepted": True}
        else:
            return {"valid": False, "accepted": False, "reason": "Wrong provider ID"}

    mbox = x_api_statement.get("actor", {}).get("mbox", "")
    if mbox.startswith("mailto:"):
        mbox = dict(enumerate(mbox.split("mailto:"))).get(1)

    if mbox.startswith("shib-connector:"):
        mbox = dict(enumerate(mbox.split("shib-connector:"))).get(1)

    account_email = (
        x_api_statement.get("actor", {}).get("account", {}).get("name", None)
    )

    if mbox is None and account_email is None:
        return {
            "valid": False,
            "accepted": False,
            "reason": "User missing in statement",
        }
    email = mbox if mbox else account_email
    try:
        email = shib_connector_resolver(email=email,provider=provider)
    except Exception as e:
        print(e)
        raise RuntimeError("Error when resolving shib_id: " + str(e))

    if settings.SHIB_ID_CONNECTOR_ENABLED:
        x_api_statement["actor"]["mbox"] = "mailto:" + email
        
    if (not "verb" in x_api_statement) or (not "id" in x_api_statement["verb"]):
        return {"valid": False, "accepted": False, "reason": "No verb given"}
    verb_id = x_api_statement["verb"]["id"]

    if (not "object" in x_api_statement) or (not "id" in x_api_statement["object"]):
        return {"valid": False, "accepted": False, "reason": "No object given"}

    # find user by email
    try:
        user = CustomUser.objects.get(email=email)
    except ObjectDoesNotExist:
        return {"valid": False, "accepted": False, "message": f"User not found ({email})"}

    x_api_statement["actor"]["mbox"] = "mailto:" + str(user.id) + "-polaris-id@polaris.com"

    # find verb
    verb_candidates = Verb.objects.filter(verb_id=verb_id, provider=provider, provider_schema=latest_schema)
    if not verb_candidates:
        return {"valid": False, "accepted": False, "message": "Verb not found"}

    # essential verbs do not require consent
    if not verb_id in [verb.verb_id for verb in latest_schema.essential_verbs()]:

        anon_verbs = [verb.verb_id for verblist in [group.verbs.all() for group in latest_schema.groups()] for verb in verblist if verb.allow_anonymized_collection]

        # has the user paused data collection altogether?
        if user.paused_data_recording:
            return {
                "valid": True,
                "accepted": False,
                "reason": "User has paused data collection",
            }

        # validate timestamp, if given
        if "timestamp" in x_api_statement.keys():
            timestamp = parser.parse(x_api_statement["timestamp"])
        else:
            timestamp = datetime.datetime.now() # if the statement has no timestamp, use the current date s.t. validation does not fail as long as consent exists

        # has the user given consent to this verb at some point and is it still valid?
        user_consent = UserConsents.objects.filter(
            user=user, provider=provider, verb__in=verb_candidates, consented=True, created__lte=timestamp, active=True
        ).first()

        if not user_consent:
            if verb_id in anon_verbs:
                return {
                    "valid": True,
                    "accepted": True,
                    "needs_anonymization": True,
                }

            return {
                "valid": True,
                "accepted": False,
                "reason": f"User has not given consent to verb id {verb_id}",
            }

        # has the user given consent to the object at hand?
        object_consent = False
        verb_objects = json.loads(user_consent.object)
        object_statement = x_api_statement["object"]["definition"]
        if not verb_objects or len(verb_objects) == 0:  # no objects defined, "consent" by default
            object_consent = True
        else:
            for obj in verb_objects:
                if (
                    obj["consented"]
                    and obj["matching"] == "id"
                    and obj["id"] == x_api_statement["object"]["id"]
                ):
                    object_consent = True
                elif (
                    obj["consented"]
                    and obj["matching"] == "definitionType"
                    and obj["definition"]["type"]
                    == x_api_statement["object"]["definition"]["type"]
                ):
                    object_consent = True

        if not object_consent:
            if verb_id in anon_verbs:
                return {
                    "valid": True,
                    "accepted": True,
                    "needs_anonymization": True,
                }
            return {
                "valid": True,
                "accepted": False,
                "reason": f"User has not given consent to this object: {object_statement} for verb {verb_id}",
            }

    return {"valid": True, "accepted": True}

def process_tan_statement(x_api_statement):
    """
    Process xAPI statement by checking for validation errors.
    """
    try:
        instance.validate(x_api_statement)
    except ValidationError as e:
        return {"valid": False, "reason": e.message}


    # expects the tan to be in a field of the actor named "tan"
    tan = x_api_statement.get("actor", {}).get("tan", None)

    if tan is None:
        return {
            "valid": False,
            "reason": "TAN missing in statement",
        }

    if (not "verb" in x_api_statement) or (not "id" in x_api_statement["verb"]):
        return {"valid": False, "reason": "No verb given"}
    verb = x_api_statement["verb"]["id"]

    if (not "object" in x_api_statement) or (not "id" in x_api_statement["object"]):
        return {"valid": False, "reason": "No object given"}

    return {"valid": True}


class CreateStatement(APIView):
    """
    xAPI create statements proxy. This endpoint filters xAPI statements based on user settings
    and passes only consented xAPI statements to the LRS.
    """

    def post(self, request):
        # Provider authorization
        auth_header = request.headers.get("Authorization")
        if not auth_header or not auth_header.startswith("Basic "):
            return JsonResponse(
                {
                    "message": "No provider authorization token supplied.",
                    "provider": "not found",
                },
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )

        auth_key = auth_header.split(" ")[1]
        try:
            provider_auth = ProviderAuthorization.objects.get(key=auth_key)
        except ObjectDoesNotExist:
            return JsonResponse(
                {
                    "message": "No provider for authorization token found.",
                    "provider": "not found",
                },
                safe=False,
                status=status.HTTP_400_BAD_REQUEST,
            )

        provider = provider_auth.provider

        # Load latest provider schema for essential verbs
        try:
            latest_schema = ProviderSchema.objects.get(
                provider=provider, superseded_by__isnull=True
            )
        except ObjectDoesNotExist:
            return JsonResponse(
                {
                    "message": "No consent provider schema found.",
                    "provider": provider.name,
                },
                safe=False,
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        # Handle list of xAPI statements as well as a single statement
        x_api_statements = (
            request.data if isinstance(request.data, list) else [request.data]
        )

        # Track total processed statements
        STATEMENTS_PROCESSED.inc(len(x_api_statements))

        # Forward to additional LRS if configured
        if (
            latest_schema.additional_lrs
            and isinstance(latest_schema.additional_lrs, list)
            and len(latest_schema.additional_lrs) > 0
        ):
            for additional_lrs in latest_schema.additional_lrs:
                headers = {
                    "Authorization": additional_lrs["token_type"]
                    + " "
                    + additional_lrs["token"]
                }
                try:
                    res = requests.post(
                        additional_lrs["url"], json=x_api_statements, headers=headers
                    )
                    if res.status_code != 200:
                        raise RuntimeError("Returned status code other than 200")
                    if settings.DEBUG:
                        print(
                            "Forwarded statement to ",
                            additional_lrs["url"],
                            ":",
                            res.reason,
                            "({})".format(res.status_code),
                        )
                except Exception as e:
                    if settings.DEBUG:
                        print("Could not forward to ", additional_lrs["url"], ":", e)
                    retry_forward_statements.delay(
                        x_api_statements,
                        additional_lrs["token_type"],
                        additional_lrs["token"],
                        additional_lrs["url"],
                    )

        if settings.SHOW_XAPI_STATEMENTS:
            print(x_api_statements)

        try:
            result = [
                process_statement(stmt, provider, latest_schema)
                for stmt in x_api_statements
            ]
        except Exception as e:
            return JsonResponse(
                {
                    "message": str(e),
                },
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        # Count rejected and accepted statements
        rejected_count = sum(
            1 for e in result if e["valid"] is False or e["accepted"] is False
        )
        accepted_count = len(x_api_statements) - rejected_count

        STATEMENTS_REJECTED.inc(rejected_count)
        STATEMENTS_ACCEPTED.inc(accepted_count)

        if settings.SHOW_XAPI_STATEMENTS:
            print(result)

        if rejected_count > 0:
            return JsonResponse(
                {
                    "message": "xAPI statements couldn't be stored in LRS",
                    "data": result,
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        else:
            # Anonymize statements where needed
            x_api_statements = [
                anonymize_statement(statement)
                if result[i].get("needs_anonymization", False)
                else statement
                for i, statement in enumerate(x_api_statements)
            ]
            uuids = list(map(store_in_db, x_api_statements))
            return JsonResponse(
                {
                    "message": "xAPI statements successfully stored in LRS",
                    "data": uuids,
                },
                safe=False,
                status=status.HTTP_200_OK,
            )


class CreateTANStatement(APIView):
    """
    xAPI create statements proxy for use with TANs. Stores all statements which contain TANs and are authorized with a provider token.
    """

    def post(self, request):
        # provider authorization
        auth_header = request.headers.get("Authorization")
        if not auth_header.startswith("Basic "):
            return JsonResponse(
                {
                    "message": "No provider authorization token supplied.",
                    "provider": "not found",
                },
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )
        auth_key = auth_header.split(" ")[1]
        try:
            provider_auth = ProviderAuthorization.objects.get(key=auth_key)
        except ObjectDoesNotExist:
            return JsonResponse(
                {
                    "message": "No provider for authorization token found.",
                    "provider": "not found",
                },
                safe=False,
                status=status.HTTP_400_BAD_REQUEST,
            )

        provider = provider_auth.provider

        # handle list of xAPI statements as well as single xAPI statement
        x_api_statements = (
            request.data if isinstance(request.data, list) else [request.data]
        )

        result = [
            process_tan_statement(stmt)
            for stmt in x_api_statements
        ]

        invalid = (
            len([e for e in result if e["valid"] == False])
            > 0
        )

        if invalid:
            return JsonResponse(
                {
                    "message": "xAPI statements couldn't be stored in LRS",
                    "data": result,
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        else:
            # store provider as well, so statements can be properly assigned when searched by users
            for index, statement in enumerate(x_api_statements):
                statement["actor"]["provider_id"] = provider.id
                x_api_statements[index] = statement

            uuids = list(map(store_in_db, x_api_statements))
            return JsonResponse(
                {
                    "message": "xAPI statements successfully stored in LRS",
                    "data": uuids,
                },
                safe=False,
                status=status.HTTP_200_OK,
            )

class StatisticView(APIView):

    def get(self, request):
        # Get the user count from Django's user model
        user_count = CustomUser.objects.count()

        # Use estimated_document_count for fast counts
        statements_collection = lrs_db["statements"]
        statement_count = statements_collection.estimated_document_count()
        results_collection = lrs_db["results"]
        result_count = results_collection.estimated_document_count()

        # Calculate the date range for the last 7 days
        end_date = datetime.datetime.utcnow() 
        start_date = end_date - timedelta(days=7)

        # Aggregation pipeline:
        # 1. Convert 'stored' to a Date (if needed)
        # 2. Filter documents in the date range
        # 3. Group by year, month, and day and sum counts
        # 4. Sort by date
        pipeline = [
            {
                "$addFields": {
                    "stored_date": {"$toDate": "$stored"}
                }
            },
            {
                "$match": {
                    "stored_date": {"$gte": start_date, "$lte": end_date}
                }
            },
            {
                "$group": {
                    "_id": {
                        "year": {"$year": "$stored_date"},
                        "month": {"$month": "$stored_date"},
                        "day": {"$dayOfMonth": "$stored_date"}
                    },
                    "count": {"$sum": 1}
                }
            },
            {
                "$sort": {
                    "_id.year": 1,
                    "_id.month": 1,
                    "_id.day": 1
                }
            }
        ]

        # Run the aggregation; using a dictionary comprehension to build a date->count map
        history_data = {
            f"{entry['_id']['year']}-{entry['_id']['month']:02d}-{entry['_id']['day']:02d}": entry["count"]
            for entry in statements_collection.aggregate(pipeline)
        }

        # Build a complete 7-day history (fill missing dates with 0)
        date_counts = {}
        for i in range(7):
            date_str = (start_date + timedelta(days=i)).strftime('%Y-%m-%d')
            date_counts[date_str] = history_data.get(date_str, 0)

        statement_history = list(date_counts.values())

        return JsonResponse({
            "message": "statistics collected",
            "user_count": user_count,
            "statement_count": statement_count,
            "result_count": result_count,
            "statement_history": statement_history
        }, safe=False, status=status.HTTP_200_OK)
