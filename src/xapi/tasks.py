import requests
from django.conf import settings
from celery import shared_task

@shared_task(bind=True, autoretry_for=(RuntimeError,Exception,), retry_backoff=True, retry_jitter=False, retry_kwargs={'max_retries': 20})  # exponential backoff, 20 retries comes down to approx. 2 weeks
def retry_forward_statements(self, data, token_type, token, url):
    headers = {"Authorization": token_type + " " + token}
    res = requests.post(url, json=data, headers=headers)
    if not res:
        raise RuntimeError(f"Could not forward {len(data)} statements to {url}.")