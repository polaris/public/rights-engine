import json
import os

from django.test import TestCase
from jsonschema.validators import validator_for
from jsonschema import ValidationError, validate

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))


class TestxAPIStatementValidation(TestCase):
    def setUp(self):
        with open(os.path.join(PROJECT_PATH, "static/xapi_statement.schema.json")) as f:
            self.schema = json.load(f)

    def test_empty_object_gets_rejected(self):
        statement = {}

        with self.assertRaises(ValidationError):
            validate(statement, self.schema)

    def test_basic_valid_statement(self):
        statement = {
            "actor": {
                "objectType": "Agent",
                "name": "Gert Frobe",
                "account": {"homePage": "http://example.adlnet.gov", "name": "1625378"},
            },
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/failed",
                "display": {"en-US": "failed"},
            },
            "object": {
                "id": "https://example.adlnet.gov/AUidentifier",
                "objectType": "Activity",
            },
        }

        try:
            validate(statement, self.schema)
        except ValueError:
            assert False

    def test_basic_valid_statement2000Times(self):
        statement = {
            "actor": {
                "objectType": "Agent",
                "name": "Gert Frobe",
                "account": {"homePage": "http://example.adlnet.gov", "name": "1625378"},
            },
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/failed",
                "display": {"en-US": "failed"},
            },
            "object": {
                "id": "https://example.adlnet.gov/AUidentifier",
                "objectType": "Activity",
            },
        }

        try:
            cls = validator_for(self.schema)
            cls.check_schema(self.schema)
            instance = cls(self.schema)
            for i in range(1, 2000):
                instance.validate(statement)
        except ValueError:
            assert False

    def test_validate_statement_with_uuid(self):
        statement = {
            "id": "e05aa883-acaf-40ad-bf54-02c8ce485fb0",
            "actor": {
                "objectType": "Agent",
                "name": "Gert Frobe",
                "account": {"homePage": "http://example.adlnet.gov", "name": "1625378"},
            },
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/failed",
                "display": {"en-US": "failed"},
            },
            "object": {
                "id": "https://example.adlnet.gov/AUidentifier",
                "objectType": "Activity",
            },
        }

        try:
            validate(statement, self.schema)
        except ValueError:
            assert False

    def test_validate_statement_with_mbox(self):
        statement = {
            "id": "12345678-1234-5678-1234-567812345678",
            "actor": {"mbox": "mailto:xapi@adlnet.gov"},
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/created",
                "display": {"en-US": "created"},
            },
            "object": {
                "id": "http://example.adlnet.gov/xapi/example/activity",
                "objectType": "Activity",
            },
        }
        try:
            validate(statement, self.schema)
        except ValueError:
            assert False

    def test_validate_statement_with_context(self):
        statement = {
            "id": "12345678-1234-5678-1234-567812345678",
            "actor": {"mbox": "mailto:xapi@adlnet.gov"},
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/created",
                "display": {"en-US": "created"},
            },
            "object": {
                "id": "http://example.adlnet.gov/xapi/example/activity",
                "objectType": "Activity",
            },
            "context": {
                "parent": [{"id": "http://example.adlnet.gov/xapi/example/test1"}],
                "grouping": [{"id": "http://example.adlnet.gov/xapi/example/Algebra1"}],
            },
        }
        try:
            validate(statement, self.schema)
        except ValueError:
            assert False

    def test_validate_statement_with_result(self):
        statement = {
            "id": "7ccd3322-e1a5-411a-a67d-6a735c76f119",
            "timestamp": "2015-12-18T12:17:00+00:00",
            "actor": {
                "objectType": "Agent",
                "name": "Example Learner",
                "mbox": "mailto:example.learner@adlnet.gov",
            },
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/attempted",
                "display": {"en-US": "attempted"},
            },
            "object": {
                "id": "http://example.adlnet.gov/xapi/example/simpleCBT",
                "objectType": "Activity",
            },
            "result": {
                "score": {"scaled": 0.95},
                "success": True,
                "completion": True,
                "duration": "PT1234S",
            },
        }

        try:
            validate(statement, self.schema)
        except ValueError:
            assert False

    def test_validate_statement_with_object_definition(self):
        statement = {
            "id": "7ccd3322-e1a5-411a-a67d-6a735c76f119",
            "timestamp": "2015-12-18T12:17:00+00:00",
            "actor": {
                "objectType": "Agent",
                "name": "Example Learner",
                "mbox": "mailto:example.learner@adlnet.gov",
            },
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/attempted",
                "display": {"en-US": "attempted"},
            },
            "object": {
                "id": "http://example.adlnet.gov/xapi/example/simpleCBT",
                # TODO write test without objectType field
                "objectType": "Activity",
                "definition": {
                    "name": {"en-US": "simple CBT course"},
                    "description": {"en-US": "A fictitious example CBT course."},
                },
            },
            "result": {
                "score": {"scaled": 0.95},
                "success": True,
                "completion": True,
                "duration": "PT1234S",
            },
        }

        try:
            validate(statement, self.schema)
        except ValueError:
            assert False

    def test_validate_statement_with_tan(self):
        statement = {
            "actor": {
                "objectType": "Agent",
                "tan": "u4gueb983fnklerg",
            },
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/failed",
                "display": {"en-US": "failed"},
            },
            "object": {
                "id": "https://example.adlnet.gov/AUidentifier",
                "objectType": "Activity",
            },
        }

        try:
            validate(statement, self.schema)
        except ValueError:
            assert False