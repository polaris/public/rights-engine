import json
import os
from datetime import datetime
from io import StringIO
from unittest.mock import patch

from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase
from rest_framework.test import APIClient
from rolepermissions.roles import assign_role

from consents.models import UserConsents
from consents.tests.tests_consent_operations import BaseTestCase
from providers.models import Provider, ProviderAuthorization, ProviderSchema, ProviderVerbGroup
from users.models import CustomUser

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))


def mock_store_in_lrs(x_api_statement):
    return True


class TestxAPIVerbAndObjectValidation(BaseTestCase):
    provider_schema = {
        "id": "h5p-0",
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "verbs": [
            {
                "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                "label": "Unlocked",
                "description": "Actor unlocked an object",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                        "label": "Course",
                        "defaultConsent": True,
                        "matching": "definitionType",
                        "definition": {
                            "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                            "name": {
                                "enUS": "A course within an LMS. Contains learning materials and activities"
                            },
                        },
                    },
                ],
            }
        ],
        "essentialVerbs": [],
    }

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_statement_ambiguous_verb_id_rejected(self):
        """ """
        # Create provider
        with StringIO(json.dumps(self.provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id
        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "definitionType",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    },
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": False,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "definitionType",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            [
                {
                    "non_field_errors": [
                        "Verb https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked contains ambiguous consent."
                    ]
                }
            ],
        )

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_statement_ambiguous_object_id_rejected(self):
        """ """
        # Create provider
        with StringIO(json.dumps(self.provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "definitionType",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    },
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "definitionType",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": False,
                                }
                            ]
                        ),
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            [
                {
                    "non_field_errors": [
                        "Verb https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked contains ambiguous object consents."
                    ]
                }
            ],
        )

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_statement_with_valid_duplicate_verb(self):
        """ """
        # Create provider
        with StringIO(json.dumps(self.provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "definitionType",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    },
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "definitionType",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["message"], "user consent saved"
        )