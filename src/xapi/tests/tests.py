import json
import os
import copy
from datetime import datetime, timedelta
from io import StringIO
from unittest import mock
from unittest.mock import patch, MagicMock

from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase
from rest_framework.test import APIClient
from rolepermissions.roles import assign_role

from consents.models import UserConsents
from consents.tests.tests_consent_operations import BaseTestCase
from providers.models import Provider, ProviderAuthorization, ProviderSchema, ProviderVerbGroup
from users.models import CustomUser

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))


def mock_store_in_lrs(x_api_statement):
    return True

def retry_forward_statements(x_api_statement):
    return True

class XAPITestCase(TestCase):
    test_user_email = "test@mail.com"
    test_user_password = "test123"

    test_provider_email = "test2@mail.com"
    test_provider_password = "test123"

    verb_groups = [
        {
            "id": "default_group",
            "label": "Default group",
            "description": "default",
            "showVerbDetails": True,
            "purposeOfCollection": "Lorem Ipsum",
            "requiresConsent": True,
            "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"}
            ]
        },
        {
            "id": "default_group",
            "label": "Group 2",
            "description": "Lorem ipsum",
            "showVerbDetails": True,
            "purposeOfCollection": "Lorem Ipsum",
            "requiresConsent": True,
            "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
            ]
        }
    ]

    def setUp(self):
        self.normal_user = normal_user = CustomUser.objects.create_user(
            self.test_user_email, self.test_user_password
        )
        provider_user = CustomUser.objects.create_user(
            self.test_provider_email, self.test_provider_password
        )
        assign_role(provider_user, "provider")
        assign_role(normal_user, "user")
        assign_role(provider_user, "provider_manager")

        response = self.client.post(
            "/api/v1/auth/token",
            {
                "email": self.test_provider_email,
                "password": self.test_provider_password,
            },
        )  # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        provider_token = response.data["access"]

        provider_client = APIClient()
        provider_client.credentials(HTTP_AUTHORIZATION="Bearer " + provider_token)

        # we need provider schemas with groups for this test
        with open(
            os.path.join(PROJECT_PATH, "static/provider_schema_h5p_v1.example.json")
        ) as fp:
            response = provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)
            self.assertTrue("message" in response.json())

        # create some verb groups
        for group in self.verb_groups:
            response = provider_client.post(
                "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
                group,
                format="json",
            )
            self.assertEqual(response.status_code, 200)

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_xapi(self):
        try:
            provider = Provider.objects.latest('id')
        except ObjectDoesNotExist:
            self.assertTrue(False)  # provider was not created when uploading schema

        try:
            provider_schema = ProviderSchema.objects.get(
                provider=provider, superseded_by__isnull=True
            )
        except ObjectDoesNotExist:
            self.assertTrue(False)  # provider schema was not created when uploading

        keys = [
            auth.key for auth in ProviderAuthorization.objects.filter(provider=provider)
        ]
        self.assertTrue(len(keys) > 0)  # keys were not created
        key = keys[0]

        first = True
        accepted_verb = {}
        denied_verb = {}
        for group in provider_schema.groups():
            for verb in group.verbs.all():
                objects = [{"id": obj.object_id, "objectType": obj.object_type, "matching": obj.matching,
                            "label": obj.label, "definition": obj.definition} for obj in verb.verbobject_set.all()]
                first_obj = True
                for obj in objects:  # consent to the first object
                    obj["consented"] = first_obj
                    first_obj = False
                # consent to the very first verb
                if first:
                    accepted_verb = verb
                    UserConsents.objects.create(
                        consented=True,
                        verb=verb,
                        object=json.dumps(objects),
                        verb_group=group,
                        provider=provider,
                        user=self.normal_user,
                    )
                    first = False
                else:
                    denied_verb = verb
                    UserConsents.objects.create(
                        consented=False,
                        verb=verb,
                        object=json.dumps(objects),
                        verb_group=group,
                        provider=provider,
                        user=self.normal_user,
                    )

        provider_client = APIClient()
        provider_client.credentials(HTTP_AUTHORIZATION="Basic " + key)

        response = provider_client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": accepted_verb.verb_id},
                "object": {
                    "id": accepted_verb.verbobject_set.first().object_id,
                    "objectType": "Activity",
                    "definition": accepted_verb.verbobject_set.first().definition,
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)

        # {'message': 'processed', 'provider': 'H5P'}
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )

        # response = provider_client.post(
        # "/xapi/statements",
        # {
        # "actor": {"mbox": f"mailto:{self.test_user_email}"},
        # "verb": {"id": denied_verb["id"]},
        # "object": {
        # "id": denied_verb["objects"][0]["id"],
        # "objectType": "Activity",
        # },
        # "timestamp": datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
        # },
        # format="json",
        # )
        # self.assertEqual(response.status_code, 403)

        ## every object after the first one will be denied
        # for object in accepted_verb["objects"][1:]:
        # response = provider_client.post(
        # "/xapi/statements",
        # {
        # "actor": {"mbox": f"mailto:{self.test_user_email}"},
        # "verb": {"id": accepted_verb["id"]},
        # "object": {
        # "id": object["id"],
        # "objectType": "Activity",
        # },
        # "timestamp": datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
        # },
        # format="json",
        # )
        # self.assertEqual(response.status_code, 403)

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_system_user(self):
        try:
            provider = Provider.objects.order_by("id").first()
        except ObjectDoesNotExist:
            self.assertTrue(False)  # provider was not created when uploading schema

        keys = [
            auth.key for auth in ProviderAuthorization.objects.filter(provider=provider)
        ]
        self.assertTrue(len(keys) > 0)  # keys were not created
        key = keys[0]

        provider_client = APIClient()
        provider_client.credentials(HTTP_AUTHORIZATION="Basic " + key)


        response = provider_client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"system:{provider.id}"},
                "verb": {"id": "some_id"},
                "object": {
                    "id": "someOtherId",
                    "objectType": "Activity",
                    "definition": {
                        "name": {
                            "de-DE": "Testobjekt"
                        }
                    },
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)

        # {'message': 'processed', 'provider': 'H5P'}
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )

class TestxAPIWithDataRecordingPause(BaseTestCase):
    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_paused_data_recording(self):
        """
        Ensure xAPI statements are rejected while user paused data recording.
        """
        # Create provider H5P
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"}
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true, "matching": "definitionType" ,"definition":{"type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF", "name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm", "name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46", "name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b", "name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        access_token_h5p = ProviderAuthorization.objects.get(provider__name="H5P").key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Basic " + access_token_h5p)

        # xAPI statements are stored
        response = client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                "object": {
                    "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                    "objectType": "Activity",
                    "definition": {
                        "type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "name": {"enUS": "2.3.1 Funktion Zirkulationsleitung"},
                    },
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )

        # Pause data recording
        response = self.user_client.post("/api/v1/auth/toggle-data-recording")
        self.assertEqual(response.status_code, 200)

        # xAPI statements isn't stored anymore
        response = client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                "object": {
                    "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                    "objectType": "Activity",
                    "definition": {
                        "type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "name": {"enUS": "2.3.1 Funktion Zirkulationsleitung"},
                    },
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {
                "message": "xAPI statements couldn't be stored in LRS",
                "data": [
                    {
                        "accepted": False,
                        "reason": "User has paused data collection",
                        "valid": True,
                    }
                ],
            },
        )


class TestxAPIStatementActorAccount(BaseTestCase):
    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_statement_with_actor_account_name(self):
        """
        Ensure xAPI statement actor containing account dict is stored.
        """
        # Create provider H5P
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"}
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true, "matching": "definitionType" ,"definition":{"type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF", "name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm", "name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46", "name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b", "name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        access_token_h5p = ProviderAuthorization.objects.get(provider__name="H5P").key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Basic " + access_token_h5p)

        # xAPI statements are stored
        response = client.post(
            "/xapi/statements",
            {
                "actor": {
                    "account": {
                        "homePage": "https://polaris.com",
                        "name": self.test_user_email,
                    }
                },
                "verb": {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                "object": {
                    "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                    "objectType": "Activity",
                    "definition": {
                        "type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "name": {"enUS": "2.3.1 Funktion Zirkulationsleitung"},
                    },
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_statement_with_invalid_actor_account_name(self):
        """
        Ensure xAPI statement actor containing invalid account dict is rejected.
        """
        # Create provider H5P
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"}
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF", "name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm", "name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46", "name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false, "matching": "definitionType" ,"definition":{"type": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b", "name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        access_token_h5p = ProviderAuthorization.objects.get(provider__name="H5P").key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Basic " + access_token_h5p)

        # xAPI statements are stored
        response = client.post(
            "/xapi/statements",
            {
                "actor": {
                    "account": {
                        "homePage": "https://polaris.com",
                        "email": self.test_user_email,
                    }
                },
                "verb": {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                "object": {
                    "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                    "objectType": "Activity",
                    "definition": {
                        "type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "name": "2.3.1 Funktion Zirkulationsleitung",
                    },
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        
    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_multiple_statements_at_once(self):
        """
        Ensure xAPI statement actor containing account dict is stored.
        """
        # Create provider H5P
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"}
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF", "name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm", "name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true, "matching": "definitionType", "definition":{"type": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46", "name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false, "matching": "definitionType" ,"definition":{"type": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b", "name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        access_token_h5p = ProviderAuthorization.objects.get(provider__name="H5P").key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Basic " + access_token_h5p)

        # xAPI statements are stored
        response = client.post(
            "/xapi/statements",
            [
                {
                    "actor": {
                        "account": {
                            "homePage": "https://polaris.com",
                            "name": self.test_user_email,
                        }
                    },
                    "verb": {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                    "object": {
                        "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "objectType": "Activity",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                            "name": {"enUS": "2.3.1 Funktion Zirkulationsleitung"},
                        },
                    },
                    "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
                },
                {
                    "actor": {
                        "account": {
                            "homePage": "https://polaris.com",
                            "name": self.test_user_email,
                        }
                    },
                    "verb": {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                    "object": {
                        "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                        "objectType": "Activity",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                            "name": {"enUS": "1.2.3 Kappenventil"},
                        },
                    },
                    "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
                },
            ],
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )


class TestxAPIObjectMatchingDefinitionType(BaseTestCase):
    provider_schema = {
        "id": "h5p-0",
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "verbs": [
            {
                "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                "label": "Unlocked",
                "description": "Actor unlocked an object",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                        "label": "Course",
                        "defaultConsent": True,
                        "matching": "definitionType",
                        "definition": {
                            "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                            "name": {
                                "enUS": "A course within an LMS. Contains learning materials and activities"
                            },
                        },
                    },
                ],
            }
        ],
        "essentialVerbs": [],
    }

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_xapi_with_object_defintion_type_matching(self):
        """
        Ensure xAPI statement is matched by object definition type.
        """
        # Create provider
        with StringIO(json.dumps(self.provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "definitionType",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    }
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        access_token_h5p = ProviderAuthorization.objects.get(provider__name="H5P").key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Basic " + access_token_h5p)

        # Send xAPI statement
        response = client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
                "object": {
                    "objectType": "Activity",
                    "definition": {
                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                        "name": {"de": "Testkurs KI:edu.nrw "},
                    },
                    "id": "https://moodle-analytics.ruhr-uni-bochum.de/course/view.php?id=127",
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )

        # Send xAPI statement with not consented object definition type
        response = client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
                "object": {
                    "objectType": "Activity",
                    "definition": {
                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/not-consented",
                        "name": {"de": "Testkurs KI:edu.nrw "},
                    },
                    "id": "https://moodle-analytics.ruhr-uni-bochum.de/course/view.php?id=127",
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json()["message"], "xAPI statements couldn't be stored in LRS"
        )


class TestxAPIObjectMatchingDefinitiondId(BaseTestCase):
    provider_schema = {
        "id": "h5p-0",
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "verbs": [
            {
                "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                "label": "Unlocked",
                "description": "Actor unlocked an object",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                        "label": "Course",
                        "defaultConsent": True,
                        "matching": "id",
                        "definition": {
                            "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                            "name": {
                                "enUS": "A course within an LMS. Contains learning materials and activities"
                            },
                        },
                    },
                ],
            }
        ],
        "essentialVerbs": [],
    }

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_xapi_with_object_defintion_type_matching(self):
        """
        Ensure xAPI statement is matched by object definition type.
        """
        # Create provider
        with StringIO(json.dumps(self.provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "id",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    }
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        access_token_h5p = ProviderAuthorization.objects.get(provider__name="H5P").key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Basic " + access_token_h5p)

        # Send xAPI statement
        response = client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
                "object": {
                    "objectType": "Activity",
                    "definition": {
                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                        "name": {"de": "Testkurs KI:edu.nrw "},
                    },
                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )


        # Send xAPI statement with not consented object id
        response = client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
                "object": {
                    "objectType": "Activity",
                    "definition": {
                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                        "name": {"de": "Testkurs KI:edu.nrw "},
                    },
                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/invalid-id",
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json()["message"], "xAPI statements couldn't be stored in LRS"
        )


class TestxAPITimestampAfterConsent(BaseTestCase):
    provider_schema = {
        "id": "h5p-0",
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "verbs": [
            {
                "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                "label": "Unlocked",
                "description": "Actor unlocked an object",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                        "label": "Course",
                        "defaultConsent": True,
                        "matching": "id",
                        "definition": {
                            "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                            "name": {
                                "enUS": "A course within an LMS. Contains learning materials and activities"
                            },
                        },
                    },
                ],
            }
        ],
        "essentialVerbs": [],
    }

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    def test_xapi_with_timestamp_validation(self):
        """
        Ensure xAPI statement "timestamp" field is a point in time after the user has given consent.
        """
        # Create provider
        with StringIO(json.dumps(self.provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "id",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    }
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        access_token_h5p = ProviderAuthorization.objects.get(provider__name="H5P").key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Basic " + access_token_h5p)

        # Send xAPI statement with timestamp after consent
        response = client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
                "object": {
                    "objectType": "Activity",
                    "definition": {
                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                        "name": {"de": "Testkurs KI:edu.nrw "},
                    },
                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                },
                "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )

        # Send xAPI statement with timestamp before consent
        response = client.post(
            "/xapi/statements",
            {
                "actor": {"mbox": f"mailto:{self.test_user_email}"},
                "verb": {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
                "object": {
                    "objectType": "Activity",
                    "definition": {
                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                        "name": {"de": "Testkurs KI:edu.nrw "},
                    },
                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/invalid-id",
                },
                "timestamp": (datetime.now() - timedelta(1,0)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "before" the consent
            },
            format="json",
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json()["message"], "xAPI statements couldn't be stored in LRS"
        )

class TextxAPIAdditionalLrs(BaseTestCase):
    provider_schema = {
        "id": "h5p-0",
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "verbs": [
            {
                "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                "label": "Unlocked",
                "description": "Actor unlocked an object",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                        "label": "Course",
                        "defaultConsent": True,
                        "matching": "id",
                        "definition": {
                            "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                            "name": {
                                "enUS": "A course within an LMS. Contains learning materials and activities"
                            },
                        },
                    },
                ],
            }
        ],
        "essentialVerbs": [],
        "additionalLrs": [
            {
                "url": "http://localhost:5555/xapi/statements",
                "token": "token_to_check",
                "token_type": "Bearer"
            }
        ]
    }

    statement = {
        "actor": {"mbox": "mailto:test@mail.com"},
        "verb": {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
        "object": {
            "objectType": "Activity",
            "definition": {
                "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                "name": {"de": "Testkurs KI:edu.nrw "},
            },
            "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
        },
        "timestamp": (datetime.now() + timedelta(0,30)).strftime("%Y-%m-%dT%H:%M:%SZ"), # timedelta is used to ensure the statement is "after" the consent
    }

    additional_lrs_auth_headers = {'Authorization': 'Bearer token_to_check'}

    @patch("xapi.views.store_in_db", mock_store_in_lrs)
    @patch("xapi.views.requests.post")
    def test_xapi_additional_lrs(self, mock_post):
        """
        Ensure xAPI statement is forwarded to external LRS if configured in provider schema.
        """

        # Create provider
        with StringIO(json.dumps(self.provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        # Create user consent for test user
        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                        "consented": True,
                        "objects": json.dumps(
                            [
                                {
                                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                                    "label": "Course",
                                    "defaultConsent": True,
                                    "matching": "id",
                                    "definition": {
                                        "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                        "name": {
                                            "enUS": "A course within an LMS. Contains learning materials and activities"
                                        },
                                    },
                                    "consented": True,
                                }
                            ]
                        ),
                    }
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        access_token_h5p = ProviderAuthorization.objects.get(provider__name="H5P").key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Basic " + access_token_h5p)

        # create mock response for external LRS
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {'message': "xAPI statements successfully stored in LRS"}
        mock_response.__bool__.return_value = True  # important since we check "if not res" in xapi.views
        # side effect to immediately create a copy of the object to preserve original properties (since, apparently, the magic mock passes the json object by reference)
        def capture_args(*args, **kwargs):
            self.captured_json = copy.deepcopy(kwargs.get('json'))
            self.captured_headers = copy.deepcopy(kwargs.get('headers'))
            return mock_response
        mock_post.return_value = mock_response
        mock_post.side_effect = capture_args

        # Send xAPI statement
        response = client.post(
            "/xapi/statements",
            self.statement,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["message"], "xAPI statements successfully stored in LRS"
        )
        mock_post.assert_called_once()
        assert mock_response.status_code == 200
        if isinstance(self.captured_json, list): self.assertEqual(self.captured_json, [self.statement])
        else: self.assertEqual(self.captured_json, self.statement)
        self.assertEqual(self.captured_headers, self.additional_lrs_auth_headers)
