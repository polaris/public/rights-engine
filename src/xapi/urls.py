from django.urls import path

from . import views

urlpatterns = [
    path('statements', views.CreateStatement.as_view()),
    path('tanStatements', views.CreateTANStatement.as_view()),
    path('statistics', views.StatisticView.as_view())
]
