from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.http.response import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.html import strip_tags
from django.contrib.auth.models import Group, Permission, Permission
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.views import APIView
from rolepermissions.checkers import has_permission
from rolepermissions.roles import assign_role

from backend.roles import Roles
from backend.utils import lrs_db
from consents.views import JsonUploadParser
from data_removal.models import DataRemovalJob

from .models import CustomUser
from .serializers import UserSerializer, GroupSerializer, PermissionSerializer


class CreateUserView(APIView):
    """ 
    Create user endpoint, which will become obsolete when integrating oauth.
    """
    permission_classes = (IsAuthenticated,)
    parser_class = (JsonUploadParser,)

    def put(self, request, format=None):
        if has_permission(request.user, Roles.CREATE_USER):
            email = request.data.get("email", None)
            password = request.data.get("password", None)
            assign_provider_role = request.data.get("assign_provider_role", None)

            if email == None or password == None or assign_provider_role == None:
                return JsonResponse(
                    {"message": "invalid form data"}, status=status.HTTP_400_BAD_REQUEST
                )

            new_user = CustomUser.objects.create_user(email, password)

            if assign_provider_role:
                assign_role(new_user, "provider")
            else:
                assign_role(new_user, "user")

            return JsonResponse(
                {"message": "user created"}, status=status.HTTP_201_CREATED
            )
        else:
            return JsonResponse(
                {"message": "missing permissions"}, status=status.HTTP_401_UNAUTHORIZED
            )


class ToggleDataRecording(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        """
        Allows a user to pause (or continue) all data recording (except essential verbs) alltogether.
        """
        user = request.user

        user.paused_data_recording = not user.paused_data_recording
        user.save()
        message = (
            "data recording paused"
            if user.paused_data_recording
            else "data recording resumed"
        )
        return JsonResponse({"message": message}, status=status.HTTP_200_OK)


class DeleteUserView(APIView):
    permission_classes = (IsAuthenticated,)

    def send_email(self, user):
        """
        Notify user about completed account removal.
        """
        [username, _] = user.email.split("@")
        html_body = render_to_string(
            "account_removed.html",
            {"name": username},
        )
        text_body = strip_tags(html_body)

        email = EmailMultiAlternatives(
            "[Polaris]: Account removed",
            text_body,
            settings.EMAIL_HOST_USER,
            ["maxlou@mailbox.org"],
        )
        email.attach_alternative(html_body, "text/html")
        email.send()

    def delete(self, request):
        """
        Creates data removal job for all user statements and deletes user record.
        """
        user = request.user

        # delete user LRS data
        filter = {"actor.mbox": f"mailto:{user.email}"}

        DataRemovalJob.objects.create(
            user=request.user, filter=filter, execute_at=timezone.now()
        )

        # delete user account
        user.delete()

        self.send_email(user)

        return JsonResponse({"message": "user deleted"}, status=status.HTTP_200_OK)

class MergeDataView(APIView):
    permission_classes = (IsAuthenticated,)
    throttle_scope = 'merge'

    def post(self, request):
        """
        Allows a user to merge data given a provider and TAN. Rate limited (see settings.py).
        """
        user = request.user

        tan = request.data.get("tan", None)
        provider_id = request.data.get("provider", None)

        if tan == None or provider_id == None:
            return JsonResponse(
                    {"status": "error", "message": "invalid form data"}, status=status.HTTP_400_BAD_REQUEST
                )

        collection = lrs_db["statements"]

        query = {
                "$and": [
                    {"actor.tan": {"$exists": True, "$eq": tan}},
                    {"actor.provider_id": {"$exists": True, "$eq": provider_id}},
                ]
            }

        result = collection.update_many(query,
                                        {
                                            "$unset": {"actor.tan": "", "actor.provider_id": ""},
                                            "$set": {"actor.mbox": f'mailto:{user.email}'}
                                        })

        if result.modified_count > 0:
            return JsonResponse({"status": "success", "message": f'{result.modified_count} records updated'}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({"status": "error", "message": f'0 records updated'}, status=status.HTTP_400_BAD_REQUEST)


class UserListView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser)

    def get(self, request):
        users = CustomUser.objects.all()
        users = UserSerializer(users, many=True).data
        return JsonResponse(users,
                            safe=False,
                            status=status.HTTP_200_OK)


class GroupListView(APIView):
    """
    API endpoint that returns all auth groups with their permissions.
    """
    def get(self, request, format=None):
        groups = Group.objects.all()
        serializer = GroupSerializer(groups, many=True)
        return JsonResponse(serializer.data,
                            safe=False,
                            status=status.HTTP_200_OK)


class PermissionListView(APIView):
    """
    API endpoint that returns all auth permissions.
    """
    def get(self, request, format=None):
        groups = Permission.objects.all()
        serializer = PermissionSerializer(groups, many=True)
        return JsonResponse(serializer.data,
                            safe=False,
                            status=status.HTTP_200_OK)

class PermissionsUpdateView(APIView):
    """
    Save permissions for a user.
    """
    permission_classes = (IsAuthenticated, IsAdminUser)

    def post(self, request, user_id):
        try:
            user = CustomUser.objects.get(pk=user_id)
        except ObjectDoesNotExist:
            return JsonResponse({"status": "error", "message": 'user not found'},
                                status=status.HTTP_404_NOT_FOUND)
        user.groups.set(request.data["groups"])
        user.user_permissions.set(request.data["user_permissions"])
        user.save()
        return JsonResponse({"message": "success"})


class AcceptPrivacyPolicyView(APIView):
    """
    Accept privacy policy if necessary.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = request.user
        return JsonResponse({"status": user.general_privacy_policy})

    def post(self, request):
        user = request.user
        user.general_privacy_policy = True
        user.save()
        return JsonResponse({"message": "success"})