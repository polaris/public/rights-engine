from django.contrib.auth.models import Group, Permission
from rest_framework import serializers
from .models import CustomUser

class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = ('id', 'name', 'codename')

class GroupSerializer(serializers.ModelSerializer):
    # Nest the PermissionSerializer to include permission details
    permissions = PermissionSerializer(many=True)

    class Meta:
        model = Group
        fields = ('id', 'name', 'permissions')

class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True)
    user_permissions = PermissionSerializer(many=True)
    class Meta:
        model = CustomUser
        exclude = ["password", "shibboleth_connector_identifier"]
