from django.test import TestCase
from rest_framework.test import APIClient
from rolepermissions.roles import assign_role

from .models import CustomUser


class UserTests(TestCase):
    test_provider_email = "test2@mail.com"
    test_provider_password = "test123"

    def setUp(self):
        provider_user = CustomUser.objects.create_user(self.test_provider_email, self.test_provider_password)

        assign_role(provider_user, 'provider')
        assign_role(provider_user, 'user')
        assign_role(provider_user, 'provider_manager')


    def test_get_token(self):
        response=self.client.post('/api/v1/auth/token', {"email": self.test_provider_email, "password": self.test_provider_password})
        self.assertEqual(response.status_code, 200)

    def test_create_user(self):
        test_email = "asdf@fgg.com"
        test_password = "1234"
        response=self.client.post('/api/v1/auth/token', {"email": self.test_provider_email, "password": self.test_provider_password}) # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue('access' in response.data)
        token = response.data['access']
        client = APIClient() # new API client that uses our token
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        response = client.put('/api/v1/auth/createUser', {"email": test_email, "password": test_password, "assign_provider_role": 0})
        self.assertEqual(response.status_code, 201) # created

        self.assertTrue(CustomUser.objects.filter(email=test_email).exists())

    def test_toggle_data_recording(self):
        response=self.client.post('/api/v1/auth/token', {"email": self.test_provider_email, "password": self.test_provider_password}) # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue('access' in response.data)
        token = response.data['access']

        previous = CustomUser.objects.get(email=self.test_provider_email).paused_data_recording

        client = APIClient() # new API client that uses our token
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        response = client.post('/api/v1/auth/toggle-data-recording', {})
        self.assertEqual(response.status_code, 200) # created
        self.assertNotEqual(CustomUser.objects.get(email=self.test_provider_email).paused_data_recording, previous)

    def test_delete_user(self):
        test_email = "asdf@fgg.com"
        test_password = "1234"

        test_user = CustomUser.objects.create_user(test_email, test_password)

        response=self.client.post('/api/v1/auth/token', {"email": test_email, "password": test_password}) # obtain token for newly created user
        self.assertEqual(response.status_code, 200)
        self.assertTrue('access' in response.data)
        token = response.data['access']

        client = APIClient() # new API client that uses our token
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        response = client.delete('/api/v1/auth/deleteUser')
        self.assertEqual(response.status_code, 200) # created
        self.assertFalse(CustomUser.objects.filter(email=test_email).exists())
