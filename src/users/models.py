from datetime import date

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from providers.models import ProviderSchema
from users.managers import CustomUserManager


class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    shibboleth_connector_identifier = models.TextField(unique=True, null=True, blank=True)
    accepted_provider_schemas = models.ManyToManyField(ProviderSchema)
    # Main field for authentication
    paused_data_recording = models.BooleanField(default=False)
    general_privacy_policy = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = CustomUserManager()

    def __str__(self):
        return f"Custom user {self.id}: {self.email}"
