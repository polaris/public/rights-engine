from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from users.custom_tokens import CustomTokenObtainPairView

from . import views

urlpatterns = [
    path("token", CustomTokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh", TokenRefreshView.as_view(), name="token_refresh"),
    path("createUser", views.CreateUserView.as_view()),
    path("toggle-data-recording", views.ToggleDataRecording.as_view()),
    path("deleteUser", views.DeleteUserView.as_view()),
    path("mergeData", views.MergeDataView.as_view()),
    path("users", views.UserListView.as_view()),
    path("groups", views.GroupListView.as_view()),
    path("permissions", views.PermissionListView.as_view()),
    path("user/<user_id>/permissions", views.PermissionsUpdateView.as_view()),
    path("accept-privacy-policy", views.AcceptPrivacyPolicyView.as_view())
]