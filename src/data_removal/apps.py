from django.apps import AppConfig


class DataRemovalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'data_removal'
