from django.conf import settings
from django.db import models


class DataRemovalJob(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    description = models.CharField(max_length=256, null=True)
    matched_statements = models.IntegerField(null=True)
    finished = models.BooleanField(default=False)
    filter = models.JSONField()
    job_id = models.CharField(max_length=256, null=True)
    execute_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Data removal job"
