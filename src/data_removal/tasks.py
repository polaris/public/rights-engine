from __future__ import absolute_import, unicode_literals

import requests
from celery import shared_task
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.timezone import now

from backend.utils import lrs_db
from data_removal.models import DataRemovalJob
from users.models import CustomUser


class DataRemovalProcessor:
    def delete_xapi_statements(self, job: DataRemovalJob) -> None:
        """
        Delete XAPI statements in lrs with job filter.
        """
        collection = lrs_db["statements"]
        query = {"$and": [job.filter]}
        cursor = collection.delete_many(query)

        job.matched_statements = cursor.deleted_count
        job.finished = True
        job.save()

        self.send_email(job.user)

    def send_email(self, user):
        """
        Notify user about data removal completion.
        """
        [username, _] = user.email.split("@")
        html_body = render_to_string("data_removal_completed.html", {"name": username})
        text_body = strip_tags(html_body)

        email = EmailMultiAlternatives(
            "[Polaris]: Data Removal completed",
            text_body,
            settings.EMAIL_HOST_USER,
            [user.email],
        )
        email.attach_alternative(html_body, "text/html")
        email.send()

    def start_unfinished_jobs(self):
        """
        Start unfinished data removal jobs for all users.
        """

        users = CustomUser.objects.all()
        for user in users:
            unfinished_jobs = DataRemovalJob.objects.filter(
                user=user, finished=False, execute_at__lt=now()
            )
            list(map(self.delete_xapi_statements, unfinished_jobs))


@shared_task
def process_data_removals():
    processor = DataRemovalProcessor()
    processor.start_unfinished_jobs()
