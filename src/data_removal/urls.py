from django.urls import path

from . import views

urlpatterns = [
    path('create', views.CreateDataRemovalJob.as_view()),
    path('list', views.GetDataRemovalJobs.as_view())
]
