from datetime import timedelta

import requests
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from .models import DataRemovalJob


def rm_user_statements_filter(user_id):
    statement_filter = {"actor.mbox": f"mailto:{user_id}-polaris-id@polaris.com"}
    description = f"Delete all statements for user {user_id}"
    return [statement_filter, description]


def rm_user_verb_statements_filter(user_id, verb_id):
    statement_filter = {
        "actor.mbox": f"mailto:{user_id}-polaris-id@polaris.com",
        "verb.id": verb_id,
    }
    description = f"Delete statements for user {user_id} and verbId {verb_id}"
    return [statement_filter, description]


def rm_user_object_statements_filter(user_id, verb_id, object_id):
    statement_filter = {
        "actor.mbox": f"mailto:{user_id}-polaris-id@polaris.com",
        "verb.id": verb_id,
        "object.id": object_id,
    }
    description = f"Delete statements for user {user_id} and verbId {verb_id} and objectId {object_id}"
    return [statement_filter, description]


def get_scope_filter(user_id, scope):

    if scope.get("verbId", None) is not None and scope.get("objectId", None) is None:
        return rm_user_verb_statements_filter(user_id, scope["verbId"])
    if (
        scope.get("verbId", None) is not None
        and scope.get("objectId", None) is not None
    ):
        return rm_user_object_statements_filter(
            user_id, scope["verbId"], scope["objectId"]
        )
    return rm_user_statements_filter(user_id)


class CreateDataRemovalJob(APIView):
    """
    Create data removal job for a given scope.
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        [statement_filter, description] = get_scope_filter(
            request.user.id, request.data["scope"]
        )
        if request.data["immedialty"]:
            DataRemovalJob.objects.create(
                user=request.user,
                filter=statement_filter,
                description=description,
                execute_at=timezone.now(),
            )
        else:
            DataRemovalJob.objects.create(
                user=request.user,
                filter=statement_filter,
                description=description,
                execute_at=timezone.now()
                + timedelta(days=int(settings.DATA_DISCLOSURE_EXPIRATION)),
            )
        return JsonResponse(
            {"message": "data removal job created."},
            safe=False,
            status=status.HTTP_200_OK,
        )


class GetDataRemovalJobs(APIView):
    """
    List exisiting data removal jobs.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        jobs = list(
            DataRemovalJob.objects.filter(user=request.user)
            .order_by("-created_at")
            .values()
        )
        return JsonResponse(jobs, safe=False, status=status.HTTP_200_OK)
