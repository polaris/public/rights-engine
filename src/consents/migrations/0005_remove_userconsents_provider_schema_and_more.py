from django.db import migrations, models
import django.db.models.deletion

def migrate_user_consent_to_verb(apps, schema_editor):
    UserConsents = apps.get_model('consents', 'UserConsents')
    Verb = apps.get_model('providers', 'Verb')
    for consent in UserConsents.objects.all():
        schema = consent.provider_schema
        verb_id = consent.verb
        verb = Verb.objects.get(verb_id=verb_id, provider_schema=schema)
        consent.verb = verb.id
        group = verb.providerverbgroup_set.first()
        consent.verb_group_id = group.id
        consent.save()

class Migration(migrations.Migration):

    dependencies = [
        ('providers', '0008_verb_remove_providerschema_essential_verbs_and_more'),
        ('consents', '0004_userconsents_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='userconsents',
            name='verb_group',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE,
                                    to='providers.providerverbgroup'),
        ),
        migrations.RunPython(migrate_user_consent_to_verb, reverse_code=migrations.RunPython.noop),
        migrations.AlterField(
            model_name='userconsents',
            name='verb',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='providers.verb'),
        ),
        migrations.RemoveField(
            model_name='userconsents',
            name='provider_schema',
        ),
        migrations.AlterField( # now we can remove the null default
            model_name='userconsents',
            name='verb_group',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                    to='providers.providerverbgroup'),
        )
    ]
