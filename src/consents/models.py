from email.policy import default

from django.conf import settings
from django.db import models

from providers.models import Provider, ProviderVerbGroup, Verb


class UserConsents(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    consented = models.BooleanField()
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    verb_group = models.ForeignKey(ProviderVerbGroup, default=None, on_delete=models.CASCADE)
    verb = models.ForeignKey(Verb, on_delete=models.CASCADE)
    object = models.JSONField()
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return "User consent for verb " + str(self.verb)
