from django.urls import path

from . import views

urlpatterns = [
    path('provider', views.GetProviderSchemasView.as_view()),
    path('provider-status/third-party', views.GetProviderStatusThirdPartyView.as_view()),
    path('provider/create', views.CreateProviderConsentView.as_view()),
    path('provider/<provider_id>/create-verb-group', views.CreateProviderVerbGroupView.as_view()), # TODO
    path('provider/<provider_id>/verb-groups', views.GetProviderVerbGroupsView.as_view()), # TODO
    path('user/save', views.SaveUserConsentView.as_view()),
    path('user/create', views.CreateUserConsentView.as_view()),
    path('user/create-via-connect-service', views.CreateUserConsentViaConnectServiceView.as_view()),
    path('user/status', views.GetUserConsentStatusView.as_view()),
    path('user/analytics-tokens', views.GetUserConsentAnalyticsTokens.as_view()),
    path('user/analytics-tokens/consent', views.SaveUserConsentAnalyticsTokens.as_view()),
    path('user/providers', views.GetUserConsentProviders.as_view()),
    path('user/history', views.GetUserConsentHistoryView.as_view()),
    path('user/update-consent-group-active', views.UpdateUserConsentGroupView.as_view()),
    path('user/revoke-consent-group', views.RevokeUserConsentGroupView.as_view()),
    path('user/<provider_id>', views.GetUserConsentView.as_view()),
    path('user/status/<user_id>/third-party', views.GetUsersConsentsThirdPartyView.as_view()),
    path('user/save/third-party', views.SaveUserConsentThirdPartyView.as_view()),
]