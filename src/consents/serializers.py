import json
from typing import Dict

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from providers.models import AnalyticsToken, Provider, ProviderSchema, Verb, VerbObject, ProviderVerbGroup
from .models import UserConsents

class ProviderSchemaSerializer(serializers.ModelSerializer):
    definition = serializers.SerializerMethodField("_definition")

    def _definition(self, obj: ProviderSchema):
        return {
            "id": obj.provider.definition_id,
            "name": obj.provider.name,
            "description": obj.provider.description,
            "verbs": VerbSerializer(obj.verbs(), many=True).data,
            "essential_verbs": VerbSerializer(obj.essential_verbs(), many=True).data,
            "additional_lrs": obj.additional_lrs,
        }

    class Meta:
        model = ProviderSchema
        fields = "__all__"


class VerbSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField("_id")
    label = serializers.SerializerMethodField("_label")
    description = serializers.SerializerMethodField("_description")
    defaultConsent = serializers.SerializerMethodField("_defaultConsent")
    objects = serializers.SerializerMethodField("_objects")

    def _id(self, obj: Verb):
        return obj.verb_id

    def _label(self, obj: Verb):
        return obj.label

    def _description(self, obj: Verb):
        return obj.description

    def _defaultConsent(self, obj: Verb):
        return obj.default_consent

    def _objects(self, obj: Verb):
        return VerbObjectSerializer(VerbObject.objects.filter(verb=obj), many=True).data

    class Meta:
        model = Verb
        exclude = ["verb_id", "default_consent", "provider", "provider_schema", "active",
                   "essential", "allow_anonymized_collection"]


class VerbObjectSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField("_id")
    objectType = serializers.SerializerMethodField("_objectType")
    defaultConsent = serializers.SerializerMethodField("_defaultConsent")
    definition = serializers.SerializerMethodField("_definition")

    def _id(self, obj: VerbObject):
        return obj.object_id

    def _objectType(self, obj: VerbObject):
        return obj.object_type

    def _definition(self, obj: VerbObject):
        return obj.definition

    def _defaultConsent(self, obj: VerbObject):
        return obj.verb.default_consent

    class Meta:
        model = VerbObject
        exclude = ["object_id", "verb"]

class ProviderVerbGroupSerializer(serializers.ModelSerializer):
    verbs = serializers.SerializerMethodField("_verbs")
    purposeOfCollection = serializers.SerializerMethodField("_purposeOfCollection")

    def _verbs(self, obj):
        return VerbSerializer(obj.verbs.all(), many=True).data

    def _purposeOfCollection(self, obj):
        return obj.purpose_of_collection

    class Meta:
        model = ProviderVerbGroup
        exclude = ["purpose_of_collection"]

class ProvidersSerializer(serializers.ModelSerializer):
    versions = serializers.SerializerMethodField("_versions")
    groups = serializers.SerializerMethodField("_groups")

    def _versions(self, obj):
        schemas = ProviderSchema.objects.filter(provider=obj).order_by("-created")
        serializer = ProviderSchemaSerializer(schemas, many=True)
        return serializer.data

    def _groups(self, obj):
        groups = ProviderVerbGroup.objects.filter(provider=obj).order_by("-created")
        serializer = ProviderVerbGroupSerializer(groups, many=True)
        return serializer.data

    class Meta:
        model = Provider
        fields = ["id", "name", "groups", "versions"]


class UserVerbSerializer(serializers.Serializer):
    id = serializers.CharField()
    group_id = serializers.PrimaryKeyRelatedField(
        queryset=ProviderVerbGroup.objects.all()
    )
    consented = serializers.BooleanField()
    objects = serializers.CharField()


class SaveUserConsentSerializer(serializers.Serializer):

    def validate(self, data):
        """
        Validate existence of submitted groups.
        """
        for group_id in data["groups"]:
            if not ProviderVerbGroup.objects.filter(id=group_id):
                raise serializers.ValidationError(
                    f"Group with id {group_id} does not exist."
                )
        return data

    groups = serializers.ListSerializer(child=serializers.IntegerField())


class GroupVerbSerializer(serializers.Serializer):
    id = serializers.CharField()


class CreateProviderVerbGroupSerializer(serializers.Serializer):
    def __init__(self, provider, **kwargs):
        super().__init__(**kwargs)
        self.provider = provider

    def validate(self, data):
        """
        Validate the existence of verbs associated with the new / updated group
        and check if the chosen group ID is available.
        """
        # new or updated groups have to use the newest schema
        provider_schema = ProviderSchema.objects.get(provider=self.provider, superseded_by__isnull=True)
        for verb_data in data["verbs"]:
            try:
                # we just validate verb IDs here since verbs are retrieved via the verb ID later
                verb = Verb.objects.filter(verb_id=verb_data["id"], active=True,
                                           provider=self.provider,
                                           provider_schema=provider_schema)
            except ObjectDoesNotExist:
                raise serializers.ValidationError(
                    f"Verb {verb['id']} not found in newest schema."
                )
        return data
    id = serializers.CharField(allow_blank=True)
    label = serializers.CharField()
    description = serializers.CharField()
    purposeOfCollection = serializers.CharField()
    requiresConsent = serializers.BooleanField()
    verbs = serializers.ListSerializer(child=GroupVerbSerializer())

class CreateUserSerializer(serializers.Serializer):
    email = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()

class CreateUserShibbolethSerializer(serializers.Serializer):
    email = serializers.CharField()

class UserConsentsSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    consented = serializers.BooleanField()
    verb = serializers.SerializerMethodField("_verb")
    object = serializers.CharField()
    active = serializers.BooleanField()

    def _verb(self, obj):
        return VerbSerializer(obj.verb).data

    class Meta:
        model = UserConsents
        exclude = ["created", "updated", "provider", "user", "verb_group"]