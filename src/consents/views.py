import hashlib
import json
import string
import random
import os
import secrets
import time
from datetime import timedelta, datetime
from pydoc import describe

from django.core.cache import cache
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import JsonResponse
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from rest_framework import exceptions, generics, permissions, status
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from backend.role_permission import IsProvider
from consents.serializers import (ProviderSchemaSerializer,
                                  ProvidersSerializer,
                                  SaveUserConsentSerializer, CreateUserSerializer, CreateUserShibbolethSerializer,
                                  ProviderVerbGroupSerializer, VerbObjectSerializer, VerbSerializer,
                                  CreateProviderVerbGroupSerializer, UserConsentsSerializer)
from providers.models import (AnalyticsToken, AnalyticsTokenVerb, Provider, ProviderAuthorization, ProviderSchema, Verb,
                              VerbObject, ProviderVerbGroup)
from providers.serializers import (AnalyticsTokenSerializer, ConsentUserVerbThirdPartySerializer,
                                   GetUsersConsentsThirdPartySerializer)
from users.models import CustomUser
from xapi.views import shib_connector_resolver, shib_connector_resolver_to_pairwaise_id

from .models import UserConsents

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))


class JsonUploadParser(FileUploadParser):
    media_type = "application/json"


def render_provider_consent(provider_schema, supersedes=None):
    return {
        "id": provider_schema.provider.id,
        "name": provider_schema.provider.name,
        "superseded_by": provider_schema.superseded_by.id
        if provider_schema.superseded_by
        else None,
        "definition": provider_schema.definition,
        "createdAt": provider_schema.created,
        "supersedes": render_provider_consent(supersedes) if supersedes else None,
    }


def find_user_verb(verb_id, user_verbs):
    for user_verb in user_verbs:
        if user_verb.verb.verb_id == verb_id:
            return user_verb

    return None


def merge_consents(provider_schema: ProviderSchema, user_verbs):
    """
    Merges provider schema with actual user consent settings
    """
    #print(user_verbs)
    user_consent = {
        "id": provider_schema.provider.id,
        "name": provider_schema.provider.name,
        "description": provider_schema.provider.description,
        "groups": ProviderVerbGroupSerializer(provider_schema.groups(), many=True).data, # all available groups
        "essential_verbs": VerbSerializer(provider_schema.essential_verbs(), many=True).data,
    }
    for group in user_consent.get("groups"):
        all_consented = True
        for verb in group.get('verbs'):
            user_verb = find_user_verb(verb["id"], user_verbs)
            if user_verb:
                verb.update({"consented": user_verb.consented})
                verb.update({"objects": json.loads(user_verb.object)})
                all_consented = all_consented and user_verb.consented
            else: all_consented = False
        group["consented"] = all_consented
    return user_consent


def get_user_consent(user, provider_id):
    """
    Get all user consents for a given provider and merge them in a provider schema.
    """
    user_consents = UserConsents.objects.filter(user=user, provider__pk=provider_id, active=True)

    if user_consents.first():
        provider_schema = user_consents.first().verb.provider_schema
        user_consent_definition = merge_consents(
            provider_schema, user_consents
        )
        return user_consent_definition
    return None


def save_user_consent(user, provider_schema, verbs):
    """
    Inactivates existing user consents for user and provider.
    Afterward, new user consents are created.
    """
    old_user_consents = UserConsents.objects.filter(
        user=user, provider=provider_schema.provider.id
    ).update(active=False)

    for verb_data in verbs:
        verb = Verb.objects.get(verb_id=verb_data["id"], provider_schema=provider_schema, active=True)
        try:
            UserConsents.objects.get(user=user, verb=verb, provider=provider_schema.provider, active=True)
            # user consent for verb already exists, skip
        except UserConsents.DoesNotExist:
            UserConsents.objects.create(
                user=user,
                consented=verb_data.get("consented"),
                provider=provider_schema.provider,
                verb_group=verb_data["group_id"],
                verb=verb,
                object=verb_data.get("objects"),
            )



def group_consents_by_date(consents):
    """
    Groups consent declaration by group and creation time (windows size 1 minute)
    to identify those likely created within the same action.
    """
    # Ensure objects are sorted by their created timestamp
    consents = sorted(consents, key=lambda con: con.created)

    # group by creation time and verb group
    grouped_consents = []
    for consent in consents:
        created_minute = consent.created.replace(second=0, microsecond=0)
        # Find existing grouping if available
        for group in grouped_consents:
            if group['created'] == created_minute and group['group'] == consent.verb_group:
                # add consent to existing grouping
                group['consents'].append(consent)
                group["updated"] = max(consent.updated, group["updated"])
                break
        else:
            # if no grouping was found, create a new one
            grouped_consents.append({
                'created': created_minute,
                'updated': created_minute,
                'group': consent.verb_group,
                'consents': [consent]
            })

    # restructure for frontend
    for grouping in grouped_consents:
        verb_ids = [consent.verb.id for consent in grouping["consents"]]
        # serialize consents
        grouping["consents"] = UserConsentsSerializer(grouping["consents"], many=True).data
        # get all verbs belonging to the group
        for verb in grouping["group"].verbs.all():
            if verb.id in verb_ids:
                continue
            # if the verb wasn't explicitly accepted, create a dummy non-consent
            grouping["consents"].append({
                "id": -1,
                "consented": False,
                "verb": VerbSerializer(verb).data,
                "object": "[]",
                "active": True,
            })
        provider = grouping["group"].provider
        grouping["group"] = ProviderVerbGroupSerializer(grouping["group"]).data
        grouping["group"]["consents"] = grouping["consents"]
        grouping["group"]["providerName"] = provider.name
        grouping["group"]["active"] = all([consent["active"] for consent in grouping["group"]["consents"]])
        grouping["group"]["consented"] = all([consent["consented"] for consent in grouping["group"]["consents"]])
        del grouping["consents"]
        del grouping["group"]["verbs"]
        del grouping["group"]["provider"]
    return grouped_consents


class GetProviderSchemasView(generics.ListAPIView):
    """
    Lists all providers including schema history.
    """

    queryset = Provider.objects.all()
    serializer_class = ProvidersSerializer
    permission_classes = [IsAuthenticated]


class GetProviderVerbGroupsView(generics.ListAPIView):
    """
    Lists all verb groups for the current provider.
    """
    def get(self, request):
        application_token = request.headers.get("Authorization", "").split("Basic ")[-1]
        provider = ProviderAuthorization.objects.filter(key=application_token).first()
        if provider is None:
            return JsonResponse(
                {"message": "invalid access token"},
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )
        verb_groups = ProviderVerbGroup.objects.filter(provider=provider.provider).all()
        serializer = ProviderVerbGroupSerializer(verb_groups, many=True)
        return JsonResponse(serializer.data, safe=False, status=status.HTTP_200_OK)


class GetProviderStatusThirdPartyView(APIView):
    """
    Allows a third party to query the details of one's provider.
    """

    def get(self, request):
        application_token = request.headers.get("Authorization", "").split("Basic ")[-1]
        provider = ProviderAuthorization.objects.filter(key=application_token).first()
        if provider is None:
            return JsonResponse(
                {"message": "invalid access token"},
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )
        serializer = ProvidersSerializer(provider.provider)
        return JsonResponse(serializer.data, safe=False, status=status.HTTP_200_OK)


class CreateProviderConsentView(APIView):
    """
    Providers can upload provider schemas. In case a provider schema already exists,
    the existing provider schema will reference the new provider schema.
    The latest provider schema has no superseding provider schema associated.
    """

    permission_classes = [IsAuthenticated, IsProvider]
    parser_class = (JsonUploadParser,)

    def put(self, request, format=None):
        file = request.data.get("provider-schema", None)
        if file == None:
            return JsonResponse(
                {"message": "invalid provider schema file"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # Create or update provider
        try:
            with open(
                os.path.join(PROJECT_PATH, "static/provider_schema.schema.json")
            ) as f:
                provider_schema = json.load(file.file)
                validate(instance=provider_schema, schema=json.load(f))

                providerModel = Provider.objects.get(
                    definition_id=provider_schema["id"]
                )  # maybe TODO: match via name, not definition_id, iff definition_id can change each definition
                providerModel.description = provider_schema["description"]
                providerModel.save()
        except OSError:
            return JsonResponse(
                {"message": "Schema missing on server."},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        except ValidationError as e:
            return JsonResponse(
                {"message": "Provider schema validation failed.", "errors": str(e)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except json.JSONDecodeError as e:
            return JsonResponse(
                {"message": "Decoding json failed.", "errors": str(e)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except ValueError as e:
            return JsonResponse(
                {"message": str(e)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except ObjectDoesNotExist:
            providerModel = Provider.objects.create(
                definition_id=provider_schema["id"],
                name=provider_schema["name"],
                description=provider_schema["description"],
            )
            auth_key = secrets.token_hex(
                32
            )  # generate random auth key for new provider

            ProviderAuthorization.objects.create(provider=providerModel, key=auth_key)

        # update provider
        providerModel.description = provider_schema["description"]
        providerModel.name = provider_schema["name"]
        providerModel.save()

        try:
            precedingProviderSchema = ProviderSchema.objects.get(
                provider=providerModel, superseded_by__isnull=True
            )
        except ObjectDoesNotExist:
            precedingProviderSchema = None
        # Create new provider schema and link older schema version to new one (if old one exists)
        providerSchemaModel = ProviderSchema.objects.create(
            provider=providerModel,
            additional_lrs=provider_schema["additionalLrs"] if "additionalLrs" in provider_schema.keys() else []
        )
        # create regular verbs
        for verb_schema in provider_schema["verbs"]:
            verb = Verb.objects.create(
                provider=providerModel,
                provider_schema=providerSchemaModel,
                verb_id=verb_schema["id"],
                label=verb_schema["label"],
                description=verb_schema["description"],
                default_consent=verb_schema["defaultConsent"],
                essential=False
            )
            for verb_object_schema in verb_schema["objects"]:
                obj = VerbObject.objects.create(
                    verb=verb,
                    object_id=verb_object_schema["id"],
                    object_type=verb_object_schema["objectType"] if "objectType" in verb_object_schema.keys() else "Activity",
                    matching=verb_object_schema["matching"] if "matching" in verb_object_schema.keys() else "definitionType",
                    definition=verb_object_schema["definition"])
        # create essential verbs
        for verb_schema in provider_schema["essentialVerbs"]:
            verb = Verb.objects.create(
                provider=providerModel,
                provider_schema=providerSchemaModel,
                verb_id=verb_schema["id"],
                label=verb_schema["label"],
                description=verb_schema["description"],
                default_consent=verb_schema["defaultConsent"],
                essential=True,
            )
            for verb_object_schema in verb_schema["objects"]:
                obj = VerbObject.objects.create(
                    verb=verb,
                    object_id=verb_object_schema["id"],
                    object_type=verb_object_schema["objectType"] if "objectType" in verb_object_schema.keys() else "Activity",
                    matching=verb_object_schema["matching"] if "matching" in verb_object_schema.keys() else "definitionType",
                    definition=verb_object_schema["definition"]
                )
        # update old schema to reference the new one
        if precedingProviderSchema:
            Verb.objects.filter(provider=providerModel, provider_schema=precedingProviderSchema).update(
                active=False)
            precedingProviderSchema.superseded_by = providerSchemaModel
            precedingProviderSchema.save()
            Verb.objects.filter(provider_schema=precedingProviderSchema).update(active=False)


        return JsonResponse(
            {"message": "provider schema processed"}, status=status.HTTP_201_CREATED
        )


class CreateProviderVerbGroupView(APIView):
    permission_classes = [IsAuthenticated, IsProvider]

    def post(self, request, provider_id):
        provider = Provider.objects.get(id=provider_id)
        serializer = CreateProviderVerbGroupSerializer(data=request.data, provider=provider)
        serializer.is_valid(raise_exception=True)

        provider_schema = ProviderSchema.objects.get(provider=provider, superseded_by__isnull=True)

        verbs = Verb.objects.filter(verb_id__in=[verb["id"] for verb in serializer.validated_data["verbs"]],
                                    active=True, provider=provider, provider_schema=provider_schema).all()
        if len(verbs) != len(serializer.validated_data["verbs"]):
            return JsonResponse(
                {"message": "at least one of the given verbs could not be found"},
                safe=False,
                status=status.HTTP_400_BAD_REQUEST
            )

        # update existing group
        if ProviderVerbGroup.objects.filter(provider=provider, provider_schema=provider_schema,
                                            group_id=serializer.validated_data["id"]).first() is not None:
            group = ProviderVerbGroup.objects.get(group_id=serializer.validated_data["id"],
                                                  provider=provider, provider_schema=provider_schema)
            group.group_id = serializer.validated_data["id"]
            group.label = serializer.validated_data["label"]
            group.description = serializer.validated_data["description"]
            group.purpose_of_collection = serializer.validated_data["purposeOfCollection"]
            group.requires_consent = serializer.validated_data["requiresConsent"]
            group.save()
        else:
            # create new group
            group = ProviderVerbGroup.objects.create(
                provider = provider,
                provider_schema = provider_schema,
                group_id = hashlib.sha3_256(serializer.validated_data["label"].encode("utf-8")).hexdigest(),
                label = serializer.validated_data["label"],
                description = serializer.validated_data["description"],
                purpose_of_collection = serializer.validated_data["purposeOfCollection"],
                requires_consent = serializer.validated_data["requiresConsent"],
            )
        # sync verbs
        group.verbs.set(verbs)

        return JsonResponse(
            {
                "message": "group has been saved",
                "group": ProviderVerbGroupSerializer(group).data,
            },
            safe=False,
            status=status.HTTP_200_OK,
        )


class GetUserConsentView(APIView):
    """
    Users are able to make a consent decision for verb groups.
    This endpoint returns the user consents along with the associated verb groups for a given provider,
    which may not always belong to the latest version of the corresponding provider schema.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, provider_id):
        user = request.user
        try:
            user_consent = get_user_consent(user, provider_id)

            if user_consent is None:
                return JsonResponse(
                    {
                        "consent": None,
                        "paused_data_recording": user.paused_data_recording,
                    },
                    safe=False,
                    status=status.HTTP_200_OK,
                )
            else:
                accepted_provider_schemas = user.accepted_provider_schemas.all()
                provider_schemas = list(
                    filter(
                        lambda schema: schema.provider.id == int(provider_id),
                        accepted_provider_schemas,
                    )
                )

                provider_schema = (
                    ProviderSchemaSerializer(provider_schemas, many=True).data[0]
                    if len(provider_schemas) > 0
                    else None
                )
                return JsonResponse(
                    {
                        "consent": user_consent,
                        "provider_schema": provider_schema,
                        "paused_data_recording": user.paused_data_recording,
                    },
                    safe=False,
                    status=status.HTTP_200_OK,
                )
        except ObjectDoesNotExist:
            return JsonResponse(
                {
                    "message": "User has no consent declaration record.",
                    "no_consent_record": True,
                },
                safe=False,
                status=status.HTTP_400_BAD_REQUEST,
            )


class GetUserConsentHistoryView(APIView):
    """
    This endpoint returns all consents declared by the user, grouped by group and date.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = request.user
        try:
            user_consents = UserConsents.objects.filter(user=user).order_by('created')

            if not user_consents.first():
                return JsonResponse(
                    {
                        "consents": [],
                    },
                    safe=False,
                    status=status.HTTP_200_OK,
                )
            else:
                consents_grouped = group_consents_by_date(consents=user_consents)
                return JsonResponse(
                    {
                        "groups": consents_grouped
                    },
                    safe=False,
                    status=status.HTTP_200_OK,
                )
        except ObjectDoesNotExist:
            return JsonResponse(
                {
                    "message": "User has no consent declaration record.",
                    "no_consent_record": True,
                },
                safe=False,
                status=status.HTTP_400_BAD_REQUEST,
            )

class UpdateUserConsentGroupView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user

        if "id" not in request.data.keys() or "active" not in request.data.keys():
            return JsonResponse(
                {
                    "message": "Group ID and active status are required.",
                },
                safe=False,
                status=status.HTTP_400_BAD_REQUEST,
            )

        UserConsents.objects.filter(user=user, verb_group_id=request.data["id"]).update(active=request.data["active"], updated=datetime.now())
        return JsonResponse(
            {
                "message": "Group status updated for current user."
            },
            safe=False,
            status=status.HTTP_200_OK,
        )

class RevokeUserConsentGroupView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        if "id" not in request.data.keys():
            return JsonResponse(
                {
                    "message": "Group ID is required.",
                },
                safe=False,
                status=status.HTTP_400_BAD_REQUEST,
            )
        UserConsents.objects.filter(user=user, verb_group_id=request.data["id"]).update(consented=False,
                                                                                        updated=datetime.now())
        # TODO: anonymize existing data for the given group and user
        return JsonResponse(
            {
                "message": "Group status updated for current user."
            },
            safe=False,
            status=status.HTTP_200_OK,
        )

class CreateUserConsentView(APIView):
    
    def post(self, request):
        application_token = request.headers.get("Authorization", "").split("Basic ")[-1]
        provider = ProviderAuthorization.objects.filter(key=application_token).first()
        if provider is None:
            return JsonResponse(
                {"message": "invalid access token"},
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )
       
        serializer = CreateUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True) 
        
        email = serializer.validated_data["email"]

        if CustomUser.objects.filter(email=email).first() is None:
           user = CustomUser.objects.create(
                email=email,
                first_name=serializer.validated_data["first_name"],
                last_name=serializer.validated_data["last_name"]
           )

        return JsonResponse(
                    {
                        "user": "user is created",
                    },
                    safe=False,
                    status=status.HTTP_200_OK,
                )
    
class CreateUserConsentViaConnectServiceView(APIView):
    
    def post(self, request):
        application_token = request.headers.get("Authorization", "").split("Basic ")[-1]
        provider = ProviderAuthorization.objects.filter(key=application_token).first()
        if provider is None:
            return JsonResponse(
                {"message": "invalid access token"},
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )
       
        serializer = CreateUserShibbolethSerializer(data=request.data)
        serializer.is_valid(raise_exception=True) 
        
        email = serializer.validated_data["email"]
        shib_id = shib_connector_resolver_to_pairwaise_id(email=email, provider=provider)

        user = CustomUser.objects.filter(shibboleth_connector_identifier=shib_id).first()

        message = "user exists"
        if not user:
            lock_key = f"user_creation_lock_{shib_id}"  # "mutex" to prevent race conditions using a cache entry
            if cache.add(lock_key, "locked", 2):  # 2 is a timeout value, after wich the cache entry is deleted
                try:
                    user = CustomUser.objects.create(
                        shibboleth_connector_identifier=shib_id, # this is the pairwise id
                        email=''.join(random.choices(string.ascii_uppercase + string.digits, k=8)) + "@manual-created.polaris",
                        first_name=''.join(random.choices(string.ascii_uppercase + string.digits, k=8)),
                        last_name=''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
                    )
                except Exception as e:
                    return JsonResponse({"message": "user could not be created"}, safe=False,
                                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                finally:
                    message = "user has been created"
                    cache.delete(lock_key)
            else:
                for _ in range(4):  # Wait up to 2x lock_timeout
                    time.sleep(0.1)
                    if not cache.get(lock_key):
                        break
                # Fetch the user created by another process
                user = CustomUser.objects.filter(shibboleth_connector_identifier=shib_id).first()
                if not user:
                    return JsonResponse({"message": "user could not be created"}, safe=False,
                                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return JsonResponse(
                    {
                        "message": message,
                    },
                    safe=False,
                    status=status.HTTP_200_OK,
                )

class SaveUserConsentView(APIView):
    """
    Saves a list of user consents, submitted as a list of group ids.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user

        serializer = SaveUserConsentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        for group_id in serializer.validated_data["groups"]:
            group = ProviderVerbGroup.objects.filter(pk=group_id).first()
            for verb in group.verbs.all():
                object_consent_data = []
                for obj in verb.verbobject_set.all():
                    object_consent_data.append({
                        "id": obj.object_id,
                        "label": obj.label,
                        "defaultConsent": verb.default_consent,
                        "matching": obj.matching,
                        "definition": obj.definition,
                        "consented": True
                    })
                consent = UserConsents.objects.create(
                    user = user,
                    consented = True,
                    provider = group.provider,
                    verb_group = group,
                    verb = verb,
                    object = object_consent_data,
                    active = True
                )

        return JsonResponse(
            {"message": "user consents saved"},
            status=status.HTTP_200_OK,
        )

class SaveUserConsentAnalyticsTokens(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        token = AnalyticsToken.objects.filter(id = request.data["analyticTokenId"]).first()
        if token is None:
            return JsonResponse(
                {"message": "not found"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        verbs = AnalyticsTokenVerb.objects.filter(analytics_token_id = token.id)
        for verb in verbs:
            consent = UserConsents.objects.filter(verb = verb.verb, user_id=user.id, provider_id = verb.provider, active=True).first()
            consent.consented = True
            consent.save()

        return JsonResponse(
            {"message": "user consent saved"},
            status=status.HTTP_200_OK,
        )

class GetUserConsentAnalyticsTokens(generics.ListAPIView):
    """
    Lists analytics tokens for the user
    """
    def get_queryset(self):
        return AnalyticsToken.objects.filter()

   # queryset = AnalyticsToken.objects.filter(creator=request.user)
    serializer_class = AnalyticsTokenSerializer
    permission_classes = [IsAuthenticated]

class GetUserConsentProviders(generics.ListAPIView):
    """
    Lists providers for the user
    """
    def get_queryset(self):
        return Provider.objects.filter()

   # queryset = AnalyticsToken.objects.filter(creator=request.user)
    serializer_class = ProvidersSerializer
    permission_classes = [IsAuthenticated]


class GetUserConsentStatusView(APIView):
    """
    Checks whether user accepted the latest provider schema for each provider, an old consent declaration
    or none and returns "latest", "outdated" or "none" respectively.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request):
        providers = Provider.objects.all()
        user = request.user

        result = {}
        accepted_user_schemas = user.accepted_provider_schemas.all()
        user_schema_ids = [schema.id for schema in accepted_user_schemas]

        for provider in providers:
            provider_latest_schema = ProviderSchema.objects.get(
                provider=provider, superseded_by=None
            )

            if len(accepted_user_schemas) == 0:
                result[provider.id] = {"status": "none"}
            else:
                acceptance_status = (
                    "latest"
                    if provider_latest_schema.id in user_schema_ids
                    else "outdated"
                )
                result[provider.id] = {
                    "status": acceptance_status,
                    "userConsent": get_user_consent(user, provider.id),
                }
        return JsonResponse({"result": result}, safe=False, status=status.HTTP_200_OK)


class GetUsersConsentsThirdPartyView(APIView):
    """
    Obtain user consent for a single user.
    This endpoint should only be used by a third party.
    """

    def get(self, request, user_id):
        application_token = request.headers.get("Authorization", "").split("Basic ")[-1]
        provider = ProviderAuthorization.objects.filter(key=application_token).first()
        if provider is None:
            return JsonResponse(
                {"message": "invalid access token"},
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )
        
        email = user_id      
        email = shib_connector_resolver(email=email, provider=provider)
        serializer = GetUsersConsentsThirdPartySerializer(data={"user": email})
        serializer.is_valid(raise_exception=True)

        user = CustomUser.objects.filter(email=email).first()
        try:
            user_consent = get_user_consent(user, provider.provider.id)

            if user_consent is None:
                return JsonResponse(
                    {
                        "consent": None,
                        "paused_data_recording": user.paused_data_recording,
                    },
                    safe=False,
                    status=status.HTTP_200_OK,
                )
            else:
                accepted_provider_schemas = user.accepted_provider_schemas.all()
                provider_schemas = list(
                    filter(
                        lambda schema: schema.provider.id == int(provider.provider.id),
                        accepted_provider_schemas,
                    )
                )

                provider_schema = (
                    ProviderSchemaSerializer(provider_schemas, many=True).data[0]
                    if len(provider_schemas) > 0
                    else None
                )
                return JsonResponse(
                    {
                        "consent": user_consent,
                        "provider_schema": provider_schema,
                        "paused_data_recording": user.paused_data_recording,
                    },
                    safe=False,
                    status=status.HTTP_200_OK,
                )
        except ObjectDoesNotExist:
            return JsonResponse(
                {
                    "message": "User has no consent declaration record.",
                    "no_consent_record": True,
                },
                safe=False,
                status=status.HTTP_400_BAD_REQUEST,
            )


class SaveUserConsentThirdPartyView(APIView):
    """
    Saves a user consent, with authentication taking place via application tokens.
    The provider schema id for each provider must always refer to the latest schema version of the respective provider,
    otherwise the request will be rejected.
    """

    def post(self, request):
        application_token = request.headers.get("Authorization", "").split("Basic ")[-1]
        provider = ProviderAuthorization.objects.filter(key=application_token).first()
        if provider is None:
            return JsonResponse(
                {"message": "invalid access token"},
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )

        serializer = ConsentUserVerbThirdPartySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data["user_id"]        
        email = shib_connector_resolver(email=email, provider=provider)
        user = CustomUser.objects.filter(email=email).first()


        # Validate user provider setting refers to the latest provider schema id
        latest_provider_schema = ProviderSchema.objects.get(
            provider=provider.provider, superseded_by=None
        )

        if (
            serializer.validated_data["provider_schema_id"].id
            != latest_provider_schema.id
        ):
            return JsonResponse(
                {"message": "provider schema id isn't latest provider schema id"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        save_user_consent(
            user,
            serializer.validated_data["provider_schema_id"],
            serializer.validated_data["verbs"],
        )

        # Add provider schema id to accepted ones for user
        user.accepted_provider_schemas.add(
            serializer.validated_data["provider_schema_id"]
        )
        user.save()
        return JsonResponse(
            {"message": "user consent saved"}, status=status.HTTP_200_OK
        )
