import json
import os
from io import StringIO

from rest_framework.test import APIClient
from rolepermissions.roles import assign_role

from consents.tests.tests_consent_operations import BaseTestCase
from providers.models import ProviderAuthorization, Provider, ProviderVerbGroup, ProviderSchema
from users.models import CustomUser

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))


class TestThirdPartyGetUserStatus(BaseTestCase):
    verb_groups = [
        {
            "provider_id": 1,
            "id": "default_group",
            "label": "Default group",
            "description": "default",
            "showVerbDetails": True,
            "purposeOfCollection": "Lorem Ipsum",
            "requiresConsent": True,
            "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"}
            ]
        },
    ]
    def setUp(self):
        normal_user = CustomUser.objects.create_user(
            self.test_user_email, self.test_user_password
        )
        provider_user = CustomUser.objects.create_user(
            self.test_provider_email, self.test_provider_password
        )

        assign_role(provider_user, "provider")
        assign_role(normal_user, "user")

        assign_role(provider_user, "provider_manager")

        response = self.client.post(
            "/api/v1/auth/token",
            {"email": self.test_user_email, "password": self.test_user_password},
        )  # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        user_token = response.data["access"]

        response = self.client.post(
            "/api/v1/auth/token",
            {
                "email": self.test_provider_email,
                "password": self.test_provider_password,
            },
        )  # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        provider_token = response.data["access"]

        self.user_client = APIClient()
        self.user_client.credentials(HTTP_AUTHORIZATION="Bearer " + user_token)

        self.provider_client = APIClient()
        self.provider_client.credentials(HTTP_AUTHORIZATION="Bearer " + provider_token)

        # Create provider schema
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # Get application token for created provider
        self.application_token = ProviderAuthorization.objects.get(
            provider__name="H5P"
        ).key
        self.third_party_client = APIClient()
        self.third_party_client.credentials(
            HTTP_AUTHORIZATION="Basic " + self.application_token
        )

        # create some verb groups
        for group in self.verb_groups:
            response = self.provider_client.post(
                "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
                group,
                format="json",
            )
            self.assertEqual(response.status_code, 200)

    def test_user_status_401(self):
        """
        Ensure requests including an invalid application token are rejected.
        """
        response = self.client.get(
            "/api/v1/consents/user/status/user1@polaris.com/third-party"
        )
        self.assertEqual(response.status_code, 401)

    def test_user_status_400_unkown_user(self):
        """
        Ensure endpoint returns 400, if user is unknown to the plattform.
        """
        response = self.third_party_client.get(
            "/api/v1/consents/user/status/user42@polaris.com/third-party"
        )
        self.assertEqual(response.status_code, 400)

    def test_user_status_empty(self):
        """
        Ensure endpoint returns empty user consent and active data recording.
        """
        response = self.third_party_client.get(
            f"/api/v1/consents/user/status/{self.test_user_email}/third-party"
        )
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {"consent": None, "paused_data_recording": False},
        )

    def test_user_status_none_empty(self):
        group_id = ProviderVerbGroup.objects.latest('id').id
        # Create user consent for first provider schema version
        first_provider_schema_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":false}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", first_provider_schema_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        # Obtain created user consent
        response = self.third_party_client.get(
            f"/api/v1/consents/user/status/{self.test_user_email}/third-party"
        )
        self.assertEqual(response.status_code, 200)

        responseDict = json.loads(str(response.content, encoding="utf8"))
        self.assertEqual(responseDict["consent"]["id"], 1)
        self.assertEqual(responseDict["paused_data_recording"], False)
        self.assertEqual(
            responseDict["consent"]["groups"][0]["verbs"][0]["consented"], False
        )
        self.assertEqual(
            responseDict["consent"]["groups"][0]["verbs"][1]["consented"], True
        )


class TestThirdPartyUserConsentUpdate(BaseTestCase):
    verb_groups = [
        {
            "provider_id": 1,
            "id": "default_group",
            "label": "Default group",
            "description": "default",
            "showVerbDetails": True,
            "purposeOfCollection": "Lorem Ipsum",
            "requiresConsent": True,
            "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"}
            ]
        },
    ]
    def setUp(self):
        normal_user = CustomUser.objects.create_user(
            self.test_user_email, self.test_user_password
        )
        provider_user = CustomUser.objects.create_user(
            self.test_provider_email, self.test_provider_password
        )

        assign_role(provider_user, "provider")
        assign_role(normal_user, "user")

        assign_role(provider_user, "provider_manager")

        response = self.client.post(
            "/api/v1/auth/token",
            {"email": self.test_user_email, "password": self.test_user_password},
        )  # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        user_token = response.data["access"]

        response = self.client.post(
            "/api/v1/auth/token",
            {
                "email": self.test_provider_email,
                "password": self.test_provider_password,
            },
        )  # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        provider_token = response.data["access"]

        self.user_client = APIClient()
        self.user_client.credentials(HTTP_AUTHORIZATION="Bearer " + user_token)

        self.provider_client = APIClient()
        self.provider_client.credentials(HTTP_AUTHORIZATION="Bearer " + provider_token)

        # Create provider schema
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # Get application token for created provider
        self.application_token = ProviderAuthorization.objects.get(
            provider__name="H5P"
        ).key
        self.third_party_client = APIClient()
        self.third_party_client.credentials(
            HTTP_AUTHORIZATION="Basic " + self.application_token
        )
        # create some verb groups
        for group in self.verb_groups:
            response = self.provider_client.post(
                "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
                group,
                format="json",
            )
            self.assertEqual(response.status_code, 200)

    def test_update_user_consent(self):
        """
        Ensure user consent gets updated via third party endpoint.
        """
        provider = ProviderAuthorization.objects.get(provider__name="H5P")
        group_id = ProviderVerbGroup.objects.latest('id').id
        payload = {
            "user_id": self.test_user_email,
            "provider_schema_id": provider.id,
            "verbs": [
                {
                    "group_id": group_id,
                    "id": "http://h5p.example.com/expapi/verbs/experienced",
                    "consented": False,
                    "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                },
                {
                    "group_id": group_id,
                    "id": "http://h5p.example.com/expapi/verbs/attempted",
                    "consented": True,
                    "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                },
                {
                    "group_id": group_id,
                    "id": "http://h5p.example.com/expapi/verbs/interacted",
                    "consented": True,
                    "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                },
                {
                    "group_id": group_id,
                    "id": "http://h5p.example.com/expapi/verbs/answered",
                    "consented": True,
                    "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                },
            ],
        }

        response = self.third_party_client.post(
            "/api/v1/consents/user/save/third-party", data=payload, format="json"
        )
        self.assertEqual(response.status_code, 200)

        # Get user consent
        response = self.third_party_client.get(
            f"/api/v1/consents/user/status/{self.test_user_email}/third-party"
        )
        self.assertEqual(response.status_code, 200)

        responseDict = json.loads(str(response.content, encoding="utf8"))
        self.assertEqual(responseDict["consent"]["id"], 1)
        self.assertEqual(responseDict["paused_data_recording"], False)
        self.assertEqual(
            responseDict["consent"]["groups"][0]["verbs"][0]["consented"], False
        )
        self.assertEqual(
            responseDict["consent"]["groups"][0]["verbs"][1]["consented"], True
        )
        self.assertEqual(
            responseDict["consent"]["groups"][0]["verbs"][2]["consented"], True
        )
        self.assertEqual(
            responseDict["consent"]["groups"][0]["verbs"][3]["consented"], True
        )

    def test_update_user_consent_fails_without_access_token(self):
        """
        Ensure unauthenticated request gets rejected.
        """
        group_id = ProviderVerbGroup.objects.latest('id').id
        payload = {
            "user_id": self.test_user_email,
            "provider_schema_id": 1,
            "verbs": [
                {
                    "group_id": group_id,
                    "id": "http://h5p.example.com/expapi/verbs/experienced",
                    "consented": False,
                    "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/attempted","consented":true,"objects":"[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/interacted","consented":true,"objects":"[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/answered","consented":false,"objects":"[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":false}]',
                }
            ],
        }

        client = APIClient()
        response = client.post(
            "/api/v1/consents/user/save/third-party", json.dumps(payload), format="json"
        )
        self.assertEqual(response.status_code, 401)
        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {"message": "invalid access token"},
        )

    def test_update_on_none_existing_provider_fails(self):
        """
        Ensure attempt to update verb on non-existing provider fails.
        """
        group_id = ProviderVerbGroup.objects.latest('id').id
        payload = {
            "user_id": self.test_user_email,
            "provider_schema_id": 2,
            "verbs": [
                {
                    "group_id": group_id,
                    "id": "http://h5p.example.com/expapi/verbs/experienced",
                    "consented": False,
                    "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/attempted","consented":true,"objects":"[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/interacted","consented":true,"objects":"[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/answered","consented":false,"objects":"[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":false}]',
                }
            ],
        }

        response = self.third_party_client.post(
            "/api/v1/consents/user/save/third-party", data=payload, format="json"
        )
        self.assertEqual(response.status_code, 400)
