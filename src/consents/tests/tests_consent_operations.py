import json
import os
from io import StringIO

from django.test import TransactionTestCase
from rest_framework.test import APIClient
from rolepermissions.roles import assign_role

from providers.models import Provider, ProviderAuthorization, ProviderSchema, ProviderVerbGroup
from users.models import CustomUser

from ..models import UserConsents

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))


class BaseTestCase(TransactionTestCase):
    reset_sequences = True

    test_user_email = "test@mail.com"
    test_user_password = "test123"

    test_provider_email = "test2@mail.com"
    test_provider_password = "test123"

    h5p_provider_schema = {
        "id": "h5p-0",
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "verbs": [
            {
                "id": "http://h5p.example.com/expapi/verbs/experienced",
                "label": "Experienced",
                "description": "Experienced",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                        "label": "1.1.1 Funktionen",
                        "defaultConsent": True,
                        "matching": "definitionType",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                            "name": {"enUS": "1.1.1 Funktionen"},
                        },
                    }
                ],
            },
            {
                "id": "http://h5p.example.com/expapi/verbs/attempted",
                "label": "Attempted",
                "description": "Attempted",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "label": "2.3.1 Funktion Zirkulationsleitung",
                        "defaultConsent": True,
                        "matching": "definitionType",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                            "name": {
                                "enUS": "2.3.1 Funktion Zirkulationsleitung"
                            },
                        },
                    }
                ],
            },
            {
                "id": "http://h5p.example.com/expapi/verbs/interacted",
                "label": "Interacted",
                "description": "Lorem ipsum",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                        "label": "1.2.3 Kappenventil",
                        "defaultConsent": True,
                        "matching": "definitionType",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                            "name": {"enUS": "1.2.3 Kappenventil"},
                        },
                    }
                ],
            },
            {
                "id": "http://h5p.example.com/expapi/verbs/answered",
                "label": "Answered",
                "description": "lorem ipsum",
                "defaultConsent": False,
                "objects": [
                    {
                        "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                        "label": "7.2.1 Ventil Basics",
                        "defaultConsent": False,
                        "matching": "definitionType",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                            "name": {"enUS": "7.2.1 Ventil Basics"},
                        },
                    }
                ],
            },
        ],
        "essentialVerbs": [
            {
                "id": "http://h5p.example.com/expapi/verbs/liked",
                "label": "Liked",
                "description": "Like interaction",
                "defaultConsent": True,
                "objects": [],
            }
        ],
    }

    h5p_provider_schema_v2 = {
        "id": "h5p-0",
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "verbs": [
            {
                "id": "http://h5p.example.com/expapi/verbs/experienced",
                "label": "Experienced",
                "description": "Experienced",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                        "label": "1.1.1 Funktionen",
                        "defaultConsent": True,
                        "matching": "definitionType",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                            "name": {"enUS": "1.1.1 Funktionen"},
                        },
                    }
                ],
            },
            {
                "id": "http://h5p.example.com/expapi/verbs/attempted",
                "label": "Attempted",
                "description": "Attempted",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "label": "2.3.1 Funktion Zirkulationsleitung",
                        "defaultConsent": True,
                        "matching": "definitionType",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                            "name": {
                                "enUS": "2.3.1 Funktion Zirkulationsleitung"
                            },
                        },
                    }
                ],
            },
        ],
        "essentialVerbs": [],
    }

    moodle_schema = {
        "id": "moodle-0",
        "name": "Moodle",
        "description": "Open-source learning management system",
        "verbs": [
                    {
                        "id": "http://moodle.example.com/expapi/verbs/completed",
                        "label": "Completed",
                        "description": "Completed",
                        "defaultConsent": True,
                        "objects": [
                            {
                                "id": "http://moodle.example.com/expapi/activity/programming-course-python",
                                "label": "Python Programming Course",
                                "defaultConsent": True,
                                "matching": "definitionType",
                                "definition": {
                                    "type": "http://moodle.example.com/expapi/activity/programming-course-python",
                                    "name": {"enUS": "Python Programming Course"},
                                },
                            },
                            {
                                "id": "http://moodle.example.com/expapi/activity/foreign-language-course",
                                "label": "Foreign language course",
                                "defaultConsent": True,
                                "matching": "definitionType",
                                "definition": {
                                    "type": "http://moodle.example.com/expapi/activity/foreign-language-course",
                                    "name": {"enUS": "Foreign language course"},
                                },
                            },
                        ],
                    },
                    {
                        "id": "http://moodle.example.com/expapi/verbs/started",
                        "label": "Started",
                        "description": "Started",
                        "defaultConsent": False,
                        "matching": "definitionType",
                        "objects": [],
                    },
                    {
                        "id": "http://moodle.example.com/expapi/verbs/paused",
                        "label": "Paused",
                        "description": "paused",
                        "defaultConsent": False,
                        "matching": "definitionType",
                        "objects": [],
                    },
                    {
                        "id": "http://moodle.example.com/expapi/verbs/created",
                        "label": "Created",
                        "description": "Created",
                        "defaultConsent": False,
                        "matching": "definitionType",
                        "objects": [],
                    },
                    {
                        "id": "http://moodle.example.com/expapi/verbs/answered",
                        "label": "Answered",
                        "description": "Answered",
                        "defaultConsent": True,
                        "objects": [
                            {
                                "id": "http://moodle.example.com/expapi/activity/poll-preferred-course-level",
                                "label": "Poll: Preferred course level",
                                "defaultConsent": True,
                                "matching": "definitionType",
                                "definition": {
                                    "type": "http://moodle.example.com/expapi/activity/poll-preferred-course-level",
                                    "name": {"enUS": "Poll: Preferred course level"},
                                },
                            }
                        ],
                    },
                ],
        "essentialVerbs": [],
    }

    def setUp(self):
        normal_user = CustomUser.objects.create_user(
            self.test_user_email, self.test_user_password
        )
        provider_user = CustomUser.objects.create_user(
            self.test_provider_email, self.test_provider_password
        )

        assign_role(provider_user, "provider")
        assign_role(normal_user, "user")

        assign_role(provider_user, "provider_manager")

        response = self.client.post(
            "/api/v1/auth/token",
            {"email": self.test_user_email, "password": self.test_user_password},
        )  # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        user_token = response.data["access"]

        response = self.client.post(
            "/api/v1/auth/token",
            {
                "email": self.test_provider_email,
                "password": self.test_provider_password,
            },
        )  # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        provider_token = response.data["access"]

        self.user_client = APIClient()
        self.user_client.credentials(HTTP_AUTHORIZATION="Bearer " + user_token)

        self.provider_client = APIClient()
        self.provider_client.credentials(HTTP_AUTHORIZATION="Bearer " + provider_token)


class ConsentDeclarationTestCase(BaseTestCase):
    def test_deny_anonymous(self):
        response = self.client.get("/api/v1/consents/provider")
        self.assertEqual(
            response.status_code, 401
        )  # we are not logged in, so return 401

    def test_provider_consent_view(self):
        response = self.user_client.get("/api/v1/consents/provider")
        self.assertEqual(response.status_code, 200)
        response = self.provider_client.get("/api/v1/consents/provider")
        self.assertEqual(response.status_code, 200)

    def test_create_provider_consent(self):
        with open(
            os.path.join(PROJECT_PATH, "static/provider_schema_h5p_v1.example.json")
        ) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)
            self.assertTrue("message" in response.json())

    def test_create_and_get_user_consent(self):
        response = self.user_client.get("/api/v1/consents/user/status")
        self.assertEqual(response.status_code, 200)


class TestProviderSchemaCreation(BaseTestCase):
    def test_create_with_group_instead_of_groups_schema_is_rejected(self):
        """
        Ensure provider schema containing typo is rejected.
        """
        invalid_provider_schema = {
            "id": "h5p-0",
            "name": "H5P",
            "description": "Open-source content collaboration framework",
            "verb": [
                {
                    "id": "http://h5p.example.com/expapi/verbs/experienced",
                    "label": "Experienced",
                    "description": "Experienced",
                    "defaultConsent": True,
                    "objects": [
                        {
                            "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                            "label": "1.1.1 Funktionen",
                            "defaultConsent": True,
                            "definition": {
                                "name": {"enUS": "1.1.1 Funktionen"}
                            },
                        }
                    ],
                },
                {
                    "id": "http://h5p.example.com/expapi/verbs/attempted",
                    "label": "Attempted",
                    "description": "Attempted",
                    "defaultConsent": True,
                    "objects": [
                        {
                            "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                            "label": "2.3.1 Funktion Zirkulationsleitung",
                            "defaultConsent": True,
                            "definition": {
                                "name": {
                                    "enUS": "2.3.1 Funktion Zirkulationsleitung"
                                }
                            },
                        }
                    ],
                },
            ],
            "essentialVerbs": [],
        }
        with StringIO(json.dumps(invalid_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 400)

    def test_create_empty_array_provider_schema_rejected(self):
        """
        Ensure provider schema containing empty array is rejected.
        """
        with StringIO(json.dumps([])) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 400)

    def test_add_new_provider_schema_version(self):
        """
        Ensure we create two providers schemas for the same provider.
        """
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        with StringIO(json.dumps(self.h5p_provider_schema_v2)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        provider = Provider.objects.get(definition_id="h5p-0")
        provider_schemas_count = ProviderSchema.objects.filter(
            provider=provider
        ).count()
        self.assertEqual(provider_schemas_count, 2)


class TestUserConsentInvalidProviderIdFails(BaseTestCase):
    def test_user_consent_invalid_provider_id_fails(self):
        """
        Ensure user consent creation fails, if referenced provider doesn't exist.
        """
        payload = [
            {
                "providerId": 42,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": 1,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":True,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": 1,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":True,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": 1,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":True,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": 1,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":False,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":True}]',
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", payload, format="json"
        )
        self.assertEqual(response.status_code, 400)


class TestUserConsentCreate(BaseTestCase):
    def test_user_consent_create(self):
        """
        Ensure initial user consent gets created.
        """
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id
        payload = [
            {
                "providerId": Provider.objects.latest('id').id,
                "providerSchemaId": ProviderSchema.objects.latest('id').id,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]


        response = self.user_client.post(
            "/api/v1/consents/user/save", payload, format="json"
        )
        self.assertEqual(response.status_code, 200)

        user_consents = UserConsents.objects.filter(user__email=self.test_user_email)
        self.assertEqual(len(user_consents), 4)
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/experienced",
                active=True
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/attempted",
                active=True
            ).consented
        )
        self.assertFalse(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/interacted",
                active=True
            ).consented
        )
        self.assertFalse(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/answered",
                active=True
            ).consented
        )


class TestUserConsentOutdatedProviderSchemaId(BaseTestCase):
    def test_user_consent_update(self):
        """
        Ensure user consent gets updated.
        """

        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"provider_id": Provider.objects.latest('id').id, "id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        payload_second_consent = [
            {
                "providerId": Provider.objects.latest('id').id,
                "providerSchemaId": ProviderSchema.objects.latest('id').id,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":True,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":True,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":True,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":False,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":True}]',
                    },
                ],
            }
        ]

        response = self.user_client.post(
            "/api/v1/consents/user/save", payload_second_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        user_consents = UserConsents.objects.filter(user__email=self.test_user_email)
        self.assertEqual(len(user_consents), 4)
        self.assertFalse(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/experienced",
                active=True,
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/attempted",
                active=True,
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/interacted",
                active=True,
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/answered",
                active=True,
            ).consented
        )

        payload_second_consent = [
            {
                "providerId": Provider.objects.latest('id').id,
                "providerSchemaId": ProviderSchema.objects.latest('id').id,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":True,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":True,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":True,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":False,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":True}]',
                    },
                ],
            }
        ]

        response = self.user_client.post(
            "/api/v1/consents/user/save", payload_second_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        user_consents = UserConsents.objects.filter(user__email=self.test_user_email)
        self.assertEqual(len(user_consents), 8)
        self.assertFalse(
            UserConsents.objects.get(
                verb="http://h5p.example.com/expapi/verbs/experienced",
                active=True,
            ).consented
        )
        self.assertFalse(
            UserConsents.objects.get(
                verb="http://h5p.example.com/expapi/verbs/attempted",
                active=True,
            ).consented
        )
        self.assertFalse(
            UserConsents.objects.get(
                verb="http://h5p.example.com/expapi/verbs/interacted",
                active=True,
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb="http://h5p.example.com/expapi/verbs/answered",
                active=True,
            ).consented
        )


class TestUserConsentOutdatedProviderSchemaId(BaseTestCase):
    def test_user_consent_save_fails_with_old_provider_schema_id(self):
        """
        Ensure user consent save is rejected, if referenced provider schema id is outdated.
        """
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        payload = [
            {
                "providerId": Provider.objects.latest('id').id,
                "providerSchemaId": ProviderSchema.objects.latest('id').id,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]

        # Add newer provider schema for H5P
        with StringIO(json.dumps(self.h5p_provider_schema_v2)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        response = self.user_client.post(
            "/api/v1/consents/user/save", payload, format="json"
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json()["message"],
            "provider schema id isn't latest provider schema id",
        )


class TestUserConsentSaveUpdatedProviderSchema(BaseTestCase):
    def test_user_consent_on_updated_provider_schema(self):
        """
        Ensure user consent is stored correctly for updated provider schema.
        """
        # Create first provider schema version
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)
        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id
        # Create user consent for first provider schema version
        first_provider_schema_consent = [
            {
                "providerId": Provider.objects.latest('id').id,
                "providerSchemaId": ProviderSchema.objects.latest('id').id,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", first_provider_schema_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        user_consents = UserConsents.objects.filter(user__email=self.test_user_email)
        self.assertEqual(len(user_consents), 4)
        self.assertFalse(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/experienced",
                active=True
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/attempted",
                active=True
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/interacted",
                active=True
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/answered",
                active=True
            ).consented
        )

        # Create second provider schema version
        with StringIO(json.dumps(self.h5p_provider_schema_v2)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        second_provider_schema_consent = [
            {
                "providerId": Provider.objects.latest('id').id,
                "providerSchemaId": ProviderSchema.objects.latest('id').id,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                ],
            }
        ]
        response = self.user_client.post(
            "/api/v1/consents/user/save", second_provider_schema_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)

        user_consents = UserConsents.objects.filter(user__email=self.test_user_email, active=True)
        self.assertEqual(len(user_consents), 2)
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/experienced",
                active=True
            ).consented
        )
        self.assertTrue(
            UserConsents.objects.get(
                verb__verb_id="http://h5p.example.com/expapi/verbs/attempted",
                active=True
            ).consented
        )


class TestMultiUserConsent(BaseTestCase):
    provider_schema = {
        "id": "h5p-0",
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "verbs": [
            {
                "id": "http://h5p.example.com/expapi/verbs/experienced",
                "label": "Experienced",
                "description": "Experienced",
                "defaultConsent": True,
                "objects": [
                    {
                        "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                        "label": "1.1.1 Funktionen",
                        "defaultConsent": True,
                        "matching": "definitionType",
                        "definition": {
                            "type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                            "name": {"enUS": "1.1.1 Funktionen"},
                        },
                    }
                ],
            },
        ],
        "essentialVerbs": [],
    }
    user_email_1 = "user1@polaris.com"
    user_email_2 = "user2@polaris.com"

    def setUp(self):
        CustomUser.objects.create_user(self.user_email_1, "polaris321")
        CustomUser.objects.create_user(self.user_email_2, "polaris321")
        # Obtain token for user 1
        response = self.client.post(
            "/api/v1/auth/token",
            {"email": self.user_email_1, "password": "polaris321"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        user_token_1 = response.data["access"]

        self.user_client_1 = APIClient()
        self.user_client_1.credentials(HTTP_AUTHORIZATION="Bearer " + user_token_1)

        # Obtain token for user 2
        response = self.client.post(
            "/api/v1/auth/token",
            {"email": self.user_email_2, "password": "polaris321"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue("access" in response.data)
        user_token_2 = response.data["access"]

        self.user_client_2 = APIClient()
        self.user_client_2.credentials(HTTP_AUTHORIZATION="Bearer " + user_token_2)

        # Create provider user
        provider_user = CustomUser.objects.create_user(
            "provider@polaris.com", "polaris321"
        )

        assign_role(provider_user, "provider")
        assign_role(provider_user, "provider_manager")

        response = self.client.post(
            "/api/v1/auth/token",
            {"email": "provider@polaris.com", "password": "polaris321"},
        )  # obtain token
        provider_token = response.data["access"]

        self.provider_client = APIClient()
        self.provider_client.credentials(HTTP_AUTHORIZATION="Bearer " + provider_token)

    def test_two_users_consent_same_verb(self):
        """
        Ensure users can consent to same verb.
        """
        # Create provider schema
        with StringIO(json.dumps(self.provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        user_consent_1 = [
            {
                "providerId": Provider.objects.latest('id').id,
                "providerSchemaId": ProviderSchema.objects.latest('id').id,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true, "matching": "definitionType" ,"definition":{"type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF", "name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    }
                ],
            }
        ]
        user_consent_2 = [
            {
                "providerId": Provider.objects.latest('id').id,
                "providerSchemaId": ProviderSchema.objects.latest('id').id,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true, "matching": "definitionType" ,"definition":{"type": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF", "name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    }
                ],
            }
        ]

        response = self.user_client_1.post(
            "/api/v1/consents/user/save", data=user_consent_1, format="json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["message"], "user consent saved")

        response = self.user_client_2.post(
            "/api/v1/consents/user/save", data=user_consent_2, format="json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["message"], "user consent saved")

        self.assertTrue(
            UserConsents.objects.get(
                user__email=self.user_email_1,
                verb__verb_id="http://h5p.example.com/expapi/verbs/experienced",
            ).consented
        )
        self.assertFalse(
            UserConsents.objects.get(
                user__email=self.user_email_2,
                verb__verb_id="http://h5p.example.com/expapi/verbs/experienced",
            ).consented
        )
