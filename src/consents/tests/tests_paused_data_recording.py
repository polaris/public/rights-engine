import json
from io import StringIO

from consents.tests.tests_consent_operations import BaseTestCase
from providers.models import Provider, ProviderVerbGroup


class TestPauseDataRecording(BaseTestCase):
    def test_data_recording_is_off_by_default(self):
        """
        Ensure data recording is inactive by default.
        """
        # Create provider H5P
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)

        provider = Provider.objects.get(definition_id="h5p-0")

        # Get current user consent for provider H5P
        response = self.user_client.get(f"/api/v1/consents/user/{provider.id}")
        self.assertEqual(response.status_code, 200)

        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {"consent": None, "paused_data_recording": False}
        )

    def test_pause_data_recording(self):
        """
        Ensure data recording can be turned on and off.
        """
        # Create provider H5P
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)
        provider = Provider.objects.get(definition_id="h5p-0")

        # Pause data recording
        response = self.user_client.post("/api/v1/auth/toggle-data-recording")
        self.assertEqual(response.status_code, 200)

        # Get current user consent for provider H5P
        response = self.user_client.get(f"/api/v1/consents/user/{provider.id}")
        self.assertEqual(response.status_code, 200)

        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {"consent": None, "paused_data_recording": True}
        )

        # Activate data recording
        response = self.user_client.post("/api/v1/auth/toggle-data-recording")
        self.assertEqual(response.status_code, 200)

        # Get current user consent for provider H5P
        response = self.user_client.get(f"/api/v1/consents/user/{provider.id}")
        self.assertEqual(response.status_code, 200)

        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {"consent": None, "paused_data_recording": False}
        )

    def test_save_consent_while_paused(self):
        """
        Ensure user can save user consent while data recording is paused.
        """
        # Create provider H5P
        with StringIO(json.dumps(self.h5p_provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)
        provider = Provider.objects.get(definition_id="h5p-0")

        # Pause data recording
        response = self.user_client.post("/api/v1/auth/toggle-data-recording")
        self.assertEqual(response.status_code, 200)

        # Get current user consent for provider H5P
        response = self.user_client.get(f"/api/v1/consents/user/{provider.id}")
        self.assertEqual(response.status_code, 200)

        self.assertJSONEqual(
            str(response.content, encoding="utf8"),
            {"consent": None, "paused_data_recording": True}
        )

        # create a verb group
        group = {"id": "default_group", "label": "Group 2",
                 "description": "Lorem ipsum", "showVerbDetails": True, "purposeOfCollection": "Lorem Ipsum",
                 "requiresConsent": True, "verbs": [
                {"id": "http://h5p.example.com/expapi/verbs/experienced"},
                {"id": "http://h5p.example.com/expapi/verbs/attempted"},
                {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                {"id": "http://h5p.example.com/expapi/verbs/answered"},
            ]}
        response = self.provider_client.post(
            "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
            group,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        group_id = ProviderVerbGroup.objects.latest('id').id

        user_consent = [
            {
                "providerId": 1,
                "providerSchemaId": 1,
                "verbs": [
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/experienced",
                        "consented": False,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF","label":"1.1.1 Funktionen","defaultConsent":true,"definition":{"name":{"enUS":"1.1.1 Funktionen"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/attempted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm","label":"2.3.1 Funktion Zirkulationsleitung","defaultConsent":true,"definition":{"name":{"enUS":"2.3.1 Funktion Zirkulationsleitung"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/interacted",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46","label":"1.2.3 Kappenventil","defaultConsent":true,"definition":{"name":{"enUS":"1.2.3 Kappenventil"}},"consented":true}]',
                    },
                    {
                        "group_id": group_id,
                        "id": "http://h5p.example.com/expapi/verbs/answered",
                        "consented": True,
                        "objects": '[{"id":"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b","label":"7.2.1 Ventil Basics","defaultConsent":false,"definition":{"name":{"enUS":"7.2.1 Ventil Basics"}},"consented":true}]',
                    },
                ],
            }
        ]
        # Attempt to save user consent
        response = self.user_client.post(
            "/api/v1/consents/user/save", user_consent, format="json"
        )
        self.assertEqual(response.status_code, 200)