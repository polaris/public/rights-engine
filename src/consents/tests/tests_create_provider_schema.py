import json
from io import StringIO

from consents.tests.tests_consent_operations import BaseTestCase
from providers.models import Provider


class TestProviderSchemaCreation(BaseTestCase):
    def test_create_provider_verb_groups(self):
        """
        Test verb group creation and update.
        """
        provider_schema = {
            "id": "h5p-0",
            "name": "H5P",
            "description": "Open-source content collaboration framework",
            "verbs": [
                {
                    "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked",
                    "label": "Unlocked",
                    "description": "Actor unlocked an object",
                    "defaultConsent": True,
                    "objects": [
                        {
                            "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/my-random-object-id",
                            "label": "Course",
                            "defaultConsent": True,
                            "matching": "id",
                            "definition": {
                                "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course",
                                "name": {
                                    "enUS": "A course within an LMS. Contains learning materials and activities"
                                },
                            },
                        },
                    ],
                },
                {
                    "id": "http://h5p.example.com/expapi/verbs/interacted",
                    "label": "Interacted",
                    "description": "Lorem ipsum",
                    "defaultConsent": True,
                    "objects": [
                        {
                            "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                            "label": "1.2.3 Kappenventil",
                            "defaultConsent": True,
                            "matching": "definitionType",
                            "definition": {
                                "type": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                                "name": {"enUS": "1.2.3 Kappenventil"},
                            },
                        }
                    ],
                },
                {
                    "id": "http://h5p.example.com/expapi/verbs/answered",
                    "label": "Answered",
                    "description": "lorem ipsum",
                    "defaultConsent": False,
                    "objects": [
                        {
                            "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                            "label": "7.2.1 Ventil Basics",
                            "defaultConsent": False,
                            "matching": "definitionType",
                            "definition": {
                                "type": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                                "name": {"enUS": "7.2.1 Ventil Basics"},
                            },
                        }
                    ],
                }
            ],
            "essentialVerbs": [],
        }

        verb_groups = [
            {
                "id": "default_group",
                "label": "Default group",
                "description": "default",
                "showVerbDetails": True,
                "purposeOfCollection": "Lorem Ipsum",
                "requiresConsent": True,
                "verbs": [
                    {"id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked"}
                ]
            },
            {
                "id": "default_group",
                "label": "Group 2",
                "description": "Lorem ipsum",
                "showVerbDetails": True,
                "purposeOfCollection": "Lorem Ipsum",
                "requiresConsent": True,
                "verbs": [
                    {"id": "http://h5p.example.com/expapi/verbs/interacted"},
                    {"id": "http://h5p.example.com/expapi/verbs/answered"},
                ]
            }
        ]

        # Upload provider schema
        with StringIO(json.dumps(provider_schema)) as fp:
            response = self.provider_client.put(
                "/api/v1/consents/provider/create",
                {"provider-schema": fp},
                format="multipart",
            )
            self.assertEqual(response.status_code, 201)
        # create some verb groups
        for group in verb_groups:
            response = self.provider_client.post(
                "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
                group,
                format="json",
            )
            self.assertEqual(response.status_code, 200)
        # one more time to simulate update
        for group in verb_groups:
            response = self.provider_client.post(
                "/api/v1/consents/provider/" + str(Provider.objects.latest('id').id) + "/create-verb-group",
                group,
                format="json",
            )
            self.assertEqual(response.status_code, 200)