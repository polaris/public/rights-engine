from django.apps import AppConfig


class DataDisclosureConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'data_disclosure'
