# Generated by Django 4.1.2 on 2023-02-09 15:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="DataDisclosure",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("running", models.BooleanField(default=False)),
                (
                    "status",
                    models.CharField(
                        blank=True,
                        choices=[("success", "success"), ("failed", "failed")],
                        max_length=7,
                        null=True,
                    ),
                ),
                ("filename", models.CharField(blank=True, max_length=256, null=True)),
                ("secret", models.CharField(max_length=65)),
                ("expires", models.DateTimeField()),
                ("created", models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
