from django.utils import timezone
from fpdf import FPDF

from consents.models import UserConsents
from consents.views import get_user_consent
from providers.models import Provider, ProviderSchema


class PDF(FPDF):
    def header(self):
        self.set_font("helvetica", "B", 15)
        self.cell(80)
        self.cell(30, 10, "Data Disclosure Report", center=True)
        self.ln(20)


def print_verbs(pdf, verbs):
    for verb in verbs:
        pdf.set_x(20)
        pdf.cell(
            0,
            5,
            f"Verb: {verb['label']}",
            new_x="LMARGIN",
            new_y="NEXT",
        )
        pdf.set_x(20)
        pdf.cell(
            0,
            5,
            f"Id: {verb['id']}",
            new_x="LMARGIN",
            new_y="NEXT",
        )
        pdf.set_x(20)
        pdf.cell(
            0,
            5,
            f"Description: {verb['description']}",
            new_x="LMARGIN",
            new_y="NEXT",
        )
        pdf.set_x(20)
        pdf.cell(
            0,
            5,
            f"Consented: {verb.get('consented', '-')}",
            new_x="LMARGIN",
            new_y="NEXT",
        )
        for object in verb["objects"]:
            pdf.set_x(40)
            pdf.cell(
                0,
                5,
                f"Object: {object['label']}",
                new_x="LMARGIN",
                new_y="NEXT",
            )
            pdf.set_x(40)
            pdf.cell(
                0,
                5,
                f"Id: {object['id']}",
                new_x="LMARGIN",
                new_y="NEXT",
            )
            pdf.set_x(40)
            pdf.cell(
                0,
                5,
                f"Consented: {object['consented']}",
                new_x="LMARGIN",
                new_y="NEXT",
            )
    return pdf


def print_group(pdf, group):
    pdf.set_font(style="B", size=12)
    pdf.cell(
        0,
        10,
        "{}".format(group["label"]),
        new_x="LMARGIN",
        new_y="NEXT",
    )
    pdf.set_font("helvetica", size=8)
    pdf = print_verbs(pdf, group["verbs"])
    return pdf


def create_pdf_report(user):
    """
    Generate pdf report for user consent declaration.
    """
    pdf = PDF()
    pdf.set_font("helvetica", size=8)
    pdf.add_page()

    pdf.cell(0, 5, "User: {}".format(user.email), 0, 1)
    pdf.cell(
        0,
        5,
        "Created: {}".format(timezone.now().strftime("%b %d %Y %H:%M:%S")),
        0,
        1,
    )

    providers = Provider.objects.all()

    for provider in providers:
        pdf.cell(
            0,
            5,
            "Provider: {}".format(provider.name),
            0,
            1,
        )
        user_consent = get_user_consent(user, provider.id)

        accepted_user_schemas = user.accepted_provider_schemas.all()
        user_schema_ids = [schema.id for schema in accepted_user_schemas]

        provider_latest_schema = ProviderSchema.objects.get(
            provider=provider, superseded_by=None
        )

        if len(accepted_user_schemas) == 0:
            pdf.cell(
                0,
                5,
                f"You have not yet submitted a consent declaration for Provider {provider.name}.",
                0,
                1,
            )
        else:
            if provider_latest_schema.id in user_schema_ids:
                pdf.cell(
                    0,
                    5,
                    "You provided a consent declaration the latest provider schema.",
                    0,
                    1,
                )
            else:
                pdf.cell(
                    0,
                    5,
                    "You provided a consent declaration an outdated provider schema.",
                    0,
                    1,
                )

        pdf.cell(
            0,
            5,
            "Paused data recording: {}".format(user.paused_data_recording),
            0,
            1,
        )

        for group in user_consent.get("groups", []):
            pdf = print_group(pdf, group)

        pdf.set_font(style="B", size=12)
        pdf.cell(
            0,
            10,
            "Essential verbs",
            new_x="LMARGIN",
            new_y="NEXT",
        )
        pdf.set_font("helvetica", size=8)
        pdf = print_verbs(pdf, user_consent["essential_verbs"])

    data = pdf.output(dest="S")
    return data
