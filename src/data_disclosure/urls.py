from django.urls import path

from . import views

urlpatterns = [
    path('create', views.CreateDataExport.as_view()),
    path('list', views.GetExports.as_view()),
    path('file_secret/<file_id>', views.GetFileSecret.as_view()),
    path('files/<file_id>/<secret>', views.GetZipFile.as_view()),
    path('<file_id>/delete', views.DeleteDataDisclosure.as_view())
]
