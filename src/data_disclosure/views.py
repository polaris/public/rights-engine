import datetime
import secrets

from celery.result import AsyncResult
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import default_storage
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.utils import timezone
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from .models import DataDisclosure
from .tasks import process_data_disclosure


class CreateDataExport(APIView):
    """
    Create new data disclosure job for a given user. The actual task of creating a data disclosure is handled by a celery job.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        expires = timezone.now() + datetime.timedelta(
            days=int(settings.DATA_DISCLOSURE_EXPIRATION)
        )
        secret = secrets.token_hex(32)

        job = DataDisclosure.objects.create(user=user, expires=expires, secret=secret)
        process_data_disclosure.delay(job.id)

        return JsonResponse(
            {"message": "data export job created."},
            safe=False,
            status=status.HTTP_200_OK,
        )


class GetExports(APIView):
    """
    Lists existing data disclosures for a given user.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user_data_disclosures = list(
            DataDisclosure.objects.filter(user=request.user)
            .order_by("-created")
            .values()
        )
        return JsonResponse(
            user_data_disclosures, safe=False, status=status.HTTP_200_OK
        )


def is_user_file_owner(user, file_id):
    data_disclosure = DataDisclosure.objects.filter(id=file_id).first()
    return bool(data_disclosure.user.id == user.id)


class GetFileSecret(APIView):
    """
    Data disclosure zip files are only accessible with a file secret, which can be obtained via this function.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, file_id):
        user = request.user

        if not is_user_file_owner(user, file_id):
            return JsonResponse(
                {"message": "you are not the owner of the requested file"},
                safe=False,
                status=status.HTTP_403_FORBIDDEN,
            )

        data_disclosure = DataDisclosure.objects.filter(id=file_id).first()

        return JsonResponse(
            {"secret": data_disclosure.secret}, status=status.HTTP_200_OK
        )


class GetZipFile(APIView):
    """
    Retrieves a zip file for a given file_id.
    """

    def get(self, request, file_id, secret):
        user = request.user

        try:
            data_disclosure = DataDisclosure.objects.get(id=file_id)

            if data_disclosure.secret != secret:
                return JsonResponse(
                    {"message": "please provide a matching secret"},
                    safe=False,
                    status=status.HTTP_403_FORBIDDEN,
                )

            with open(data_disclosure.filename, "rb") as f:
                file_data = f.read()

            response = HttpResponse(file_data, content_type="application/zip")
            filename = "data_disclosure-{date:%Y-%m-%d_%H_%M_%S}.zip".format(
                date=datetime.datetime.now()
            )
            response["Content-Disposition"] = f'attachment; filename="{filename}"'
            return response

        except (IOError, ObjectDoesNotExist):
            return JsonResponse(
                {"message": "file not found"},
                safe=False,
                status=status.HTTP_404_NOT_FOUND,
            )


class DeleteDataDisclosure(APIView):
    """
    Deletes a specific data disclosure job and its associated zip file.
    """

    permission_classes = (IsAuthenticated,)

    def delete(self, request, file_id):
        if not is_user_file_owner(request.user, file_id):
            return JsonResponse(
                {"message": "you are not the owner of the requested file"},
                safe=False,
                status=status.HTTP_403_FORBIDDEN,
            )
        try:
            data_disclosure = DataDisclosure.objects.get(id=file_id)
            default_storage.delete(data_disclosure.filename)
            data_disclosure.delete()

            user_data_disclosures = list(
                DataDisclosure.objects.filter(user=request.user).values()
            )
            return JsonResponse(
                user_data_disclosures, safe=False, status=status.HTTP_200_OK
            )
        except ObjectDoesNotExist:
            return JsonResponse(
                {"message": "file not found"},
                safe=False,
                status=status.HTTP_404_NOT_FOUND,
            )
