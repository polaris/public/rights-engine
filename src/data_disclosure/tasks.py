from __future__ import absolute_import, unicode_literals

import io
import json
import zipfile
from typing import Dict, List

from celery import shared_task
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import default_storage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.timezone import now

from backend.utils import lrs_db
from data_disclosure.pdf_report import create_pdf_report

from .models import DataDisclosure


class DataDisclosureProcessor:
    def create_zip(self, job, pdf, data_arr):
        """
        Create zip file containing data disclosure PDF report and collected XAPI statements.
        """
        zip_buffer = io.BytesIO()

        with zipfile.ZipFile(zip_buffer, "w", zipfile.ZIP_DEFLATED, False) as zip_file:
            zip_file.writestr("report.pdf", bytes(pdf))
            for file_name, data in data_arr:
                zip_file.writestr(file_name, data.getvalue())

        file_name = default_storage.save(
            f"{settings.DATA_DISCLOSURE_LOCATION}/{job.id}.zip", zip_buffer
        )
        return file_name

    def get_xapi_statements(self, user_email: str) -> List[Dict]:
        """
        Get all XAPI statements matching a user email address from database.
        """
        collection = lrs_db["statements"]
        query = { "$or" : [ {"actor.account.name": f"{user_email}"}, { "actor.mbox": f"mailto:{user_email}" } ] }
        """ query = {"actor.account.name": f"{user_email}"} """
        cursor = collection.find(query)
        return [
            (
                "xapi_ statements.json",
                io.StringIO(json.dumps(list(cursor), default=str)),
            )
        ]

    def send_email(self, user):
        """
        Notify user about data disclosure completion.
        """
        [username, _] = user.email.split("@")
        html_body = render_to_string(
            "data_disclosure_available.html",
            {"name": username, "days": settings.DATA_DISCLOSURE_EXPIRATION},
        )
        text_body = strip_tags(html_body)

        email = EmailMultiAlternatives(
            "[Polaris]: Data disclosure available",
            text_body,
            settings.EMAIL_HOST_EMAIL,
            [user.email],
        )
        email.attach_alternative(html_body, "text/html")
        email.send()

    def process_job(self, job):
        """
        Implements data disclousure process for a single user.

        1. collect XAPI statements
        2. create data disclosure PDF report
        3. send notification email to user
        """
        job.running = True
        job.save()

        statements = self.get_xapi_statements(job.user.email)

        pdf_report = create_pdf_report(job.user)
        job.filename = self.create_zip(job, pdf_report, statements)
        job.running = False
        job.status = "success"
        job.save()
        self.send_email(job.user)


@shared_task
def process_data_disclosure(job_id):
    try:
        job = DataDisclosure.objects.get(id=job_id)
        processor = DataDisclosureProcessor()
        processor.process_job(job)
    except ObjectDoesNotExist:
        print("couldn't find job")


@shared_task
def remove_expired():
    jobs = DataDisclosure.objects.filter(expires__lte=now())
    [default_storage.delete(job.filename) for job in jobs]
    jobs.delete()
