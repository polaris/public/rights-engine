from django.db import models
from django.conf import settings


class DataDisclosure(models.Model):
    STATUS = [
        ('success', 'success'),
        ('failed', 'failed')
    ]
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    running = models.BooleanField(default=False)
    status = models.CharField(
        max_length=7, choices=STATUS, blank=True, null=True)
    filename = models.CharField(max_length=256, blank=True, null=True)
    secret = models.CharField(max_length=65)
    expires = models.DateTimeField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Data disclosure"
