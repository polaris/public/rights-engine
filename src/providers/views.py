from datetime import datetime, timedelta
import os
import uuid
import jwt
import time
from bson.objectid import ObjectId
from cryptography.hazmat.primitives import serialization
from django.conf import settings
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.forms.models import model_to_dict
from django.http import Http404
from django.http.response import JsonResponse,HttpResponse
from django.shortcuts import render
from django.utils import timezone
from rest_framework import exceptions, generics, permissions, status
from rest_framework.exceptions import ParseError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rolepermissions.checkers import has_permission
from rolepermissions.roles import get_user_roles

from backend.role_permission import IsAnalyst, IsProviderManager, IsProvider
from backend.roles import Roles
from backend.utils import lrs_db
from consents.views import JsonUploadParser
from providers.serializers import (AnalyticsEngineAccessSerializer,
                                   CreateAnalyticsTokenSerializer,
                                   CreateVisualizationTokenSerializer,
                                   GetProviderDataRequestSerializer,
                                   AnalyticsTokenSerializer,
                                   ProviderDataSerializer)
from xapi.views import shib_connector_resolver
from users.models import CustomUser
from .models import (AnalyticsToken, AnalyticsTokenVerb, Provider, ProviderAuthorization,
                     ProviderSchema)
from pymongo import MongoClient


def render_provider(provider):
    return {
        "id": provider.id,
        "name": provider.name,
        "description": provider.description,
        "schema": render_provider_schema(
            ProviderSchema.objects.get(provider=provider, superseded_by=None)
        ),
        "createdAt": provider.created,
    }


def render_provider_schema(provider_schema):
    return {
        "id": provider_schema.id,
        # "object": provider_schema.object,
        "isLatestVersion": provider_schema.superseded_by == None,
    }


def render_provider_keys(provider):
    return {
        "provider": render_provider(provider),
        "keys": [
            auth.key for auth in ProviderAuthorization.objects.filter(provider=provider)
        ],
    }

class GetProvidersView(APIView):
    """
    Lists available providers
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request):
        providers = Provider.objects.all()
        result = [*map(render_provider, providers)]
        return JsonResponse(result, safe=False, status=status.HTTP_200_OK)


class CreateProviderSchemaView(APIView):
    """
    Creates provider
    """

    permission_classes = (IsAuthenticated,)
    parser_class = (JsonUploadParser,)

    def post(self, request, provider_id):
        if has_permission(request.user, Roles.CREATE_PROVIDER):
            object = request.data.get("object", None)
            if object == None:
                return JsonResponse(
                    {"message": "invalid form data"}, status=status.HTTP_400_BAD_REQUEST
                )
            provider = Provider.objects.get(provider_id)
            try:
                precedingSchema = ProviderSchema.objects.get(superseded_by__isnull=True)

                newSchema = ProviderSchema.objects.create(
                    object=object, provider=provider
                )

                precedingSchema.superseded_by = newSchema
                precedingSchema.save()
            except ObjectDoesNotExist:
                newSchema = ProviderSchema.objects.create(
                    object=object, provider=provider
                )

            return JsonResponse(
                {"message": "provider schema submitted"}, status=status.HTTP_201_CREATED
            )
        else:
            return JsonResponse(
                {"message": "missing permissions to create providers"}, status=status.HTTP_401_UNAUTHORIZED
            )


class GetAllProviderKeysView(APIView):
    """
    Lists applications tokens and associated provider details.
    """

    permission_classes = [IsAuthenticated, IsProviderManager]

    def get(self, request):
        providers = Provider.objects.all()
        result = [*map(render_provider_keys, providers)]
        return JsonResponse(result, safe=False, status=status.HTTP_200_OK)


class GetProviderKeysView(APIView):
    """
    Returns details of single provider.
    """

    permission_classes = [IsAuthenticated, IsProviderManager]

    def get(self, request, provider_id):
        try:
            provider = Provider.objects.get(id=provider_id)
        except ObjectDoesNotExist:
            return JsonResponse(
                {"message": "Unknown provider id"}, status=status.HTTP_404_NOT_FOUND
            )

        return JsonResponse(render_provider_keys(provider), status=status.HTTP_200_OK)


class AnalyticsTokensList(generics.ListAPIView):
    """
    Lists analytics token
    """
    def get_queryset(self):
        return AnalyticsToken.objects.filter(creator=self.request.user.id)

   # queryset = AnalyticsToken.objects.filter(creator=request.user)
    serializer_class = AnalyticsTokenSerializer
    permission_classes = [IsAuthenticated, IsAnalyst]

class GetAnalyticsTokensAvailableVerbsList(APIView):
    """
    Lists analytics token
    """
    permission_classes = [IsAuthenticated, IsProviderManager]

    def get(self, request):
        def createAnalyticTokenVerbs(groups, essential_verbs, provider):
            verbs = []
            for group in groups:
                for verb in group.verbs.all():
                    verbs.append({"id" : verb.verb_id, "label" : verb.label, "description" : verb.description, "provider" : provider.id})
            for verb in essential_verbs:  # append essential verbs which might not be included in any groups
                if len([verb_ for verb_ in verbs if verb_["id"] == verb.verb_id]) == 0:
                    verbs.append({"id" : verb.verb_id, "label" : verb.label, "description" : verb.description, "provider" : provider.id})
            return verbs
        def prepareSchema(schema):
            return {
                "providerId" : schema.provider.id,
                "label" : schema.provider.name,
                "analyticTokenVerbs" : createAnalyticTokenVerbs(schema.groups(), schema.essential_verbs(), schema.provider)
            }
        schemas = ProviderSchema.objects.filter(superseded_by=None)
        s = list(map(prepareSchema , schemas))
        if settings.SHOW_XAPI_STATEMENTS:
            print(s)
        return JsonResponse(s, status=status.HTTP_200_OK, safe=False)


class CreateAnalyticsToken(APIView):
    """
    Creates analytics token
    """

    permission_classes = [IsAuthenticated, IsAnalyst]

    def get_provider(self, pk):
        try:
            return Provider.objects.get(pk=pk)
        except Provider.DoesNotExist:
            raise Http404

    def post(self, request):
        try:
            serializer = CreateAnalyticsTokenSerializer(
                data=request.data, context={"current_user": request.user}
            )
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError:
            return Response(
                {"message": "name already in use"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class UpdateAnalyticsTokensVerbs(APIView):
    """
    Analytics tokens and their corresponding engines are restricted to process a limited list of verbs.
    This endpoint updates the verb list for an analytics token.
    """

    permission_classes = [IsAuthenticated, IsAnalyst]

    def post(self, request, token_id):
        try:
            analytics_token = AnalyticsToken.objects.get(creator=request.user, id=token_id)
            if (
                analytics_token.expires is not None
                and analytics_token.expires <= timezone.now()
            ):
                return Response(
                    {"message": "Token has expired."},
                    status=status.HTTP_401_UNAUTHORIZED,
                )

            # delete all currently assignes verbs
            AnalyticsTokenVerb.objects.filter(analytics_token=analytics_token.id).delete();
            for activeverb in request.data["active_verbs"]:
                v = AnalyticsTokenVerb()
                v.verb = activeverb["verb"]
                v.provider = Provider.objects.filter(id=activeverb["provider"]).first()
                v.analytics_token = analytics_token
                v.save()
            
            
            return Response({"message": "sucess"}, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response(
                {"message": "token doesn't exist"}, status=status.HTTP_404_NOT_FOUND
            )

class UpdateAnalyticsTokenImage(APIView):

  permission_classes = [IsAuthenticated, IsAnalyst]

  def delete(self, request, token_id):
      token = AnalyticsToken.objects.filter(id=token_id).first()
      if not token:
         return Response({"message": "token doesn't exist"}, status=status.HTTP_404_NOT_FOUND)
      os.remove("static/upload/analytic-tokens-images/"+token.image_path)
      token.image_path = None
      token.save()
      return Response({"message": "token image deleted"}, status=status.HTTP_200_OK)
  
  def post(self, request, token_id):
      token = AnalyticsToken.objects.filter(id=token_id).first()
      if not token:
         return Response({"message": "token doesn't exist"}, status=status.HTTP_404_NOT_FOUND)
      
      file = request.FILES["image"]
      ext = os.path.splitext(file.name)[1]
      valid_extensions = ['.png','.jpeg','.jpg']
      if not ext in valid_extensions:
         return Response({"message": "not a valid file."}, status=status.HTTP_400_BAD_REQUEST)
  
      if token.image_path:
          os.remove("static/upload/analytic-tokens-images/"+token.image_path)
      hash = str(uuid.uuid4())
      final_name = hash+ext
      with open("static/upload/analytic-tokens-images/"+final_name, "wb+") as destination:
        for chunk in file.chunks():
            destination.write(chunk)
      token.image_path = final_name
      token.save()
      return Response({"message": "token image updated"}, status=status.HTTP_200_OK)



class GetAnalyticsTokenImage(APIView):
    """
    """
    def get(self, request, token_id, image_name):
      token = AnalyticsToken.objects.filter(id=token_id).first()
      if not token:
         return Response({"message": "token doesn't exist"}, status=status.HTTP_404_NOT_FOUND)
      if token.image_path != image_name:
         return Response({"message": "image not found"}, status=status.HTTP_404_NOT_FOUND)

      image_data = open("static/upload/analytic-tokens-images/"+token.image_path, "rb").read()
      return HttpResponse(image_data, content_type='application/image')


class DeleteAnalyticsToken(generics.DestroyAPIView):
    """
    Deletes analytics token
    """

    permission_classes = [IsAuthenticated, IsAnalyst]
    serializer_class = AnalyticsTokenSerializer

    def get_queryset(self):
        return AnalyticsToken.objects.filter(
            creator=self.request.user, id=self.kwargs["pk"]
        )


class CheckAnalyticsTokenNameAvailable(APIView):
    """
    Analytics token are required to have a unique name. This endpoint checks whether a provided name is already in use.
    """

    permission_classes = [IsAuthenticated, IsAnalyst]

    def post(self, request):
        name = request.data.get("name")
        if name is None:
            return Response(
                {"message": "please provide a name"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        exists = AnalyticsToken.objects.filter(name=name).exists()
        return Response(
            {"is_available": not exists},
            status=status.HTTP_200_OK,
        )


def get_system_statement_query(providers=[]):
    """
    :param providers: optional list of providers to filter system statements by
    :return: query object which filters for system statements and optionally the supplied providers
    """
    query = {"$and": [
            {"actor.mbox": {"$exists": True}}
        ]}
    if len(providers) > 0:
        provider_filters = []
        for provider in providers:
            provider_filters.append("system:" + str(provider))
        query["$and"].append({"actor.mbox": {"$in": provider_filters}})
    else:
        query["$and"].append({"actor.mbox": {"$regex": "^system"}})

    return query


def get_system_statements(collection, providers=[]):
    """
    Filters statements in LRS db by special system user and optionally a list of providers.

    :param providers: optional list of provider objects used for selecting relevant system statements
    :param collection: lrs database containing xapi statements
    :return: all relevant statements
    """

    system_statement_query = get_system_statement_query(providers)
    return collection.find(system_statement_query)


def replace_provider_id(statement, providers = []):
    if statement.get("actor", {}).get("mbox", "").startswith("system:"):
        provider_id = int((statement.get("actor", {}).get("mbox", "").split("system:"))[1])
        provider = next((x for x in providers if x.id == provider_id), None)
        if provider:
            statement["actor"]["mbox"] = "system:" + provider.name
        else:
            # fallback - return unmodified statement
            return statement
    else:
        return statement


def dprint(*args, **kwargs):
    """Print only if DEBUG is enabled."""
    if settings.DEBUG:
        print(*args, **kwargs)

class GetProviderData(APIView):
    """
    Endpoint that allows an analytics engine to obtain provider statements from the LRS.
    """

    def build_mongo_query(self, filters, active_verbs, anon_verbs, providers):
        """
        Dynamically construct MongoDB query based on request filters.

        All filters (except for 'last_object_id') are applied directly,
        so you can pass MongoDB operators in your JSON payload.
        """
        query = {"$and": [
            {"actor.tan": {"$exists": False}},
            {"verb.id": {"$in": active_verbs}}
        ]}

        # Handle 'last_object_id' separately.
        if filters.get("last_object_id"):
            try:
                query["$and"].append({"_id": {"$gt": ObjectId(filters["last_object_id"])}})
            except Exception:
                pass  # Ignore invalid ObjectId

        # Add all remaining filters directly.
        for key, value in filters.items():
            if key == "last_object_id":
                continue  # Already handled
            query["$and"].append({key: value})

        # Append additional fixed filtering logic.
        query["$and"].append({
            "$or": [
                {
                    "$and": [
                        {"actor.mbox": {"$exists": True}},
                        {"actor.mbox": {"$regex": f"^{settings.ANONYMIZATION_HASH_PREFIX}"}},
                        {"verb.id": {"$in": anon_verbs}},
                    ]
                },
                {"actor.mbox": {"$exists": False}},
                {
                    "$and": [
                        {"actor.mbox": {"$exists": True}},
                        {"actor.mbox": {"$regex": "^mailto"}},
                    ]
                },
                get_system_statement_query(providers),
            ]
        })

        return query

    def post(self, request):
        start_time = time.time()

        # Extract Authorization Token from header (without "Basic " prefix)
        token = request.headers.get("Authorization", "").replace("Basic ", "").strip()
        dprint(f"Token extracted: {token}")

        try:
            # Validate incoming request data
            req_serializer = GetProviderDataRequestSerializer(data=request.data)
            req_serializer.is_valid(raise_exception=True)
            dprint("Request serializer validated")

            # Fetch the analytics token based on the provided key
            analytics_token = AnalyticsToken.objects.get(key=token)
            dprint("Analytics token retrieved")

            if analytics_token.expires and analytics_token.expires <= timezone.now():
                dprint("Token has expired")
                return Response(
                    {"message": "Token has expired."},
                    status=status.HTTP_401_UNAUTHORIZED
                )

            # Extract filters from the request payload
            filters = request.data.copy()
            page_size = int(filters.pop("page_size", settings.DEFAULT_PAGE_SIZE))

            # Fetch active verbs for this token
            db_fetch_start = time.time()
            active_verbs = list(
                AnalyticsTokenVerb.objects.filter(analytics_token_id=analytics_token)
                .values_list("verb", flat=True)
            )
            dprint(f"Active verbs retrieved in {time.time() - db_fetch_start:.6f} seconds: {active_verbs}")

            # Fetch providers associated with the token
            providers = list(
                AnalyticsTokenVerb.objects.filter(analytics_token_id=analytics_token)
                .values_list("provider", flat=True)
                .distinct()
            )
            
            collection = lrs_db["statements"]

            # Determine anonymous verbs based on provider schemas and counts
            anon_verbs = []
            for provider in providers:
                try:
                    latest_schema = ProviderSchema.objects.get(provider=provider, superseded_by__isnull=True)
                except ObjectDoesNotExist:
                    dprint(f"No consent provider schema found for provider: {provider}")
                    return JsonResponse(
                        {"message": "No consent provider schema found.", "provider": str(provider)},
                        safe=False,
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR
                    )

                groups = latest_schema.groups()  
                dprint(groups)

                # Iterate over groups and their verbs
                for group in groups:
                    for verb in group.verbs.all():
                        if verb.verb_id in active_verbs and verb.allow_anonymized_collection:
                            min_count = getattr(
                                verb,
                                "allow_anonymized_collection_min_count",
                                settings.ANONYMIZATION_DEFAULT_MINIMUM_COUNT
                            )
                            
                            query_start = time.time()
                            current_count = len(
                                collection.distinct("actor.mbox", {
                                    "$and": [
                                        {"verb.id": verb.verb_id},
                                        {"actor.mbox": {"$exists": True}},
                                        {"actor.mbox": {"$regex": f"^{settings.ANONYMIZATION_HASH_PREFIX}"}}
                                    ]
                                })
                            )
                            dprint(f"Query for verb {verb.verb_id} executed in {time.time() - query_start:.6f} seconds")

                            if current_count >= min_count:
                                anon_verbs.append(verb.verb_id)
            dprint(f"Anonymous verbs determined: {anon_verbs}")

            # Construct the final MongoDB query
            query_start_time = time.time()
            query = self.build_mongo_query(filters, active_verbs, anon_verbs, providers)
            dprint(f"Query constructed in {time.time() - query_start_time:.6f} seconds: {query}")

            # Execute the MongoDB query with limit and batch size
            execution_start = time.time()
            cursor = collection.find(query).limit(page_size).batch_size(page_size)
            dprint(f"Query executed in {time.time() - execution_start:.6f} seconds")

            # Process the result set
            data_time = time.time()
            statements = [replace_provider_id(item) for item in cursor]
            dprint(f"Data mapping executed in {time.time() - data_time:.6f} seconds")

            response_data = {
                "verbs": list(set(active_verbs)),  # Remove duplicates
                "statements": statements,
                "page_size": page_size,
            }

            # Validate and serialize response data
            serializer = ProviderDataSerializer(data=response_data)
            if serializer.is_valid():
                dprint(f"Total execution time: {time.time() - start_time:.6f} seconds")
                return Response(serializer.data, status=status.HTTP_200_OK)

            dprint("Failed to serialize response data")
            return Response(
                {"message": "Failed to serialize response data.", "errors": serializer.errors},
                status=status.HTTP_400_BAD_REQUEST,
            )

        except ObjectDoesNotExist:
            dprint(f"Invalid analytics token: {token}")
            return Response(
                {"message": "Invalid analytics token " + token},
                status=status.HTTP_401_UNAUTHORIZED
            )

class StoreAnalyticsEngineResult(APIView):
    """
    Endpoint that allows an analytics engine to store results data in the database.
    """

    def post(self, request):
        token = request.data.get("token")
        
        if token is None:
            auth_header = request.headers.get("Authorization")
            if auth_header and "Basic " in auth_header:
                token = auth_header.split("Basic ")[1]
            else:
                return Response(
                    {"message": "Missing token in request data and Authorization header."},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        
        try:
            analytics_token = AnalyticsToken.objects.get(key=token)
            collection = lrs_db["results"]
            collection.insert_one(
                {
                    "created_at": datetime.now(),
                    #"provider_id": analytics_token.provider.id,
                    "name": analytics_token.name,
                    "analytics_token": token,
                    "result": request.data.get("result"),
                    "description": request.data.get(
                        "description",
                        {
                            "de": "Es wurde leider bislang keine Beschreibung hinterlegt.",
                            "en": "Sorry, no description has been deposited yet.",
                        },
                    ),
                    "context_id": request.data.get("context_id"),
                }
            )
            return Response(
                {"message": "Result stored successfully."},
                status=status.HTTP_200_OK,
            )
        except ObjectDoesNotExist:
            return Response(
                {"message": "Invalid analytics token."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except Exception as e:
            print(e)
            return Response(
                {"message": "Failed to store analytics engine result."},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class GetAnalyticsEngineResult(APIView):
    """
    Endpoint to get analytics engine results. The request needs to contain a valid JWT Token.
    """

    def get(self, request):
        try:
            token = request.headers.get("Authorization").split("Basic ")[1]
            header_data = jwt.get_unverified_header(token)
            public_key = settings.JWT_AUTH["JWT_PUBLIC_KEY"]
            payload = jwt.decode(
                token,
                key=public_key,
                algorithms=[
                    header_data["alg"],
                ],
            )

            if payload.get("engines") is None:
                return Response(
                    {"message": "Invalid token"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            engines = AnalyticsToken.objects.filter(id__in=payload["engines"]).all()
            engine_names = [engine.name for engine in engines]

            collection = lrs_db["results"]

            engine_results = collection.aggregate(
                [
                    {"$match": {"name": {"$in": engine_names}}},
                    {"$sort": {"created_at": -1}},
                    {
                        "$group": {
                            "_id": "$name",
                            "latest_result": {"$first": "$result"},
                            "description": {"$first": "$description"},
                            "created_at": {"$first": "$created_at"},
                        }
                    },
                    {
                        "$project": {
                            "engine_name": "$_id",
                            "_id": 0,
                            "latest_result": 1,
                            "description": 1,
                            "created_at": 1,
                        }
                    },
                ]
            )

            result_dict = {}
            for result in engine_results:
                result_dict[result["engine_name"]] = result

            context_id = payload.get("context_id")

            if context_id is not None:
                engines_with_context = AnalyticsToken.objects.filter(
                    id__in=payload["engines_with_context"]
                ).all()

                names = [engine.name for engine in engines_with_context]
                context_engine_results = collection.aggregate(
                    [
                        {
                            "$match": {
                                "$and": [
                                    {"context_id": context_id},
                                    {"name": {"$in": names}},
                                ]
                            }
                        },
                        {"$sort": {"created_at": -1}},
                        {
                            "$group": {
                                "_id": "$name",
                                "latest_result": {"$first": "$result"},
                                "created_at": {"$first": "$created_at"},
                            }
                        },
                        {
                            "$project": {
                                "engine_name": "$_id",
                                "_id": 0,
                                "latest_result": 1,
                                "created_at": 1,
                            }
                        },
                    ]
                )
                for result in context_engine_results:
                    result_dict[result["engine_name"]] = result

            return Response(result_dict, status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError as e:
            return Response(
                {"message": "Token expired"},
                status=status.HTTP_401_UNAUTHORIZED,
            )
        except ObjectDoesNotExist:
            return Response(
                {"message": "Invalid analytics token"},
                status=status.HTTP_401_UNAUTHORIZED,
            )
        except Exception as e:
            print(e)
            return Response(
                {"message": "Failed to get analytics engine result."},
                status=status.HTTP_400_BAD_REQUEST,
            )



class GetAnalyticsEngineResults(APIView):
    """
    Obtain existing analytics engine results from the database. An analytics token has access to its own results and
    analytics engine results, which are set in the relationship "can_access".
    Supports pagination with optional page number and page size parameters.
    Supports optional filtering by any field within the result object.
    """

    def get(self, request):
        return self.get_analytics_result(request)

    def post(self, request):
        return self.get_analytics_result(request)
    
    def get_analytics_result(self, request):
        auth_header = request.headers.get("Authorization")
        if not auth_header or "Basic " not in auth_header:
            return Response(
                {"message": "Missing or invalid Authorization header."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        
        try:
            token = auth_header.split("Basic ")[1]
            analytics_token = AnalyticsToken.objects.get(key=token)

            collection = lrs_db["results"]
            # Pagination parameters
            filters = request.data if request.method == "GET" else request.data
            page = int(filters.get("page", filters.get("page", 1)))
            page_size = int(filters.get("page_size", filters.get("page_size", 10)))
            skip = (page - 1) * page_size
            
            # Filtering parameters
            query = {"name": analytics_token.name}  # Ensure it's a single value, not an array
            
            # Apply filters dynamically
            for key, value in filters.items():
                if key in ["page", "page_size"]:
                    continue  # Skip pagination params
                if key == "created_at":
                    try:
                        date_obj = datetime.strptime(value, "%Y-%m-%d")
                        query["created_at"] = {"$gte": datetime(date_obj.year, date_obj.month, date_obj.day, 0, 0, 0),
                                                 "$lt": datetime(date_obj.year, date_obj.month, date_obj.day, 23, 59, 59)}
                    except ValueError:
                        return Response({"message": "Invalid date format. Use YYYY-MM-DD."}, status=status.HTTP_400_BAD_REQUEST)
                elif key.startswith("result."):
                    query[key] = value  # Filter inside the result JSON object
                else:
                    query[key] = value
            
            total_results = collection.count_documents(query)
            total_pages = (total_results + page_size - 1) // page_size
            
            cursor = collection.find(query, sort=[("created_at", -1)]).skip(skip).limit(page_size)
            
            results_dict = {}
            for result in cursor:
                results_dict.setdefault(result.get("name", "unknown"), []).append(
                    {
                        "created_at": result.get("created_at", "unknown"),
                        "result": result.get("result", {}),
                        "context_id": result.get("context_id", "No context ID available"),
                        "description": result.get("description", "No description available")
                    }
                )
            
            return Response({
                "page": page,
                "page_size": page_size,
                "total_pages": total_pages,
                "total_results": total_results,
                "results": results_dict
            }, status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response(
                {"message": "Invalid analytics token."},
                status=status.HTTP_401_UNAUTHORIZED,
            )
        except Exception as e:
            print(e)
            return Response(
                {"message": "Failed to retrieve analytics engine results."},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class CreateVisualizationToken(APIView):
    """
    Creates visualization token for engines, engines_with_context and context_id. Requests need a valid application token in the header.
    """

    def post(self, request):
        # Check application token validity

        serializer = CreateVisualizationTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        application_token = serializer.validated_data.get("application_token")
        if application_token is None or application_token == "":
            application_token = request.headers.get("Authorization", "").split("Basic ")[-1]

        provider = ProviderAuthorization.objects.filter(key=application_token).first()
        if provider is None:
            print("Invalid access token: " + application_token)
            return JsonResponse(
                {"message": "invalid access token"},
                safe=False,
                status=status.HTTP_401_UNAUTHORIZED,
            )

        private_key = settings.JWT_AUTH["JWT_PRIVATE_KEY"]

        # replace in user_email the value with the polaris email adresse, if the shib connector is active 
        context = serializer.validated_data.get("context_id")
        if context and "user_" in context:
            context_without_user = context.replace("user_","")
            email = shib_connector_resolver(email=context_without_user,provider=provider)
            if email != "":
                user = CustomUser.objects.filter(email=email).first()
                context = context.replace(context_without_user,str(user.id) + "-polaris-id@polaris.com")

        key = serialization.load_ssh_private_key(private_key.encode(), password=b"")
        # use delta from settings
        exp = datetime.now() + timedelta(
            # minutes=settings.JWT_AUTH["JWT_EXPIRATION_DELTA_MINUTES"]
            days=7
        )
        payload_data = {
            "context_id": context,
            "exp": int(exp.timestamp()),
            "engines": [engine.id for engine in serializer.validated_data["engines"]],
            "engines_with_context": [
                engine.id
                for engine in serializer.validated_data["engines_with_context"]
            ],
        }
        new_token = jwt.encode(payload=payload_data, key=key, algorithm="RS256")
        return Response(
            {"token": new_token},
            status=status.HTTP_200_OK,
        )


class RunResultsRetention(APIView):
    """
    This data retention keeps only the last 31 results in the database for a specific provider.
    The provider is determined by the analytics token.
    """

    def post(self, request):
        token = request.headers.get("Authorization").split("Basic ")[1]
        try:
            AnalyticsToken.objects.get(key=token)
        except ObjectDoesNotExist:
            return Response(
                {"message": "Invalid analytics token"},
                status=status.HTTP_401_UNAUTHORIZED,
            )
        collection = lrs_db["results"]
        query = {"analytics_token": token}
        for context_id_distinct in collection.distinct("context_id",query):
            print("Run result retention {0}".format(context_id_distinct))
            cursor = collection.find({"analytics_token": token, "context_id": context_id_distinct}, sort=[("created_at", -1)]).skip(settings.ANALYTICS_RESULTS_RETENTION)
            document_ids = [document["_id"] for document in cursor]
            if len(document_ids) > 0:
                query = {"_id": {"$in": document_ids}}
                cursor = collection.delete_many(query)
                return Response({"deleted_documents": cursor.deleted_count}, status=status.HTTP_200_OK)
        return Response({"deleted_documents": 0}, status=status.HTTP_200_OK)




class AddAnalyticsEngineAccess(APIView):
    """
    An analytics engine can access results from another analytics engine only
    if the used analytics token was set in the can_access relation.
    This function add an analytics token to the can_access relation of an analytics token.
    """

    permission_classes = [IsAuthenticated, IsAnalyst]

    def post(self, request):
        serializer = AnalyticsEngineAccessSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        serializer.validated_data["target"].can_access.set(
            serializer.validated_data["ids"]
        )
        serializer.validated_data["target"].save()

        serializer = AnalyticsTokenSerializer(
            serializer.validated_data["target"], many=False
        )
        return Response(serializer.data, status=status.HTTP_200_OK)