from django.urls import path

from . import views

urlpatterns = [
    path("list", views.GetProvidersView.as_view()),
    path("<int:provider_id>/schema", views.CreateProviderSchemaView.as_view()),
    path("keys", views.GetAllProviderKeysView.as_view()),
    path("<int:provider_id>/keys", views.GetProviderKeysView.as_view()),
    path(
        "analytics-tokens", views.AnalyticsTokensList.as_view(), name="analyticstokens"
    ),
    path(
        "analytics-tokens/create",
        views.CreateAnalyticsToken.as_view(),
        name="analyticstokens",
    ),
    path(
        "analytics-tokens/<int:token_id>/update-verbs",
        views.UpdateAnalyticsTokensVerbs.as_view(),
        name="analyticstokensactiveverbs",
    ),
    path(
        "analytics-tokens/<int:token_id>/image",
        views.UpdateAnalyticsTokenImage.as_view(),
        name="analyticstokensactiveverbs",
    ),
        path(
        "analytics-tokens/<int:token_id>/image/<image_name>",
        views.GetAnalyticsTokenImage.as_view(),
        name="analyticstokensactiveverbs",
    ),
    path(
        "analytics-tokens/<int:pk>/delete",
        views.DeleteAnalyticsToken.as_view(),
        name="analyticstokensdelete",
    ),
    path(
        "analytics-tokens/name-available",
        views.CheckAnalyticsTokenNameAvailable.as_view(),
        name="analyticstokensnameavailable",
    ),
        path(
        "analytics-tokens/available-verbs",
        views.GetAnalyticsTokensAvailableVerbsList.as_view(),
        name="analyticstokensavailableverbs",
    ),
    path("data", views.GetProviderData.as_view(), name="providerdata"),
    path(
        "store-result",
        views.StoreAnalyticsEngineResult.as_view(),
        name="storeanalyticsengineresult",
    ),
    path(
        "result",
        views.GetAnalyticsEngineResult.as_view(),
        name="geteanalyticsengineresult",
    ),
    path(
        "results",
        views.GetAnalyticsEngineResults.as_view(),
        name="geteanalyticsengineresults",
    ),
    path(
        "visualization-tokens/create",
        views.CreateVisualizationToken.as_view(),
        name="createvisualizationtoken",
    ),
    path(
        "results-retention",
        views.RunResultsRetention.as_view(),
        name="resultsretention",
    ),
    path(
        "analytics-tokens/sync-engine-access",
        views.AddAnalyticsEngineAccess.as_view(),
        name="addengineaccess",
    ),
]
