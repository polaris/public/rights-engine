import os

from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase
from rest_framework.test import APIClient
from rolepermissions.roles import assign_role

from users.models import CustomUser

from .models import Provider, ProviderAuthorization

PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))

class ProviderTestCase(TestCase):

    test_user_email = "test@mail.com"
    test_user_password = "test123"

    test_provider_email = "test2@mail.com"
    test_provider_password = "test123"

    def setUp(self):
        normal_user = CustomUser.objects.create_user(self.test_user_email, self.test_user_password)
        provider_user = CustomUser.objects.create_user(self.test_provider_email, self.test_provider_password)

        assign_role(provider_user, 'provider')
        assign_role(normal_user, 'user')
        assign_role(provider_user, 'provider_manager')

        response=self.client.post('/api/v1/auth/token', {"email": self.test_user_email, "password": self.test_user_password}) # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue('access' in response.data)
        user_token = response.data['access']

        response=self.client.post('/api/v1/auth/token', {"email": self.test_provider_email, "password": self.test_provider_password}) # obtain token
        self.assertEqual(response.status_code, 200)
        self.assertTrue('access' in response.data)
        provider_token = response.data['access']

        self.user_client = APIClient()
        self.user_client.credentials(HTTP_AUTHORIZATION='Bearer ' + user_token)

        self.provider_client = APIClient()
        self.provider_client.credentials(HTTP_AUTHORIZATION='Bearer ' + provider_token)

        # we need provider schemas for many of these tests
        with open(os.path.join(PROJECT_PATH, "static/provider_schema_h5p_v1.example.json")) as fp:
            response=self.provider_client.put('/api/v1/consents/provider/create', {
                    "provider-schema": fp
                }, format='multipart')
            self.assertEqual(response.status_code, 201)
            self.assertTrue('message' in response.json())

    def test_deny_anonymous(self):
        response=self.client.get('/api/v1/provider/list')
        self.assertEqual(response.status_code, 401) # we are not logged in, so return 401

    def test_provider_list_view(self):
        response=self.user_client.get('/api/v1/provider/list')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.json()) > 0)
        self.assertTrue("id" in response.json()[0])

    def test_provider_keys(self):
        response=self.provider_client.get('/api/v1/provider/keys')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.json()) > 0)
        keydict = response.json()[0]
        self.assertTrue("provider" in keydict and "keys" in keydict)
        self.assertTrue(len(keydict["keys"]) > 0)

    def test_single_provider_key(self):
        try:
            provider = Provider.objects.order_by('id').first()
        except ObjectDoesNotExist:
            self.assertTrue(False)
        keys = [auth.key for auth in ProviderAuthorization.objects.filter(provider=provider)]
        self.assertTrue(len(keys) > 0)

        response=self.provider_client.get('/api/v1/provider/' + str(provider.id) + '/keys')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.json()) > 0)
        self.assertTrue("provider" in response.json() and "keys" in response.json())
        self.assertTrue(len(response.json()["keys"]) > 0)
        self.assertTrue(response.json()["keys"][0] == keys[0])
