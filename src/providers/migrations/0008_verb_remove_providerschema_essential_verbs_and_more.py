import json

from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations, models
import django.db.models.deletion

groups_map = {}

def migrate_schema_to_objects(apps, schema_editor):
    ProviderSchema = apps.get_model('providers', 'ProviderSchema')
    Verb = apps.get_model('providers', 'Verb')
    VerbObject = apps.get_model('providers', 'VerbObject')
    ProviderVerbGroup = apps.get_model('providers', 'ProviderVerbGroup')

    for schema in ProviderSchema.objects.all():
        groups_map[schema.id] = []
        groups_data = schema.groups
        essential_verbs = schema.essential_verbs
        for group_data in groups_data:
            group = ProviderVerbGroup.objects.create(
                provider=schema.provider,
                provider_schema=schema,
                group_id = group_data["id"],
                label = group_data["label"],
                description = group_data["description"],
                purpose_of_collection = group_data["purposeOfCollection"],
                requires_consent = True,
            )
            groups_map[schema.id].append(group.id)
            for verb_data in group_data["verbs"]:
                try:
                    verb = Verb.objects.get(
                        provider=schema.provider,
                        provider_schema=schema,
                        verb_id=verb_data["id"],
                    )
                except ObjectDoesNotExist:
                    verb = Verb.objects.create(
                        provider=schema.provider,
                        provider_schema = schema,
                        verb_id = verb_data["id"],
                        label = verb_data["label"],
                        description = verb_data["description"],
                        default_consent = verb_data["defaultConsent"] if "defaultConsent" in verb_data.keys() else False,
                        active = schema.superseded_by is None,
                        essential = False,
                        allow_anonymized_collection = verb_data["allowAnonymizedCollection"]
                            if "allowAnonymizedCollection" in verb_data.keys() else False,
                    )
                    for verb_object_data in verb_data["objects"]:
                        verb_object = VerbObject.objects.create(
                            verb=verb,
                            object_id=verb_object_data["id"],
                            label=verb_object_data["label"] if "label" in verb_object_data.keys() else "",
                            object_type=verb_object_data["objectType"] if "objectType" in verb_object_data.keys() else "Activity",
                            matching=verb_object_data["matching"] if "matching" in verb_object_data.keys() else "definitionType",
                            definition=verb_object_data["definition"]
                        )
                group.verbs.add(verb)
        for verb_data in essential_verbs:
            try:
                verb = Verb.objects.get(
                    provider=schema.provider,
                    provider_schema=schema,
                    verb_id=verb_data["id"],
                )
                verb.essential = True
                verb.save()
            except ObjectDoesNotExist:
                verb = Verb.objects.create(
                    provider=schema.provider,
                    provider_schema=schema,
                    verb_id=verb_data["id"],
                    label=verb_data["label"],
                    description=verb_data["description"],
                    default_consent=verb_data["defaultConsent"],
                    active=schema.superseded_by is None,
                    essential=True,
                    allow_anonymized_collection=verb_data["allowAnonymizedCollection"]
                    if "allowAnonymizedCollection" in verb_data.keys() else False,
                )
                for verb_object_data in verb_data["objects"]:
                    verb_object = VerbObject.objects.create(
                        verb=verb,
                        object_id=verb_object_data["id"],
                        label=verb_object_data["label"] if "label" in verb_object_data.keys() else "",
                        object_type=verb_object_data["objectType"] if "objectType" in verb_object_data.keys() else "Activity",
                        matching=verb_object_data["matching"] if "matching" in verb_object_data.keys() else "definitionType",
                        definition=json.dumps(verb_object_data["definition"])
                    )


class Migration(migrations.Migration):

    dependencies = [
        ('providers', '0007_providerschema_additional_lrs'),
    ]

    operations = [
        migrations.CreateModel(
            name='Verb',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('verb_id', models.CharField(max_length=250)),
                ('label', models.TextField()),
                ('description', models.TextField()),
                ('default_consent', models.BooleanField()),
                ('active', models.BooleanField(default=True)),
                ('essential', models.BooleanField(default=False)),
                ('allow_anonymized_collection', models.BooleanField(default=False)),
                ('provider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='providers.provider')),
            ],
        ),
        migrations.CreateModel(
            name='VerbObject',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('object_id', models.CharField(max_length=250)),
                ('object_type', models.CharField(max_length=250)),
                ('matching', models.CharField(max_length=250)),
                ('label', models.CharField(max_length=250)),
                ('definition', models.JSONField()),
                ('verb', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='providers.verb')),
            ],
        ),
        migrations.AddField(
            model_name='verb',
            name='provider_schema',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='providers.providerschema'),
        ),
        migrations.CreateModel(
            name='ProviderVerbGroup',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_id', models.CharField(max_length=250)),
                ('label', models.TextField()),
                ('description', models.TextField()),
                ('purpose_of_collection', models.TextField()),
                ('requires_consent', models.BooleanField(default=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('provider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='providers.provider')),
                ('provider_schema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='providers.providerschema')),
                ('verbs', models.ManyToManyField(to='providers.verb')),
            ],
        ),
        migrations.RunPython(migrate_schema_to_objects, reverse_code=migrations.RunPython.noop),
        migrations.RemoveField(
            model_name='providerschema',
            name='essential_verbs',
        ),
        migrations.RemoveField(
            model_name='providerschema',
            name='groups',
        ),
    ]
