from django.conf import settings
from django.db import models


class Provider(models.Model):
    definition_id = models.CharField(
        max_length=255
    )  # id gotten from uploaded definition
    name = models.CharField(max_length=200)
    description = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Provider: " + self.name


class ProviderSchema(models.Model):
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    superseded_by = models.ForeignKey("self", null=True, on_delete=models.SET_NULL)
    additional_lrs = models.JSONField(default=list)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def groups(self):
        return ProviderVerbGroup.objects.filter(provider_schema=self).all()

    def verbs(self):
        return Verb.objects.filter(essential=False, provider_schema=self).all()

    def essential_verbs(self):
        return Verb.objects.filter(essential=True, provider_schema=self).all()

    def __str__(self):
        return f"Provider Schema {self.id}: ({len(self.verbs())} verbs, {len(self.essential_verbs())} essential verbs, {len(self.groups())} groups)"

class Verb(models.Model):
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    provider_schema = models.ForeignKey(ProviderSchema, on_delete=models.CASCADE)
    verb_id = models.CharField(max_length=250)
    label = models.TextField()
    description = models.TextField()
    default_consent = models.BooleanField()
    active = models.BooleanField(default=True)
    essential = models.BooleanField(default=False)
    allow_anonymized_collection = models.BooleanField(default=False)

    def __str__(self):
        return self.verb_id

class VerbObject(models.Model):
    verb = models.ForeignKey(Verb, on_delete=models.CASCADE)
    object_id = models.CharField(max_length=250)
    object_type = models.CharField(max_length=250)
    matching = models.CharField(max_length=250)
    label = models.CharField(max_length=250)
    definition = models.JSONField()


class ProviderVerbGroup(models.Model):
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    provider_schema = models.ForeignKey(ProviderSchema, on_delete=models.CASCADE)
    group_id = models.CharField(max_length=250)
    label = models.TextField()
    description = models.TextField()
    purpose_of_collection = models.TextField()
    requires_consent = models.BooleanField(default=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    verbs = models.ManyToManyField(Verb)

    def __str__(self):
        return (f"Provider Verb Group {self.id}:\n"
                f"- contains {len(self.verbs.all())} verbs\n"
                f"- group ID: {self.group_id}\n"
                f"- provider Schema ID: {self.provider_schema_id}\n")



class ProviderAuthorization(models.Model):
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    key = models.CharField(max_length=200)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.key


class AnalyticsToken(models.Model):
    name = models.CharField(max_length=64, db_index=True, unique=True)
    description = models.TextField(max_length=2500, null=True)
    creator = models.ForeignKey("users.CustomUser", on_delete=models.CASCADE)
    key = models.CharField(max_length=200)
    can_access = models.ManyToManyField(
        "self", related_name="result_access", symmetrical=False
    )
    expires = models.DateTimeField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    image_path = models.CharField(max_length=250, null=True)
        
    def __str__(self):
        return "Analytics Token " + self.key

class AnalyticsTokenVerb(models.Model):
    verb = models.CharField(max_length=250)
    analytics_token = models.ForeignKey("providers.AnalyticsToken", on_delete=models.CASCADE)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Analytics Token Verb: " + self.verb
