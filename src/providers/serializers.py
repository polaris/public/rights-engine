import secrets
from datetime import datetime, timedelta

from rest_framework import serializers

from providers.models import AnalyticsToken, AnalyticsTokenVerb, Provider, ProviderSchema, ProviderVerbGroup
from users.models import CustomUser


class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider
        fields = ["id"]


class ConsentVerbSerializer(serializers.Serializer):
    id = serializers.CharField()
    group_id = serializers.PrimaryKeyRelatedField(
        queryset=ProviderVerbGroup.objects.all()
    )
    consented = serializers.BooleanField()
    objects = serializers.CharField()


class SubAnalyticsTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalyticsToken
        fields = "__all__"


class CreateVisualizationTokenSerializer(serializers.Serializer):
    engines = serializers.ListSerializer(
        child=serializers.SlugRelatedField(
            queryset=AnalyticsToken.objects.all(), slug_field="name"
        )
    )
    engines_with_context = serializers.ListSerializer(
        child=serializers.SlugRelatedField(
            queryset=AnalyticsToken.objects.all(), slug_field="name"
        ),
        required=False,
        default=[],
    )
    context_id = serializers.CharField(required=False)
    application_token = serializers.CharField(required=False)


class AnalyticsTokenVerbSerializer(serializers.ModelSerializer):

    class Meta:
        model = AnalyticsTokenVerb
        fields = "__all__"

class AnalyticsTokenSerializer(serializers.ModelSerializer):
    can_access = SubAnalyticsTokenSerializer(many=True, read_only=True)
    analyticTokenVerbs = serializers.SerializerMethodField("_analytic_token_verbs")

    def _analytic_token_verbs(self, obj):
        analyticTokenVerbs = AnalyticsTokenVerb.objects.filter(analytics_token_id = obj.id)
        serializer = AnalyticsTokenVerbSerializer(analyticTokenVerbs, many=True)
        return serializer.data

    class Meta:
        model = AnalyticsToken
        fields = "__all__"

class CreateAnalyticsTokenSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    description = serializers.CharField()
    key = serializers.CharField(required=False)
    created_at = serializers.IntegerField(read_only=True)
    expires = serializers.CharField(required=False, allow_null=True)
    creator = serializers.PrimaryKeyRelatedField(  queryset=CustomUser.objects.all(), required=False)
    can_access = SubAnalyticsTokenSerializer(many=True, read_only=True)

    class Meta:
        model = AnalyticsToken
        fields = "__all__"

    def validate_expires(self, expires):
        if expires is None:
            return None
        else:
            try:
                date = datetime.strptime(expires, "%m/%d/%Y")
                if date < (datetime.now() + timedelta(days=5)):
                    raise serializers.ValidationError(
                        "Expires needs to be at least 5 days in the future."
                    )
                return date
            except ValueError as e:
                raise serializers.ValidationError(e)

    def create(self, validated_data):
        token = secrets.token_hex(32)
        current_user = self.context["current_user"]
        newToken = AnalyticsToken.objects.create(
            creator=current_user,
            expires=validated_data["expires"],
            name=validated_data["name"],
            description=validated_data["description"],
            key=token,
        )
        return newToken


class VerbSerializer(serializers.Serializer):
    id = serializers.CharField()
    label = serializers.CharField()
    selected = serializers.BooleanField()


class StatementSerializer(serializers.Serializer):
    def to_internal_value(self, value):
        value["_id"] = str(value["_id"])
        return value

    def to_representation(self, instance):
        return instance


class ProviderDataSerializer(serializers.Serializer):
    verbs = serializers.ListSerializer(child=serializers.CharField())
    statements = StatementSerializer(many=True)
    page_size = serializers.IntegerField()


class GetProviderDataRequestSerializer(serializers.Serializer):
    last_object_id = serializers.CharField(required=False)
    page_size = serializers.IntegerField(required=False)


class AnalyticsEngineAccessSerializer(serializers.Serializer):
    target = serializers.PrimaryKeyRelatedField(queryset=AnalyticsToken.objects.all())
    ids = serializers.ListSerializer(
        child=serializers.PrimaryKeyRelatedField(queryset=AnalyticsToken.objects.all())
    )


class GetUsersConsentsThirdPartySerializer(serializers.Serializer):
    user = serializers.SlugRelatedField(
        queryset=CustomUser.objects.all(), slug_field="email"
    )

class ConsentUserVerbThirdPartySerializer(serializers.Serializer):
    user_id = serializers.CharField()
    #user_id = serializers.SlugRelatedField(
    #    queryset=CustomUser.objects.all(), slug_field="email"
    #)
    provider_schema_id = serializers.PrimaryKeyRelatedField(
        queryset=ProviderSchema.objects.all()
    )
    verbs = serializers.ListSerializer(child=ConsentVerbSerializer())
