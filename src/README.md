# Polaris Backend

## virtualenv
First, you should create a virtual environment to start the development:

```console
$ pip3 install virtualenv
$ virtualenv env
$ source env/bin/activate
```

## Setup environment

```console
$ pip3 install -r requirements.txt
$ cp backend/.env.dist backend/.env
```

Set all variables in the file `backend/.env`. TODO Explain each variable 
The jwt files can be generated using the following command:

```console
$ cd backend
$ ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key
$ openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
```

## Dev server

Adding export to .bashrc

```console
$ export DJANGO_DEVELOPMENT=true
```

or

```console
$ DJANGO_DEVELOPMENT=true python manage.py runserver
```

## Migrations and create mongo index

```console
$ python manage.py migrate
$ python manage.py create_mongo_index
```

## Make migration

```console
$ python manage.py makemigrations
```

## Create superuser

```console
$ python3 manage.py createsuperuser
```

## Create normal user

### Role provider

```console
$ python3 manage.py create_user provider@polaris.com polaris321 -p
```

### Role provider, provider manager and analyst

```console
$ python3 manage.py create_user provider@polaris.com polaris321 -p -m -a
```

### Role user

```console
$ python3 manage.py create_user user1@polaris.com polaris321
```

## Create fixtures (seeds)

```console
$ python3 manage.py dumpdata --natural-foreign --natural-primary --indent 4 > fixtures/initial_db.json
```

```console
$ python3 manage.py dumpdata --indent 4 > fixtures/initial_db.json
```

| E-Mail               | Password   | Superuser | Provider | Provider Manage | Analyst |
| -------------------- | ---------- | --------- | -------- | --------------- | ------- |
| admin@polaris.com    | polaris321 | x         | x        | x               | x       |
| provider@polaris.com | polaris321 | -         | x        | x               | x       |
| user1@polaris.com    | polaris321 | -         | -        | -               | -       |
| user2@polaris.com    | polaris321 | -         | -        | -               | -       |
| user3@polaris.com    | polaris321 | -         | -        | -               | -       |
| user4@polaris.com    | polaris321 | -         | -        | -               | -       |
| user5@polaris.com    | polaris321 | -         | -        | -               | -       |

## Load fixtures

```console
$ python3 manage.py loaddata fixtures/initial_db.json
```

## Clear database

```console
$ python3 manage.py sqlflush | python3 manage.py dbshell
```

## Background jobs

Start redis server

```console
$ redis-server
```

Start celery worker

```console
$ celery -A backend worker --loglevel=info
```

Start celery worker for scheduled tasks

```console
$ celery -A backend beat -l info
```

## API Documentation

Set `DEBUG=True` in `.env` and open `http://localhost:[PORT]/redoc/` in browser.

# Create visualization token

```
$ curl -X POST 127.0.0.1:8003/api/v1/provider/visualization-tokens/create --data '{"engine_names": ["my_engine", "count_statements_one_week"]}' -H "Content-Type: application/json"
```

## Polaris Architecture Overview

![](documentation/polaris_overview.drawio.png "Architecture")

## Running tests

```console
$ python manage.py test
```
