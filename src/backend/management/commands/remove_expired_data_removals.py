from django.core.management.base import BaseCommand

from data_disclosure.tasks import remove_expired


class Command(BaseCommand):
    help = "Removes expired data removal jobs and deletes associated zip files."

    def handle(self, *args, **kwargs):
        remove_expired()
