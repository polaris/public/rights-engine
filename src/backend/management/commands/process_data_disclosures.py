from celery.result import AsyncResult
from django.core.management.base import BaseCommand

from data_disclosure.models import DataDisclosure
from data_disclosure.tasks import process_data_disclosure
from users.models import CustomUser


class Command(BaseCommand):
    help = "Creates user requested data disclosures zip files."

    def get_jobs(self):
        users = CustomUser.objects.all()
        new_jobs = []
        for user in users:
            jobs = DataDisclosure.objects.filter(user=user, status__isnull=True)
            new_jobs.extend(jobs)
        return new_jobs

    def handle(self, *args, **kwargs):
        jobs = self.get_jobs()

        for job in jobs:
            process_data_disclosure.delay(job.id)