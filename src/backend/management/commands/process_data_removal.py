from django.core.management.base import BaseCommand

from data_removal.tasks import process_data_removals


class Command(BaseCommand):
    help = 'Handles data removal jobs.'

    def handle(self, *args, **kwargs):
        process_data_removals.delay()
