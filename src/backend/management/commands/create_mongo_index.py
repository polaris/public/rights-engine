from django.core.management.base import BaseCommand
from pymongo import ASCENDING, DESCENDING, HASHED
from backend.utils import lrs_db

class Command(BaseCommand):
    help = "Create indexes for the MongoDB 'results' and 'statement' collections"

    def handle(self, *args, **kwargs):
        # Define collections and their indexes
        collections_indexes = {
            "results": [
                {"key": [("_id", ASCENDING)], "name": "_id_"},
                {"key": [("context_id", HASHED)], "name": "context_id_hashed"},
                {"key": [("name", ASCENDING)], "name": "name_1"},
                {"key": [("created_at", DESCENDING)], "name": "created_at_-1"},
                {"key": [("name", ASCENDING), ("created_at", DESCENDING)], "name": "name_1_created_at_-1"},
            ],
            "statement": [
                {"key": [("_id", ASCENDING)], "name": "_id_"},
                {"key": [("timestamp", ASCENDING)], "name": "timestamp_1"},
                {"key": [("stored", DESCENDING)], "name": "stored_-1"},
                {"key": [("timestamp", ASCENDING), ("stored", DESCENDING)], "name": "timestamp_1_stored_-1"},
            ]
        }

        # Iterate through each collection and create indexes
        for collection_name, indexes in collections_indexes.items():
            collection = lrs_db[collection_name]
            self.stdout.write(self.style.SUCCESS(f"Creating indexes for collection: {collection_name}"))
            for index in indexes:
                collection.create_index(index["key"], name=index["name"])
                self.stdout.write(self.style.SUCCESS(
                    f"Index '{index['name']}' created successfully in collection '{collection_name}'"
                ))
        
        self.stdout.write(self.style.SUCCESS("All indexes have been created for both collections!"))
