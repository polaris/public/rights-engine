from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
from rolepermissions.roles import assign_role

from users.models import CustomUser


class Command(BaseCommand):
    help = 'Create new user'

    def add_arguments(self, parser):
        parser.add_argument('email', type=str, help='email address')
        parser.add_argument('password', type=str, help='password')

        # Optional arguments
        parser.add_argument('-p', '--provider', action='store_true',
                            help='Assign role provider', )
        parser.add_argument('-m', '--provider-manager', action='store_true',
                            help='Assign role provider manager', )
        parser.add_argument('-a', '--analyst', action='store_true',
                            help='Assign role analyst', )

    def handle(self, *args, **kwargs):
        email = kwargs['email']
        password = kwargs['password']
        assign_provider_role = kwargs['provider']
        assign_provider_manager_role = kwargs['provider_manager']
        assign_analyst_role = kwargs['analyst']

        new_user = CustomUser.objects.create_user(email, password)

        if assign_provider_role:
            assign_role(new_user, 'provider')
        if assign_provider_manager_role:
            assign_role(new_user, 'provider_manager')
        if assign_analyst_role:
            assign_role(new_user, 'analyst')
        assign_role(new_user, 'user')
