from __future__ import absolute_import, unicode_literals

import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')

app = Celery('polaris')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = {
    "remove-expired-data-disclosures-every-hour": {
        "task": "data_disclosure.tasks.remove_expired",
        "schedule": crontab(hour='*/1')
    },
    "process-data-removal-jobs-every-hour": {
        "task": "data_removal.tasks.process_data_removals",
        "schedule": crontab(minute='*/1')
    }
}

app.autodiscover_tasks()
