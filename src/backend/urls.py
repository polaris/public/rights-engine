from django.conf import settings
from django.contrib import admin
from django.http import StreamingHttpResponse
from django.shortcuts import redirect, render
from django.urls import include, path, re_path
from django.views.decorators.csrf import csrf_exempt
from django.views.static import serve  
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

import os

from ssoauth import views

schema_view = get_schema_view(
    openapi.Info(
        title="Polaris Rights Engine Documentation",
        default_version="v1",
        description="REST API Documentation",
        terms_of_service="",
        contact=openapi.Contact(email=""),
        license=openapi.License(name=" License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

def detect_lang_redirect(request):
    #print(request.LANGUAGE_CODE)
    return redirect('/app/de/')

def render_angular_de(request):
    return render(request, "de/index.html")

def render_angular_en(request):
    return render(request, "en/index.html")

def get_static_text(filename):
    @csrf_exempt
    def display_text(request):
        content = open(filename, 'r').read()
        response = StreamingHttpResponse(content)
        response['Content-Type'] = 'text/plain; charset=utf8'
        return response
    return display_text

urlpatterns = [
    path('robots.txt', get_static_text('static/robots_deny.txt' if settings.ALLOW_ROBOTS == "False" or settings.ALLOW_ROBOTS == False else "static/robots_allow.txt")),
    path("admin/", admin.site.urls),
    path('', include('django_prometheus.urls')),
    path("api/v1/consents/", include("consents.urls")),
    path("api/v1/auth/", include("users.urls")),
    path("api/v1/provider/", include("providers.urls")),
    path("api/v1/data-disclosure/", include("data_disclosure.urls")),
    path("api/v1/data-removal/", include("data_removal.urls")),
    path("api/v1/settings/", include("settings.urls")),
    path("xapi/", include("xapi.urls")),
    path("api/v1/xapi/", include("xapi.urls")),
    path("login-success/", views.LogInSuccessView.as_view(), name="login-success"),
    re_path(r"^(?:.*/)?login/?$", views.LogInView.as_view(), name="sso-login"),  # aggressive login pattern helps against apps that provide own login pages and forms
    re_path(r"^logout/?$", views.LogOutView.as_view(), name="sso-logout"),
    re_path(r"^logout/message/?$", views.LoggedOutLocallyView.as_view(), name="sso-logged-out-locally"),
    re_path(r"^logout/idp/?$", views.IdpLogoutRedirectView.as_view(), name="sso-logout-idp"),
    re_path(r"^saml2/acs/?$", views.ACSAuthNView.as_view(), name="sso-saml2-acs"),
    re_path(r"^saml2/sls/?$", views.SLSView.as_view(), name="sso-saml2-sls"),
    re_path(r"^saml2/meta(?:data)?/?$", views.MetadataView.as_view(), name="sso-saml2-meta"),
    re_path(r"^sso-dev/?$", views.DevView.as_view(), name="sso-dev"),

    path("app/de/", render_angular_de),
    path("app/en/", render_angular_en),
    re_path(r'^app/de/(?P<path>.*)$', serve,{'document_root': os.path.join(settings.BASE_DIR,  "frontend/dist/frontend/de")}),
    re_path(r"^$", detect_lang_redirect),
    re_path(r"^(?:.*)/?$", detect_lang_redirect),
]

if settings.DEBUG == "True":
    urlpatterns.append(
        re_path(
            r"^redoc/$",
            schema_view.with_ui("redoc", cache_timeout=0),
            name="schema-redoc",
        ),
    )
    #urlpatterns.append(
        #re_path(r"^sso-dev/?$", views.DevView.as_view(), name="sso-dev"),
    #)