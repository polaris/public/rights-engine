from django.conf import settings
from rest_framework.permissions import BasePermission
from rolepermissions.checkers import has_permission

from backend.roles import Roles


class IsProvider(BasePermission):
    """
    Allows access only to users with provider role.
    """

    def has_permission(self, request, view):
        return bool(has_permission(request.user, Roles.CREATE_PROVIDER))


class IsProviderManager(BasePermission):
    def has_permission(self, request, view):
        return bool(has_permission(request.user, Roles.MANAGE_PROVIDER_KEYS))


class IsAnalyst(BasePermission):
    def has_permission(self, request, view):
        return bool(has_permission(request.user, Roles.MANAGE_ANALYTICS_TOKENS))
