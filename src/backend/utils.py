from django.conf import settings
from pymongo import MongoClient

if settings.TESTING == "True":
    lrs_db = None
else:
    lrs_db_client = MongoClient(settings.LRS_CONNECTION_STRING)
    lrs_db = lrs_db_client[settings.LRS_MONGO_DB_NAME]

    # Create indexes (ignored if indexes exist)

    lrs_db["results"].create_index("name")
    lrs_db["results"].create_index([("created_at", -1)])
