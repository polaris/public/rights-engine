import base64
from django.conf import settings
from django.http import HttpResponse

class MetricsAuthMiddleware:
    """
    Diese Middleware schützt den /metrics-Endpunkt mittels HTTP Basic Authentication.
    Die Zugangsdaten werden aus den Settings (PROMETHEUS_METRICS_AUTH_USERNAME und
    PROMETHEUS_METRICS_AUTH_PASSWORD) ausgelesen.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Nur für den /metrics-Endpunkt prüfen
        if request.path.startswith('/metrics'):
            auth_header = request.META.get('HTTP_AUTHORIZATION')
            if auth_header and auth_header.startswith('Basic '):
                encoded_credentials = auth_header.split(' ')[1].strip()
                try:
                    decoded_credentials = base64.b64decode(encoded_credentials).decode('utf-8')
                except Exception:
                    return self.unauthorized_response()
                # Erwartetes Format: "username:password"
                if ':' in decoded_credentials:
                    username, password = decoded_credentials.split(':', 1)
                    if (username == settings.PROMETHEUS_METRICS_AUTH_USERNAME and
                        password == settings.PROMETHEUS_METRICS_AUTH_PASSWORD):
                        return self.get_response(request)
            # Kein oder falscher Auth-Header -> Zugriff verweigern
            return self.unauthorized_response()
        return self.get_response(request)

    def unauthorized_response(self):
        response = HttpResponse('Unauthorized', status=401)
        response['WWW-Authenticate'] = 'Basic realm="Metrics"'
        return response
