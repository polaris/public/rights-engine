from rolepermissions.roles import AbstractUserRole
from strenum import StrEnum


class Roles(StrEnum):
    CREATE_USER = 'create_user',
    CREATE_PROVIDER = 'create_provider',
    CHANGE_PROVIDER = 'change_provider',
    MANAGE_PROVIDER_KEYS = 'manage_provider_keys',
    MANAGE_ANALYTICS_TOKENS = 'manage_analytics_tokens',
    CREATE_USER_CONSENT = 'create_user_consent'


class Provider(AbstractUserRole):
    available_permissions = {
        Roles.CREATE_PROVIDER: True,
        Roles.CHANGE_PROVIDER: True,
        Roles.CREATE_USER: True,
    }

class ProviderManager(AbstractUserRole):
    available_permissions = {
        Roles.MANAGE_PROVIDER_KEYS: True,
    }

class Analyst(AbstractUserRole):
    available_permissions = {
        Roles.MANAGE_ANALYTICS_TOKENS: True,
    }

class User(AbstractUserRole):
    available_permissions = {
        Roles.CREATE_USER_CONSENT: True,
    }
