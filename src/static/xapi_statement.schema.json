{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://example.com/product.schema.json",
  "title": "xAPI Statement",
  "description": "Statement for the Experience API (xAPI)",
  "type": "object",
  "properties": {
    "id": {
      "type": "string",
      "format": "uuid"
    },
    "actor": {
      "$ref": "#/$defs/actor"
    },
    "verb": {
      "$ref": "#/$defs/verb"
    },
    "object": {
      "$ref": "#/$defs/object"
    },
    "result": {
      "$ref": "#/$defs/result"
    },
    "context": {
      "$ref": "#/$defs/context"
    },
    "timestamp": {
      "$ref": "#/$defs/timestamp"
    },
    "attachments": {
      "type": "array",
      "items": {
        "$ref": "#/$defs/attachment"
      }
    }
  },
  "required": ["actor", "verb", "object"],
  "$defs": {
    "iri": { "type": "string", "pattern": "^[a-zA-Z0-9_]+" },
    "languageMap": { "type": "object" },
    "agent": {
      "type": "object",
      "properties": {
        "objectType": { "const": "Agent" },
        "name": { "type": "string" },
        "mbox": { "$ref": "#/$defs/mbox" },
        "mbox_sha1sum": { "$ref": "#/$defs/mbox_sha1sum" },
        "openid": { "$ref": "#/$defs/iri" },
        "account": { "$ref": "#/$defs/account" }
      }
    },
    "group": {
      "type": "object",
      "properties": {
        "objectType": { "const": "Group" },
        "member": {
          "type": "array",
          "items": {
            "$ref": "#/$defs/actor"
          }
        }
      }, "required": ["member"]
    },
    "activity": {
      "type": "object",
      "properties": {
        "objectType": { "const": "Activity" },
        "id": { "$ref": "#/$defs/iri" },
        "definition": {
          "$ref": "#/$defs/definition"
        }
      },
      "required": ["objectType", "id"]
    },
    "substatement": {
      "type": "object",
      "properties": {
        "objectType": { "const": "SubStatement" },
        "actor": {
          "$ref": "#/$defs/actor"
        },
        "verb": {
          "$ref": "#/$defs/verb"
        },
        "object": {
          "$ref": "#/$defs/object"
        },
        "result": {
          "$ref": "#/$defs/result"
        },
        "context": {
          "$ref": "#/$defs/context"
        },
        "timestamp": {
          "$ref": "#/$defs/timestamp"
        },
        "attachments": {
          "type": "array",
          "items": {
            "$ref": "#/$defs/attachment"
          }
        }
      },
      "required": ["actor", "verb", "object"]
    },
    "statementref": {
      "type": "object",
      "properties": {
        "objectType": { "const": "StatementRef" },
        "id": { "type": "string", "format": "uuid" }
      },
      "required": ["id"]
    },
    "mbox": { "type": "string", "pattern": "(mailto:[a-z0-9._%+!$&*=^|~#%{}/-]+@([a-z0-9-]+.){1,}([a-z]{2,22}))|(system:[0-9]+)|shib-connector:[a-zA-Z0-9@-]+" },
    "mbox_sha1sum": { "type": "string", "pattern": "^\b[0-9a-f]{5,40}$" },
    "account": {
      "type": "object",
      "properties": {
        "homePage": {
          "$ref": "#/$defs/iri"
        },
        "name": { "type": "string" }
      },
      "required": ["homePage", "name"]
    },
    "actor": {
      "oneOf": [{ "$ref": "#/$defs/agent" }, { "$ref": "#/$defs/group" }]
    },
    "definition": {
      "type": "object",
      "properties": {
        "name": { "$ref": "#/$defs/languageMap" },
        "description": { "$ref": "#/$defs/languageMap" },
        "type": { "$ref": "#/$defs/iri" },
        "moreInfo": { "$ref": "#/$defs/iri" },
        "extensions": { "$ref": "#/$defs/extensions" },
        "interactionType": { "$ref": "#/$defs/interactionType" },
        "correctResponsesPattern": { "type": "array", "items": { "type": "string" } },
        "choices": { "type": "array", "items": { "$ref": "#/$defs/interactionComponent" } },
        "scale": { "type": "array", "items": { "$ref": "#/$defs/interactionComponent" } },
        "source": { "type": "array", "items": { "$ref": "#/$defs/interactionComponent" } },
        "target": { "type": "array", "items": { "$ref": "#/$defs/interactionComponent" } },
        "steps": { "type": "array", "items": { "$ref": "#/$defs/interactionComponent" } }
      }
    },
    "extensions": { "type": "object" },
    "interactionType": {
      "enum": [
        "true-false",
        "choice",
        "fill-in",
        "long-fill-in",
        "matching",
        "performance",
        "sequencing",
        "likert",
        "numeric",
        "other"
      ]
    },
    "interactionComponent": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string"
        },
        "description": { "$ref": "#/$defs/languageMap" }
      },
      "required": ["id"]
    },
    "verb": {
      "type": "object",
      "properties": {
        "id": { "$ref": "#/$defs/iri" },
        "display": { "$ref": "#/$defs/languageMap" }
      },
      "required": ["id"]
    },
    "object": {
      "oneOf": [
        { "$ref": "#/$defs/agent" },
        { "$ref": "#/$defs/group" },
        { "$ref": "#/$defs/activity" },
        { "$ref": "#/$defs/substatement" },
        { "$ref": "#/$defs/statementref" }
      ]
    },
    "result": {
      "type": "object",
      "properties": {
        "score": {
          "type": "object",
          "properties": {
            "scaled": {
              "type": "number"
            },
            "raw": {
              "type": "number"
            },
            "min": {
              "type": "number"
            },
            "max": {
              "type": "number"
            }
          }
        },
        "success": {
          "type": "boolean"
        },
        "completion": {
          "type": "boolean"
        },
        "response": {
          "type": "string"
        },
        "duration": {
          "type": "string"
        },
        "extensions": { "$ref": "#/$defs/extensions" }
      }
    },
    "context": {
      "type": "object",
      "properties": {
        "registration": {
          "type": "string",
          "format": "uuid"
        },
        "instructor": {
          "$ref": "#/$defs/actor"
        },
        "team": {
          "$ref": "#/$defs/group"
        },
        "contextActivities": {
          "type": "object"
        },
        "revision": {
          "type": "string"
        },
        "platform": {
          "type": "string"
        },
        "language": {
          "type": "string",
          "pattern": "^(([a-zA-Z]{2,8}((-[a-zA-Z]{3}){0,3})(-[a-zA-Z]{4})?((-[a-zA-Z]{2})|(-d{3}))?(-[a-zA-Zd]{4,8})*(-[a-zA-Zd](-[a-zA-Zd]{1,8})+)*)|x(-[a-zA-Zd]{1,8})+|en-GB-oed|i-ami|i-bnn|i-default|i-enochian|i-hak|i-klingon|i-lux|i-mingo|i-navajo|i-pwn|i-tao|i-tsu|i-tay|sgn-BE-FR|sgn-BE-NL|sgn-CH-DE)$"
        },
        "statement": {
          "$ref": "#/$defs/statementRef"
        },
        "extensions": {
          "$ref": "#/$defs/extensions"
        }
      }
    },
    "statementRef": {
      "type": "object",
      "properties": {
        "objectType": { "const": "StatementRef" },
        "id": {
          "type": "string",
          "format": "uuid"
        }
      },
      "required": ["id"]
    },
    "timestamp": { "type": "string" },
    "attachment": {
      "type": "object",
      "properties": {
        "usageType": {
          "$ref": "#/$defs/iri"
        },
        "display": {
          "$ref": "#/$defs/languageMap"
        },
        "description": {
          "$ref": "#/$defs/languageMap"
        },
        "contentType": {
          "$ref": "#/$defs/imt"
        },
        "length": {
          "type": "integer"
        },
        "sha": {
          "type": "string"
        },
        "fileUrl": {
          "$ref": "#/$defs/iri"
        }
      },
      "required": ["usageType", "display", "contentType", "length", "sha2"]
    },
    "imt": {
      "type": "string",
      "pattern": "^((application|audio|example|image|message|model|multipart|text|video)(/[-w+.]+)(;s*[-w]+=[-w]+)*;?)$"
    }
  }
}
