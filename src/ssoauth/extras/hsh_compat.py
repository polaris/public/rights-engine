"""
Compatibility features with the `hsh` app
"""

from .. import logger
from django.db.models import Q


try:
    from hsh.models import Account
    HSH_APP_COMPAT_POSSIBLE = True
except ImportError:
    HSH_APP_COMPAT_POSSIBLE = False


ALLOWED_AUTH_PROVIDERS = ["fh-h.de"]
GROUP_NAME_LOOKUP = Q(name__istartswith="IDM_") | Q(name__istartswith="WEB_")



def get_group_names_for_user(user):
    """
    Resolve group names for the user directly from hsh app.
    It's better to get groups directly from the database, instead of trusting AD data received via SAML2.
    """
    try:
        hsh_account = Account.objects.get(username=user.username, auth_provider__in=ALLOWED_AUTH_PROVIDERS)
    except (Account.DoesNotExist, Account.MultipleObjectsReturned,) as e:
        logger.error("hsh.Account not found for {user}. {e.__class__.__name__}: {e}".format(user=user, e=e))
        return set()
    hsh_groups = hsh_account.auth_groups.filter(GROUP_NAME_LOOKUP)
    return {g.name for g in hsh_groups}

