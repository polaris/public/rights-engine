from django import conf
from . import views

try:
    from django.urls import re_path
except ImportError:
    # minimal support for ancient django versions
    # TODO: delete the except clause when we don't have django 1.x anywhere
    from django.conf.urls import url as re_path


urlpatterns = (
    re_path(r"^(?:.*/)?login/?$", views.LogInView.as_view(), name="sso-login"),  # aggressive login pattern helps against apps that provide own login pages and forms
    re_path(r"^logout/?$", views.LogOutView.as_view(), name="sso-logout"),
    re_path(r"^logout/message/?$", views.LoggedOutLocallyView.as_view(), name="sso-logged-out-locally"),
    re_path(r"^logout/idp/?$", views.IdpLogoutRedirectView.as_view(), name="sso-logout-idp"),
    re_path(r"^saml2/acs/?$", views.ACSAuthNView.as_view(), name="sso-saml2-acs"),
    re_path(r"^saml2/sls/?$", views.SLSView.as_view(), name="sso-saml2-sls"),
    re_path(r"^saml2/meta(?:data)?/?$", views.MetadataView.as_view(), name="sso-saml2-meta"),
)

if conf.settings.DEBUG:
    # add the dev view
    urlpatterns += (
        re_path(r"^sso-dev/?$", views.DevView.as_view(), name="sso-dev"),
    )
