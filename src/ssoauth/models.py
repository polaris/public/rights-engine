from django import conf
from django.db import models


class UserMapping(models.Model):
    user = models.OneToOneField(conf.settings.AUTH_USER_MODEL, primary_key=True, on_delete=models.CASCADE, related_name="sso_mapping")
    #uuid = models.UUIDField(null=False)
    uid = models.CharField(null=True, max_length=200)
    anonymized = models.BooleanField(null=False, default=False)
    imported_on = models.DateField(null=False, auto_now_add=True)

    def __str__(self):
        return "{user} <-> {uuid}".format(user=self.user, uuid=self.uuid)

