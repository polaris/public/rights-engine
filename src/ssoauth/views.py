from collections import OrderedDict
from datetime import timedelta

from django import conf, forms, http, urls
from django.contrib import auth as contrib_auth
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth import models as contrib_auth_models
from django.contrib.sites.shortcuts import get_current_site
from django.core import exceptions
from django.shortcuts import redirect
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView, RedirectView, TemplateView, View
from onelogin.saml2.auth import OneLogin_Saml2_Auth
from rest_framework_simplejwt.tokens import RefreshToken

from users.custom_tokens import CustomTokenObtainPairSerializer

from . import app_settings, auth_utils, logger


class SAMLMixin:

    @classmethod
    def get_onelogin_auth(cls, request):
        """ :return: that weird auth object used by the onelogin toolkit """
        if not app_settings.sso_is_configured():
            raise exceptions.ImproperlyConfigured("SSO is not configured.")
        return OneLogin_Saml2_Auth(
            request_data=cls._get_onelogin_request_data(request),
            old_settings=app_settings.ONELOGIN_SETTINGS_OBJECT
        )

    @classmethod
    def _get_onelogin_request_data(cls, request):
        return {
            # since nginx is in the way, the settings cannot be fetched from the request
            "https": "on" if app_settings.SP_SSL else "off",
            "http_host": app_settings.SP_HOST,  # nginx might change it
            "server_port": app_settings.SP_PORT,  # nginx will change it
            "script_name": request.META["PATH_INFO"],
            "get_data": request.GET.copy(),
            "post_data": request.POST.copy(),
        }


@method_decorator(never_cache, "dispatch")
class LogInView(SAMLMixin, RedirectView):
    """
    This view initiates SSO log in.
    To change behavior for already logged in users, you can use:
      - LogInView.as_view(already_authenticated_403=True)
      - LogInView.as_view(already_authenticated_redirect="some-url-name")
    """

    already_authenticated_403 = False  # raise 403 if the user is already authenticated

    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            logger.info("{u} is already authenticated and attempts to log in again.".format(u=self.request.user))
            if self.already_authenticated_403:
                raise exceptions.PermissionDenied("Already authenticated")
            if self.request.user.last_login > timezone.now() - timedelta(seconds=60):
                # most likely a redirect loop (django.contrib.admin loves to cause these)
                logger.warning("{u} has already logged in recently. Probably a redirect loop, happens in django due to insufficient permissions to display some page.".format(u=self.request.user))
                raise exceptions.PermissionDenied()
        if conf.settings.DEBUG and not app_settings.sso_is_configured():
            logger.info("SSO is not configured, redirecting to the dev view instead.")
            return self._get_redirect_to_dev()
        # logging in
        auth = self.get_onelogin_auth(self.request)
        idp_login_url = auth.login(return_to=self.get_next_url(self.request))
        return idp_login_url

    @staticmethod
    def get_next_url(request):
        next_url = request.GET.get(REDIRECT_FIELD_NAME, None) or conf.settings.LOGIN_REDIRECT_URL or "/"
        logger.debug("Will ask IDP to redirect after login to: {}".format(next_url))
        return str(next_url)

    def _get_redirect_to_dev(self):
        redirect_to = urls.reverse("sso-dev")
        if REDIRECT_FIELD_NAME in self.request.GET:  # preserve the next url
            redirect_to = "{0}?{1}={2}".format(
                redirect_to,
                REDIRECT_FIELD_NAME,
                self.request.GET.get(REDIRECT_FIELD_NAME)
            )
        return redirect_to


@method_decorator(never_cache, "dispatch")
class LogOutView(RedirectView):
    """
    Logs the user out locally.
    Redirects the user somewhere depending on the project settings.
    """

    def get_redirect_url(self, *args, **kwargs):
        # log the user out first, no matter SLO or not
        contrib_auth.logout(self.request)
        # decide where to redirect the user
        url_next = self.request.GET.get(REDIRECT_FIELD_NAME, None)
        settings = conf.settings.LOGOUT_REDIRECT_URL
        default = urls.reverse_lazy("sso-logged-out-locally")
        return url_next or settings or default


class LoggedOutLocallyView(TemplateView):
    """
    If you use it, you should better override the default template. See: https://docs.djangoproject.com/en/1.11/howto/overriding-templates/
    """
    template_name = "ssoauth/logged_out_locally.html"

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            # fool-proof
            raise exceptions.PermissionDenied("This user is logged in!")
        else:
            return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["IDP_LOGOUT_URL"] = app_settings.IDP_LOGOUT_URL or "#"
        context["THIS_SITE"] = get_current_site(self.request)
        return context


class IdpLogoutRedirectView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        if app_settings.IDP_LOGOUT_URL:
            return http.HttpResponseRedirect(redirect_to=app_settings.IDP_LOGOUT_URL)
        else:
            raise exceptions.ImproperlyConfigured("IDP_LOGOUT_URL is not set.")


@method_decorator(never_cache, "dispatch")
@method_decorator(csrf_exempt, "dispatch")
class ACSAuthNView(SAMLMixin, View):
    """
    This is NOT a universal ACS. It can only consume artifacts with an AuthN statement.
    It's how OneLogin toolkit works, cannot easily detect/process other statements here, so I don't even try.
    """

    def get(self, *args, **kwargs):
        return http.HttpResponse(status=405)

    def post(self, request, *args, **kwargs):
        auth = self.get_onelogin_auth(request)
        auth.process_response()
        if auth.is_authenticated():
            self.log_in_user(request, auth)
            if conf.settings.DEBUG:
                request.session["DEBUG_SAML2_ATTRS"] = attrs = auth.get_attributes()
                attrs["SAML2.NameID"] = auth.get_nameid()
                attrs["SAML2.NameID.Format"] = auth.get_nameid_format()
                attrs["SAML2.SessionIndex"] = auth.get_session_index()
                attrs["SAML2.Errors"] = auth.get_errors()
            #return http.HttpResponseRedirect(self.get_next_url(request))
            return redirect("login-success")
        else:
            logger.error("Not authenticated. Errors: {0}".format(auth.get_errors()))
            raise exceptions.PermissionDenied()

    @staticmethod
    def get_next_url(request):
        next_url = request.POST.get("RelayState", None)
        if not next_url:
            logger.warning("Did not receive RelayState (redirect target) from the IDP.")
            next_url = conf.settings.LOGOUT_REDIRECT_URL or "/"
        logger.debug("From ACS redirecting to {}".format(next_url))
        return str(next_url)

    def log_in_user(self, request, auth):

        def get_attr(attribute, nullable=False, multivalued=False):
            values = auth.get_attributes().get(attribute, list())
            if not values and not nullable:
                raise ValueError("Received no value(s) for {0}".format(attribute))
            if multivalued:
                return values
            else:
                if len(values) == 0:
                    return "" # that is the case if nullable is True && no value found 
                if len(values) == 1:
                    return values[0]
                else:
                    raise ValueError("Received {1} values for {0}, didn't expect that many.".format(attribute, len(values)))

        logger.error("Synchronizing user using SAML2 data: {}".format(auth.get_attributes()))

        # Build user login email address from uid and email attributes
        
        user_login_email = get_attr(app_settings.ATTRIBUTE_EMAIL)
        if app_settings.ATTRIBUTE_ACCOUNT_UUID is not None:
           user_login_email = get_attr(app_settings.ATTRIBUTE_ACCOUNT_UUID) + "@" + get_attr(app_settings.ATTRIBUTE_EMAIL).split("@")[1]
        # get the user
        user = auth_utils.get_or_create_user(
            uid=get_attr(app_settings.ATTRIBUTE_PAIRWISE_ID),
            email=user_login_email
        )
        # update user data
        auth_utils.update_user_data(
            user=user,
            first_name=get_attr(app_settings.ATTRIBUTE_FORENAME , nullable=True),
            last_name=get_attr(app_settings.ATTRIBUTE_SURNAME , nullable=True),
            email=user_login_email,
            shibboleth_connector_identifier=get_attr(app_settings.ATTRIBUTE_PAIRWISE_ID)
        )
        auth_utils.set_user_groups(
            user=user,
            group_names=get_attr(app_settings.ATTRIBUTE_GROUPS, nullable=True, multivalued=True) or list()
        )
        # other necessary steps
        auth_utils.cleanup_direct_permissions(user=user)  # in case somebody was acting funny
        auth_utils.update_user_compat_flags(user=user)  # superuser, staff
        user.backend = app_settings.PRETEND_AUTH_BACKEND
        request.user = user
        if user.is_active:
            contrib_auth.login(request, user)
            logger.debug("Logged in {user}".format(**locals()))
        else:
            msg = "Inactive user attempted to log in: {user}".format(**locals())
            logger.info(msg)
            raise exceptions.PermissionDenied(msg)


@method_decorator(never_cache, "dispatch")
@method_decorator(xframe_options_exempt, "dispatch")
class SLSView(SAMLMixin, View):
    """
    SLS (Single Logout Service) binding.
    When this view is opened by a user (usually inside of a frame) this user must be unconditionally logged off.
    """

    def dispatch(self, request, *args, **kwargs):
        if app_settings.SP_SLS_ENABLED:
            return super().dispatch(request, *args, **kwargs)
        else:
            logger.warning("SLS is disabled.")
            raise exceptions.PermissionDenied()

    def get(self, *args, **kwargs):

        def logout_callback():
            nonlocal self
            logger.warning("Logging out the user: {0}".format(self.request.user))
            contrib_auth.logout(self.request)
            # self.request.session.flush()

        errors = list()
        try:
            auth = self.get_onelogin_auth(self.request)
            response_url = auth.process_slo(delete_session_cb=logout_callback)
            errors.extend(auth.get_errors())
            if response_url:
                response = http.HttpResponseRedirect(response_url)
            else:
                raise AssertionError("Did not get a response URL.")  # OneLogin toolkit fails to raise an exception
        except Exception as e:
            logger.error("SLO failed. {e.__class__.__name__}: {e}".format(e=e))
            errors.append("{e.__class__.__name__}: {e}".format(e=e))
            response = http.HttpResponseBadRequest(content="Bad SLS request.")
        for error in errors:
            logger.error(error)
        if app_settings.SP_SLS_X_FRAME_OPTIONS:
            response["X-Frame-Options"] = app_settings.SP_SLS_X_FRAME_OPTIONS
        return response


@method_decorator(never_cache, "dispatch")
class MetadataView(SAMLMixin, View):

    def get(self, request, *args, **kwargs):
        meta = app_settings.ONELOGIN_SETTINGS_OBJECT.get_sp_metadata()
        errors = app_settings.ONELOGIN_SETTINGS_OBJECT.validate_metadata(meta)
        if errors:
            for e in errors:
                logger.error(e)
            return http.HttpResponseServerError()
        return http.HttpResponse(content_type="text/xml", content=meta)


@method_decorator(never_cache, "dispatch")
class DevView(FormView):

    class DevForm(forms.Form):
        username = forms.CharField(required=False)
        toggle_group = forms.ModelChoiceField(
            queryset=contrib_auth_models.Group.objects.all(),
            required=False,
        )

    template_name = "ssoauth/dev.html"
    form_class = DevForm

    def __init__(self, *args, **kwargs):
        if not conf.settings.DEBUG:
            raise exceptions.PermissionDenied()
        else:
            super().__init__(*args, **kwargs)

    @property
    def next_url(self):
        return self.request.GET.get(REDIRECT_FIELD_NAME, None)

    def get_context_data(self, **kwargs):
        if not conf.settings.DEBUG:
            raise exceptions.PermissionDenied
        context = super().get_context_data(**kwargs)
        user = self.request.user
        groups = list(user.groups.all()) if user.is_authenticated else list()
        permissions = list(user.get_all_permissions())
        context["tables"] = [
            ["User", OrderedDict([
                ["user", "{0} ({1})".format(self.request.user.username, self.request.user.__class__.__name__)],
                ["groups", ", ".join(str(g) for g in groups)],
                ["permissions", ", ".join(str(p) for p in permissions)],
            ])],
            ["SAML2 Attributes", self.request.session.get("DEBUG_SAML2_ATTRS")],
            ["Session", dict(self.request.session)],
        ]
        context.update(dict(
            sso_is_configured=bool(app_settings.sso_is_configured()),
            next_url=self.next_url,
        ))
        return context

    def form_valid(self, form):
        if not conf.settings.DEBUG:
            raise exceptions.PermissionDenied
        log_in_as_username = form.cleaned_data["username"]
        toggle_group = form.cleaned_data["toggle_group"]
        local_logout = bool(self.request.POST.get("local-logout", None))
        # perform an action
        if local_logout:
            logger.info("Logging out {u}".format(u=self.request.user))
            contrib_auth.logout(self.request)
            if log_in_as_username or toggle_group:
                log_in_as_username, toggle_group = None, None  # single page and single form for everything, avoid weird effects
        if log_in_as_username:
            logger.info("Logging in as {0}".format(log_in_as_username))
            try:
                user = auth_utils.get_user(username=log_in_as_username)
            except exceptions.ObjectDoesNotExist:
                import uuid
                user = auth_utils.get_or_create_user(username=log_in_as_username, uuid=uuid.uuid4())
            user.backend = app_settings.PRETEND_AUTH_BACKEND
            self.request.user = user
            contrib_auth.login(request=self.request, user=user)
        if toggle_group:
            logger.info("Toggling group: {0}".format(toggle_group))
            if self.request.user.is_authenticated:
                if toggle_group in self.request.user.groups.all():
                    self.request.user.groups.remove(toggle_group)
                else:
                    self.request.user.groups.add(toggle_group)
            else:
                logger.warning("Too anonymous to join groups.")
        # update the compat flags, might be needed when user or their groups change
        if self.request.user.is_authenticated:
            auth_utils.update_user_compat_flags(self.request.user, False)
        # redirect
        return http.HttpResponseRedirect(self.next_url or urls.reverse("sso-dev"))

class LogInSuccessView(TemplateView):
    template_name = "ssoauth/login_success.html"

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise exceptions.PermissionDenied("This user is not logged in!")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = CustomTokenObtainPairSerializer.get_token(self.request.user)
        context["refreshToken"] = token
        return context
