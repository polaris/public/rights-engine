import logging

logger = logging.getLogger("ssoauth")

default_app_config = "ssoauth.apps.SSOAuthConfig"


# permission codenames for builtin django features
STAFF_PERM_CODENAME = "staff"
SUPERUSER_PERM_CODENAME = "superuser"

