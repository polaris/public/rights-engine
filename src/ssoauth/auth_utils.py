from uuid import UUID, uuid4

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from . import (STAFF_PERM_CODENAME, SUPERUSER_PERM_CODENAME, app_settings,
               logger, models)
from .extras import hsh_compat


def _validate_username(username):
    if not isinstance(username, str):
        raise TypeError
    if not username == username.lower():  # weird check because .islower() returns False when no letters present
        raise ValueError("Username must be lowere case")


def get_user(uuid=None, username=None):
    """ This helper just gets a user instance. """
    assert bool(uuid) ^ bool(username), "Need uuid OR username"
    if uuid:
        assert isinstance(uuid, UUID), "Not an UUID instance"
        return models.UserMapping.objects.get(uuid=uuid).user
    elif username:
        assert isinstance(username, str), "Seriously?"
        return get_user_model().objects.get(username__iexact=username.lower())

def get_user_by_email(uid=None):
    """ This helper just gets a user instance. """
    assert bool(uid), "Need uuid OR username"
    assert isinstance(uid, str), "user email should be str"
    try:
        return models.UserMapping.objects.get(uid=uid).user
    except ObjectDoesNotExist:
        print(f"User with {uid} doesn't yet exist.")
        return None

@transaction.atomic()
def get_or_create_user(uid, email):
    """
    Returns a user.
    Should be able to process a bunch of weird cases like changed username or duplicate usernames.
    If it dies with an exception, with 90% probability the data state is very bad.
    """

    def free_up_username(username):
        try:
            other_user = get_user(username=username)
        except get_user_model().DoesNotExist:
            return
        new_username = "{0}_OLD_{1}".format(other_user.username, uuid4())
        logger.warning("Found another user with username {old_username}. Renaming {old_username} to {new_username}".format(old_username=other_user.username, new_username=new_username))
        other_user.username = new_username
        other_user.save()

    def get_existing_user(uid):
        try:
            user = get_user_by_email(uid)
            return user
        except models.UserMapping.DoesNotExist:
            return None
        
    def create_user(email):
        #_validate_username(username)
        #free_up_username(username)
        user = get_user_model().objects.create(email=email, is_staff=False)
        user.set_unusable_password()
        user.save()
        models.UserMapping.objects.create(user=user, uid=uid)
        logger.info("Created user with email: {email}".format(**locals()))
        return user

    # prepare
    #if isinstance(uuid, str):
    #    uuid = UUID(uuid)
    #assert isinstance(uuid, UUID) and isinstance(username, str)
    #username = username.lower()
    # get or create
    #user = get_existing_user(uuid, username) or create_user(uuid, username)
    user = get_existing_user(uid) or create_user(email)
    # just in case, ensure the user object complies with the security rules
    cleanup_direct_permissions(user)
    return user


def update_user_data(user, first_name="",last_name="", email=None, shibboleth_connector_identifier=""):
    """ Updates User instance with data received from SSO. """
    assert isinstance(user, get_user_model())
    user.first_name = first_name
    user.last_name = last_name
    user.shibboleth_connector_identifier = shibboleth_connector_identifier
    user.email = email or ""
    user.save()


def _groups_from_group_names(group_names):
    """ This default group resolver returns list of Group objects based on group names """
    groups = set(Group.objects.filter(name__in=group_names))
    # log missing groups, might help to understand what is going on
    found_group_names = {g.name for g in groups}
    missing_group_names = set(group_names) - found_group_names
    if missing_group_names:
        n = len(missing_group_names)
        logger.info("Received {n} names of nonexistent groups: {missing_group_names}".format(**locals()))
    # done
    return groups


def set_user_groups(user, group_names):
    if hsh_compat.HSH_APP_COMPAT_POSSIBLE:
        logger.info("Resolving groups names for {user} directly from hsh app.".format(user=user))
        group_names = hsh_compat.get_group_names_for_user(user)
    groups = set(_groups_from_group_names(group_names))
    # update user groups
    if set(user.groups.all()) != groups:
        user.groups.set(groups)
        assert set(user.groups.all()) == groups
        logger.info("Groups for {user} were updated.".format(user=user))
    # done
    logger.info("User {user} is member of: {groups}".format(user=user, groups=[str(g) for g in groups]))


def cleanup_direct_permissions(user):
    if user.user_permissions.exists():
        logger.critical("Who attached permissions directly to {user} ?!?!".format(**locals()))
        user.user_permissions.clear()


def get_superuser_groups():
    """ :return: QuerySet with all groups that have superuser permission """
    return Group.objects.filter(permissions__codename=SUPERUSER_PERM_CODENAME)


def update_user_compat_flags(user, check_login_perm=True):

    def get_full_perm_name(codename):
        return "{app}.{codename}".format(
            app=get_user_model()._meta.app_label,
            codename=codename,
        )
    # defaults are restrictive
    user.is_staff = False
    user.is_superuser = False
    # is_active must be initially set to True, otherwise has_perm() will not function (e.g. for is_staff)
    user.is_active = True
    # is_superuser (carefully resolving using groups, not using has_perm because checks is_superuser internally)
    user_groups = user.groups.all()
    su_groups = get_superuser_groups()
    if bool(set(user_groups) & set(su_groups)):
        logger.info("User {0} is superuser (has all permissions).".format(user))
        user.is_superuser = True
    # is_staff
    if user.has_perm(get_full_perm_name(STAFF_PERM_CODENAME)):
        logger.info("User {0} is staff (has admin access).".format(user))
        user.is_staff = True
    # is_active (actually represents the log in permission)
    if app_settings.LOGIN_PERM_CODENAME and check_login_perm:
        if user.has_perm(get_full_perm_name(app_settings.LOGIN_PERM_CODENAME)):
            logger.info("User {0} is active.".format(user))
            user.is_active = True
        else:
            logger.info("User {0} is inactive - does not have login permission {1}.".format(user, app_settings.LOGIN_PERM_CODENAME))
            user.is_active = False
    else:
        user.is_active = True  # without LOGIN_PERM_CODENAME every SSO user is considered active
    # done
    user.save()
