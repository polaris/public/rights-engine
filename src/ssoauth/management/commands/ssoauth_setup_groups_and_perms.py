from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Group, Permission
from ... import app_settings
from ... import logger
from ... import SUPERUSER_PERM_CODENAME, STAFF_PERM_CODENAME


def get_user_content_type():
    return ContentType.objects.get_for_model(get_user_model())


def get_or_create_permission(codename, create_with_name=None):
    """
    :return: permission object
    Ensures the permissions exists. Creates it if needed.
    """
    # the intended content type of the existing permission is not known
    # therefore there might me multiple permissions with this codename
    matching_perms = list(Permission.objects.filter(codename=codename))
    if len(matching_perms) == 0:
        perm = Permission.objects.create(
            codename=codename,
            name=create_with_name or codename,
            content_type=get_user_content_type()  # user model as fallback, also it fits perfectly for staff/superuser
        )
        logger.info("Created permission: {0}".format(perm))
        return perm
    elif len(matching_perms) == 1:
        return matching_perms[0]
    else:
        logger.warning("There are multiple permissions with codename {0}, picking the most recent one".format(codename))
        logger.warning("You probably need to cleanup the permissions for your project.")
        return matching_perms[-1]


def get_or_create_group(name):
    group, created = Group.objects.get_or_create(name=name)
    if created:
        logger.info("Created group: {0}".format(group))
    return group


def setup_predefined_groups():
    for group_name, perm_codenames in app_settings.PREDEFINED_GROUPS.items():
        group = get_or_create_group(group_name)
        for perm_codename in perm_codenames:
            perm = get_or_create_permission(perm_codename)
            if perm not in group.permissions.all():
                group.permissions.add(perm)
                logger.info("Added permission {0} to group {1}".format(perm, group))


class Command(BaseCommand):
    help = """
    Set up groups and permissions (both predefined and compatibility).
    Runs automatically after migrations.
    If running it manually (better don't), ensure that all referenced permissions are created in prior.
    """
    requires_migrations_checks = True

    def handle(self, *args, **options):
        try:
            # compat permissions
            for codename in [SUPERUSER_PERM_CODENAME, STAFF_PERM_CODENAME]:
                get_or_create_permission(codename, "{0} (SSO)".format(codename))
            # predefined groups
            setup_predefined_groups()
        except Exception as e:
            raise CommandError("Could not set up groups and permissions: {0}".format(e))

