import os
from datetime import timedelta

from django.conf import settings as django_settings

"""
Changing settings:
  - put the setting it into your django project settings
  - optionally prefix the setting with "SSOAUTH_"
"""


"""
Settings you might want to change:
"""

# host and port, not what Django thinks, but what nginx serves
SP_HOST = "localhost"  # e.g. "141.71.foo.bar", for development can use "localhost" and set FORCE_ENTITY_ID
SP_PORT = 443
SP_SSL = True

IDP_META_URL = None  # e.g. "https://idp-test.hs-hannover.de/idp/shibboleth"
IDP_LOGOUT_URL = None  # e.g. "https://idp-test.it.hs-hannover.de/idp/profile/Logout"

SP_KEY = "{project_settings}/cert/sp.key"
SP_CERT = "{project_settings}/cert/sp.pem"

SSO_REQUIRED_IN_DEBUG = False
SSO_REQUIRED_IN_PRODUCTION = False  # disabled because of e.g. collectstatic on the static server
SSO_REQUIRED_OUTSIDE_MANAGE_PY = True  # enabled to ensure that production (that uses WSGI) has SSO

SP_SLS_ENABLED = False  # single log out creates too many problems, so it is disabled for now
SP_SLS_X_FRAME_OPTIONS = None  # in case you encounter problems with SLS view not allowed inside of an iframe, e.g. "ALLOW-FROM idp-test.it.hs-hannover.de idp.hs-hannover.de"

LOGIN_PERM_CODENAME = None  # None or str; value "can_log_in" will require this permission for users to log in

PREDEFINED_GROUPS = {
    # Predefined groups and the corresponding permissions are here.
    # Both groups and permissions are created/updated automatically after applying migrations.
    # First, permissions are created:
    #   - django.contrib.auth is responsible for handling vanilla permissions (mostly model permissions).
    #   - All other explicitly assigned to groups permissions are automatically created.
    # Second, groups are created and/or updated.
    #
    # !IMPORTANT! Group naming:
    # Give your local groups the same name as the AuthGroup they will be mapped to
    # (e.g. your local group for students will be named IDM_Studierende)
    #
    # {"example_group": ["perm_codename", "another_perm_codename"]}
    # {"superusers": [ssoauth.SUPERUSER_PERM_CODENAME]}
}

# if last login has been long ago then users are cleaned up
CLEANUP_DEACTIVATE_AFTER = timedelta(days=7)  # people are getting suspicious because of the old users that still seem active according to django
CLEANUP_DELETE_USER_AFTER = timedelta(days=180)


"""
Settings you might want to change on development (don't change them for production):
"""

SP_FORCE_ENTITY_ID = None  # do NOT set for production, set to some unique string on development if SSO is enabled


"""
Settings you DON'T want to change (in fact, you want to avoid even thinking about them):
"""

# if you really really need to add/modify something in OneLogin settings, add it to ONELOGIN_OVERRIDES
ONELOGIN_OVERRIDES = {"strict": False}  # e.g.: ONELOGIN_OVERRIDES = { "strict": False }

SP_METADATA_LIFETIME_DAYS = 365 * 20

PROJECT_NAME = os.environ.get('DJANGO_SETTINGS_MODULE').split('.')[0]

PRETEND_AUTH_BACKEND = django_settings.AUTHENTICATION_BACKENDS[0]  # pretend to be this backend; django does not expect that it is possible to log in without an authentication backend

# the block below defines from which SAML2 attributes this SP receives user data
ATTRIBUTE_USERNAME = "urn:oid:0.9.2342.19200300.100.1.1"  # "uid"
ATTRIBUTE_EMAIL = "urn:oid:0.9.2342.19200300.100.1.3"  # "mail"
ATTRIBUTE_FORENAME = "urn:oid:2.16.840.1.113730.3.1.241"  # "givenName"
ATTRIBUTE_SURNAME = "urn:oid:2.16.840.1.113730.3.1.241"  # "sn"
ATTRIBUTE_DISPLAY_NAME = "urn:oid:2.16.840.1.113730.3.1.241" # displayName
ATTRIBUTE_PAIRWISE_ID ="urn:oasis:names:tc:SAML:attribute:pairwise-id" # samlPairwiseID
ATTRIBUTE_ACCOUNT_UUID = None  # custom stuff, this one has no OID
ATTRIBUTE_GROUPS = "urn:oid:1.3.6.1.4.1.5923.1.5.1.1"  # "isMemberOf"

GROUP_RESOLVER = None  # deprecated

"""
This information will be published in this SP metadata...
"""

SP_CONTACTS = {
    "technical": {
        "givenName": "Service Desk",
        "emailAddress": "mailto:support@digitallearning.gmbh",
    },
    "support": {
        "givenName": "Service Desk",
        "emailAddress": "mailto:support@digitallearning.gmbh",
    },
    "administrative": {
        "givenName": "Service Desk",
        "emailAddress": "mailto:support@digitallearning.gmbh",
    },
}

SP_ORGANIZATION = {
    "de": {
        "name": "Polaris Rights Engine",
        "displayname": "Polaris Rights Engine",
        "url": "https://www.digitallearning.gmbh",
    },
    "en": {
        "name": "Polaris Rights Engine",
        "displayname": "Polaris Rights Engine",
        "url": "https://www.digitallearning.gmbh",
    },
}

