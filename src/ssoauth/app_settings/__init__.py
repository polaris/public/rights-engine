import sys
from datetime import datetime, timedelta

from django import conf
from onelogin.saml2 import settings as onelogin_settings
from onelogin.saml2.constants import \
    OneLogin_Saml2_Constants as onelogin_constants

from .defaults import *

# merge defaults with customized user settings

for setting_name in [k for k in globals().keys() if k.isupper()]:
    for name in [setting_name, "SSOAUTH_" + setting_name]:
        try:
            globals()[setting_name] = getattr(conf.settings, name)
        except (KeyError, AttributeError):
            pass  # not set


# template for OneLogin toolkit settings

_SET_ON_RUNTIME = None and "will be set on runtime"

ONELOGIN_SETTINGS_TEMPLATE = {
    # Don't change it (you cannot in fact)
    # If you really need to adjust something here, please use ONELOGIN_OVERRIDES instead
    "strict": True,
    "debug": conf.settings.DEBUG,
    "sp": {
        "entityId": _SET_ON_RUNTIME or str(),
        "assertionConsumerService": {
            "url":  _SET_ON_RUNTIME or str(),
            "binding": onelogin_settings.OneLogin_Saml2_Constants.BINDING_HTTP_POST,
        },
        "singleLogoutService": {
            "url":  _SET_ON_RUNTIME or str(),
            "binding": onelogin_settings.OneLogin_Saml2_Constants.BINDING_HTTP_REDIRECT,
        },
        "x509cert": _SET_ON_RUNTIME,
        "privateKey": _SET_ON_RUNTIME,
        "NameIDFormat": onelogin_constants.NAMEID_TRANSIENT,
    },
    "idp": {
        "entityId": IDP_META_URL,
        "x509certMulti": {
            "signing": _SET_ON_RUNTIME or dict(),
            "encryption": _SET_ON_RUNTIME or dict(),
        },
        "singleSignOnService": {
            "url": _SET_ON_RUNTIME or str(),
            "binding": onelogin_settings.OneLogin_Saml2_Constants.BINDING_HTTP_REDIRECT,
        },
        "singleLogoutService": {
            # note: as for now, SP-initiated SLO is not supported by this app
            "url": _SET_ON_RUNTIME or str(),
            "binding": onelogin_settings.OneLogin_Saml2_Constants.BINDING_HTTP_REDIRECT,
        }
    },
    "security": {
        "nameIdEncrypted": False,
        "authnRequestsSigned": True,
        "logoutRequestSigned": True,
        "logoutResponseSigned": False,
        "signMetadata": True,
        "wantMessagesSigned": True,
        "wantAssertionsSigned": True,
        "wantNameId": False,  # set to True for SLO support (Single Log Out)
        "wantNameIdEncrypted": False,
        "wantAssertionsEncrypted": True,
        "signatureAlgorithm": "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512",
        "metadataCacheDuration": "P{n}D".format(n=SP_METADATA_LIFETIME_DAYS),
        "metadataValidUntil": (datetime.now() + timedelta(days=SP_METADATA_LIFETIME_DAYS)).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    },
    "contactPerson": SP_CONTACTS,
    "organization": SP_ORGANIZATION,
}

ONELOGIN_SETTINGS_TEMPLATE.update(ONELOGIN_OVERRIDES)

ONELOGIN_SETTINGS_OBJECT = _SET_ON_RUNTIME

# helpers

def sso_is_configured():
    return bool(ONELOGIN_SETTINGS_OBJECT)


SSO_REQUIRED = any([
    SSO_REQUIRED_IN_DEBUG and conf.settings.DEBUG,
    SSO_REQUIRED_IN_PRODUCTION and not conf.settings.DEBUG,
    SSO_REQUIRED_OUTSIDE_MANAGE_PY and not any("manage.py" in arg.lower() for arg in sys.argv),
])

