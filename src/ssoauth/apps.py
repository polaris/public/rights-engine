from django.apps import AppConfig
from django.contrib.auth import get_user_model
from django.contrib.auth.management import create_permissions
from django.core import management
from django.core.management import call_command
from django.db.models import Q
from django.db.models.signals import post_migrate
from django.db.utils import OperationalError, ProgrammingError
from django.utils import timezone

from backend import settings

from . import \
    checks  # As for Django 1.11 it still doesn't auto-import checks >.<
from . import app_settings, logger, sso_utils


class SSOAuthConfig(AppConfig):
    name = "ssoauth"

    def ready(self, *args, **kwargs):
        if not checks:
            raise RuntimeError("Importing/running checks would be nice...")
        super().ready(*args, **kwargs)
        # OneLogin settings stuff
        try:
            app_settings.ONELOGIN_SETTINGS_OBJECT = sso_utils.create_onelogin_settings()
        except Exception as e:
            msg = "SSO failed to start. {ec}: {e}".format(ec=e.__class__.__name__, e=str(e),)
            if app_settings.SSO_REQUIRED:
                raise RuntimeError(msg)
            else:
                logger.warning(msg)
        # default groups
        post_migrate.connect(self.post_migrate_callback, sender=self)
        # cleanup
        warnings = list()
        try:
            self.cleanup_users()
        except (OperationalError, ProgrammingError,) as e:
            warnings.append(Warning("ssoauth could not cleanup users. Not migrated yet?"))
        try:
            self.cleanup_sessions()
        except Exception as e:
            warnings.append(Warning("ssoauth could not cleanup sessions. {}".format(e)))
        return warnings or None

    @staticmethod
    def post_migrate_callback(*args, **kwargs):
        # first, let django create its own permissions
        create_permissions(*args, **kwargs)  # calling create_permissions() before using the permissions
        # custom and compatibility groups and permissions
        management.call_command("ssoauth_setup_groups_and_perms")

    def cleanup_users(self):
        # for user in get_user_model().objects.filter(Q(last_login__isnull=True) | Q(last_login__lte=timezone.now() - app_settings.CLEANUP_DELETE_USER_AFTER)):
        #     logger.info("Deleting inactive user: {0}".format(user))
        #     user.delete()
        # for user in get_user_model().objects.filter(last_login__lte=timezone.now() - app_settings.CLEANUP_DEACTIVATE_AFTER).filter(Q(is_active=True) | Q(is_superuser=True) | Q(is_staff=True)):
        #     logger.info("Deactivating user: {0}".format(user))
        #     user.is_active = False
        #     user.is_superuser = False
        #     user.is_staff = False
        #     user.save()
        return 

    def cleanup_sessions(self):
        call_command("clearsessions")

