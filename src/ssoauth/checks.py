from django import conf, urls
from django.apps import apps
from django.contrib.auth import get_user_model
from django.core.checks import Error, Tags, Warning, register
from django.db.utils import OperationalError, ProgrammingError

from . import SUPERUSER_PERM_CODENAME, app_settings


def _get_abstract_user():
    """ Cannot import when loading the module. """
    from django.contrib.auth.models import AbstractUser
    return AbstractUser


def _ignore_db_errors(function):
    """ Needed for checks that read data. """
    def wrapper(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except (OperationalError, ProgrammingError,) as e:
            return [Warning("Could not perform a check. Not migrated yet?", obj=function.__name__)]
    return wrapper


@register(Tags.security)
@_ignore_db_errors
def no_passwords_stored(app_configs, **kwargs):
    errors = list()
    user_model = get_user_model()
    for user in user_model.objects.all():
        if user.has_usable_password():
            #user.set_unusable_password()
            #user.save()
            #errors.append(Warning("User \"{0}\" had usable password. Automatically fixed.".format(user), obj=user_model))
            errors.append(Warning("User \"{0}\" has usable password".format(user), obj=user_model))
    return errors


@register(Tags.security)
@_ignore_db_errors
def not_too_many_superuser_groups(app_configs, **kwargs):
    errors = list()
    group_model = apps.get_model("auth", "Group")
    su_groups = group_model.objects.filter(permissions__codename=SUPERUSER_PERM_CODENAME)
    if len(su_groups) > 1:
        errors.append(Error("There are too many superuser groups: {0}".format(su_groups)))
        for g in su_groups:
            g.permissions.clear()
            errors.append(Error("Purged permissions of group {0}".format(g), obj=group_model))
    return errors


@register(Tags.compatibility)
@_ignore_db_errors
def deprecated_su_group_name(app_configs, **kwargs):
    errors = list()
    if hasattr(conf.settings, "SSOAUTH_SUPERUSER_GROUP_NAME") or hasattr(conf.settings, "SUPERUSER_GROUP_NAME"):
        errors.append(Warning("[SSOAUTH_]SUPERUSER_GROUP_NAME is deprecated and will be ignored. Create your own superuser group using [SSOAUTH_]PREDEFINED_GROUPS.", obj=conf.settings))
    return errors


@register(Tags.compatibility)
def compatible_user_model(app_configs, **kwargs):
    errors = list()
    if not issubclass(get_user_model(), _get_abstract_user()):
        errors.append(Error(
            "{} is probably incompatible with ssoauth.".format(get_user_model()),
            obj=get_user_model(),
        ))
    return errors


@register(Tags.compatibility)
def host_without_protocol(app_configs, **kwargs):
    errors = list()
    if app_settings.SP_HOST is not None:
        if app_settings.SP_HOST.lower().startswith(("http:", "https:",)):
            errors.append(Error("Bad setting SP_HOST. Need host name without protocol and port.", obj=conf.settings))
    return errors


@register(Tags.compatibility)
def old_settings(app_configs, **kwargs):
    def settings_have(key):
        try:
            getattr(conf.settings, key)
            return True
        except AttributeError:
            return False
    errors = list()
    if settings_have("SSO_DISABLED") or settings_have("SSOAUTH_SSO_DISABLED"):
        errors.append(Warning("SSO_DISABLED is deprecated.", obj=conf.settings))
    if settings_have("IDP_IGNORE") or settings_have("SSOAUTH_IDP_IGNORE"):
        errors.append(Warning("IDP_IGNORE is deprecated.", obj=conf.settings))
    if errors:
        errors.append(Warning("Please check the README. Alternatively simply remove *all* ssoauth settings from your project settings, "
                      "you don't need them anymore as long as you don't need a working SSO.", obj=conf.settings))
    return errors


@register(Tags.compatibility)
def sp_host_is_not_localhost(app_configs, **kwargs):
    errors = list()
    if app_settings.SP_HOST:
        if app_settings.SP_HOST.lower() in ("localhost", "127.0.0.1", "::1",):
            if app_settings.SSO_REQUIRED:
                errors.append(Error("SP_HOST should not be set to localhost: it causes entityID collisions, breaks SLO and does all sorts of nasty stuff.", obj=conf.settings))
    return errors


@register(Tags.security)
def session_lifetime(app_configs, **kwargs):
    errors = list()
    max_wanted = 10 * 60 * 60  # in seconds
    if conf.settings.SESSION_COOKIE_AGE > max_wanted or conf.settings.SESSION_COOKIE_AGE == 0:
        errors.append(Error(
            "Please reduce SESSION_COOKIE_AGE to at most {max_wanted}".format(**locals()),
            obj=conf.settings,
        ))
    if not conf.settings.SESSION_EXPIRE_AT_BROWSER_CLOSE:
        errors.append(Warning(
            "Recommended value for SESSION_EXPIRE_AT_BROWSER_CLOSE = True",
            obj=conf.settings,
        ))
    return errors


@register(Tags.compatibility)
def production_entity_id(app_configs, **kwargs):
    errors = list()
    if not conf.settings.DEBUG and app_settings.SP_FORCE_ENTITY_ID:
        errors.append(Warning(
            "You set SP_FORCE_ENTITY_ID. Should better avoid doing so on production.",
            obj=conf.settings,
        ))
    return errors


@register(Tags.compatibility)
def group_resolver_deprecated(app_configs, **kwargs):
    errors = list()
    if app_settings.GROUP_RESOLVER is not None:
        errors.append(Warning(
            "GROUP_RESOLVER is deprecated. If hsh app is present, groups are resolved from it automatically instead of the received SAML group names.",
            obj=conf.settings,
        ))
    return errors

