import urllib.request
from xml.etree import ElementTree
from lxml import etree
from . import logger
from . import app_settings
from django import urls
from onelogin.saml2.settings import OneLogin_Saml2_Settings
from copy import copy
import os
from django import conf
from pathlib import Path


def get_project_settings_path():
    module = os.environ.get(conf.ENVIRONMENT_VARIABLE)
    path = os.path.abspath(os.path.join(*module.split(".")))
    if not os.path.isdir(path):
        # module, not a package
        path = os.path.dirname(path)
    return path


def read_key(path):
    path = path.format(project_settings=get_project_settings_path())
    try:
        with open(path, "r") as f:
            return f.read()
    except FileNotFoundError:
        raise FileNotFoundError("Could not read the key: {0}".format(path))


def get_idp_runtime_info(meta_url):
    """ Grab data directly from the running IDP instance """
    # fetch the meta
    BASE_DIR = Path(__file__).resolve().parent.parent
    # parse the xml
    try:
        parser1 = etree.XMLParser(encoding="utf-8", recover=True)
        idp_meta = ElementTree.parse(os.path.join(BASE_DIR,'static/shibboleth.xml'))
    except Exception as e:
        logger.error("Could not parse IDP meta.")
        raise e
    # namespaces
    namespaces = {
        "ds": "http://www.w3.org/2000/09/xmldsig#",
        "saml2": "urn:oasis:names:tc:SAML:2.0:metadata",
    }
    for prefix, uri in namespaces.items():
        ElementTree.register_namespace(prefix, uri)
    # get the required data
    signing = idp_meta.findall(".//ds:X509Certificate", namespaces)
    encryption = idp_meta.findall(".//ds:X509Certificate", namespaces)
    bindings_sso_redirect = idp_meta.findall(".//saml2:SingleSignOnService[@Binding='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect']", namespaces)
    bindings_slo_redirect = idp_meta.findall(".//saml2:SingleLogoutService[@Binding='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect']", namespaces)
    # log and check
    logger.error("From the IDP metadata received {s} signing certs, {e} encryption certs, {sso} SSO bindings, {slo} SLO bindings.".format(
        s=len(signing), e=len(encryption), sso=len(bindings_sso_redirect), slo=len(bindings_slo_redirect),
    ))
    if not (signing and encryption):
        raise RuntimeError("Could not find certificates in IDP meta.")
    if bindings_sso_redirect is None:
        raise RuntimeError("Could not find SSO HTTP-Redirect binding.")

    # pack the received data
    return {
        "certificates": {
            "signing": [e.text for e in signing],
            "encryption": [e.text for e in encryption]
        },
        "bindings": {
            "sso_redirect": bindings_sso_redirect[0].get("Location"),
            "slo_redirect": bindings_slo_redirect[0].get("Location") if bindings_slo_redirect else None,
        }
    }


def create_onelogin_settings(template=app_settings.ONELOGIN_SETTINGS_TEMPLATE):
    """ This function is intended to be run only once, on app startup. Raises exceptions. """
    # get the template
    settings = copy(template)
    # prepare some values
    if not app_settings.SP_HOST:
        raise ValueError("SP_HOST is not set")  # before OneLogin toolkit chokes with "sp_acs_url_invalid,sp_sls_url_invalid"
    host_full = "{protocol}://{host}{port_suffix}".format(
        host=app_settings.SP_HOST,
        protocol="https" if app_settings.SP_SSL else "http",
        port_suffix="" if app_settings.SP_PORT in [80, 443] else ":{0}".format(app_settings.SP_PORT)
    )
    # IDP settings
    if not app_settings.IDP_META_URL:
        raise ValueError("IDP_META_URL is not set")  # before get_idp_runtime_info starts logging errors
    idp_info = get_idp_runtime_info(app_settings.IDP_META_URL)
    settings["idp"]["x509certMulti"]["signing"] = idp_info["certificates"]["signing"]
    settings["idp"]["x509certMulti"]["encryption"] = idp_info["certificates"]["encryption"]
    settings["idp"]["singleSignOnService"]["url"] = idp_info["bindings"]["sso_redirect"]
    settings["idp"]["singleLogoutService"]["url"] = idp_info["bindings"]["slo_redirect"]
    # SP settings
    settings["sp"]["entityId"] = app_settings.SP_FORCE_ENTITY_ID or (host_full + urls.reverse("sso-saml2-meta"))
    settings["sp"]["assertionConsumerService"]["url"] = host_full + urls.reverse("sso-saml2-acs")
    if app_settings.SP_SLS_ENABLED:
        settings["sp"]["singleLogoutService"]["url"] = host_full + urls.reverse("sso-saml2-sls")
    else:
        del settings["sp"]["singleLogoutService"]
    settings["sp"]["x509cert"] = read_key(app_settings.SP_CERT)
    settings["sp"]["privateKey"] = read_key(app_settings.SP_KEY)
    # done
    return OneLogin_Saml2_Settings(settings=settings, sp_validation_only=True)

