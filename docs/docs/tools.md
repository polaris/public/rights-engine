## Tools

### XAPI Statements Generator

Helper tool that randomly generates xAPI statements or importes xAPI statements form a JSON file. The tranformed xAPI statements are sent to an xAPI endpoint (LRS/Rights Engine).

## Usage
 
Import JSON file
```console
$ python generator.py -a <TOKEN> -i stupla_x_api_statements.json -t http://localhost:8003/xapi/statements -o
```

Start random xAPI statement generation
```console
$ cp provider_config.json.example provider_config.json
```

Customize provider configuration and provide tokens

```console
$ python generator.py -t http://localhost:8003/xapi/statements -r
```

## Application Tokens

Application tokens are accessible for privileged polaris users on the polaris website and limited to a single provider. Without a valid token, requests are rejected by the polaris backend.