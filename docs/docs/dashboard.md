# Dashboard

Polaris provides an example dashboard [project](https://git.rwth-aachen.de/polaris/entwicklung/dashboard-example).

## Install

Please check your installed node version (`node -v`) and make sure that the version is at least `v19.6.0`.

```console
$ npm install
```

## Development server

```console
$ npm run dev
```

Visit [http://localhost:8005](http://localhost:8005)

## Visualization Token

Creating different visualization tokens might be helpful while developing a dashboard.

Request

```console
curl -X POST http://[RIGHTS_ENGINE_URL]/api/v1/provider/visualization-tokens/create --data '{"engines": ["count_h5p_statements", "count_moodle_statements"]}' -H "Content-Type: application/json" -H "Authorization: Basic [APPLICATION_TOKEN]"
```

Reponse

```js
{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJjb250ZXh0X2lkIjpudWxsLCJleHAiOjE2NzcxNDUzODIsImVuZ2luZXMiOlsxLDJdLCJlbmdpbmVzX3dpdGhfY29udGV4dCI6W119.pZB5tUAJf12OJpXikJuTZkex8b5XlyXTjDIhbJUAHhg-y-ozH1aJ_rLbleycCOCuMQ5Ui2Mg-MWg0P6bWYHnzeqT49YCMd2LdH6zxrjl91_G9gFLvYxX5aIEWYqOeh9wIjBh9a4jOKgK3-Y0mm5cn3YXmJJ_vB3YHnl9Eu-eN4Yf444UIs1eMU1gRzhxi29PEAbmoujQ6lGyDAn7rp7vIz9LmxH29OHPv6H7L9AT-0Cbg97GX58PU9rOqEj9ngEEO7GkzmcaqnEMX1xm7rRalJfbdXBiexqhy8kSVt5ndHHNj3ZOl647hTL80BcLUiL-SypwcLT3RF1OucnJa5BD_YCSdLts45hUgssYGgW1L_GDQUiLde3uLGcjRfO0lES36p94pSuw5awz8yS5ru7-qKs5c89MX1WNLIDJBUB5zAv1yRISaUcSwC1ImPQvGBJlLuE-ZfNp1_s3HUxpXM00jRe7_UuILGtO52BAo7aeWFFdkJQsJCzRlDmU2pRF50J4t_RMoYvN-_Q-OsL3TVyVZk3LNzxh4rRHJ6aHJ3v0H9RLO-s87YRg6OmMxZc_3bHsI52EdvJhsJMxetnKOV1MFNax7YFj1kSzVnPJivLdVmtrZTOPLGbG3Tzn-DOu3I4cA5dKBoGjH0DlN1Fu7cr9_iPYtPWzc8LfmYwwOcoe6pk"}
```

Details on how visualization tokens work can be found [here](./rights_engine.md#visualization-token)

## Architecture

![Dashboard Architecture](./img/polaris_dashboard_sdk.drawio.png)

## Building a custom Dashboard

The Polaris project provides an [example project](https://git.rwth-aachen.de/polaris/entwicklung/dashboard-example), which implements a dashboard using the `dashboard-sdk`.

### Configuration

Fundamentally, the generation of visualization tokens should be implemented first. The example project uses a hardcoded visualization token. Additionally, the URL under which the Rights Engine is accessible should be configured (`dashboard-example/src/js/app.js`).

### Requesting analytics results

The `dashboard-sdk` provides a `getResult` function, that helps fetching analtytics engine results.

```js
import { getResult } from "@polaris/dashboard-sdk";
```

```js
getResult(TOKEN, URL).then((data) => {
    // data contains analytics engine results object
}
```

Example analytics engine result object

```js
{
  "count_h5p_statements": {
    "latest_result": [
      {
        "column1": 1678281540,
        "column2": 648
      }
    ],
    "description": {
      "de": "Diese Analyse summiert sämtliche gespeicherten xAPI Statements, die im Primären-LRS gespeichert sind",
      "en": "This analysis sums all stored xAPI statements stored in the primary LRS"
    },
    "created_at": "2023-03-08T13:19:00.514000",
    "engine_name": "count_h5p_statements"
  },
  "count_moodle_statements": {
    "latest_result": [
      {
        "column1": 1678281480,
        "column2": 802
      }
    ],
    "description": {
      "de": "Diese Analyse summiert sämtliche gespeicherten xAPI Statements, die im Primären-LRS gespeichert sind",
      "en": "This analysis sums all stored xAPI statements stored in the primary LRS"
    },
    "created_at": "2023-03-08T13:18:00.562000",
    "engine_name": "count_moodle_statements"
  }
}
```

The object keys match analytics engine names. Since analytics engine are designed to run at scheduled intervals, the results object contains only the latest result.

### Dashboard grid

The dashboard grid structure is described using objects. Each object should contain grid position values `x` and `y`, size values `w` and `h` as well as a `widgetId`. The `widgetId` is used to associate analytics engine results with a specific widget on the grid. Nested grids can be described using a `subGrid` object. The example project dashboard grid uses the following grid, illustrating regular widgets and a nested grid.

```js
const subGrid = [
  { x: 0, y: 0, w: 4, h: 4, widgetId: "second-widget" },
  { x: 4, y: 0, w: 4, h: 4, widgetId: "second-widget" },
];

/**
 * Setup initial widget position and sizes
 */
const widgets_config = [
  {
    x: 4,
    y: 0,
    w: 4,
    h: 8,
    widgetId: "second-widget",
  },
  {
    x: 4,
    y: 0,
    w: 4,
    h: 4,
    widgetId: "fourth-widget",
  },
  {
    x: 8,
    y: 4,
    w: 4,
    h: 8,
    widgetId: "sixth-widget",
  },
  {
    x: 4,
    y: 8,
    w: 4,
    h: 8,
    widgetId: "seventh-widget",
  },
  {
    x: 8,
    y: 8,
    w: 4,
    h: 8,
    widgetId: "eigth-widget",
  },
  {
    x: 8,
    y: 12,
    w: 4,
    h: 8,
    widgetId: "ninth-widget",
  },
  {
    x: 0,
    y: 12,
    w: 4,
    h: 8,
    widgetId: "tenth-widget",
  },
  {
    x: 0,
    y: 12,
    w: 4,
    h: 8,
    widgetId: "eleventh-widget",
  },
  { x: 0, y: 0, w: 8, h: 4, subGrid: { children: subGrid, id: "sub1_grid", class: "subgrid" } },
];
```

### Associating analytics results with widgets

```js
const widgets = {
  "second-widget": new BarChartWidget(
    "Statements H5P",
    data["count_h5p_statements"]?.description,
    data["count_h5p_statements"]?.latest_result,
    {
      xAxisLabel: "Datum",
      yAxisLabel: "#Statements",
      transform: (d) => ({
        column1: new Date(d.column1 * 1000),
        column2: d.column2,
      }),
      onShowDesc,
    }
  ),
  "fourth-widget": new PieChartWidget(
    "Persönliche Statement Verteilung",
    data["h5p_statements_distribution"]?.description,
    data["h5p_statements_distribution"]?.latest_result,
    {
      showLegend: true,
      xAxisLabel: "Jahr",
      yAxisLabel: "#Akitivitäten",
      onShowDesc,
    }
  ),
  "sixth-widget": new LineChartWidget(
    "Semesterabschluss",
    data["collect_h5p_count_statements"]?.description,
    data["collect_h5p_count_statements"]?.latest_result,
    {
      xAxisLabel: "Monat",
      yAxisLabel: "Index",
      transform: (d) => ({
        column1: new Date(d.column1 * 1000),
        column2: d.column2,
      }),
      onShowDesc,
    }
  ),
  "seventh-widget": new CourseRatingChart(
    "Bewertungen für Kurse",
    data["random_course_rating"]?.description,
    data["random_course_rating"]?.latest_result,
    {
      xAxisLabel: "Note",
      yAxisLabel: "Kurs",
      onShowDesc,
    }
  ),
  "eigth-widget": new AreaChartWidget(
    "Statements H5P",
    data["collect_h5p_count_statements"]?.description,
    data["collect_h5p_count_statements"]?.latest_result,
    {
      xAxisLabel: "Monat",
      yAxisLabel: "Index",
      transform: (d) => ({
        column1: new Date(d.column1 * 1000),
        column2: d.column2,
      }),
      onShowDesc,
    }
  ),
  "ninth-widget": new BarChartWidget(
    "Statements Moodle",
    data["count_moodle_statements"]?.description,
    data["count_moodle_statements"]?.latest_result,
    {
      xAxisLabel: "Datum",
      yAxisLabel: "#Statements",
      transform: (d) => ({
        column1: new Date(d.column1 * 1000),
        column2: d.column2,
      }),
      onShowDesc,
    }
  ),
  "tenth-widget": new StackedBarChartWidget(
    "Statements",
    data["collect_counts_all_providers"]?.description,
    data["collect_counts_all_providers"]?.latest_result,
    ["overall"],
    {
      showLegend: true,
      xAxisLabel: "Provider",
      yAxisLabel: "#Statements",
      onShowDesc,
    }
  ),
  "eleventh-widget": new BarChartWidget(
    "Personal H5P xAPI Statements",
    data["h5p_count_user_statements"]?.description,
    data["h5p_count_user_statements"]?.latest_result,
    {
      xAxisLabel: "Datum",
      yAxisLabel: "#Statements",
      transform: (d) => ({
        column1: new Date(d.column1 * 1000),
        column2: d.column2,
      }),
      onShowDesc,
    }
  ),
};
```

```js
const grid = initGrid(widgets, widgets_confgg);
```

### Toggeling sidebar (Administration Mode)

```js
const toggleBtn = document.getElementById("toggle-sidebar-btn");
toggleBtn.onclick = grid.toggleSidebar;
```

### Widget description callback

```js
const onShowDesc = (desc) => {
  const modalContent = document.getElementById("modal-content-body");
  modalContent.innerText = desc?.de ?? "-";
  const modal = new bootstrap.Modal(document.getElementById("myModal"), {});
  modal.show();
};
```
