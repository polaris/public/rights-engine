# Deployment

## Local Deployment

### Login in the docker registry

If you are not already logged in, you need to authenticate to the Container Registry by using your GitLab username and password. If you have Two-Factor Authentication enabled, use a Personal Access Token instead of a password.

```bash
docker login registry.git.rwth-aachen.de
```

### Create Docker network

Different containers communicate via a Docker network `web`, which must be created before Docker compose files are executed.

```bash
docker network create web
```

### Create private/public keys and .env

Please change all passwords in the configuration file and leave the passphrase empty.

```bash
cd single-compose
ssh-keygen -b 4096 -f id_rsa
cp .env.sample .env
```

### Start containers

```bash
docker compose up -d
```

Please check whether all services started successfully.

```bash
docker compose ps
```

After that you should be able to visit http://localhost:8004/ and see the rights engine.

### Migrate rights-engine database (only required once after first start) and create mongo db index

```bash
docker compose exec -it rights-engine sh -c 'python3 manage.py sqlflush | sed s/TRUNCATE/DROP\ TABLE\ IF\ EXISTS/g | python3 manage.py dbshell && echo DROP\ TABLE\ IF\ EXISTS\ django_migrations\; | python3 manage.py dbshell && python3 manage.py migrate && python3 manage.py loaddata fixtures/initial_db.json && python3 manage.py create_mongo_index'
```

### Migrate analytics-engine database (only required once after first start)

```bash
docker compose exec -it scheduler sh -c 'scheduler create-db'
```

### Adding analytics engine jobs

Analytics engines jobs are configured via yml files and read from the `configuration` directory, which is a volume.

Example configuration file `configuration/h5p_engines.yml`

```yml
h5p_statements_count_engine:
  crontab: "*/1 * * * *"
  repo: "https://scheduler:glpat-MsDsrHMH-k3-DzEfNRgk@gitlab.digitallearning.gmbh/polaris/engines/dummy-engine.git"
  analytics_token: "b6a4ec069ef9f688e781161d46c2a85c14a761a4eaf0074099656c7de44a65d9"
```

Example configuration file `configuration/moodle_engines.yml`

```yml
moodle_statements_count_engine:
  crontab: "*/1 * * * *"
  repo: "https://scheduler:glpat-MsDsrHMH-k3-DzEfNRgk@gitlab.digitallearning.gmbh/polaris/engines/dummy-engine.git"
  analytics_token: "0482a0f3259c966dfddb38de867573a95995ee5e10898bb71f9ae0e99efe54ef"
```

Update analytics engine scheduler

```bash
docker compose exec -it scheduler sh -c 'scheduler read-configs'
```

### Create visualization token

```bash
curl -X POST http://localhost:8004/api/v1/provider/visualization-tokens/create --data '{"engines": ["count_h5p_statements", "count_moodle_statements"]}' -H "Content-Type: application/json"
```

Returns JWT Token for dashboard

```
{"token":[JWT_TOKEN]"}
```

### Loading JSON statements

It is recommended to import a set of sample statements into the LRS so that the analytics engines can work on this data. Furhtermore, users (user1@polaris.com and user2@polaris.com) can test the data disclousure and data deletion process.

```console
$ docker compose exec -it mongodb_container sh -c 'mongoimport --authenticationDatabase admin --username root --password CHANGE_ME --db lrs --collection statements --drop --file ./lrs_statements_dump.json'
```

### Start dashboard

1. Clone ![https://git.rwth-aachen.de/polaris/entwicklung/dashboard-example](Dashboad SDK example)
   - `cd dashboard-example`
2. Download latest `@polaris/dashboard-sdk-X.X.X.tgz` from registry and `npm install @polaris/dashboard-sdk-1.0.2.tgz` it (TODO: improve with npm login)
3. Update `TOKEN` in `dashboard-example/src/js/app.js`
4. Run `npm run dev`
5. Visit http://localhost:8005/

### (Optional) Filling DB with random statements

1. Clone rights-engine
2. Create provider config

```bash
$ cd rights-engine/tools/xapi-statement-generator
$ cp provider_config.json.example provider_config.json
```

3. Open `provider_config.json` and insert Application Tokens (visible at http://localhost:8004 (Rights Engine UI), if you login as a provider)
4. Run `python generator.py -t http://localhost:8003/xapi/statements -r`

## Remote Deployment via Gitlab CI Pipeline

It is recommended to deploy the rights engine through a Gitlab CI pipeline. The deployment repository contains a suitable pipeline that should be adapted to the deployment target.

## Remote Deployment on three VMs

Instead of using a Gitlab CI Pipeline, one can use three VMs for the Polaris deployment. Each VM is responsible one of the three Polaris components _Rights Engine_, _Analytics Engine_ and _Mongo (LRS)_.

First, for each VM, the [deployment repository](https://git.rwth-aachen.de/polaris/entwicklung/deployment) is checked out, configured, and the appropriate Docker services are started.

> Care should be taken to always check out the branch `digitallearning`.

Requirements 1. Machines must be able to access the Gitlab from Aachen, full internet access would be better 2. docker and docker compose must be installed 3. SSL certificates and domains must be available 4. Machines must be able to communicate with each other via HTTP and HTTPS

> Passwords should not contain special characters, since this often lead to problems. 

> Docker creates folder, if the corresponding volume not exists. 

### Mongo (LRS) VM setup

#### 1. Clone deployment repository
Clone the deployment repository for a quick start. 

```console
$ git clone https://git.rwth-aachen.de/polaris/entwicklung/deployment.git
```

#### 2. Configuration
Next, we update the .env file of the Mongo database. 

Settings for `MONGO_USER` and `MONGO_PASSWORT` need to match settings in `.env` for Rights Engine.

```console
$ cd mongodb
$ cp .env.sample .env
$ vi .env
```

#### 3. Create Docker network
Next, we need to create a Docker network that enables a secure network connection between the mongo database and the rights engine of Polaris.

```console
$ docker network create web
```

If you like to check if the network is created, use the following command

```console
$ docker network ls
```

#### 4. Starting Docker services
Finally, start the Mongo database with the following command

```console
$ docker compose up -d
```

Check, if the Mongo database is running:

```console
$ docker compose ps
```

### Rights Engine VM setup

#### 1. Clone deployment repository
Clone the deployment repository for a quick start. 

```console
$ git clone https://git.rwth-aachen.de/polaris/entwicklung/deployment.git
```

#### 2. Configuration

```console
$ cd rights-engine
$ cp .env.sample .env
$ vi .env
```

#### 3. Generate key pair

```console
$ ssh-keygen -b 4096 -f id_rsa
```

#### 4. Create Docker network

```console
$ docker network create web
```

#### 5. Install Traefik

See section `Installing Traefik`

#### 6. Starting Docker services

```console
$ docker compose up -d
```

#### 7. Migrate DB

```console
$ docker compose exec -it rights-engine sh -c 'python3 manage.py sqlflush | sed s/TRUNCATE/DROP\ TABLE\ IF\ EXISTS/g | python3 manage.py dbshell && echo DROP\ TABLE\ IF\ EXISTS\ django_migrations\; | python3 manage.py dbshell && python3 manage.py migrate && python3 manage.py loaddata fixtures/initial_db.json'
```

### Analytics Engine VM setup

#### 1. Clone deployment repository

```console
$ git clone https://git.rwth-aachen.de/polaris/entwicklung/deployment.git
```

#### 2. Checkout `digitallearning` branch

```console
$ git checkout digitallearning
```

#### 3. Configuration

```console
$ cd analytics-engine
$ cp .env.sample .env
$ vi .env
```

#### 4. Create Docker network

```console
$ docker network create web
```

#### 5. Install Traefik

See section `Installing Traefik`

#### 6. Starting Docker services

```console
$ docker compose up -d
```

#### 7. Migrate DB

```console
$ docker compose exec -it scheduler sh -c 'scheduler create-db'
```

## Update Docker Images

```bash
docker compose pull
```

## Installing Traefik

```console
$ mkdir traefik && cd traefik
```

```console
$ touch docker-compose.yml
```

Content `docker-compose.yml`

```yml
services:
  traefik:
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
      - "$PWD/traefik.toml:/traefik.toml"
      - "$PWD/traefik_dynamic.toml:/traefik_dynamic.toml"
      - "$PWD/acme.json:/acme.json"
      - "$PWD/access.log:/access.log"
    ports:
      - "80:80"
      - "443:443"
      - "8082:8080"
    network_mode: web
    container_name: traefik
    restart: always
    image: "traefik:v2.9"
```

```console
$ touch acme.json && touch access.log && chmod 600 acme.json
$ touch traefik_dynamic.toml
```

Content `traefik_dynamic.toml`

```toml
[http.middlewares.simpleAuth.basicAuth]
  users = [
    "admin:$apr1$SaCzhK8I$aShLLTn2wYgq3MyOQyTrG."
  ]

[http.routers.api]
  rule = "Host(`polaris.uni-bochum.de`)"
  entrypoints = ["websecure"]
  middlewares = ["simpleAuth"]
  service = "api@internal"
  [http.routers.api.tls]
    certResolver = "lets-encrypt"

[http.middlewares]
    [http.middlewares.security-headers.headers]
      # CORS
      #AccessControlAllowMethods = ["GET", "OPTIONS", "PUT"]
      #AccessControlAllowOrigin = "origin-list-or-null"
      #AccessControlMaxAge = 100
      #AddVaryHeader = true
      BrowserXssFilter = true
      ContentTypeNosniff = true
      ForceSTSHeader = true
      FrameDeny = true
      SSLRedirect = true
      STSIncludeSubdomains = true
      STSPreload = true
      #ContentSecurityPolicy = "default-src 'self' 'unsafe-inline'"
      CustomFrameOptionsValue = "SAMEORIGIN"
      #ReferrerPolicy = "same-origin"
      #FeaturePolicy = "vibrate 'self'"
      STSSeconds = 315360000
[tls.options]
  [tls.options.default]
    minVersion = "VersionTLS12"
    curvePreferences = ["CurveP521", "CurveP384"]
    sniStrict = true
    cipherSuites = [
      "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
      "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
      "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305",
      "TLS_AES_128_GCM_SHA256",
      "TLS_AES_256_GCM_SHA384",
      "TLS_CHACHA20_POLY1305_SHA256",
    ]
```

```console
$ touch traefik.toml
```

Content `traefik.toml`

- Please change __email@email.de__

```toml
[entryPoints]
  [entryPoints.web]
    address = ":80"
    [entryPoints.web.http.redirections.entryPoint]
      to = "websecure"
      scheme = "https"

  [entryPoints.websecure]
    address = ":443"

[api]
  dashboard = true

[certificatesResolvers.lets-encrypt.acme]
  email = "email@email.de"
  storage = "acme.json"
  [certificatesResolvers.lets-encrypt.acme.httpChallenge]
    entryPoint = "web"

[providers.docker]
  watch = true
  network = "web"
  exposedByDefault= "false"

[providers.file]
  filename = "traefik_dynamic.toml"

[accessLog]
  filePath = "access.log"
  format = "json"

  [accessLog.filters]
    statusCodes = ["200", "300-302"]
```

