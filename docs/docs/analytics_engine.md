# Analytics Engine

The analysis engine is used to evaluate stored XAPI statements. The results generated in this way can then be integrated into an existing application via the Dashboard SDK.

## Configuring Engines

Analytics Engines are controlled via YAML files, which are stored in the `configuration` directory. The analytics engines scheduler needs to be notified about configuration changes by running `scheduler read-configs`, which reads all existing YAML files and starts or updates analytics engines.

If you are running the analytics engine in a Docker container, you should run the following command.

```bash
$ docker compose exec -it scheduler sh -c 'scheduler read-configs'
```

## API Endpoints

### Retrieve Provider Statements

**Endpoint:**

```
POST /provider/data
```


**Description:**  
Retrieves provider statements from the LRS (Learning Record Store) based on dynamic filters provided in the request. This endpoint allows analytics engines to query statements efficiently with support for pagination and filtering. **Note:** All additional filters should be provided using standard MongoDB query syntax.

**Request Headers:**

- `Authorization`: `Basic <token>` (Required) – Authentication token.

**Request Parameters (JSON Body):**

| Parameter              | Type    | Description                                                                                                                                                          |
| ---------------------- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `last_object_id`       | String  | (Optional) The `_id` from the last retrieved document, used for pagination.                                                                                         |
| `page_size`            | Integer | (Optional) Number of statements to retrieve per request (default: system-configured).                                                                                |
| **Additional Filters** | Object  | (Optional) Any other filters should be specified as key-value pairs using standard MongoDB query syntax. For example:<br><br> - `"verb.id": {"$in": ["run", "jump"]}`<br> - `"actor.mbox": {"$regex": "^mailto"}`<br> - `"actor.tan": {"$exists": false}`<br> - `"created_at": {"$gt": "2024-01-01T00:00:00Z"}` |

**Example Request:**

```json
{
    "last_object_id": "65bfc2a...",
    "page_size": 10,
    "verb.id": {"$in": ["run", "jump"]},
    "actor.mbox": {"$regex": "^mailto"},
    "actor.tan": {"$exists": false},
    "created_at": {"$gt": "2024-01-01T00:00:00Z"}
}

**Response:**

```json
{
    "verbs": ["run", "jump"],
    "statements": [
        {
            "_id": "65bfc2a...",
            "actor": {
                "mbox": "mailto:example@example.com"
            },
            "verb": {
                "id": "run"
            },
            "object": {
                "id": "example-object"
            }
        }
    ],
    "page_size": 10
}
```


**Response Codes:**

| Status Code                 | Description                                         |
| --------------------------- | --------------------------------------------------- |
| `200 OK`                    | Successful response with filtered statements.       |
| `400 Bad Request`           | Invalid request parameters.                         |
| `401 Unauthorized`          | Authentication token is missing or invalid.         |
| `500 Internal Server Error` | Server-side error, such as missing provider schema. |



# Retrieve Analytics Result  

```
POST /provider/results
```

## Description  
This endpoint retrieves analytics results from the database based on provided filters. It supports pagination and allows filtering by various fields inside the result object, including engine name.

---

## Request Headers  
- `Authorization`: `Basic <token>` (Required) - Authentication token required to access the analytics results.

---

## Request Parameters (JSON Body)  

| Parameter       | Type     | Description                                                                 |
| -------------- | -------- | --------------------------------------------------------------------------- |
| `page`         | Integer  | (Optional) Page number for pagination (default: 1).                        |
| `page_size`    | Integer  | (Optional) Number of results per page (default: 10).                       |
| `name`         | String   | (Optional) Name of the analytics engine to filter results.                 |
| `context_id`   | String   | (Optional) Unique identifier for the result context.                       |
| `created_at`   | String   | (Optional) Filters results created on a specific date (`YYYY-MM-DD`).      |
| `result.score` | Integer  | (Optional) Filters results based on the score value inside the result object. |

---

## Example Request  

```json
{
    "page": 2,
    "page_size": 10,
    "name": "engine_xyz",
    "context_id": "context-abc123",
    "created_at": "2025-03-07",
    "result.score": 88
}
```

---

## Response  

```json
{
    "page": 2,
    "page_size": 10,
    "total_pages": 5,
    "total_results": 50,
    "results": {
        "engine_xyz": [
            {
                "created_at": "2025-03-07T14:20:30Z",
                "result": {
                    "score": 88,
                    "status": "completed"
                },
                "context_id": "context-abc123",
                "description": "Analytics result for context-abc123"
            }
        ]
    }
}
```

---

## Response Codes  

| Status Code                 | Description                                              |
| --------------------------- | -------------------------------------------------------- |
| `200 OK`                    | Successfully retrieved analytics results.               |
| `400 Bad Request`           | Invalid request parameters, such as incorrect date format. |
| `401 Unauthorized`          | Missing or invalid authentication token.               |
| `500 Internal Server Error` | An unexpected error occurred while processing request. |

