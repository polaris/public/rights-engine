# Moodle Plugin Settings

The Moodle plugin must be configured to connect to the Rights Engine.

You need to configure the URL of the Rights Engine that accepts xAPI statements and set an appropriate application token.

- LRS: `https://[RIGHTS_ENGINE_URL]/xapi/statements` (e.g. `https://polaris.ruhr-uni-bochum.de/xapi/statements`)
- Authentifizierungstoken: Login as provider to Rights Engine -> View and copy application token

## Privacy settings

The standard logginglevel needs to set to `personalized`. Otherwise, the Rights Engine can't match xAPI statements to users and their consents. 

> Admin needs to adjust settings in plugin moodle, UI setting doesn't seem to work