## Third Party Access

The rights engine allows a third party to request user consents, create user consents and update user consents.

### External user consent

The following endpoints allow a third party to implement the user consent process already present in the Rights Engine. This can be of particular interest, if you want to avoid users having to switch to the Rights Engine interface.

First, it should be noted that different provider schemas for each provider may exist. The different versions each refer to the provider schema they supersede. The newest version is therefore the one that is not superseded by any provider schema.

The endpoint `/api/v1/consents/provider-status/third-party` lists the existing provider schemas under `versions`. The provider schemas are sorted, listing the latest provider schema (`superseded_by` is null) first.

If a user hasn't yet consented to any provider schema, the user should simply be presented with the latest provider schema and asked to a consent declaration.

In the event that a user has given a consent declaration, but there have been changes to the provider schema in the meantime, the changes between the provider schema version accepted by the user and the latest one should be listed.

In cases were neither a provider schema is available or the provider hasn't yet uploaded a provider schema, the user isn't required to act.

### Get user consent

This endpoint returns the current user consent for a given user along with the provider schema the user accepted and a flag for data recording pausation.

- HTTP Method: **GET**
- URL `/api/v1/consents/user/status/user1@polaris.com/third-party`

###### URL Parameters

| Name      | Type | Required | Default |
| --------- | ---- | -------- | ------- |
| `user_id` | str  | Yes      | None    |

###### Returns

| Code  | Type | Description                                                                                  |
| ----- | ---- | -------------------------------------------------------------------------------------------- |
| `200` | dict | dict containing `user_consent`, `provider_schema` and `paused_data_recording` (boolean flag) |
| `401` | dict | invalid token                                                                                |
| `400` | dict | invalid request data e.g. `{"user_id":["Object with email=user1@polaris does not exist."]}`  |

###### Example

```console
$ curl -X GET http://[RIGHT_ENGINE_URL]/api/v1/consents/user/status/user1@polaris.com/third-party -H "Content-Type: application/json" -H "Authorization: Basic [APPLICATION_TOKEN]"
```

Response

```js
{
    "consent": {
        "id": 1,
        "name": "H5P",
        "description": "Open-source content collaboration framework",
        "groups": [{
            "label": "Default group",
            "description": "default",
            "verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/experienced",
                "label": "Experienced",
                "description": "Experienced",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                    "label": "1.1.1 Funktionen",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "1.1.1 Funktionen"
                        }
                    },
                    "consented": true
                }],
                "consented": true
            }, {
                "id": "http://h5p.example.com/expapi/verbs/attempted",
                "label": "Attempted",
                "description": "Attempted",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                    "label": "2.3.1 Funktion Zirkulationsleitung",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "2.3.1 Funktion Zirkulationsleitung"
                        }
                    },
                    "consented": true
                }],
                "consented": true
            }],
            "isDefault": true
        }, {
            "label": "Group 2",
            "description": "Lorem ipsum",
            "verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/interacted",
                "label": "Interacted",
                "description": "Lorem ipsum",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                    "label": "1.2.3 Kappenventil",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "1.2.3 Kappenventil"
                        }
                    },
                    "consented": true
                }],
                "consented": true
            }, {
                "id": "http://h5p.example.com/expapi/verbs/answered",
                "label": "Answered",
                "description": "lorem ipsum",
                "defaultConsent": false,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                    "label": "7.2.1 Ventil Basics",
                    "defaultConsent": false,
                    "definition": {
                        "name": {
                            "enUS": "7.2.1 Ventil Basics"
                        }
                    },
                    "consented": true
                }],
                "consented": true
            }],
            "isDefault": false
        }],
        "essential_verbs": [{
            "id": "http://h5p.example.com/expapi/verbs/liked",
            "label": "Liked",
            "description": "Like interaction",
            "defaultConsent": true,
            "objects": []
        }]
    },
    "provider_schema": {
        "id": 1,
        "definition": {
            "id": "h5p-0",
            "name": "H5P",
            "description": "Open-source content collaboration framework",
            "groups": [{
                "label": "Default group",
                "description": "default",
                "verbs": [{
                    "id": "http://h5p.example.com/expapi/verbs/experienced",
                    "label": "Experienced",
                    "description": "Experienced",
                    "defaultConsent": true,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                        "label": "1.1.1 Funktionen",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "1.1.1 Funktionen"
                            }
                        }
                    }]
                }, {
                    "id": "http://h5p.example.com/expapi/verbs/attempted",
                    "label": "Attempted",
                    "description": "Attempted",
                    "defaultConsent": true,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "label": "2.3.1 Funktion Zirkulationsleitung",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "2.3.1 Funktion Zirkulationsleitung"
                            }
                        }
                    }]
                }],
                "isDefault": true
            }, {
                "label": "Group 2",
                "description": "Lorem ipsum",
                "verbs": [{
                    "id": "http://h5p.example.com/expapi/verbs/interacted",
                    "label": "Interacted",
                    "description": "Lorem ipsum",
                    "defaultConsent": true,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                        "label": "1.2.3 Kappenventil",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "1.2.3 Kappenventil"
                            }
                        }
                    }]
                }, {
                    "id": "http://h5p.example.com/expapi/verbs/answered",
                    "label": "Answered",
                    "description": "lorem ipsum",
                    "defaultConsent": false,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                        "label": "7.2.1 Ventil Basics",
                        "defaultConsent": false,
                        "definition": {
                            "name": {
                                "enUS": "7.2.1 Ventil Basics"
                            }
                        }
                    }]
                }],
                "isDefault": false
            }],
            "essential_verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/liked",
                "label": "Liked",
                "description": "Like interaction",
                "defaultConsent": true,
                "objects": []
            }]
        },
        "groups": [{
            "label": "Default group",
            "description": "default",
            "verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/experienced",
                "label": "Experienced",
                "description": "Experienced",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                    "label": "1.1.1 Funktionen",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "1.1.1 Funktionen"
                        }
                    }
                }]
            }, {
                "id": "http://h5p.example.com/expapi/verbs/attempted",
                "label": "Attempted",
                "description": "Attempted",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                    "label": "2.3.1 Funktion Zirkulationsleitung",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "2.3.1 Funktion Zirkulationsleitung"
                        }
                    }
                }]
            }],
            "isDefault": true
        }, {
            "label": "Group 2",
            "description": "Lorem ipsum",
            "verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/interacted",
                "label": "Interacted",
                "description": "Lorem ipsum",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                    "label": "1.2.3 Kappenventil",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "1.2.3 Kappenventil"
                        }
                    }
                }]
            }, {
                "id": "http://h5p.example.com/expapi/verbs/answered",
                "label": "Answered",
                "description": "lorem ipsum",
                "defaultConsent": false,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                    "label": "7.2.1 Ventil Basics",
                    "defaultConsent": false,
                    "definition": {
                        "name": {
                            "enUS": "7.2.1 Ventil Basics"
                        }
                    }
                }]
            }],
            "isDefault": false
        }],
        "essential_verbs": [{
            "id": "http://h5p.example.com/expapi/verbs/liked",
            "label": "Liked",
            "description": "Like interaction",
            "defaultConsent": true,
            "objects": []
        }],
        "updated": "2023-02-09T12:45:57.562000Z",
        "created": "2023-02-09T12:45:57.562000Z",
        "provider": 1,
        "superseded_by": null
    },
    "paused_data_recording": false
}
```

### Get provider details

Allows a third party to query the details of one's provider. Among other things, all created schemas are supplied here.

- HTTP Method: **GET**
- URL `/api/v1/consents/provider-status/third-party`

###### Examples

```console
$ curl -X GET 127.0.0.1:8003/api/v1/consents/provider-status/third-party -H "Content-Type: application/json" -H "Authorization: Basic [APPLICATION_TOKEN]"
```

Response

```js
{
    "id": 1,
    "name": "H5P",
    "versions": [{
        "id": 5,
        "definition": {
            "id": "h5p-0",
            "name": "H5P",
            "description": "Open-source content collaboration framework",
            "groups": [{
                "label": "Default group",
                "description": "default",
                "verbs": [{
                    "id": "http://h5p.example.com/expapi/verbs/experienced",
                    "label": "Experienced",
                    "description": "Experienced",
                    "defaultConsent": false,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                        "label": "1.1.1 Funktionen",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "1.1.1 Funktionen"
                            }
                        }
                    }]
                }, {
                    "id": "http://h5p.example.com/expapi/verbs/attempted",
                    "label": "Attempted",
                    "description": "Attempted",
                    "defaultConsent": true,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "label": "2.3.1 Funktion Zirkulationsleitung",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "2.3.1 Funktion Zirkulationsleitung"
                            }
                        }
                    }]
                }],
                "isDefault": true
            }, {
                "label": "Group 2",
                "description": "Lorem ipsum",
                "verbs": [{
                    "id": "http://h5p.example.com/expapi/verbs/interacted",
                    "label": "Interacted",
                    "description": "Lorem ipsum",
                    "defaultConsent": true,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                        "label": "1.2.3 Kappenventil",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "1.2.3 Kappenventil"
                            }
                        }
                    }]
                }, {
                    "id": "http://h5p.example.com/expapi/verbs/answered",
                    "label": "Answered",
                    "description": "lorem ipsum",
                    "defaultConsent": false,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                        "label": "7.2.1 Ventil Basics",
                        "defaultConsent": false,
                        "definition": {
                            "name": {
                                "enUS": "7.2.1 Ventil Basics"
                            }
                        }
                    }]
                }],
                "isDefault": false
            }],
            "essential_verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/liked",
                "label": "Liked",
                "description": "Like interaction",
                "defaultConsent": true,
                "objects": []
            }, {
                "id": "http://h5p.example.com/expapi/verbs/clicked",
                "label": "Clicked",
                "description": "Clicked interaction",
                "defaultConsent": true,
                "objects": []
            }]
        },
        "groups": [{
            "label": "Default group",
            "description": "default",
            "verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/experienced",
                "label": "Experienced",
                "description": "Experienced",
                "defaultConsent": false,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                    "label": "1.1.1 Funktionen",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "1.1.1 Funktionen"
                        }
                    }
                }]
            }, {
                "id": "http://h5p.example.com/expapi/verbs/attempted",
                "label": "Attempted",
                "description": "Attempted",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                    "label": "2.3.1 Funktion Zirkulationsleitung",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "2.3.1 Funktion Zirkulationsleitung"
                        }
                    }
                }]
            }],
            "isDefault": true
        }, {
            "label": "Group 2",
            "description": "Lorem ipsum",
            "verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/interacted",
                "label": "Interacted",
                "description": "Lorem ipsum",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                    "label": "1.2.3 Kappenventil",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "1.2.3 Kappenventil"
                        }
                    }
                }]
            }, {
                "id": "http://h5p.example.com/expapi/verbs/answered",
                "label": "Answered",
                "description": "lorem ipsum",
                "defaultConsent": false,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                    "label": "7.2.1 Ventil Basics",
                    "defaultConsent": false,
                    "definition": {
                        "name": {
                            "enUS": "7.2.1 Ventil Basics"
                        }
                    }
                }]
            }],
            "isDefault": false
        }],
        "essential_verbs": [{
            "id": "http://h5p.example.com/expapi/verbs/liked",
            "label": "Liked",
            "description": "Like interaction",
            "defaultConsent": true,
            "objects": []
        }, {
            "id": "http://h5p.example.com/expapi/verbs/clicked",
            "label": "Clicked",
            "description": "Clicked interaction",
            "defaultConsent": true,
            "objects": []
        }],
        "updated": "2023-02-09T10:19:51.566456Z",
        "created": "2023-02-09T10:19:51.566471Z",
        "provider": 1,
        "superseded_by": null
    }, {
        "id": 1,
        "definition": {
            "id": "h5p-0",
            "name": "H5P",
            "description": "Open-source content collaboration framework",
            "groups": [{
                "label": "Default group",
                "description": "default",
                "verbs": [{
                    "id": "http://h5p.example.com/expapi/verbs/experienced",
                    "label": "Experienced",
                    "description": "Experienced",
                    "defaultConsent": true,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                        "label": "1.1.1 Funktionen",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "1.1.1 Funktionen"
                            }
                        }
                    }]
                }, {
                    "id": "http://h5p.example.com/expapi/verbs/attempted",
                    "label": "Attempted",
                    "description": "Attempted",
                    "defaultConsent": true,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                        "label": "2.3.1 Funktion Zirkulationsleitung",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "2.3.1 Funktion Zirkulationsleitung"
                            }
                        }
                    }]
                }],
                "isDefault": true
            }, {
                "label": "Group 2",
                "description": "Lorem ipsum",
                "verbs": [{
                    "id": "http://h5p.example.com/expapi/verbs/interacted",
                    "label": "Interacted",
                    "description": "Lorem ipsum",
                    "defaultConsent": true,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                        "label": "1.2.3 Kappenventil",
                        "defaultConsent": true,
                        "definition": {
                            "name": {
                                "enUS": "1.2.3 Kappenventil"
                            }
                        }
                    }]
                }, {
                    "id": "http://h5p.example.com/expapi/verbs/answered",
                    "label": "Answered",
                    "description": "lorem ipsum",
                    "defaultConsent": false,
                    "objects": [{
                        "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                        "label": "7.2.1 Ventil Basics",
                        "defaultConsent": false,
                        "definition": {
                            "name": {
                                "enUS": "7.2.1 Ventil Basics"
                            }
                        }
                    }]
                }],
                "isDefault": false
            }],
            "essential_verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/liked",
                "label": "Liked",
                "description": "Like interaction",
                "defaultConsent": true,
                "objects": []
            }]
        },
        "groups": [{
            "label": "Default group",
            "description": "default",
            "verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/experienced",
                "label": "Experienced",
                "description": "Experienced",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF",
                    "label": "1.1.1 Funktionen",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "1.1.1 Funktionen"
                        }
                    }
                }]
            }, {
                "id": "http://h5p.example.com/expapi/verbs/attempted",
                "label": "Attempted",
                "description": "Attempted",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm",
                    "label": "2.3.1 Funktion Zirkulationsleitung",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "2.3.1 Funktion Zirkulationsleitung"
                        }
                    }
                }]
            }],
            "isDefault": true
        }, {
            "label": "Group 2",
            "description": "Lorem ipsum",
            "verbs": [{
                "id": "http://h5p.example.com/expapi/verbs/interacted",
                "label": "Interacted",
                "description": "Lorem ipsum",
                "defaultConsent": true,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46",
                    "label": "1.2.3 Kappenventil",
                    "defaultConsent": true,
                    "definition": {
                        "name": {
                            "enUS": "1.2.3 Kappenventil"
                        }
                    }
                }]
            }, {
                "id": "http://h5p.example.com/expapi/verbs/answered",
                "label": "Answered",
                "description": "lorem ipsum",
                "defaultConsent": false,
                "objects": [{
                    "id": "http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b",
                    "label": "7.2.1 Ventil Basics",
                    "defaultConsent": false,
                    "definition": {
                        "name": {
                            "enUS": "7.2.1 Ventil Basics"
                        }
                    }
                }]
            }],
            "isDefault": false
        }],
        "essential_verbs": [{
            "id": "http://h5p.example.com/expapi/verbs/liked",
            "label": "Liked",
            "description": "Like interaction",
            "defaultConsent": true,
            "objects": []
        }],
        "updated": "2023-02-09T10:19:51.568203Z",
        "created": "2023-02-09T09:45:15.405764Z",
        "provider": 1,
        "superseded_by": 5
    }]
}
```

### Save user consent

The provider schema id for each provider must always refer to the latest schema version of the respective provider, otherwise the request will be rejected

- HTTP Method: **POST**
- URL `/api/v1/consents/user/save/third-party`

###### Parameters

| Name                 | Type       | Required | Default |
| -------------------- | ---------- | -------- | ------- |
| `user_id`            | str        | Yes      | None    |
| `provider_schema_id` | int        | Yes      | None    |
| `verbs`              | list[dict] | Yes      | None    |

###### Returns

| Code  | Type | Description                                                                                           |
| ----- | ---- | ----------------------------------------------------------------------------------------------------- |
| `200` | dict | dict with entry for each user contain list of consented verb and object ids                           |
| `401` | dict | invalid token                                                                                         |
| `403` | dict | reaised if a verb for a provider other than the one associated with the application token is provided |
| `400` | dict | invalid request data e.g. `{"user_id":["Object with email=user1@polaris does not exist."]}`           |

###### Examples

```console
curl -X POST 127.0.0.1:8003/api/v1/consents/user/save/third-party --data '{ "user_id": "user1@polaris.com", "provider_schema_id": 3, "verbs":[{"provider":1,"id":"http://h5p.example.com/expapi/verbs/experienced","consented":false,"objects":"[{\"id\":\"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF\",\"label\":\"1.1.1 Funktionen\",\"defaultConsent\":true,\"definition\":{\"name\":{\"enUS\":\"1.1.1 Funktionen\"}},\"consented\":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/attempted","consented":true,"objects":"[{\"id\":\"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm\",\"label\":\"2.3.1 Funktion Zirkulationsleitung\",\"defaultConsent\":true,\"definition\":{\"name\":{\"enUS\":\"2.3.1 Funktion Zirkulationsleitung\"}},\"consented\":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/interacted","consented":true,"objects":"[{\"id\":\"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46\",\"label\":\"1.2.3 Kappenventil\",\"defaultConsent\":true,\"definition\":{\"name\":{\"enUS\":\"1.2.3 Kappenventil\"}},\"consented\":true}]"},{"provider":1,"id":"http://h5p.example.com/expapi/verbs/answered","consented":false,"objects":"[{\"id\":\"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b\",\"label\":\"7.2.1 Ventil Basics\",\"defaultConsent\":false,\"definition\":{\"name\":{\"enUS\":\"7.2.1 Ventil Basics\"}},\"consented\":false}]"}]}' -H "Content-Type: application/json" -H "Authorization: Basic [APPLICATION_TOKEN]"
```


Formatted request payload
```js
{
    "user_id": "user1@polaris.com",
    "provider_schema_id": 3,
    "verbs": [
        {
            "provider": 1,
            "id": "http://h5p.example.com/expapi/verbs/experienced",
            "consented": false,
            "objects": "[{\"id\":\"http://h5p.example.com/expapi/activity/QKGPPiIhI4zx9YAZZksLKigqyf7yW4WF\",\"label\":\"1.1.1 Funktionen\",\"defaultConsent\":true,\"definition\":{\"name\":{\"enUS\":\"1.1.1 Funktionen\"}},\"consented\":true}]"
        },
        {
            "provider": 1,
            "id": "http://h5p.example.com/expapi/verbs/attempted",
            "consented": true,
            "objects": "[{\"id\":\"http://h5p.example.com/expapi/activity/VeH7S8NeGlCRM1myYRDBjHMCknLqDLgm\",\"label\":\"2.3.1 Funktion Zirkulationsleitung\",\"defaultConsent\":true,\"definition\":{\"name\":{\"enUS\":\"2.3.1 Funktion Zirkulationsleitung\"}},\"consented\":true}]"
        },
        {
            "provider": 1,
            "id": "http://h5p.example.com/expapi/verbs/interacted",
            "consented": true,
            "objects": "[{\"id\":\"http://h5p.example.com/expapi/activity/ofN2ODcnLRaVu30lUpzrWPqF2AcG7g46\",\"label\":\"1.2.3 Kappenventil\",\"defaultConsent\":true,\"definition\":{\"name\":{\"enUS\":\"1.2.3 Kappenventil\"}},\"consented\":true}]"
        },
        {
            "provider": 1,
            "id": "http://h5p.example.com/expapi/verbs/answered",
            "consented": false,
            "objects": "[{\"id\":\"http://h5p.example.com/expapi/activity/K34IszYvGE4R0cC72Ean6msLfLCJtQ8b\",\"label\":\"7.2.1 Ventil Basics\",\"defaultConsent\":false,\"definition\":{\"name\":{\"enUS\":\"7.2.1 Ventil Basics\"}},\"consented\":false}]"
        }
    ]
}
```

Success response

- HTTP status: `200`

```js
{"message": "user consent saved"}
```

Example failure reponses

- HTTP status: `400`

```js
{"message": "provider schema id isn't latest provider schema id"
```

```js
{"message": "user consent contains missing provider schema verb consents"}
```

```js
{"provider_schema_id":["Invalid pk \"7\" - object does not exist."]}
```

```js
{"user_id":["Object with email=user10@polaris.com does not exist."]}
```