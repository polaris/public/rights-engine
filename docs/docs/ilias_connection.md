# ILIAS Integration
ILIAS, which stands for "Integrated Learning, Information and Work Cooperation System" in German (Integriertes Lern-, Informations- und Arbeitskooperations-System), is an open-source web-based learning management system (LMS). It facilitates learning content management, adhering to SCORM 2004 standards, and offers an array of collaboration, communication, evaluation, and assessment tools. The software is released under the GNU General Public License, allowing it to operate on any server supporting PHP and MySQL.

ILIAS possesses the capability to generate xAPI statements for various interactions. In the subsequent section, we will provide a concise explanation of how to establish a connection between ILIAS and Polaris.

## Plugins

First, we need to install the Plugin Events2Lrs (`https://github.com/internetlehrer/Events2Lrs`). 
From the Readme of the project: 

> The Events2Lrs plugin sends data to a Learning Record Store (LRS). Data sources can include information from (optionally extended) ILIAS events and data from the usage of H5P objects integrated (even through the page editor). The plugin responds to events such as status updates of ILIAS objects indicating learning progress, such as courses and the learning modules contained within.

The plugin ensures that ILIAS can generate xAPI statements from the course, test, and learning module.

## LRS Configuration
You can establish a connection between ILIAS and Polaris within the administration settings of 'Extending ILIAS' > 'LRS'. Now configure the new LRS with the following properties:

![](img/ilias_configuration.png "Configuration of Polaris")

- Endpoint: `https://[RIGHTS_ENGINE_URL]/xapi/statements` (e.g. `https://polaris.ruhr-uni-bochum.de/xapi/statements`)
- Key: token
- Secret / Password: Login as provider to Rights Engine -> View and copy application token
- User identification: E-Mail Adress (or use the Shibboleth-Connector)

![](img/ilias_configuration_detail.png "Details of the configuration of Polaris in ILIAS")

## Disclaimer

The integration between Polaris and ILIAS was tested only to a limited extent; therefore, it cannot be ruled out that problems may arise in certain courses or situations.