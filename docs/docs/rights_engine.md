# Rights Engine

## Architecture

![](img/polaris_overview.drawio.png "Architecture")

## Configuration

Tips and tricks about the configuration can be found in the deployment chapter. If you need information how to create a schema for adding new plattform, look at the provider schema chapter.  

## Visualization Token

Dashboards obtain data for different charts using a JWT visualization token. The token should be created in backend of the dashboard environment e.g. moodle backend.

A visualization token provides access to a list of analysis engine results. The following command creates a visualization token for the analytics engines `number_h5p_statements` and `number_moodle_statements`.
The names of the engines can be found at the rights engines adminstration view. Login in as a provider, navigate to the analytics-token section in the navbar. The name should match with the parameters. 
Note: The engine names used in the analytics engines configuration can be differ and should not be used here. 

```console
$ curl -X POST http://[RIGHTS_ENGINE_URL]/api/v1/provider/visualization-tokens/create --data '{"engines": ["count_h5p_statements", "count_moodle_statements"]}' -H "Content-Type: application/json" -H "Authorization: Basic [APPLICATION_TOKEN]"
```

Response

```js
{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJjb250ZXh0X2lkIjpudWxsLCJleHAiOjE2NzcxNDUzODIsImVuZ2luZXMiOlsxLDJdLCJlbmdpbmVzX3dpdGhfY29udGV4dCI6W119.pZB5tUAJf12OJpXikJuTZkex8b5XlyXTjDIhbJUAHhg-y-ozH1aJ_rLbleycCOCuMQ5Ui2Mg-MWg0P6bWYHnzeqT49YCMd2LdH6zxrjl91_G9gFLvYxX5aIEWYqOeh9wIjBh9a4jOKgK3-Y0mm5cn3YXmJJ_vB3YHnl9Eu-eN4Yf444UIs1eMU1gRzhxi29PEAbmoujQ6lGyDAn7rp7vIz9LmxH29OHPv6H7L9AT-0Cbg97GX58PU9rOqEj9ngEEO7GkzmcaqnEMX1xm7rRalJfbdXBiexqhy8kSVt5ndHHNj3ZOl647hTL80BcLUiL-SypwcLT3RF1OucnJa5BD_YCSdLts45hUgssYGgW1L_GDQUiLde3uLGcjRfO0lES36p94pSuw5awz8yS5ru7-qKs5c89MX1WNLIDJBUB5zAv1yRISaUcSwC1ImPQvGBJlLuE-ZfNp1_s3HUxpXM00jRe7_UuILGtO52BAo7aeWFFdkJQsJCzRlDmU2pRF50J4t_RMoYvN-_Q-OsL3TVyVZk3LNzxh4rRHJ6aHJ3v0H9RLO-s87YRg6OmMxZc_3bHsI52EdvJhsJMxetnKOV1MFNax7YFj1kSzVnPJivLdVmtrZTOPLGbG3Tzn-DOu3I4cA5dKBoGjH0DlN1Fu7cr9_iPYtPWzc8LfmYwwOcoe6pk"}
```

The example visualization token contain the following payload:

```js
{
  "context_id": null,
  "exp": 1677145382,
  "engines": [
    1,
    2
  ],
  "engines_with_context": []
}
```

The `engines` field lists the analytics engines ids this token allowed to read.

### Scoped visualization tokens

Since some analytics engine results are scoped to a single user, or another scoped context, it is possible to create visualization tokens that grant access to these scoped results.

The following example creates such a scoped visualization token for `user1@polaris.com` and the analytics engine `h5p_count_user_statements`, which calcutes the number of user specific statements for the provider H5P.

```console
$ curl -X POST http://[RIGHTS_ENGINE_URL]/api/v1/provider/visualization-tokens/create --data '{"engines": ["count_h5p_statements", "count_moodle_statements"], "context_id": "user1@polaris.com", "engines_with_context": ["h5p_count_user_statements"]}' -H "Content-Type: application/json" -H "Authorization: Basic [APPLICATION_TOKEN]
```

We use scoped visualization tokens, since analytics results, that are targeted to an individual user or very small group needs to be protected from other users access.

### Token Expiration

Visualization token expire after a certain time. It is possible to adjust the expiration duration in the Rights Engine via `JWT_EXPIRATION_DELTA_DAYS`. The Dashboard should take care of requesting a new token, before a token expires.

### xAPI Proxy Endpoint

The Rights Engine effectively acts as a proxy for xAPI statements and exposes and xAPI endpoint at `[BASE_URL]/xapi/statements`.

xAPI Statements can be send separately as single objects or as a list of objects using a `POST` request and a valid application token in the header (`Authorization: Basic [APPLICATION_TOKEN]`).
If all specified xAPI statements are valid, a list of UUIDs matching the order of the request statements is returned. If the request contains invalid xAPI statements, all statements are rejected. The `data` field of the response lists a detailed object for each xAPI statement, explaining the cause of the rejection.

#### Example

```console
$ curl -i -X POST http://localhost:9001/xapi/statements --data '{ "actor": {"account": { "name": "user1@polaris.com" }}, "timestamp": "2022-03-18T18:07:32+01:00", "context": { "platform": "Moodle", "contextActivities": { "category": { "objectType": "Activity", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/lms", "name": { "de": "moosltest" } }, "id": "https://moosltest.it-services.ruhr-uni-bochum.de" }, "grouping": [ { "objectType": "Activity", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course", "name": { "de": "Structured Query Language" } }, "id": "https://moosltest.it-services.ruhr-uni-bochum.de/course/view.php?id=2" }, { "objectType": "Activity", "id": "https://moosltest.it-services.ruhr-uni-bochum.de/mod/quiz/view.php?id=2", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz", "name": { "de": "SQL-Test" } } } ] }, "extensions": { "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course/extensions/roles": "student", "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz_attempt/extensions/timestart": 1647610537, "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz_attempt/extensions/timefinish": 1647623252 } }, "verb": { "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/accessed" }, "object": { "objectType": "Activity", "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz_attempt", "name": { "de": "SQL-Test" } } }, "result": { "score": { "raw": 0 } } }' -H "Content-Type: application/json" -H "Authorization: Basic ff9b69722ad9fcf3c77cf7a54b31da6c651f546101ded46f03e53a8f008a66de"
```

Response (HTTP 400)

```js
{"message": "xAPI statements couldn't be stored in LRS", "data": [{"valid": true, "accepted": false, "reason": "User has not given consent to this object"}]
```

```console
$ curl -i -X POST http://localhost:9001/xapi/statements --data '{ "actor": {"account": { "name": "user1@polaris.com" }}, "timestamp": "2022-03-18T18:07:32+01:00", "context": { "platform": "Moodle", "contextActivities": { "category": { "objectType": "Activity", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/lms", "name": { "de": "moosltest" } }, "id": "https://moosltest.it-services.ruhr-uni-bochum.de" }, "grouping": [ { "objectType": "Activity", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course", "name": { "de": "Structured Query Language" } }, "id": "https://moosltest.it-services.ruhr-uni-bochum.de/course/view.php?id=2" }, { "objectType": "Activity", "id": "https://moosltest.it-services.ruhr-uni-bochum.de/mod/quiz/view.php?id=2", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz", "name": { "de": "SQL-Test" } } } ] }, "extensions": { "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course/extensions/roles": "student", "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz_attempt/extensions/timestart": 1647610537, "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz_attempt/extensions/timefinish": 1647623252 } }, "verb": { "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/accessed" }, "object": { "objectType": "Activity", "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/book", "name": { "de": "SQL-Test" } } }, "result": { "score": { "raw": 0 } } }' -H "Content-Type: application/json" -H "Authorization: Basic ff9b69722ad9fcf3c77cf7a54b31da6c651f546101ded46f03e53a8f008a66de"
```

Response (HTTP 200)

```js
{"message": "xAPI statements successfully stored in LRS", "data": ["6411d628e31a30251d6391f0"]}
```

### Anonymized xAPI proxy endpoint

In addition to the aforementioned endpoint for regular xAPI statements, the rights engine offers an endpoint to submit anonymized statements which are not directly associated with a user.
This works by supplying a "TAN", an arbitrary string sequence, instead of the usual user account information.
Providers have to keep a list / matching of TANs and users, since the rights engine has no way of knowing which user a given TAN belongs to.

The endpoint is available at `[BASE_URL]/xapi/tanStatements` and accepts valid xAPI statements that contain a TAN in the user object. 

#### Example

```console
$ curl -i -X POST http://localhost:9001/xapi/tanStatements --data '{ "actor": {"objectType": "Agent", "tan": "u4gueb983fnklerg"}, "timestamp": "2022-03-18T18:07:32+01:00", "context": { "platform": "Moodle", "contextActivities": { "category": { "objectType": "Activity", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/lms", "name": { "de": "moosltest" } }, "id": "https://moosltest.it-services.ruhr-uni-bochum.de" }, "grouping": [ { "objectType": "Activity", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course", "name": { "de": "Structured Query Language" } }, "id": "https://moosltest.it-services.ruhr-uni-bochum.de/course/view.php?id=2" }, { "objectType": "Activity", "id": "https://moosltest.it-services.ruhr-uni-bochum.de/mod/quiz/view.php?id=2", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz", "name": { "de": "SQL-Test" } } } ] }, "extensions": { "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course/extensions/roles": "student", "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz_attempt/extensions/timestart": 1647610537, "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz_attempt/extensions/timefinish": 1647623252 } }, "verb": { "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/accessed" }, "object": { "objectType": "Activity", "id": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/course", "definition": { "type": "https://xapi.elearn.rwth-aachen.de/definitions/lms/activities/quiz_attempt", "name": { "de": "SQL-Test" } } }, "result": { "score": { "raw": 0 } } }' -H "Content-Type: application/json" -H "Authorization: Basic ff9b69722ad9fcf3c77cf7a54b31da6c651f546101ded46f03e53a8f008a66de"
```

Error response when the endpoint is used without a TAN (HTTP 400)

```js
{"message": "xAPI statements couldn't be stored in LRS", "data": [{"valid": false, "reason": "TAN missing in statement"}]
```

The other responses are analogous to the non-anonymized endpoint.

Users can associate anonymized data to their account by using the merge tool included in the frontend.
For data to be associated, a user has to give a correct TAN and provider.
In order to prevent abuse by brute-forcing TANs, this tool is rate-limited per user.
